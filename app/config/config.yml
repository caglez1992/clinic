imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

    - { resource: fos_user.yml }
    - { resource: vich_uploader.yml }
    - { resource: ewz_recaptcha.yml }
    - { resource: gos_web.yml }
    - { resource: assetic.yml }

    - { resource: sonata/sonata_config.yml }
    - { resource: sonata/sonata_doctrine_orm_admin.yml }
    - { resource: sonata/sonata_block.yml }
#    - { resource: sonata/sonata_dashboard.yml }
    - { resource: sonata/sonata_services.yml }
    - { resource: sonata/sonata_menu.yml }
    - { resource: sonata/sonata_formatter.yml }

    - { resource: "@PatientBundle/Resources/config/services.yml" }
    - { resource: "@WorkerBundle/Resources/config/services.yml" }
    - { resource: "@UserBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en
    locales: en|es

framework:
    #esi: ~
    translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    templating:
        engines: ['twig']
    csrf_protection: ~
    validation: { enable_annotations: true }
    #serializer: { enable_annotations: true }
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: session.handler.native_file
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    #fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true
    #fragments: { path: /_fragment }
    ide: phpstorm

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    globals:
        languages: '%locales%'
        container: "@service_container"
        sonata_pool: "@sonata.admin.pool"
        d: "@doctrine"
        clinic_util: "@clinic.util"
    form_themes:
        - CoreBundle:Form:fields.html.twig
        - CoreBundle:Form:my_date_picker.html.twig
        - '@SonataFormatter/Form/formatter.html.twig'
        - SonataCoreBundle:Form:datepicker.html.twig

# Doctrine Configuration
doctrine:
    dbal:
        driver: '%database_driver%'
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            gedmo_translatable:
                type: annotation
                prefix: Gedmo\Translatable\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"
                alias: GedmoTranslatable # (optional) it will default to the name set for the mapping
                is_bundle: false
            gedmo_translator:
                type: annotation
                prefix: Gedmo\Translator\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translator/Entity"
                alias: GedmoTranslator # (optional) it will default to the name set for the mapping
                is_bundle: false
            gedmo_loggable:
                type: annotation
                prefix: Gedmo\Loggable\Entity
                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity"
                alias: GedmoLoggable # (optional) it will default to the name set for the mapping
                is_bundle: false

stof_doctrine_extensions:
    default_locale: en_US
    translation_fallback: true
    orm:
        default:
            translatable: true
            sluggable: true
            timestampable: true
            loggable: true
            softdeleteable: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    port:      "%mailer_port%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    encryption: "%mailer_encryption%"
    auth_mode:  login
    spool: { type: memory }

#config of annotations services bundle JMS
jms_di_extra:
    locations:
        all_bundles: true
    cache_warmer:
        controller_file_blacklist:
            - '%kernel.project_dir%/vendor/sonata-project/formatter-bundle/src/Controller/CkeditorAdminController.php'

knp_snappy:
    pdf:
        enabled:    true
        # binary:     /usr/local/bin/wkhtmltopdf
        binary:     '%kernel.project_dir%/vendor/bin/wkhtmltopdf-amd64'
        options:
            margin_bottom: '0.7in'
            margin_top: '0.7in'
            margin_left: '0.5in'
            margin_right: '0.5in'