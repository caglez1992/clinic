#!/bin/bash
#by LocurA(Cesar)

DIR="/var/www/html/clinic/"
BDNAME="clinic"
MYUSER="www-data"

if [ "$(whoami)" != "root" ]; then
	echo "[1;31mRoot permissions required[0;39m";
	exit 1; 
fi
clear
sleep 1

if [ "$1" == "-h" ]; then
	echo "[1;34m-c cache clear[0;39m";
	echo "[1;34m-a assets generation[0;39m";
	echo "[1;34m-cc delete all caches[0;39m";
	echo "[1;34m-u schema udpdate --force[0;39m";
	echo "[1;34m-ne create new entity[0;39m";
	echo "[1;34m-ue <BUNDLE> update entities[0;39m";
	echo "[1;34m-admin generate sonata admin[0;39m";
	echo "[1;34m-dr route debbugin[0;39m";
	echo "[1;34m-chmod :)[0;39m";
	exit 1;
fi

echo "[1;32mStarting...[0;39m";
echo "[1;31m [0;39m";

if [ "$1" == "-ad" ]; then
	sudo -H -u www-data php ${DIR}bin/console assetic:dump
	chmod 777 -R ${DIR}
	echo "[1;32mHecho...[0;39m";
	exit 1;
fi

if [ "$1" == "-adprod" ]; then
	sudo -H -u www-data php ${DIR}bin/console assetic:dump --env=prod
	chmod 777 -R ${DIR}
	echo "[1;32mHecho...[0;39m";
	exit 1;
fi

if [ "$1" == "-chmod" ]; then
	chown ${MYUSER}:${MYUSER} -R ${DIR}/src	
	chmod 777 -R ${DIR}	
	echo "[1;32mDone...[0;39m";
	exit 1;
fi 

if [ "$1" == "-dr" ]; then
	echo "[1;32mBorrando la famosa cache completamente...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console debug:router
	chmod 777 -R ${DIR}
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-admin" ]; then
	echo "[1;32mBorrando la famosa cache completamente...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console arch:sonata:admin:generate
	chown ${MYUSER}:${MYUSER} -R ${DIR}/src
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-ue" ]; then
	echo "[1;32mBorrando la famosa cache completamente...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console doctrine:generate:entities "$2"
	chown ${MYUSER}:${MYUSER} -R ${DIR}/src
	echo "[1;32mDone...[0;39m";
	echo "[1;32mActualizando Esquema de la BD...[0;39m";
	chmod 777 -R ${DIR}/
	php ${DIR}bin/console doctrine:schema:update --force
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-ne" ]; then
	echo "[1;32mBorrando la famosa cache completamente...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console doctrine:generate:entity
	chown ${MYUSER}:${MYUSER} -R ${DIR}/src
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-cc" ]; then
	echo "[1;32mBorrando la famosa cache completamente...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-u" ]; then
	echo "[1;32mActualizando Esquema de la BD...[0;39m";
	chmod 777 -R ${DIR}/
	php ${DIR}bin/console doctrine:schema:update --force
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-c" ]; then
	echo "[1;32mBorrando la famosa cache...[0;39m";
	chmod 777 -R ${DIR}
	sudo -H -u www-data php ${DIR}bin/console cache:clear
	chmod 777 -R ${DIR}
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-a" ]; then
	echo "[1;32mBorrando los assets anteriores para evitar conflicto...[0;39m";
	chmod 777 -R ${DIR}web/bundles/
	rm -r ${DIR}web/bundles/*	
	echo "[1;32mHecho...[0;39m";
	echo "[1;32mPublicando los assets con symlink...[0;39m";
	chmod 777 -R ${DIR}
	php ${DIR}bin/console assets:install ${DIR}web --symlink
	chmod 777 -R ${DIR}web/bundles/
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-f" ]; then
	echo "[1;32mCache clear...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console doctrine:fixtures:load -n
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ "$1" == "-test" ]; then
	echo "[1;32mCache clear...[0;39m";
	chmod 777 -R ${DIR}
	rm -r ${DIR}var/cache/
	mkdir ${DIR}var/cache/
	chmod 777 -R ${DIR}/var/cache
	chmod 777 -R ${DIR}
	php ${DIR}/bin/console core:test --env=dev
	echo "[1;32mDone...[0;39m";
	exit 1;
fi

if [ ! -f /root/.pgpass ]; then
	echo "[1;32mCreando fichero de postgres...[0;39m";
	echo 'postgres' >/root/.pgpass;
	chmod 600 /root/.pgpass;
	echo "[1;32mDone...[0;39m";
fi

#echo "[1;32mReiniciando Servicio Postgres...[0;39m";
#service postgresql restart
#echo "[1;32mHecho...[0;39m";

echo "[1;32mInstall app at (${DIR})...[0;39m";
chmod 777 -R ${DIR}/
php ${DIR}bin/console core:install
chmod 777 -R ${DIR}

echo "[1;32mDeleting cache...[0;39m";
chmod 777 -R ${DIR}
rm -r ${DIR}var/cache/
mkdir ${DIR}var/cache/
chmod 777 -R ${DIR}/var/cache
echo "[1;32mDone...[0;39m";
