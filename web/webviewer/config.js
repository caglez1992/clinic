$.extend(ReaderControl.config.ui, {
    hideAnnotationPanel: true,
    hideSidePanel: true
});

Annotations.WidgetAnnotation.getCustomStyles = function(widget) {
    if (widget instanceof Annotations.ChoiceWidgetAnnotation) {
        return {
            'background-color': 'lightblue',
            // 'min-height': '40px',
            'min-width': '35px',
            'font-size': '10px',
            color: 'brown'
        };
    }

    /*if (widget instanceof Annotations.TextWidgetAnnotation) {
        return {
            'background-color': 'lightblue',
            // 'min-height': '40px',
            'min-width': '35px',
            'font-size': '10px',
            'color': 'brown',
            'text-color': 'brown',
            opacity: 0.8
        };
    }*/
};