<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 1/16/19
 * Time: 10:56 AM
 */

namespace Core\UserBundle\SecurityListener;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener;

class FormAuthenticationListener extends UsernamePasswordFormAuthenticationListener
{
    public function attemptAuthentication(Request $request)
    {
        if (!$this->verifyRecaptcha($request->request->get('g-recaptcha-response'))) {
            throw new AuthenticationException('Invalid captcha.');
        }

        return parent::attemptAuthentication($request);
    }

    private function verifyRecaptcha($recaptchaResponse)
    {
        $client = new Client();

        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' => [
                    'secret'   => '6LdAUYoUAAAAAMVQ7FDcldUuhNiTlVCcHYXRXa9t',
                    'response' => $recaptchaResponse,
                ]
            ]
        );

        $result = json_decode($response->getBody(), true);

        return !empty($result['success']);
    }
}