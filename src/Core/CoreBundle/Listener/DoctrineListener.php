<?php
/**
 * Created by PhpStorm.
 * User: Locura
 * Date: 17/01/17
 * Time: 15:41
 */

namespace Core\CoreBundle\Listener;

use Core\CoreBundle\Entity\Log;
use Core\CoreBundle\Entity\Loggable;
use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use DateTime;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class DoctrineListener
{
    private $container;
    private $translator;

    /**
     * List of log entries which do not have the foreign
     * key generated yet - MySQL case. These entries
     * will be updated with new keys on postPersist event
     *
     * @var array
     */
    protected $pendingLogEntryInserts = array();

    public function __construct(ContainerInterface $container = null, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->translator = $translator;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $object = $args->getObject();
        $oid = spl_object_hash($object);

        if ($this->pendingLogEntryInserts && array_key_exists($oid, $this->pendingLogEntryInserts)) {
            $logEntry = $this->pendingLogEntryInserts[$oid];
            $logEntry->setIdentifier($object->getId());
            $uow->scheduleExtraUpdate($logEntry, array(
                'identifier' => array(null, $object->getId()),
            ));
            unset($this->pendingLogEntryInserts[$oid]);
        }
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $ignoreFields = ['updatedAt', 'createdAt', 'usernameCanonical', 'emailCanonical', 'lastLogin', 'password', 'passwordRequestedAt', 'confirmationToken', ];
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        $userReal = $this->container->get(UserManager::class)->getRealUser();
        $createAction = NomUtil::getNomenclatorsByName(NomUtil::NOM_ACTION_TYPE_CREATE);
        $updateAction = NomUtil::getNomenclatorsByName(NomUtil::NOM_ACTION_TYPE_UPDATE);
        $deleteAction = NomUtil::getNomenclatorsByName(NomUtil::NOM_ACTION_TYPE_DELETE);
        
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $this->persistNewLog($em, $entity, null, null, null, $userReal, $createAction);
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $changeSet = $uow->getEntityChangeSet($entity);
            foreach ($changeSet as $field => $value){
                if(!in_array($field, $ignoreFields)) {
                    $this->persistNewLog($em, $entity, $field,
                        $this->proccessValue($value[0]),
                        $this->proccessValue($value[1]),
                        $userReal,
                        $updateAction
                    );
                }
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $this->persistNewLog($em, $entity, null, null, null, $userReal, $deleteAction);
        }

        /*foreach ($uow->getScheduledCollectionDeletions() as $col) {

        }*/

        foreach ($uow->getScheduledCollectionUpdates() as $col) {
            $entity = $col->getOwner();
            $elements = $col->getValues();
            $mapping = $col->getMapping();
            $this->persistNewLog($em, $entity, $mapping['fieldName'], null, $this->proccessValue($elements), $userReal, $updateAction);
        }
    }

    /**
     * @param EntityManagerInterface $em
     * @param $entity
     * @param $field
     * @param $prev
     * @param $new
     * @param $user
     * @param $action
     */
    private function persistNewLog($em, $entity, $field, $prev, $new, $user, $action){
        if($entity instanceof Loggable) {
            $uow = $em->getUnitOfWork();

            $log = new Log();
            $log->setIdentifier($entity->getId());
            $log->setClassName(get_class($entity));
            $log->setClassLabel($this->translator->trans($entity->getLabel(), array(), 'BackEndBundle'));
            $log->setField($field);
            $log->setPreviousValue($prev);
            $log->setActualValue($new);
            $log->setUser($user);
            $log->setAction($action);
            $em->persist($log);
            $uow->computeChangeSet($em->getClassMetadata(get_class($log)), $log);

            if(is_null($entity->getId())){
                $this->pendingLogEntryInserts[spl_object_hash($entity)] = $log;
            }
        }
    }

    private function proccessValue($data)
    {
        if (is_array($data)) {
            return $this->convertArray($data);
        }

        if(is_bool($data) && $data){
            return 'yes';
        }

        if(is_bool($data) && !$data){
            return 'no';
        }

        if($data instanceof DateTime){
            return $data->format('m/d/Y H:i:s');
        }

        if($data instanceof Nomenclator){
            return $this->translator->trans($data->getName(), array(), 'BackEndBundle');
        }

        return $data;
    }

    /**
     * @param $array
     * @return string
     */
    private function convertArray($array){
        $str = '';
        foreach ($array as $key => $value){
            if($value instanceof Nomenclator){
                $value = $this->translator->trans($value, array(), 'BackEndBundle');
            }
            $str .= $value;
            $str .= ', ';
        }
        return substr($str, 0, strlen($str) - 2);
    }
}