<?php

namespace Core\CoreBundle\Listener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuListener
{

    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function hideMenuItems(ConfigureMenuEvent $event)
    {
        /*$activeModules = $this->container->get('Core\CoreBundle\Manager\ModuleManager')->getArrayActiveModules();
        $menu = $event->getMenu();

        $activeModules = ['CoreBundle'];

        foreach ($menu->getChildren() as $key => $child){
            if(!in_array($key, $activeModules))
                $child->setDisplay(false);
        }*/

        /*$child = $menu->addChild('reports', [
            'label' => 'Daily and monthly reports',
            'route' => 'app_reports_index',
        ])->setExtras([
            'icon' => '<i class="fa fa-bar-chart"></i>',
        ]);*/
    }

}
