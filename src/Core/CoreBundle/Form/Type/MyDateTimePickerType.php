<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 5/11/18
 * Time: 11:00 AM
 */
namespace Core\CoreBundle\Form\Type;

use DateTime;
use Sonata\CoreBundle\Form\Type\BasePickerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MyDateTimePickerType extends BasePickerType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $minDate = new DateTime();
        $resolver->setDefaults(array_merge($this->getCommonDefaults(), [
            'format' => 'MMM d, y, h:mm a',
            'date_format' => null,
            'dp_min_date' => $minDate->format('M d, Y'),
        ]));
    }

    public function getParent()
    {
        return DateTimeType::class;
    }

    public function getBlockPrefix()
    {
        return 'my_date_time_picker';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
