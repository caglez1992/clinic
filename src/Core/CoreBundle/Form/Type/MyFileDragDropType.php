<?php
/**
 * Created By Cesar for symfony 3.4
 */

namespace Core\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vich\UploaderBundle\Form\DataTransformer\FileTransformer;
/**
 * @DI\Service("form.type.myfile.drag")
 * @DI\Tag(name = "form.type", attributes = { "alias" : "my_file_drag" })
 *
 */
class MyFileDragDropType extends AbstractType
{

    protected $container;

     /**
     * @param ContainerInterface $container
     * @DI\InjectParams({
     *      "container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //'allow_delete'  => true,
//            'file_type_label' => 'xalix.file_type.no_file',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, array(
            'required' => $options['required'],
            'label'    => $options['label'],
            'attr'     => $options['attr'],
        ));

        $builder->addModelTransformer(new FileTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $object = $form->getParent()->getData();
        $propertyName = $form->getName();

//        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
//        $path = $helper->asset($object, $propertyName);

        $isCreate = $object->getId() == null;

        $view->vars['isCreate'] = $isCreate;
//        $view->vars['path'] = $path;
        $view->vars['object'] = $object;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'my_file_drag';
    }

}
