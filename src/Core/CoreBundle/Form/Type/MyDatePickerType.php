<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 5/11/18
 * Time: 11:00 AM
 */
namespace Core\CoreBundle\Form\Type;

use DateTime;
use Sonata\CoreBundle\Form\Type\BasePickerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MyDatePickerType extends BasePickerType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $minDate = new DateTime('now');
        $resolver->setDefaults(array_merge($this->getCommonDefaults(), [
            'dp_pick_time' => false,
            'format' => DateType::DEFAULT_FORMAT,
            'dp_min_date' => $minDate->format('M d, Y'),
        ]));
    }

    public function getParent()
    {
        return DateType::class;
    }

    public function getBlockPrefix()
    {
        return 'my_date_picker';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
