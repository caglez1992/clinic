<?php
/**
 * Created By Cesar for symfony 3.4
 */

namespace Core\CoreBundle\Form\Type;

use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vich\UploaderBundle\Form\DataTransformer\FileTransformer;
/**
 * @DI\Service("form.type.myfile")
 * @DI\Tag(name = "form.type", attributes = { "alias" : "my_file" })
 *
 */
class MyFileType extends AbstractType
{

    protected $container;

     /**
     * @param ContainerInterface $container Objeto continer.
     * @DI\InjectParams({
     *      "container"        = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //'allow_delete'  => true,
            'file_type_label' => 'xalix.file_type.no_file',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, array(
            'required' => $options['required'],
            'label'    => $options['label'],
            'attr'     => $options['attr'],
        ));

        $builder->addModelTransformer(new FileTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $manipulate = $this->container->get('image.utils');

        try {
            $file_base = 'bundles/core/plugins-js/bootstrap-fileinput/image.png';
            if (file_exists($file_base)) {
                $locales    = $this->container->getParameter('locales');
                $locales    = explode('|', $locales);
                $translator = $this->container->get('translator');
                foreach ($locales as $local) {
                    $text       = $options['file_type_label'];
                    $trans_text = $translator->trans($text, array(), 'app', $local);
                    $img        = $manipulate->addTextInPNGCenter($file_base, $trans_text);
                    $path = sprintf('bundles/core/plugins-js/bootstrap-fileinput/%s-%s.png',$text,$local);
                    $manipulate->saveImagePNG($img, $path);
                    imagedestroy($img);
                }
            }
        } catch (Exception $e) {}

        $entity       = $form->getParent()->getData();
        $propertyName = $form->getName();

        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path   = $helper->asset($entity, $propertyName);

        $path = (!is_null($path)) ? $path : 'null';

        $view->vars['file_path']    = $path;
        $view->vars['file_type_label'] = $options['file_type_label'];
        $view->vars['object'] = $form->getParent()->getData();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'my_file';
    }

}
