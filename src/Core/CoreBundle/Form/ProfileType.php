<?php

namespace Core\CoreBundle\Form;

use Core\CoreBundle\Form\Type\MyFileType;
use Core\CoreBundle\Form\Type\MyInputType;
use FOS\UserBundle\Form\Type\ProfileFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Util\NomUtil;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', MyInputType::class, array(
            'label' => 'general.name',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-ioxhost',
            'required' => true,
            'constraints' => array(
                new NotBlank()
            )
        ));

        $builder->add('lastname', MyInputType::class, array(
            'label' => 'form.user.lastname',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-ioxhost',
            'required' => true,
            'constraints' => array(
                new NotBlank()
            )
        ));

        $builder->add('genderType', null, array(
            'placeholder' => 'form.user.gender.type',
            'translation_domain' => 'BackEndBundle',
            'choice_translation_domain' => 'app',
            'query_builder' =>function(EntityRepository $er){
                return  $er->createQueryBuilder('nn')
                    ->join('nn.nomNomenclatorType','nnn')
                    ->where('nnn.type = :nom_type')
                    ->setParameter('nom_type',NomUtil::NOM_SEX_TYPE);
            },
            'required' => true,
        ));

        $builder->add('phone', MyInputType::class, array(
            'label' => 'form.user.phone',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-phone',
            'required' => false,
        ));

        $builder->add('country', null, array(
            'placeholder' => 'form.user.country',
            'translation_domain' => 'BackEndBundle',
        ));

        $builder->add('passaport', MyInputType::class, array(
            'label' => 'form.user.passaport',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-plane',
            'required' => false,
        ));

        $builder->add('boletin', null, array(
            'attr' => array('class' => 'icheck')
        ));

        $builder->add('mediaFile', MyFileType::class, array(
            'required' => false,
        ));
    }

    public function getParent()
    {
        return ProfileFormType::class;
    }

    public function getName()
    {
        return 'app_user_profile';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}