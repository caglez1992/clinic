<?php
// src/AppBundle/Form/RegistrationType.php

namespace Core\CoreBundle\Form;

use Core\CoreBundle\Form\Type\MyFileType;
//use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Core\CoreBundle\Form\Type\MyInputType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Util\NomUtil;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', MyInputType::class, array(
            'label' => 'general.name',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-ioxhost',
        ));

        $builder->add('lastname', MyInputType::class, array(
            'label' => 'form.user.lastname',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-ioxhost'
        ));

        $builder->add('genderType', null, array(
            'placeholder' => 'form.user.gender.type',
            'translation_domain' => 'BackEndBundle',
            'choice_translation_domain' => 'app',
            'query_builder' =>function(EntityRepository $er){
                return  $er->createQueryBuilder('nn')
                    ->join('nn.nomNomenclatorType','nnn')
                    ->where('nnn.type = :nom_type')
                    ->setParameter('nom_type',NomUtil::NOM_SEX_TYPE);
            },
            'required' => true,
        ));

        $builder->add('phone', MyInputType::class, array(
            'label' => 'form.user.phone',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-phone',
            'required' => false,
        ));

        $builder->add('country', null, array(
            'placeholder' => 'form.user.country',
            'translation_domain' => 'BackEndBundle',
        ));

        $builder->add('passaport', MyInputType::class, array(
            'label' => 'form.user.passaport',
            'translation_domain' => 'BackEndBundle',
            'icon' => 'fa-plane',
            'required' => false,
        ));

        $builder->add('boletin', null, array(
            'attr' => array('class' => 'icheck')
        ));

        $builder->add('mediaFile', MyFileType::class, array(
            'required' => false,
        ));

//        $builder->add('recaptcha', EWZRecaptchaType::class);
        $builder->add('recaptcha', EWZRecaptchaType::class, array(
            'attr' => array(
                'options' => array(
                    'theme' => 'light',
                    'type'  => 'image',
                    'size' => 'invisible',              // set size to invisible
                    'defer' => true,
                    'async' => true,
                    //'callback' => 'onReCaptchaSuccess', // callback will be set by default if not defined (along with JS function that validate the form on success)
                    'bind' => 'register-submit',             // this is the id of the form submit button
                    // ...
                )
            )
        ));
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('Default', 'Register', 'Registration'),
        ));
    }*/

    public function getParent()
    {
        return RegistrationFormType::class;
    }

    public function getName()
    {
        return 'app_user_registration';
    }

    
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}