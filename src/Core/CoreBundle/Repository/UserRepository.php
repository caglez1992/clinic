<?php

namespace Core\CoreBundle\Repository;

use Core\CoreBundle\Entity\User;
use Core\PatientBundle\Entity\Patient;
use DateTime;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    /**
     * @param integer $limit
     * @return User[]
     */
    public function getRecentUsers($limit){
    	$date = new DateTime('now');
    	$date->setTimestamp($date->getTimestamp() - (60 * 60 * 24 * 7) );

        $query = $this->createQueryBuilder('user')
            ->select('user')
            ->where('user.enabled = :true')
            ->andWhere('user.createdAt > :date')
            ->setParameter('true', true)
            ->setParameter('date', $date);
        return $query->getQuery()->setMaxResults($limit)->setFirstResult(0)->getResult();
    }

    /**
     * @param string|array $roles
     * @param bool $onlyQb
     * @return array|\Core\CoreBundle\Entity\User[]
     */
    public function getUsersByRole($roles, $onlyQb = false){
        $qb = $this->createQueryBuilder('user')
            ->select('user')
            ->where('user.enabled = true');

        if(is_string($roles)){
            $qb->andWhere('user.roles LIKE :rol')
                ->setParameter('rol', '%'.$roles.'%');
        }

        if(is_array($roles)){
            $condition = '';
            $parameters = [];

            for($i = 0; $i < count($roles); $i++){
                $condition .= 'user.roles LIKE :rol' . $i;
                if($i < count($roles) - 1){
                    $condition .= ' OR ';
                }
                $parameters['rol'.$i] = '%'.$roles[$i].'%';
            }

            $qb->andWhere($condition)
                ->setParameters($parameters);
        }

        if($onlyQb)
            return $qb;
        
        return $qb->getQuery()->getResult();
    }


    public function getUsersEmailNotification(){
        $query = $this->createQueryBuilder('user')
            ->select('user')
            ->where('user.enabled = true AND user.emailNotification = true');
        return $query->getQuery()->getResult();
    }

    /**
     * @param User $worker
     * @return User[]
     */
    public function getSupervisorsByWorker($worker){
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->join('u.assignedWorkers', 'aw')
            ->where('aw = :worker')
            ->setParameter('worker', $worker);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param bool $onlyQb
     * @return User[]|\Doctrine\ORM\QueryBuilder
     */
    public function getWorkersWithTask($onlyQb = false){
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->join('u.tasks', 'task');

        if($onlyQb)
            return $qb;

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param bool $onlyQb
     * @return User[]|\Doctrine\ORM\QueryBuilder
     */
    public function getWorkersWithDiagnosis($onlyQb = false){
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->join('u.emittedDiagnosis', 'diagnosis');

        if($onlyQb)
            return $qb;

        return $qb->getQuery()->getArrayResult();
    }

    /*
     * @param Patient $patient
     * @return User[]
     */
    /*public function getWorkerHistory($patient){

        $sql = "SELECT DISTINCT w.* FROM fos_user p 
                LEFT JOIN authorization auth ON p.id = auth.patient_id
                GROUP BY p.id
                HAVING cant_auth = 0";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(Patient::class, 'p');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        //$query->setParameter(1, $nomOpen->getId());

        return $query->getResult();
        $qb = $this->createQueryBuilder('w')
            ->distinct(true)
            ->join('p.services', 's')
            ->where('s.worker = :worker')
            ->setParameter('worker', $worker);
        return $qb->getQuery()->getResult();
    }*/
}