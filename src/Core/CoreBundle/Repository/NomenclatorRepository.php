<?php

namespace Core\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NomenclatorRepository extends EntityRepository
{

    public function findNomNomenclatorsQuery($nomenclator_type)
    {
        return $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->innerJoin('c.nomNomenclatorType', 'nomty')
            ->where('nomty.name = :name')
            ->setParameter('name', $nomenclator_type);
    }

    public function findNomNomenclators($nomenclator_type)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->innerJoin('c.nomNomenclatorType', 'nomty')
            ->where('nomty.type = :type')
            ->setParameter('type', $nomenclator_type);

        return $qb->getQuery()->getResult();
    }

}