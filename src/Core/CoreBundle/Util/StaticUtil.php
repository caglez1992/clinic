<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\CoreBundle\Util;

use Core\PatientBundle\Entity\Service;
use DateTime;


class StaticUtil
{
    /**
     * @param string $init
     * @return string
     */
    public static function generateRandomColor($init){
        return $init . static::generateRandom('ABCEDF0123456789', 6);
    }

    /*public static function generateRandomColor($init){
        return '#'.$this->generateRandom('ABCEDF0123456789', 6);
    }*/
    
    /*public static function generateRandomColor2(){
        return '#EFC'.$this->generateRandom('ABCEDF0123456789', 3);
    }*/

    /**
     * @param string $c
     * @param int $l
     * @param bool $u
     * @return string
     */
    public static function generateRandom($c, $l, $u = FALSE) {
        if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++);
        else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s));
        return $s;
    }
    
    /**
     * @param DateTime $now
     * @return array
     */
    public static function getFiscalYearRange($now){
        #Fiscal Year from 1 Jul 200X to 30 Jun 200(X+1)
        $now = DateTime::createFromFormat('Y-m-d H:i', $now->format('Y-m-d') . ' 00:00');

        $dates = [];
        while($now->format('n') != 7){
            static::addMonths($now, -1);
        }
        $now->setDate($now->format('Y'), $now->format('n'), 1);
        $dates['begin'] = clone $now;

        $now->setDate((int)$now->format('Y') + 1, $now->format('n') - 1, 30);
        $now->setTimestamp($now->getTimestamp() + 86399);
        $dates['end'] = clone $now;
        return $dates;
    }

    /**
     * @param DateTime $now
     * @return array
     */
    public static function getMonthRange($now){
        $now = DateTime::createFromFormat('Y-m-d H:i', $now->format('Y-m-d') . ' 00:00');

        $dates = [];
        $thisMonth = $now->format('n');
        $now->setDate($now->format('Y'), $now->format('n'), 1);
        $dates['begin'] = clone $now;

        while($now->format('n') == $thisMonth){
            $now->modify('+1 day');
        }
        $now->modify('-1 day');
        $now->setTimestamp($now->getTimestamp() + 86399);
        $dates['end'] = clone $now;

        return $dates;
    }

    /**
     * @param DateTime $now
     * @return array
     */
    public static function getWeekRange($now){
        $now = DateTime::createFromFormat('Y-m-d H:i', $now->format('Y-m-d') . ' 00:00');

        $dates = [];

        while($now->format('N') != 1){
            $now->modify('-1 day');
        }
        $dates['begin'] = clone $now;

        $now->setTimestamp($now->getTimestamp() + 604799);
        $dates['end'] = clone $now;

        return $dates;
    }

    /**
     * @param DateTime $now
     * @return array
     */
    public static function getDayRange($now){
        $now = DateTime::createFromFormat('Y-m-d H:i', $now->format('Y-m-d') . ' 00:00');
        $dates = [];
        $dates['begin'] = clone $now;

        $now->setTimestamp($now->getTimestamp() + 86399);
        $dates['end'] = clone $now;
        
        return $dates;
    }

    /**
     * @param DateTime $date
     * @param integer $months
     */
    public static function addMonths($date, $months){
        $init = clone $date;
        $modifier = $months.' months';
        $back_modifier = -$months.' months';

        $date->modify($modifier);
        $back_to_init = clone $date;
        $back_to_init->modify($back_modifier);

        while($init->format('m') != $back_to_init->format('m')){
            $date->modify('-1 day');
            $back_to_init = clone $date;
            $back_to_init->modify($back_modifier);
        }
    }

    /**
     * @param array $rangeA
     * @param array $rangeB
     * @return bool
     */
    public static function datesFreeIntersection($rangeA, $rangeB){
        if($rangeA['begin'] < $rangeB['begin']){
            $FFr1 = $rangeA['end'];
            $FIr2 = $rangeB['begin'];
        }else{
            $FFr1 = $rangeB['end'];
            $FIr2 = $rangeA['begin'];
        }
        if($FFr1 >= $FIr2) {
            return false;
        }
        return true;
    }

    /**
     * Service $1 is the first range ina time line.
     * @param Service $service
     * @param Service $oldService
     * @return bool
     */
    public static function checkDateRanges($service, $oldService){
        if($service->getScheduleStart() < $oldService->getScheduleStart()) {
            $FFr1 = $service->getScheduleEnd();
            $FIr2 = $oldService->getScheduleStart();
        }else{
            $FFr1 = $oldService->getScheduleEnd();
            $FIr2 = $service->getScheduleStart();
        }

        if($FFr1 >= $FIr2) {
            return false;
        }
        return true;
    }

    /**
     * @param float $totalUnits
     * @return float
     */
    public static function tcmPayFormula($totalUnits){
        switch ($totalUnits){
            case $totalUnits < 100:
                return $totalUnits * 4.5;
            case $totalUnits >= 100 && $totalUnits <= 159:
                return $totalUnits * 5.25;
            case $totalUnits > 159:
                return $totalUnits * 6.25;
        }
        return 0.0;
    }
}
