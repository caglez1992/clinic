<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\CoreBundle\Util;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\Service;
use Core\PatientBundle\Entity\ServiceForm;
use Core\WorkerBundle\Entity\Note;
use DateTime;
use Exception;
use JMS\DiExtraBundle\Annotation as DI;
use mikehaertl\pdftk\Pdf;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @DI\Service("pdf.util", public=true)
 */
class PdfUtil
{
    
    private $container;
    private $engine;

    /**
     * @param ContainerInterface $container Container.
     * @param EngineInterface $engine
     * 
     * @DI\InjectParams({
     *     "container"  = @DI\Inject("service_container"),
     *     "engine"     = @DI\Inject("templating"),
     * })
     */
    public function __construct(ContainerInterface $container, EngineInterface $engine)
    {
        $this->container = $container;
        $this->engine = $engine;
    }

    public function get($service)
    {
        return $this->container->get($service);
    }

    /**
     * @param PdfForm $originForm
     * @param ServiceForm $serviceForm
     * @param Service $service
     */
    public function copyOriginForm($originForm, $serviceForm, $service){
        $dirWeb = $this->container->getParameter('dir.web');
        $dirPdf = $this->container->getParameter('dir.pdf');
        $originPdfPath = $dirWeb . $originForm->getFullPath();
        $formsServicePath = $dirPdf . '/services/';

        $servicePath = $formsServicePath . 'service' . $service->getId();
        if (!is_dir($servicePath)) {
            $this->mkdir($servicePath);
        }

        try {
            copy($originPdfPath, $servicePath . '/' . $originForm->getFilename());
        }catch(Exception $e){
            $this->container->get('logger')->critical('pdf copy failed, not permissions '. $e->getMessage());
        }
    }

    /**
     * @param Note $note
     * @param string $pdfName
     * @param string $folder
     */
    public function copyNote($note, $pdfName, $folder){
        $dirPdf = $this->container->getParameter('dir.pdf');
        $originPdfPath = $dirPdf . "/originals/$pdfName";
        $destinyPath = $dirPdf . "/notes/$folder/";

        try {
            copy($originPdfPath, $destinyPath . 'note' . $note->getId() . '.pdf');
        }catch(Exception $e){
            $this->container->get('logger')->critical('pdf copy failed, not permissions '. $e->getMessage());
        }
    }

    /**
     * @param Note $note
     */
    public function fillTcmNote($note){
        $this->container->get('doctrine')->getManager()->refresh($note);

        $dirPdf = $this->container->getParameter('dir.pdf');
        $notePdf = "$dirPdf/notes/tcm/note" . $note->getId() . '.pdf';

        $data = [];

        $settingRow = 'SettingRow';
        $domainRow = 'DomainRow';
        $startTimeRow = 'Start TimeRow';
        $endTimeRow = 'End TimeRow';
        $ffRow = 'Face to Face';
        $timeRow = 'UnitRow';

        $i = 1;
        $sumTime = 0;
        foreach ($note->getServices() as $service){
            $data[$settingRow.$i] = $service->getSettingCode()->getCode();
            $data[$domainRow.$i] = '12';
            $data[$startTimeRow.$i] = $service->getScheduleStart()->format('g:i a');
            $data[$endTimeRow.$i] = $service->getScheduleEnd()->format('g:i a');
            if($service->getPresence()->getName() == NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF){
                $data[$ffRow . '.' . ($i-1)] = 'Yes';
            }
            $data[$timeRow.$i] = $service->getServiceDurationMin();
            $i++;
            $sumTime += $service->getServiceDurationMin();
        }

        $now = new DateTime();

        $data['Total-Unit'] = (int)(($sumTime + 7 ) / 15);
        $data['Client NameRow1'] = $note->getPatient()->getFullName();
        $data['Client NumberRow1'] = $note->getPatient()->getCaseNumber();
        $data['Diagnostic'] = is_null($note->getPatient()->getActiveDiagnosis()) ? '' : $note->getPatient()->getActiveDiagnosis()->getDiagnosisTemplate()->__toString();
        $data['Provider NumberRow1'] = $note->getWorker()->getWorkerNumber();
        $data['Full-Name-TCM'] = $note->getWorker()->getFullName();
        $data['Date-Pro-Note'] = $now->format('m/d/Y');
        $data['Date of ServiceRow1'] = $note->getServiceDate()->format('m/d/Y');

        $note->setTime($sumTime);
        $note->setUnits( (int)(($sumTime + 7 ) / 15) );
        $this->container->get(BaseManager::class)->save($note);

        $pdf = new Pdf($notePdf);
        $pdf->fillForm($data)
            ->needAppearances()
            ->saveAs($notePdf);
    }

    /**
     * @param Note $note
     */
    public function updateTamNoteBillableStatus($note){
        //$this->container->get('doctrine')->getManager()->refresh($note);
        $note->setNoBillable(!$note->getNoBillable());

        $dirPdf = $this->container->getParameter('dir.pdf');
        $notePdf = "$dirPdf/notes/tcm/note" . $note->getId() . '.pdf';

        $sumTime = 0;
        foreach ($note->getServices() as $service){
            $sumTime += $service->getServiceDurationMin();
        }

        $data = [];
        $data['Total-Unit'] = $note->getNoBillable() ? 0 : (int)(($sumTime + 7 ) / 15);
        $data['Service CodeRow1'] = $note->getNoBillable() ? 'T1017-NB' : 'T1017';
        $data['Non Billable'] = $note->getNoBillable() ? '****NON-BILLABLE****' : '';

        $note->setTime($sumTime);
        $note->setUnits($data['Total-Unit']);
        $this->container->get(BaseManager::class)->save($note);

        $pdf = new Pdf($notePdf);
        $pdf->fillForm($data)
            ->needAppearances()
            ->saveAs($notePdf);
    }

    /**
     * @param Note $note
     * @param Service $service
     */
    public function fillPsrNote($note, $service){
        $dirPdf = $this->container->getParameter('dir.pdf');
        $notePdf = "$dirPdf/notes/cmh/note" . $note->getId() . '.pdf';

        $data = [
            'Client Name' => $note->getPatient()->getFullName(),
            'Date' => $note->getServiceDate()->format('m/d/Y'),
            'Start Time' => $service->getScheduleStart()->format('g:i a'),
            'End Time' => $service->getScheduleEnd()->format('g:i a'),
            'Select Service' => $service->getSettingCode()->__toString(),
            'Printed Name/Credentials' => $note->getWorker()->getFullName(),
        ];

        $note->setTime(240);
        $note->setUnits(16);
        $this->container->get(BaseManager::class)->save($note);

        $pdf = new Pdf($notePdf);
        $pdf->fillForm($data)
            ->needAppearances()
            ->saveAs($notePdf);
    }

    /**
     * @param Note $note
     * @param Service $service
     */
    public function fillH2019Note($note, $service){
        $dirPdf = $this->container->getParameter('dir.pdf');
        $notePdf = "$dirPdf/notes/cmh/note" . $note->getId() . '.pdf';

        $data = [
            'Client Name' => $note->getPatient()->getFullName(),
            'Client Number' => $note->getPatient()->getCaseNumber(),
            'Plase of Srvice' => $service->getSettingCode()->getCode(),
            'Staff Name' => $note->getWorker()->getFullName(),
            'DIAGNOSIS' => is_null($note->getPatient()->getActiveDiagnosis()) ? '' : $note->getPatient()->getActiveDiagnosis()->getDiagnosisTemplate()->__toString(),
            'Date' => $note->getServiceDate()->format('m/d/Y'),
            'Start-Time' => $service->getScheduleStart()->format('g:i a'),
            'End-Time' => $service->getScheduleEnd()->format('g:i a'),
            'Total-Time' => $service->getServiceDurationMin(),
            'UNITS' => $service->getUnitsUsed(),
            'Therapy Select' => $service->getServiceParent()->isGroup() ? 'Choice1' : 'Individual',
        ];

        $note->setTime($service->getServiceDurationMin());
        $note->setUnits($service->getUnitsUsed());
        $this->container->get(BaseManager::class)->save($note);

        $pdf = new Pdf($notePdf);
        $pdf->fillForm($data)
            ->needAppearances()
            ->saveAs($notePdf);
    }

    private function mkdir($location, $p = 0775){
        try {
            mkdir($location, $p);
            chown($location, "www-data");
            chgrp($location, "www-data");
        }catch(Exception $e){
            $this->container->get('logger')->critical('pdf mkdir failed, not permissions '. $e->getMessage());
        }
    }
}
