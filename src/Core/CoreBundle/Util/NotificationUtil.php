<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\CoreBundle\Util;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @DI\Service("notification.util")
 */
class NotificationUtil
{
    
    private $container;

    /**
     * @param ContainerInterface $container
     * @DI\InjectParams({
     *     "container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function generateRandomColor(){
        return '#'.$this->generateRandom('ABCEDF0123456789', 6);
    }
}
