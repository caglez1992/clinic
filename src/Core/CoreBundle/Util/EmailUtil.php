<?php

/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 13/03/17
 * Time: 15:48
 */
namespace Core\CoreBundle\Util;

use Core\CoreBundle\Entity\User;
use Sonata\AdminBundle\Admin\Pool;
use Swift_Attachment;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Email Util
 *
 * @DI\Service("email.utils")
 */
class EmailUtil
{
    private $container;
    private $pool;
    private $translator;

    /**
     * @param ContainerInterface $container Container.
     * @param Pool $pool SonataPool.
     *
     * @DI\InjectParams({
     *     "container" = @DI\Inject("service_container"),
     *     "pool" = @DI\Inject("sonata.admin.pool"),
     * })
     */
    public function __construct(ContainerInterface $container, Pool $pool)
    {
        $this->container = $container;
        $this->pool = $pool;
        $this->translator = $this->container->get('translator');
    }

    protected function get($id)
    {
        return $this->container->get($id);
    }

    /**
     * Low level send html messages.
     *
     * @param string $to
     * @param $subject
     * @param $html
     * @param string|null $attachment
     */
    public function send($to, $subject, $html, $attachment){
        $from = $this->container->getParameter('mailer_email');

        $messageEmail = Swift_Message::newInstance();
        $messageEmail->setSubject($subject);
        $messageEmail->setFrom($from);
        $messageEmail->setTo($to);
        $messageEmail->setBody($html, 'text/html');
        if(!is_null($attachment)){
            $messageEmail->attach(Swift_Attachment::fromPath($attachment));
        }
        $this->container->get('mailer')->send($messageEmail);
    }

    /**
     * @param string $template
     * @param array $data
     */
    private function renderTemplate($template, $data){
        $engine = $this->get('templating');
        return $engine->render($template, $data);
    }

    /**
     * @param mixed $to
     * @param string $title
     * @param string $body
     * @param mixed $attachment
     * @param bool $isRole
     */
    public function sendGeneralEmail($to, $title, $body, $attachment = null, $isRole = true){
        if(is_string($to) && !$isRole){
            $html = $this->renderTemplate(NomUtil::TEMPLATE_EMAIL_BASE, array(
                'title' => $title,
                'body' => $body,
            ));
            $this->send($to, $title, $html, $attachment);
        }

        $users = [];
        if(is_string($to)){
            $users = $this->get('doctrine')->getRepository(User::class)->getUsersByRole($to);
        }

        if(!is_array($to) && $to instanceof User) {
            $users[] = $to;
        }

        if(is_array($to) && count($to) > 0 && $to[0] instanceof User){
            $users = $to;
        }

        if(is_array($to) && count($to) > 0 && is_string($to[0])){
            foreach ($to as $t){
                $usersByRole = $this->get('doctrine')->getRepository(User::class)->getUsersByRole($t);
                $users = array_merge($users, $usersByRole);
            }
        }

        /** @var User $u */
        foreach ($users as $u) {
            $html = $this->renderTemplate(NomUtil::TEMPLATE_EMAIL_BASE, array(
                'title' => $title,
                'body' => $body,
            ));
            $this->send($u->getEmail(), $title, $html, $attachment);
        }
    }
}