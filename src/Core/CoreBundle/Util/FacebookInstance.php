<?php

/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 13/03/17
 * Time: 15:48
 */

namespace Core\CoreBundle\Util;

use Facebook\Facebook;

if(!session_id()) {
    session_start();
}

class FacebookInstance
{
    ///Condition 1 - Presence of a static member variable
    private static $_instance = null;

    private static $_face = null;

    ///Condition 2 - Locked down the constructor
    private function __construct() {} //Prevent any oustide instantiation of this class

    ///Condition 3 - Prevent any object or instance of that class to be cloned
    private function __clone() {} //Prevent any copy of this object

    ///Condition 4 - Have a single globally accessible static method

    public static function getInstance()
    {
        if (!is_object(self::$_instance)) {  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new FacebookInstance();
            self::$_face = new Facebook([
                'app_id' => '532811947120177',
                'app_secret' => '9bc8f8701e5fae7965fc311837234150',
                'default_graph_version' => 'v3.0',
                'persistent_data_handler'=>'session',
            ]);
        }

        return self::$_instance;
    }

    /**
     * @return Facebook
     */
    public function getFace(){
        return self::$_face;
    }
}