<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\CoreBundle\Util;

use Core\CoreBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ImageUtils
 *
 * @DI\Service("image.utils")
 */
class ImageUtil
{
    private $container;
    private $em;

    /**
     * Constructor of the Object.
     *
     * @param ContainerInterface $container Container.
     *
     * @param EntityManager $em EntityManager.
     *
     * @DI\InjectParams({
     *     "container"        = @DI\Inject("service_container"),
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param Media $media
     * @param integer $width
     * @param string $newName
     */
    public function resizeImage($media, $width, $newName = null){

    	$root = $this->container->getParameter('param.medias');
        
        $subject = $media->getMedia();
        $search = '.';
        $replace = '$.$';
        $oririginalName = strrev(implode(strrev($replace), explode(strrev($search), strrev($subject), 2)));

        $exp = explode("$.$", $oririginalName);
        $ext = strtolower($exp[1]);
        if(is_null($newName))
        	$img = $exp[0].".jpeg";
        else
        	$img = $newName.'-'.$exp[0].'.jpeg';

        if(!file_exists($root .'/'. $media->getMedia()))
            return;

        @chmod($root .'/'. $media->getMedia(), 0777);

        // Get dimensions
        list($width_orig, $height_orig) = getimagesize($root .'/'. $media->getMedia());

        $proportion = $width_orig/$height_orig;

        $height = $width / $proportion;

        // Resample
        $image_p = imagecreatetruecolor($width, $height);
        imagealphablending($image_p, false);
        imagesavealpha($image_p, true);
        if ($ext == "jpeg" || $ext == "jpg")
            $image = imagecreatefromjpeg($root .'/'. $media->getMedia());
        if ($ext == "bmp")
            $image = $this->ImageCreateFromBMP($root .'/'. $media->getMedia());
        if ($ext == "gif")
            $image = imagecreatefromgif($root .'/'. $media->getMedia());
        if ($ext == "png")
            $image = imagecreatefrompng($root .'/'. $media->getMedia());

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output
        @chmod($root .'/'. $media->getMedia(), 0777);
        imagejpeg($image_p, $root .'/'. $img);
        imagedestroy($image);
        imagedestroy($image_p);
    }

    /*********************************************/
    /* Fonction: ImageCreateFromBMP              */
    /* Author:   DHKold                          */
    /* Contact:  admin@dhkold.com                */
    /* Date:     The 15th of June 2005           */
    /* Version:  2.0B                            */
    /*********************************************/
    public function ImageCreateFromBMP($filename)
    {
        //Ouverture du fichier en mode binaire
        if (!$f1 = fopen($filename, "rb")) return FALSE;

        //1 : Chargement des ent�tes FICHIER
        $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1, 14));
        if ($FILE['file_type'] != 19778) return FALSE;

        //2 : Chargement des ent�tes BMP
        $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel' .
            '/Vcompression/Vsize_bitmap/Vhoriz_resolution' .
            '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1, 40));
        $BMP['colors'] = pow(2, $BMP['bits_per_pixel']);
        if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
        $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel'] / 8;
        $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
        $BMP['decal'] = ($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] -= floor($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] = 4 - (4 * $BMP['decal']);
        if ($BMP['decal'] == 4) $BMP['decal'] = 0;

        //3 : Chargement des couleurs de la palette
        $PALETTE = array();
        if ($BMP['colors'] < 16777216) {
            $PALETTE = unpack('V' . $BMP['colors'], fread($f1, $BMP['colors'] * 4));
        }

        //4 : Creation de image
        $IMG = fread($f1, $BMP['size_bitmap']);
        $VIDE = chr(0);

        $res = imagecreatetruecolor($BMP['width'], $BMP['height']);
        $P = 0;
        $Y = $BMP['height'] - 1;
        while ($Y >= 0) {
            $X = 0;
            while ($X < $BMP['width']) {
                if ($BMP['bits_per_pixel'] == 24)
                    $COLOR = unpack("V", substr($IMG, $P, 3) . $VIDE);
                elseif ($BMP['bits_per_pixel'] == 16) {
                    $COLOR = unpack("n", substr($IMG, $P, 2));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 8) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, $P, 1));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 4) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 2) % 2 == 0) $COLOR[1] = ($COLOR[1] >> 4); else $COLOR[1] = ($COLOR[1] & 0x0F);
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 1) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 8) % 8 == 0) $COLOR[1] = $COLOR[1] >> 7;
                    elseif (($P * 8) % 8 == 1) $COLOR[1] = ($COLOR[1] & 0x40) >> 6;
                    elseif (($P * 8) % 8 == 2) $COLOR[1] = ($COLOR[1] & 0x20) >> 5;
                    elseif (($P * 8) % 8 == 3) $COLOR[1] = ($COLOR[1] & 0x10) >> 4;
                    elseif (($P * 8) % 8 == 4) $COLOR[1] = ($COLOR[1] & 0x8) >> 3;
                    elseif (($P * 8) % 8 == 5) $COLOR[1] = ($COLOR[1] & 0x4) >> 2;
                    elseif (($P * 8) % 8 == 6) $COLOR[1] = ($COLOR[1] & 0x2) >> 1;
                    elseif (($P * 8) % 8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } else
                    return FALSE;
                imagesetpixel($res, $X, $Y, $COLOR[1]);
                $X++;
                $P += $BMP['bytes_per_pixel'];
            }
            $Y--;
            $P += $BMP['decal'];
        }

        //Fermeture du fichier
        fclose($f1);

        return $res;
    }


    public function addTextInPNGCenter($image_url, $text)
    {
        $im = imagecreatefrompng($image_url);

        $size = getimagesize($image_url);

        $font_size  = 5;
        $width      = imagefontwidth($font_size) * strlen($text);
        $x_pos      = ($size[0]/2)-($width/2);
        $y_pos      = $size[1] / 2;
        $text_color = imagecolorallocate($im, 0, 0, 0);
        imagestring($im,$font_size,$x_pos,$y_pos, $text,$text_color );
        return $im;
    }

    /**
     * Salva la imagen en el disco duro.
     *
     * @param resource $img_resource
     * @param string $target_path
     *
     * @return bool
     */
    public function saveImagePNG($img_resource, $target_path)
    {
        return imagepng($img_resource, $target_path, 0,null);
    }


}
