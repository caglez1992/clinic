<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\CoreBundle\Util;

use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\ServiceParent;
use Core\WorkerBundle\Entity\ServiceAcl;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @DI\Service("clinic.util", public=true)
 */
class ClinicUtil
{
    
    private $container;
    private $d;

    /**
     * @param ContainerInterface $container
     * @param Registry $d
     * @DI\InjectParams({
     *     "container" = @DI\Inject("service_container"),
     *     "d" = @DI\Inject("doctrine"),
     * })
     */
    public function __construct(ContainerInterface $container, Registry $d)
    {
        $this->container = $container;
        $this->d = $d;
    }

    public function get($service)
    {
        return $this->container->get($service);
    }

    /**
     * @param string $rol
     * @return ArrayCollection|PdfForm[]
     */
    public function getFormsByRol($rol){
        $serviceAcl = $this->d->getRepository(ServiceAcl::class)->findOneBy(['role' => $rol]);

        $forms = new ArrayCollection();
        /** @var ServiceParent $serviceParent */
        foreach ($serviceAcl->getServices() as $serviceParent){
            foreach($serviceParent->getForms() as $form){
                if(!$forms->contains($form)){
                    $forms->add($form);
                }
            }
        }

        return $forms;
    }
}
