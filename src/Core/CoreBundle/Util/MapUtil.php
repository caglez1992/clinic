<?php

/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 13/03/17
 * Time: 15:48
 */
namespace Core\CoreBundle\Util;

use Core\CircuitBundle\Entity\Circuit;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Overlay\Animation;
use Ivory\GoogleMap\Overlay\Icon;
use Ivory\GoogleMap\Overlay\IconSequence;
use Ivory\GoogleMap\Overlay\Marker;
use Ivory\GoogleMap\Overlay\MarkerShape;
use Ivory\GoogleMap\Overlay\MarkerShapeType;
use Ivory\GoogleMap\Overlay\Polyline;
use Ivory\GoogleMap\Overlay\Symbol;
use Ivory\GoogleMap\Overlay\SymbolPath;
use Symfony\Component\DependencyInjection\ContainerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Maps Util
 *
 * @DI\Service("maps.utils")
 */
class MapUtil
{
    private $container;

    /**
     * Constructor of the Object.
     *
     * @param ContainerInterface $container Container.
     *
     * @DI\InjectParams({
     *     "container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Display a google maps with the places
     *
     * @param Circuit $circuit
     *
     * @return Map
     */
    public function buildMap($circuit)
    {
        $places = $this->container->get('circuit.manager')->getPlacesByCircuits($circuit);
        if ($places->count() == 0)
            return null;

        $map = new Map();
        $map->setHtmlAttribute('class', 'width-100-p');
        $map->setMapOption('zoom', 8);
        $map->setCenter(new Coordinate(22.9915379, -82.4275737));

        foreach ($places as $place) {
            $marker = new Marker(
            //new Coordinate(25.774, -80.190),
                new Coordinate($place->getLatitude(), $place->getLongitude()),
                Animation::DROP,
                new Icon(),
                new Symbol(SymbolPath::CIRCLE),
                new MarkerShape(MarkerShapeType::CIRCLE, [1.1, 2.1, 0.5]),
                ['clickable' => true]
            );
            $map->getOverlayManager()->addMarker($marker);
        }


        /*$polyline = new Polyline(
            [
                new Coordinate(25.774, -80.190),
                new Coordinate(18.466, -66.118),
                new Coordinate(32.321, -64.757),
                //new Coordinate(25.774, -80.190),
            ],
            [
                new IconSequence(new Symbol(SymbolPath::FORWARD_OPEN_ARROW)),
            ],
            ['fillOpacity' => 0.5]
        );


        $map->getOverlayManager()->addPolyline($polyline);*/

        return $map;
    }

    /**
     * Display a google maps with the contact palce
     *
     * @param string $latitude
     * @param string $longitude
     * @return Map
     */
    public function buildContactMap($latitude, $longitude)
    {
        $map = new Map();
        $map->setHtmlAttribute('class', 'width-100-p height-430');
        $map->setMapOption('zoom', 13);
        $map->setCenter(new Coordinate($latitude, $longitude));

        $marker = new Marker(
            new Coordinate($latitude, $longitude),
            Animation::DROP,
            new Icon(),
            new Symbol(SymbolPath::CIRCLE),
            new MarkerShape(MarkerShapeType::CIRCLE, [1.1, 2.1, 0.5]),
            ['clickable' => true]
        );
        $map->getOverlayManager()->addMarker($marker);

        return $map;
    }

}