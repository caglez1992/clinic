<?php


namespace Core\CoreBundle\Util;

use Core\CoreBundle\Entity\Nomenclator;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Service;
use Core\WorkerBundle\Entity\Note;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpKernel\Kernel as AppKernel;

/**
 * @DI\Service
 */
class NomUtil
{

    //Nomenclatures
    const NOM_INSURANCE_TYPE = 'NOM_INSURANCE_TYPE';
    const MEDICARE = 'medicare';
    const MEDICAID = 'medicaid';
    
    const NOM_STATUS = 'NOM_STATUS';
    const NOM_STATUS_OPEN = 'open';
    const NOM_STATUS_CLOSE = 'close';
    const NOM_STATUS_PENDING = 'pending';

    const NOM_INTAKE_TYPE = 'NOM_INTAKE_TYPE';
    const NOM_INTAKE_TYPE_TCM = 'nom.intake.type.tcm';
    const NOM_INTAKE_TYPE_CMH = 'nom.intake.type.cmh';

    const NOM_SEX_TYPE = 'NOM_SEX_TYPE';
    const NOM_SEX_TYPE_M = 'nom.sex.type.m';
    const NOM_SEX_TYPE_F = 'nom.sex.type.f';

    const NOM_RACE_TYPE = 'NOM_RACE_TYPE';
    const NOM_RACE_TYPE_W = 'nom.race.type.white';
    const NOM_RACE_TYPE_B = 'nom.race.type.black';
    const NOM_RACE_TYPE_AA = 'nom.race.type.african.american';
    const NOM_RACE_TYPE_NA = 'nom.race.type.native.american';
    const NOM_RACE_TYPE_AS = 'nom.race.type.asian';
    const NOM_RACE_TYPE_MR = 'nom.race.type.multiracial';
    const NOM_RACE_TYPE_UN = 'nom.race.type.unknown';

    const NOM_ETHNICITY_TYPE = 'NOM_ETHNICITY_TYPE';
    const NOM_ETHNICITY_TYPE_HL = 'nom.ethnicity.type.latino';
    const NOM_ETHNICITY_TYPE_NHL = 'nom.ethnicity.type.non.latino';

    const NOM_MARITAL_STATUS_TYPE = 'NOM_MARITAL_STATUS_TYPE';
    const NOM_MARITAL_STATUS_TYPE_SINGLE = 'nom.marital.status.type.single';
    const NOM_MARITAL_STATUS_TYPE_MARRIED = 'nom.marital.status.type.married';
    const NOM_MARITAL_STATUS_TYPE_COHABITING = 'nom.marital.status.type.cohabiting';
    const NOM_MARITAL_STATUS_TYPE_DIVORCED = 'nom.marital.status.type.divorced';
    const NOM_MARITAL_STATUS_TYPE_SEPARATED = 'nom.marital.status.type.separated';
    const NOM_MARITAL_STATUS_TYPE_WIDOWED = 'nom.marital.status.type.widowed';
    const NOM_MARITAL_STATUS_TYPE_UNKNOWN = 'nom.marital.status.type.unknown';

    const NOM_EDUCATIONAL_LEVEL_TYPE = 'NOM_EDUCATIONAL_LEVEL_TYPE';
    const NOM_EDUCATIONAL_LEVEL_TYPE_NO_EDUCATION = 'nom.educational.level.type.no.education';
    const NOM_EDUCATIONAL_LEVEL_TYPE_ELEMENTARY = 'nom.educational.level.type.elementary';
    const NOM_EDUCATIONAL_LEVEL_TYPE_MIDDLE_SCHOOL = 'nom.educational.level.type.middle.school';
    const NOM_EDUCATIONAL_LEVEL_TYPE_HIGH_SCHOOL = 'nom.educational.level.type.high.school';
    const NOM_EDUCATIONAL_LEVEL_TYPE_TECH = 'nom.educational.level.type.tech';
    const NOM_EDUCATIONAL_LEVEL_TYPE_COLLEGE = 'nom.educational.level.type.college';
    const NOM_EDUCATIONAL_LEVEL_TYPE_PHD = 'nom.educational.level.type.phd';

    const NOM_LANGUAGE_TYPE = 'NOM_LANGUAGE_TYPE';
    const NOM_LANGUAGE_TYPE_S = 'nom.language.type.spanish';
    const NOM_LANGUAGE_TYPE_E = 'nom.language.type.english';
    const NOM_LANGUAGE_TYPE_C = 'nom.language.type.creole';

    const NOM_EMPLOYMENT_TYPE = 'NOM_EMPLOYMENT_TYPE';
    const NOM_EMPLOYMENT_TYPE_FT = 'nom.employment.type.ft';
    const NOM_EMPLOYMENT_TYPE_PT = 'nom.employment.type.pt';
    const NOM_EMPLOYMENT_TYPE_RETIRED = 'nom.employment.type.retired';
    const NOD_EMPLOYMENT_TYPE_DISABLED = 'nom.employment.type.disabled';
    const NOM_EMPLOYMENT_TYPE_HM = 'nom.employment.type.home.maker';
    const NOM_EMPLOYMENT_TYPE_STUDENT = 'nom.employment.type.student';
    const NOM_EMPLOYMENT_TYPE_UNEMPLOYED = 'nom.employment.type.unemployed';
    const NOM_EMPLOYMENT_TYPE_NA = 'nom.employment.type.na';

    const NOM_RESIDENTIAL_TYPE = 'NOM_RESIDENTIAL_TYPE';
    const NOM_RESIDENTIAL_TYPE_A = 'nom.residential.type.alone';
    const NOM_RESIDENTIAL_TYPE_R = 'nom.residential.type.relatives';
    const NOM_RESIDENTIAL_TYPE_NR = 'nom.residential.type.non.relatives';
    const NOM_RESIDENTIAL_TYPE_ALF = 'nom.residential.type.alf';
    const NOM_RESIDENTIAL_TYPE_FC = 'nom.residential.type.fc';
    const NOM_RESIDENTIAL_TYPE_H = 'nom.residential.type.hospital';
    const NOM_RESIDENTIAL_TYPE_SIPP = 'nom.residential.type.sipp';
    const NOM_RESIDENTIAL_TYPE_C = 'nom.residential.type.correctional';
    const NOM_RESIDENTIAL_TYPE_HL = 'nom.residential.type.homeless';

    const NOM_REFERRED_TYPE = 'NOM_REFERRED_TYPE';
    const NOM_REFERRED_TYPE_S = 'nom.referred.type.self';
    const NOM_REFERRED_TYPE_A = 'nom.referred.type.agency';

    const NOM_AGENCY_TYPE = 'NOM_AGENCY_TYPE';
    const NOM_AGENCY_TYPE_COLONIA = 'nom.agency.type.colonia';
    const NOM_AGENCY_TYPE_LEON = 'nom.agency.type.leon';
    const NOM_AGENCY_TYPE_PASTEUR = 'nom.agency.type.pasteur';
    const NOM_AGENCY_TYPE_CAC = 'nom.agency.type.cac';
    const NOM_AGENCY_TYPE_DOCTORS = 'nom.agency.type.doctors';
    const NOM_AGENCY_TYPE_WELLMAX = 'nom.agency.type.wellmax';
    const NOM_AGENCY_TYPE_ULTRACARE = 'nom.agency.type.ultracare';
    const NOM_AGENCY_TYPE_MERCEDES = 'nom.agency.type.mercedes';
    const NOM_AGENCY_TYPE_INTERAMERICAN = 'nom.agency.type.interamerican';
    const NOM_AGENCY_TYPE_UNIVERSITY = 'nom.agency.type.university';
    const NOM_AGENCY_TYPE_OTHER = 'nom.agency.type.other';

    const NOM_REFERRAL_REASONS_TYPE = 'NOM_REFERRAL_REASONS_TYPE';
    const NOM_REFERRAL_REASONS_TYPE_SA = 'nom.referral.reasons.type.substance.abuse';
    const NOM_REFERRAL_REASONS_TYPE_PS = 'nom.referral.reasons.type.personal.safety';
    const NOM_REFERRAL_REASONS_TYPE_HP = 'nom.referral.reasons.type.health.problems';
    const NOM_REFERRAL_REASONS_TYPE_CR = 'nom.referral.reasons.type.crisis';
    const NOM_REFERRAL_REASONS_TYPE_PH = 'nom.referral.reasons.type.potential.harm';
    const NOM_REFERRAL_REASONS_TYPE_EN = 'nom.referral.reasons.type.engagement';
    const NOM_REFERRAL_REASONS_TYPE_HO = 'nom.referral.reasons.type.housing';
    const NOM_REFERRAL_REASONS_TYPE_DL = 'nom.referral.reasons.type.daily.living';
    const NOM_REFERRAL_REASONS_TYPE_CS = 'nom.referral.reasons.type.community.skills';
    const NOM_REFERRAL_REASONS_TYPE_LI = 'nom.referral.reasons.type.legal.issues';
    const NOM_REFERRAL_REASONS_TYPE_SS = 'nom.referral.reasons.type.self.sufficiency';
    const NOM_REFERRAL_REASONS_TYPE_CMH = 'nom.referral.reasons.type.mental.health';
    const NOM_REFERRAL_REASONS_TYPE_AS = 'nom.referral.reasons.type.availability.support';
    const NOM_REFERRAL_REASONS_TYPE_US = 'nom.referral.reasons.type.utilization.support';

    const NOM_SCHOOL_PROGRAM_TYPE = 'NOM_SCHOOL_PROGRAM_TYPE';
    const NOM_SCHOOL_PROGRAM_REGULAR = 'nom.school.program.type.regular';
    const NOM_SCHOOL_PROGRAM_ESE = 'nom.school.program.type.ese';
    const NOM_SCHOOL_PROGRAM_EBD = 'nom.school.program.type.ebd';
    const NOM_SCHOOL_PROGRAM_ESOL = 'nom.school.program.type.esol';
    const NOM_SCHOOL_PROGRAM_HHIP = 'nom.school.program.type.hhip';
    const NOM_SCHOOL_PROGRAM_OTHER = 'nom.school.program.type.other';

    const NOM_RESIDENTIAL_STATUS_TYPE = 'NOM_RESIDENTIAL_STATUS_TYPE';
    const NOM_RESIDENTIAL_STATUS_TYPE_CITIZEN = 'nom.residential.status.type.citizen';
    const NOM_RESIDENTIAL_STATUS_TYPE_RESIDENT = 'nom.residential.status.type.resident';
    const NOM_RESIDENTIAL_STATUS_TYPE_OTHER = 'nom.residential.status.type.other';

    const NOM_ACTION_TYPE = 'NOM_ACTION_TYPE';
    const NOM_ACTION_TYPE_CREATE = 'nom.action.type.create';
    const NOM_ACTION_TYPE_UPDATE = 'nom.action.type.update';
    const NOM_ACTION_TYPE_DELETE = 'nom.action.type.delete';
    
    //SERVICE
    const NOM_SERVICE_TYPE = 'NOM_SERVICE_TYPE';
    const NOM_SERVICE_TYPE_EVALUATION = 'nom.service.type.evaluation';
    const NOM_SERVICE_TYPE_INTERVENTION = 'nom.service.type.intervention';

    const NOM_SERVICE_PRESENCE_TYPE = 'NOM_SERVICE_PRESENCE_TYPE';
    const NOM_SERVICE_PRESENCE_TYPE_FTF = 'nom.service.presence.type.face.to.face';
    const NOM_SERVICE_PRESENCE_TYPE_OB = 'nom.service.presence.type.on.behave';

    const NOM_SERVICE_SESSION_TYPE = 'NOM_SERVICE_SESSION_TYPE';
    const NOM_SERVICE_SESSION_TYPE_AM = 'nom.service.session.type.am';
    const NOM_SERVICE_SESSION_TYPE_PM = 'nom.service.session.type.pm';

    const NOM_MODIFIER_AMOUNT_TYPE = 'NOM_MODIFIER_AMOUNT_TYPE';
    const NOM_MODIFIER_AMOUNT_TYPE_AMOUNT = 'nom.modifier.amount.type.amount';
    const NOM_MODIFIER_AMOUNT_TYPE_HOURS = 'nom.modifier.amount.type.hours';
    const NOM_MODIFIER_AMOUNT_TYPE_UNITS = 'nom.modifier.amount.type.units';

    const NOM_FREQUENCY_TYPE = 'NOM_FREQUENCY_TYPE';
    const NOM_FREQUENCY_TYPE_YEAR = 'nom.frequency.type.year';
    const NOM_FREQUENCY_TYPE_MONTH = 'nom.frequency.type.month';
    const NOM_FREQUENCY_TYPE_WEEK = 'nom.frequency.type.week';
    const NOM_FREQUENCY_TYPE_DAY = 'nom.frequency.type.day';

    const NOM_ACTION_REGISTRY_PATIENT_TYPE = 'NOM_ACTION_REGISTRY_PATIENT_TYPE';
    const NOM_ACTION_REGISTRY_PATIENT_TYPE_ADMITTED = 'nom.action.registry.patient.type.admitted';
    const NOM_ACTION_REGISTRY_PATIENT_TYPE_MODIFY = 'nom.action.registry.patient.type.modify';
    const NOM_ACTION_REGISTRY_PATIENT_TYPE_DISCHARGE = 'nom.action.registry.patient.type.discharge';

    const NOM_REGISTRY_PD_ACTION = 'NOM_REGISTRY_PD_ACTION';
    const NOM_REGISTRY_PD_ACTION_ASSESSMENT = 'nom.registry.pd.action.assessment';
    const NOM_REGISTRY_PD_ACTION_INTERVENTION = 'nom.registry.pd.action.intervention';

    const NOM_PARENT_TYPE = 'NOM_PARENT_TYPE';
    const NOM_PARENT_TYPE_AUNT = 'nom.parent.type.aunt';
    const NOM_PARENT_TYPE_BROTHER = 'nom.parent.type.brother';
    const NOM_PARENT_TYPE_BROTHER_LAW = 'nom.parent.type.brother.law';
    const NOM_PARENT_TYPE_BROTHER_WIDOWER = 'nom.parent.type.brother.widower';
    const NOM_PARENT_TYPE_COUSIN = 'nom.parent.type.cousin';
    const NOM_PARENT_TYPE_DAUGHTER = 'nom.parent.type.daughter';
    const NOM_PARENT_TYPE_DAUGHTER_LAW = 'nom.parent.type.daughter.law';
    const NOM_PARENT_TYPE_DAUGHTER_WIDOWER = 'nom.parent.type.daughter.widower';
    const NOM_PARENT_TYPE_FATHER = 'nom.parent.type.father';
    const NOM_PARENT_TYPE_FATHER_LAW = 'nom.parent.type.father.law';
    const NOM_PARENT_TYPE_FATHER_DAUGHTER = 'nom.parent.type.father.daughter';
    const NOM_PARENT_TYPE_FATHER_SON = 'nom.parent.type.father.son';
    const NOM_PARENT_TYPE_FATHER_WIDOW = 'nom.parent.type.father.widow';
    const NOM_PARENT_TYPE_GDH = 'nom.parent.type.gdh';
    const NOM_PARENT_TYPE_GDW = 'nom.parent.type.gdw';
    const NOM_PARENT_TYPE_GF = 'nom.parent.type.gf';
    const NOM_PARENT_TYPE_GFW = 'nom.parent.type.gfw';
    const NOM_PARENT_TYPE_GFWI = 'nom.parent.type.gfwi';
    const NOM_PARENT_TYPE_GM = 'nom.parent.type.gm';
    const NOM_PARENT_TYPE_GMH = 'nom.parent.type.gmh';
    const NOM_PARENT_TYPE_GMW = 'nom.parent.type.gmw';
    const NOM_PARENT_TYPE_GS = 'nom.parent.type.gs';
    const NOM_PARENT_TYPE_GSW = 'nom.parent.type.gsw';
    const NOM_PARENT_TYPE_GSWI = 'nom.parent.type.gswi';
    const NOM_PARENT_TYPE_GGD = 'nom.parent.type.ggd';
    const NOM_PARENT_TYPE_GGF = 'nom.parent.type.ggf';
    const NOM_PARENT_TYPE_GGM = 'nom.parent.type.ggm';
    const NOM_PARENT_TYPE_GGS = 'nom.parent.type.ggs';
    const NOM_PARENT_TYPE_HB = 'nom.parent.type.hb';
    const NOM_PARENT_TYPE_HSH = 'nom.parent.type.hsh';
    const NOM_PARENT_TYPE_HBW = 'nom.parent.type.hbw';
    const NOM_PARENT_TYPE_HS = 'nom.parent.type.hs';
    const NOM_PARENT_TYPE_H = 'nom.parent.type.h';
    const NOM_PARENT_TYPE_MIL = 'nom.parent.type.mil';
    const NOM_PARENT_TYPE_MD = 'nom.parent.type.md';
    const NOM_PARENT_TYPE_MS = 'nom.parent.type.ms';
    const NOM_PARENT_TYPE_MW = 'nom.parent.type.mw';
    const NOM_PARENT_TYPE_N = 'nom.parent.type.n';
    const NOM_PARENT_TYPE_NI = 'nom.parent.type.ni';
    const NOM_PARENT_TYPE_S = 'nom.parent.type.s';
    const NOM_PARENT_TYPE_SIL = 'nom.parent.type.sil';
    const NOM_PARENT_TYPE_SON = 'nom.parent.type.son';
    const NOM_PARENT_TYPE_SONIL = 'nom.parent.type.sonil';
    const NOM_PARENT_TYPE_SONW = 'nom.parent.type.sonw';
    const NOM_PARENT_TYPE_SB = 'nom.parent.type.sb';
    const NOM_PARENT_TYPE_SD = 'nom.parent.type.sd';
    const NOM_PARENT_TYPE_SDH = 'nom.parent.type.sdh';
    const NOM_PARENT_TYPE_SF = 'nom.parent.type.sf';
    const NOM_PARENT_TYPE_SM = 'nom.parent.type.sm';
    const NOM_PARENT_TYPE_SS = 'nom.parent.type.ss';
    const NOM_PARENT_TYPE_SSON = 'nom.parent.type.sson';
    const NOM_PARENT_TYPE_SSONWI = 'nom.parent.type.ssonwi';
    const NOM_PARENT_TYPE_U = 'nom.parent.type.u';
    const NOM_PARENT_TYPE_W = 'nom.parent.type.w';

    /*const NOM_NOTE_TYPE = 'NOM_NOTE_TYPE';
    const NOM_NOTE_TYPE_TCM = 'nom.note.type.tcm';
    const NOM_NOTE_TYPE_CMH_PSR = 'nom.note.type.cmh.psr';
    const NOM_NOTE_TYPE_CMH_GT = 'nom.note.type.cmh.gt'; //Group Therapy*/

    const NOM_STATE_TYPE = 'NOM_STATE_TYPE';
    const NOM_STATE_TYPE_AL = 'nom.state.type.al';
    const NOM_STATE_TYPE_AK = 'nom.state.type.ak';
    const NOM_STATE_TYPE_AS = 'nom.state.type.as';
    const NOM_STATE_TYPE_AZ = 'nom.state.type.az';
    const NOM_STATE_TYPE_AR = 'nom.state.type.ar';
    const NOM_STATE_TYPE_CA = 'nom.state.type.ca';
    const NOM_STATE_TYPE_CO = 'nom.state.type.co';
    const NOM_STATE_TYPE_CT = 'nom.state.type.ct';
    const NOM_STATE_TYPE_DE = 'nom.state.type.de';
    const NOM_STATE_TYPE_DC = 'nom.state.type.dc';
    const NOM_STATE_TYPE_FM = 'nom.state.type.fm';
    const NOM_STATE_TYPE_FL = 'nom.state.type.fl';
    const NOM_STATE_TYPE_GA = 'nom.state.type.ga';
    const NOM_STATE_TYPE_GU = 'nom.state.type.gu';
    const NOM_STATE_TYPE_HI = 'nom.state.type.hi';
    const NOM_STATE_TYPE_ID = 'nom.state.type.id';
    const NOM_STATE_TYPE_IL = 'nom.state.type.il';
    const NOM_STATE_TYPE_IN = 'nom.state.type.in';
    const NOM_STATE_TYPE_IA = 'nom.state.type.ia';
    const NOM_STATE_TYPE_KS = 'nom.state.type.ks';
    const NOM_STATE_TYPE_KY = 'nom.state.type.ky';
    const NOM_STATE_TYPE_LA = 'nom.state.type.la';
    const NOM_STATE_TYPE_ME = 'nom.state.type.me';
    const NOM_STATE_TYPE_MH = 'nom.state.type.mh';
    const NOM_STATE_TYPE_MD = 'nom.state.type.md';
    const NOM_STATE_TYPE_MA = 'nom.state.type.ma';
    const NOM_STATE_TYPE_MI = 'nom.state.type.mi';
    const NOM_STATE_TYPE_MN = 'nom.state.type.mn';
    const NOM_STATE_TYPE_MS = 'nom.state.type.ms';
    const NOM_STATE_TYPE_MO = 'nom.state.type.mo';
    const NOM_STATE_TYPE_MT = 'nom.state.type.mt';
    const NOM_STATE_TYPE_NE = 'nom.state.type.ne';
    const NOM_STATE_TYPE_NV = 'nom.state.type.nv';
    const NOM_STATE_TYPE_NH = 'nom.state.type.nh';
    const NOM_STATE_TYPE_NJ = 'nom.state.type.nj';
    const NOM_STATE_TYPE_NM = 'nom.state.type.nm';
    const NOM_STATE_TYPE_NY = 'nom.state.type.ny';
    const NOM_STATE_TYPE_NC = 'nom.state.type.nc';
    const NOM_STATE_TYPE_ND = 'nom.state.type.nd';
    const NOM_STATE_TYPE_MP = 'nom.state.type.mp';
    const NOM_STATE_TYPE_OH = 'nom.state.type.oh';
    const NOM_STATE_TYPE_OK = 'nom.state.type.ok';
    const NOM_STATE_TYPE_OR = 'nom.state.type.or';
    const NOM_STATE_TYPE_PW = 'nom.state.type.pw';
    const NOM_STATE_TYPE_PA = 'nom.state.type.pa';
    const NOM_STATE_TYPE_PR = 'nom.state.type.pr';
    const NOM_STATE_TYPE_RI = 'nom.state.type.ri';
    const NOM_STATE_TYPE_SC = 'nom.state.type.sc';
    const NOM_STATE_TYPE_SD = 'nom.state.type.sd';
    const NOM_STATE_TYPE_TN = 'nom.state.type.tn';
    const NOM_STATE_TYPE_TX = 'nom.state.type.tx';
    const NOM_STATE_TYPE_UT = 'nom.state.type.ut';
    const NOM_STATE_TYPE_VT = 'nom.state.type.vt';
    const NOM_STATE_TYPE_VI = 'nom.state.type.vi';
    const NOM_STATE_TYPE_VA = 'nom.state.type.va';
    const NOM_STATE_TYPE_WA = 'nom.state.type.wa';
    const NOM_STATE_TYPE_WV = 'nom.state.type.wv';
    const NOM_STATE_TYPE_WI = 'nom.state.type.wi';
    const NOM_STATE_TYPE_WY = 'nom.state.type.wy';






    //Only constantsOH
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';
    const TYPE_DANGER = 'danger';

    const TEMPLATE_EMAIL_BASE = '@Core/email/base_email.html.twig';

    const REL = array(
        'patient' => Patient::class,
        'service' => Service::class,
        'note' => Note::class,
    );

    private $em;

    /**
     * @param EntityManagerInterface $em
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Return the static entity manager
     * @return EntityManagerInterface
     */
    public static function getEM()
    {
        $kernel = $GLOBALS['kernel'] instanceof AppKernel? $GLOBALS['kernel']: $GLOBALS['kernel']->getKernel();
        $em = $kernel->getContainer()->get('doctrine')->getManager();
        return $em;
    }

    /**
     * @param string $nom_type
     * @return array
     */
    public static function getNomenclatorsByType($nom_type)
    {
        $em = self::getEM();
        $qb = $em->createQueryBuilder();

        $qb->select('n')
            ->from('Core\CoreBundle\Entity\Nomenclator', 'n')
            ->where('n.type = :type')
            ->setParameter('type', $nom_type);

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * @param string $nom_name
     * @return Nomenclator
     */
    public static function getNomenclatorsByName($nom_name)
    {
        $em = self::getEM();
        $qb = $em->createQueryBuilder();

        $qb->select('n')
            ->from('Core\CoreBundle\Entity\Nomenclator', 'n')
            ->where('n.name = :nom_name')
            ->setParameter('nom_name', $nom_name);

        $query = $qb->getQuery();
        $result = $query->getResult();
        
        return isset($result[0]) ? $result[0] : null;
    }
}