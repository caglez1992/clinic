<?php

namespace Core\CoreBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Vich\Uploadable
 */
abstract class BaseFile
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable = true)
     */
    protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="integer", nullable = true)
     */
    protected $size;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", nullable = true)
     */
    protected $mimeType;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Media
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    public function isImage(){
        return strpos($this->mimeType, 'image') !== false;
    }

    public function getThumbnail(){
        if($this->isImage())
            return $this->getPath() . $this->filename;
        $mime = str_replace('/', '-', $this->mimeType);
        return "/images/ext/$mime.png";
    }

    /**
     * @return string
     */
    public function __toString(){
        return (string) $this->name;
    }

    public function formatSize() {
        if ($this->size >= 1000000000) {
            return round($this->size / 1000000000, 2) . ' GB';
        }
        if ($this->size >= 1000000) {
            return round($this->size / 1000000, 2) . ' MB';
        }
        return round($this->size / 1000) . ' KB';
    }

    abstract public function checkNewMedia($context);

    /**
     * @return string
     */
    abstract public function getPath();
}
