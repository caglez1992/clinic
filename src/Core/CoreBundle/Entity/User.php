<?php

namespace Core\CoreBundle\Entity;

use Core\CoreBundle\Traitss\BaseEntityTrait;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\ServiceModifier;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="\Core\CoreBundle\Repository\UserRepository")
 * @Vich\Uploadable
 *
 * @UniqueEntity(
 *     fields={"workerNumber"},
 *     errorPath="workerNumber",
 * )
 *
 * @ExclusionPolicy("all")
 */
class User extends BaseUser implements Loggable
{
    use BaseEntityTrait;

    const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_FRONT_DESK = 'ROLE_FRONT_DESK';
    const ROLE_HR = 'ROLE_HR';
    const ROLE_AUTHORIZATION_CHECKER = 'ROLE_AUTHORIZATION_CHECKER';
    const ROLE_SERVICE_WORKER = 'ROLE_SERVICE_WORKER';
    const ROLE_TCM = 'ROLE_TCM';
    const ROLE_CMH = 'ROLE_CMH';
    const ROLE_SPECIALIST = 'ROLE_SPECIALIST';
    const ROLE_SUPERVISOR = 'ROLE_SUPERVISOR';
    const ROLE_SUPERVISOR_TCM = 'ROLE_SUPERVISOR_TCM';
    const ROLE_SUPERVISOR_CMH = 'ROLE_SUPERVISOR_CMH';
    const ROLE_QA_TCM = 'ROLE_QA_TCM';
    const ROLE_QA_CMH = 'ROLE_QA_CMH';
    const ROLE_BILLING = 'ROLE_BILLING';
    const ROLE_UTILIZATION_MANAGER = 'ROLE_UTILIZATION_MANAGER';
    const ROLE_OPERATION_MANAGER = 'ROLE_OPERATION_MANAGER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="worker_number", type="string", nullable=true)
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    private $workerNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     *
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     *
     * @Expose
     */
    private $lastname;

    /**
     * @var DateTime
     * @ORM\Column(name="contraction_date", type="datetime", nullable=true)
     */
    private $contractionDate;

    /**
     * @var DateTime
     * @ORM\Column(name="fired_date", type="datetime", nullable=true)
     */
    private $firedDate;

    /**
     * @var DateTime
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=255, nullable=true)
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="social_number", type="string", length=255, nullable=true)
     */
    private $socialNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="work_permission", type="string", length=255, nullable=true)
     */
    private $workPermission;

    /**
     * @var string
     *
     * @ORM\Column(name="residence", type="boolean", nullable=true)
     */
    private $residence;

    /**
     * @var string
     *
     * @ORM\Column(name="citizenship", type="boolean", nullable=true)
     */
    private $citizenship;

    /**
     * @var DateTime
     * @ORM\Column(name="work_permission_expired_date", type="datetime", nullable=true)
     */
    private $workPermissionExpiredDate;

    /**
     * @var string
     *
     * @ORM\Column(name="drive_licence", type="string", length=255, nullable=true)
     */
    private $driveLicence;

    /**
     * @var DateTime
     * @ORM\Column(name="drive_licence_expired_date", type="datetime", nullable=true)
     */
    private $driveLicenceExpiredDate;

    /**
     * @var string
     *
     * @ORM\Column(name="pro_licence", type="string", length=255, nullable=true)
     */
    private $proLicence;

    /**
     * @var DateTime
     * @ORM\Column(name="pro_licence_expired_date", type="datetime", nullable=true)
     */
    private $proLicenceExpiredDate;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_number", type="string", length=255, nullable=true)
     */
    private $providerNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="dcf_fars_id", type="string", length=255, nullable=true)
     */
    private $dcfFarsId;

    /**
     * @var DateTime
     * @ORM\Column(name="ahca_background_date", type="datetime", nullable=true)
     */
    private $ahcaBackgroundDate;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255, nullable = true)
     */
    private $media;

    /**
     * @Vich\UploadableField(mapping="user_images", fileNameProperty="media")
     * @var File
     */
    private $mediaFile;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\ServiceModifier")
     */
    private $modifiers;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="workers")
     * @ORM\JoinTable(name="worker_patient",
     *      joinColumns={@ORM\JoinColumn(name="worker_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="patient_id", referencedColumnName="id")}
     * )
     */
    private $patients;

    /**
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinTable(name="supervisor_workers")
     */
    private $assignedWorkers;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\Task", mappedBy="worker", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $tasks;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\DiagnosisPatient", mappedBy="createdBy")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $emittedDiagnosis;

    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->assignedWorkers = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }


    /**
     * @param File|UploadedFile $media
     */
    public function setMediaFile(File $media = null)
    {
        $this->mediaFile = $media;

        if (null !== $media) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getMediaFile()
    {
        return $this->mediaFile;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return Media
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->getName() . ' ' . $this->getLastname();
    }

    /**
     * Get picture
     *
     * @return \Core\CoreBundle\Entity\Media
     */
    public function getAvatar()
    {
        $path = '/images/users/';
        if(is_null($this->media))
            return $path.'default_user.png';
        return $path.$this->getMedia();
    }

    /**
     * @return string
     */
    public function getLabel(){
        return 'menu.entity.user';
    }

    /**
     * @return string
     */
    public function getWorkerNumber()
    {
        return $this->workerNumber;
    }

    /**
     * @param string $workerNumber
     */
    public function setWorkerNumber($workerNumber)
    {
        $this->workerNumber = $workerNumber;
    }

    /**
     * Set contractionDate
     *
     * @param \DateTime $contractionDate
     *
     * @return User
     */
    public function setContractionDate($contractionDate)
    {
        $this->contractionDate = $contractionDate;

        return $this;
    }

    /**
     * Get contractionDate
     *
     * @return \DateTime
     */
    public function getContractionDate()
    {
        return $this->contractionDate;
    }

    /**
     * Set firedDate
     *
     * @param \DateTime $firedDate
     *
     * @return User
     */
    public function setFiredDate($firedDate)
    {
        $this->firedDate = $firedDate;

        return $this;
    }

    /**
     * Get firedDate
     *
     * @return \DateTime
     */
    public function getFiredDate()
    {
        return $this->firedDate;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set socialNumber
     *
     * @param string $socialNumber
     *
     * @return User
     */
    public function setSocialNumber($socialNumber)
    {
        $this->socialNumber = $socialNumber;

        return $this;
    }

    /**
     * Get socialNumber
     *
     * @return string
     */
    public function getSocialNumber()
    {
        return $this->socialNumber;
    }

    /**
     * Set workPermission
     *
     * @param string $workPermission
     *
     * @return User
     */
    public function setWorkPermission($workPermission)
    {
        $this->workPermission = $workPermission;

        return $this;
    }

    /**
     * Get workPermission
     *
     * @return string
     */
    public function getWorkPermission()
    {
        return $this->workPermission;
    }

    /**
     * Set residence
     *
     * @param boolean $residence
     *
     * @return User
     */
    public function setResidence($residence)
    {
        $this->residence = $residence;

        return $this;
    }

    /**
     * Get residence
     *
     * @return boolean
     */
    public function getResidence()
    {
        return $this->residence;
    }

    /**
     * Set citizenship
     *
     * @param boolean $citizenship
     *
     * @return User
     */
    public function setCitizenship($citizenship)
    {
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Get citizenship
     *
     * @return boolean
     */
    public function getCitizenship()
    {
        return $this->citizenship;
    }

    /**
     * Set workPermissionExpiredDate
     *
     * @param \DateTime $workPermissionExpiredDate
     *
     * @return User
     */
    public function setWorkPermissionExpiredDate($workPermissionExpiredDate)
    {
        $this->workPermissionExpiredDate = $workPermissionExpiredDate;

        return $this;
    }

    /**
     * Get workPermissionExpiredDate
     *
     * @return \DateTime
     */
    public function getWorkPermissionExpiredDate()
    {
        return $this->workPermissionExpiredDate;
    }

    /**
     * Set driveLicence
     *
     * @param string $driveLicence
     *
     * @return User
     */
    public function setDriveLicence($driveLicence)
    {
        $this->driveLicence = $driveLicence;

        return $this;
    }

    /**
     * Get driveLicence
     *
     * @return string
     */
    public function getDriveLicence()
    {
        return $this->driveLicence;
    }

    /**
     * Set driveLicenceExpiredDate
     *
     * @param \DateTime $driveLicenceExpiredDate
     *
     * @return User
     */
    public function setDriveLicenceExpiredDate($driveLicenceExpiredDate)
    {
        $this->driveLicenceExpiredDate = $driveLicenceExpiredDate;

        return $this;
    }

    /**
     * Get driveLicenceExpiredDate
     *
     * @return \DateTime
     */
    public function getDriveLicenceExpiredDate()
    {
        return $this->driveLicenceExpiredDate;
    }

    /**
     * Set proLicence
     *
     * @param string $proLicence
     *
     * @return User
     */
    public function setProLicence($proLicence)
    {
        $this->proLicence = $proLicence;

        return $this;
    }

    /**
     * Get proLicence
     *
     * @return string
     */
    public function getProLicence()
    {
        return $this->proLicence;
    }

    /**
     * Set proLicenceExpiredDate
     *
     * @param \DateTime $proLicenceExpiredDate
     *
     * @return User
     */
    public function setProLicenceExpiredDate($proLicenceExpiredDate)
    {
        $this->proLicenceExpiredDate = $proLicenceExpiredDate;

        return $this;
    }

    /**
     * Get proLicenceExpiredDate
     *
     * @return \DateTime
     */
    public function getProLicenceExpiredDate()
    {
        return $this->proLicenceExpiredDate;
    }

    /**
     * Set providerNumber
     *
     * @param string $providerNumber
     *
     * @return User
     */
    public function setProviderNumber($providerNumber)
    {
        $this->providerNumber = $providerNumber;

        return $this;
    }

    /**
     * Get providerNumber
     *
     * @return string
     */
    public function getProviderNumber()
    {
        return $this->providerNumber;
    }

    /**
     * Set dcfFarsId
     *
     * @param string $dcfFarsId
     *
     * @return User
     */
    public function setDcfFarsId($dcfFarsId)
    {
        $this->dcfFarsId = $dcfFarsId;

        return $this;
    }

    /**
     * Get dcfFarsId
     *
     * @return string
     */
    public function getDcfFarsId()
    {
        return $this->dcfFarsId;
    }

    /**
     * Set ahcaBackgroundDate
     *
     * @param \DateTime $ahcaBackgroundDate
     *
     * @return User
     */
    public function setAhcaBackgroundDate($ahcaBackgroundDate)
    {
        $this->ahcaBackgroundDate = $ahcaBackgroundDate;

        return $this;
    }

    /**
     * Get ahcaBackgroundDate
     *
     * @return \DateTime
     */
    public function getAhcaBackgroundDate()
    {
        return $this->ahcaBackgroundDate;
    }

    /**
     * Add modifier
     *
     * @param ServiceModifier $modifier
     *
     * @return User
     */
    public function addModifier(ServiceModifier $modifier)
    {
        $this->modifiers[] = $modifier;

        return $this;
    }

    /**
     * Remove modifier
     *
     * @param ServiceModifier $modifier
     */
    public function removeModifier(ServiceModifier $modifier)
    {
        $this->modifiers->removeElement($modifier);
    }

    /**
     * Get modifiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }


    /**
     * Add patient
     *
     * @param Patient $patient
     *
     * @return User
     */
    public function addPatient(Patient $patient)
    {
        if(!$this->getPatients()->contains($patient))
            $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param Patient $patient
     */
    public function removePatient(Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }


    /**
     * Add assignedWorker
     *
     * @param User $assignedWorker
     *
     * @return User
     */
    public function addAssignedWorker(User $assignedWorker)
    {
        $this->assignedWorkers[] = $assignedWorker;

        return $this;
    }

    /**
     * Remove assignedWorker
     *
     * @param User $assignedWorker
     */
    public function removeAssignedWorker(User $assignedWorker)
    {
        $this->assignedWorkers->removeElement($assignedWorker);
    }

    /**
     * Get assignedWorkers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedWorkers()
    {
        return $this->assignedWorkers;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $str = $this->getFullName() . ' (' . $this->getTranslateRol() . ')';
        return (string) $str;
    }

    /**
     * @return string
     */
    public function getTranslateRol()
    {
        $rol = $this->getRoles();
        $rol = $rol[0];

        $rol = str_replace('ROLE_', '', $rol);
        $rol = str_replace('_', ' ', $rol);
        return (string) $rol;
    }

    /**
     * @return bool
     */
    public function isServiceWorker(){
        return $this->hasRole(User::ROLE_TCM) || $this->hasRole(User::ROLE_CMH) || $this->hasRole(User::ROLE_SPECIALIST);
    }

    /**
     * @return bool
     */
    public function isSupervisor(){
        return $this->hasRole(User::ROLE_SUPERVISOR_CMH) || $this->hasRole(User::ROLE_SUPERVISOR_TCM);
    }

    /**
     * @return bool
     */
    public function isQa(){
        return $this->hasRole(User::ROLE_QA_CMH) || $this->hasRole(User::ROLE_QA_TCM);
    }

    /**
     * Add task
     *
     * @param \Core\PatientBundle\Entity\Task $task
     *
     * @return User
     */
    public function addTask(\Core\PatientBundle\Entity\Task $task)
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param \Core\PatientBundle\Entity\Task $task
     */
    public function removeTask(\Core\PatientBundle\Entity\Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Add emittedDiagnosi
     *
     * @param \Core\PatientBundle\Entity\DiagnosisPatient $emittedDiagnosi
     *
     * @return User
     */
    public function addEmittedDiagnosi(\Core\PatientBundle\Entity\DiagnosisPatient $emittedDiagnosi)
    {
        $this->emittedDiagnosis[] = $emittedDiagnosi;

        return $this;
    }

    /**
     * Remove emittedDiagnosi
     *
     * @param \Core\PatientBundle\Entity\DiagnosisPatient $emittedDiagnosi
     */
    public function removeEmittedDiagnosi(\Core\PatientBundle\Entity\DiagnosisPatient $emittedDiagnosi)
    {
        $this->emittedDiagnosis->removeElement($emittedDiagnosi);
    }

    /**
     * Get emittedDiagnosis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmittedDiagnosis()
    {
        return $this->emittedDiagnosis;
    }
}
