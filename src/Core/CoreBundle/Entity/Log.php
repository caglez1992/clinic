<?php

namespace Core\CoreBundle\Entity;

use Core\CoreBundle\Traitss\BaseEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Core\CoreBundle\Repository\LogRepository")
 */
class Log
{
    use BaseEntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="class_name", type="string", nullable=true)
     */
    private $className;

    /**
     * @var string
     *
     * @ORM\Column(name="class_label", type="string", nullable=true)
     */
    private $classLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="previous_value", type="text", nullable=true)
     */
    private $previousValue;

    /**
     * @var string
     *
     * @ORM\Column(name="actual_value", type="text", nullable=true)
     */
    private $actualValue;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_file", type="boolean", nullable=true)
     */
    private $pdfFile;

    /**
     * Set className
     *
     * @param string $className
     *
     * @return Log
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     *
     * @return Log
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return Log
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set previousValue
     *
     * @param string $previousValue
     *
     * @return Log
     */
    public function setPreviousValue($previousValue)
    {
        $this->previousValue = $previousValue;

        return $this;
    }

    /**
     * Get previousValue
     *
     * @return string
     */
    public function getPreviousValue()
    {
        return $this->previousValue;
    }

    /**
     * Set actualValue
     *
     * @param string $actualValue
     *
     * @return Log
     */
    public function setActualValue($actualValue)
    {
        $this->actualValue = $actualValue;

        return $this;
    }

    /**
     * Get actualValue
     *
     * @return string
     */
    public function getActualValue()
    {
        return $this->actualValue;
    }

    /**
     * Set user
     *
     * @param \Core\CoreBundle\Entity\User $user
     *
     * @return Log
     */
    public function setUser(\Core\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Core\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set action
     *
     * @param \Core\CoreBundle\Entity\Nomenclator $action
     *
     * @return Notification
     */
    public function setAction(\Core\CoreBundle\Entity\Nomenclator $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \Core\CoreBundle\Entity\Nomenclator
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set classLabel
     *
     * @param string $classLabel
     *
     * @return Log
     */
    public function setClassLabel($classLabel)
    {
        $this->classLabel = $classLabel;

        return $this;
    }

    /**
     * Get classLabel
     *
     * @return string
     */
    public function getClassLabel()
    {
        return $this->classLabel;
    }

    /**
     * @return string
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    /**
     * @param string $pdfFile
     */
    public function setPdfFile($pdfFile)
    {
        $this->pdfFile = $pdfFile;
    }
}
