<?php

namespace Core\CoreBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Core\CoreBundle\Repository\MediaRepository")
 * @Vich\Uploadable
 *
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 * )
 */
class Media implements Loggable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255, nullable = true)
     */
    private $media;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="media")
     * @var File
     */
    private $mediaFile;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User", inversedBy="test3", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setMediaFile(File $media = null)
    {
        $this->mediaFile = $media;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($media) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getMediaFile()
    {
        return $this->mediaFile;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Media
     */
    public function setName($name)
    {
        $this->name = str_replace(" ", "_", $name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return Media
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    public function getMediaOrigin(){
       return 'origin-'.$this->name.'.jpeg';
    }

    public function getMediaHugh(){
        return 'hugh-'.$this->name.'.jpeg';
    }

    public function getMediaMega(){
        return 'mega-'.$this->name.'.jpeg';
    }

    public function __toString(){
        return (string) $this->name;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Media
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @Assert\Callback
     */
    public function checkNewMedia(ExecutionContextInterface $context)
    {
        if(is_null($this->media) && is_null($this->mediaFile)){
            $context->buildViolation('media.validator.message.not.blank')->atPath('mediaFile')->addViolation();
        }

    }

    /**
     * Set user
     *
     * @param \Core\CoreBundle\Entity\User $user
     *
     * @return Media
     */
    public function setUser(\Core\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Core\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getLabel()
    {
        return "My media";
    }
}
