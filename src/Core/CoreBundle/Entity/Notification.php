<?php

namespace Core\CoreBundle\Entity;

use Core\CoreBundle\Util\NomUtil;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Exclude;
use Sonata\TranslationBundle\Model\Gedmo\AbstractTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Core\CoreBundle\Repository\NotificationRepository")
 */
class Notification extends AbstractTranslatable implements TranslatableInterface
{
    const SUCCESS = 'success';
    const ERROR = 'error';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * @var string
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="msg", type="string", length=255)
     */
    private $msg;

    /**
     * @var boolean
     *
     * @ORM\Column(name="readd", type="boolean")
     */
    private $read;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rise", type="boolean", nullable=true)
     */
    private $rise;

    /**
     * @var boolean
     *
     * @ORM\Column(name="important", type="boolean", nullable=true)
     */
    private $important;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * @Exclude
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * Notification constructor.
     */
    public function __construct()
    {
        $this->read = false;
        $this->rise = false;
        $this->important = false;
        $this->type = NomUtil::TYPE_SUCCESS;
        $this->url = null;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set read
     *
     * @param boolean $read
     *
     * @return Notification
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * @return boolean
     */
    public function isRise()
    {
        return $this->rise;
    }

    /**
     * @param boolean $rise
     */
    public function setRise($rise)
    {
        $this->rise = $rise;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set owner
     *
     * @param \Core\CoreBundle\Entity\User $owner
     *
     * @return Notification
     */
    public function setOwner(\Core\CoreBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Core\CoreBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set url
     *
     * @param string $url
     *
     * @return Notification
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set msg
     *
     * @param string $msg
     *
     * @return Notification
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }


    public function getIcon(){
        if($this->read)
            return 'fa-bell-o';
        return 'fa-bell';
    }

    public function getColor(){
       switch ($this->type){
           case NomUtil::TYPE_SUCCESS:
               return 'green';
           case NomUtil::TYPE_INFO:
               return 'blue';
           case NomUtil::TYPE_WARNING:
               return 'yellow';
           case NomUtil::TYPE_DANGER:
               return 'red';
           default:
               return null;
       }
    }

    /**
     * Set important
     *
     * @param boolean $important
     *
     * @return Notification
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important
     *
     * @return bool
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Get rise
     *
     * @return boolean
     */
    public function getRise()
    {
        return $this->rise;
    }
}
