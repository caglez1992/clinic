<?php

namespace Core\CoreBundle\Entity;

use Core\CoreBundle\Traitss\BaseEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="\Core\CoreBundle\Repository\NomenclatorRepository")
 */
class Nomenclator
{
    use BaseEntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="class_css", type="string", length=255, nullable=true)
     */
    private $classCss;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_name", type="string", length=255, nullable=true)
     */
    private $pdfName;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    function __toString()
    {
        return $this->name;
    }



    /**
     * Set classCss
     *
     * @param string $classCss
     *
     * @return Nomenclator
     */
    public function setClassCss($classCss)
    {
        $this->classCss = $classCss;

        return $this;
    }

    /**
     * Get classCss
     *
     * @return string
     */
    public function getClassCss()
    {
        return $this->classCss;
    }

    /**
     * Set pdfName
     *
     * @param string $pdfName
     *
     * @return Nomenclator
     */
    public function setPdfName($pdfName)
    {
        $this->pdfName = $pdfName;

        return $this;
    }

    /**
     * Get pdfName
     *
     * @return string
     */
    public function getPdfName()
    {
        return $this->pdfName;
    }
}
