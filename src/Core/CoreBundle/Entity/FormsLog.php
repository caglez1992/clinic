<?php

namespace Core\CoreBundle\Entity;

use Core\CoreBundle\Traitss\BaseEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Core\CoreBundle\Repository\FormsLogRepository")
 */
class FormsLog
{
    use BaseEntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="form_name", type="string", length=255, nullable=true)
     */
    private $formName;

    /**
     * @var string
     *
     * @ORM\Column(name="container", type="string", length=255, nullable=true)
     */
    private $container;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    private $worker;

    

    /**
     * Set formName
     *
     * @param string $formName
     *
     * @return FormsLog
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;

        return $this;
    }

    /**
     * Get formName
     *
     * @return string
     */
    public function getFormName()
    {
        return $this->formName;
    }

    /**
     * Set container
     *
     * @param string $container
     *
     * @return FormsLog
     */
    public function setContainer($container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Get container
     *
     * @return string
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     *
     * @return FormsLog
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return FormsLog
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return FormsLog
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set worker
     *
     * @param \Core\CoreBundle\Entity\User $worker
     *
     * @return FormsLog
     */
    public function setWorker(\Core\CoreBundle\Entity\User $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return \Core\CoreBundle\Entity\User
     */
    public function getWorker()
    {
        return $this->worker;
    }
}
