<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 8/5/18
 * Time: 12:01 PM
 */

namespace Core\CoreBundle\Entity;


interface Loggable
{
    public function getLabel();
}