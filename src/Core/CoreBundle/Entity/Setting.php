<?php

namespace Core\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Core\CoreBundle\Repository\ConfigRepository")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="actual_case_number", type="integer", nullable=true)
     */
    private $actualCaseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="google_maps_api_key", type="string", nullable=true)
     */
    private $googleMapsApiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="ssh_username", type="string", nullable=true)
     */
    private $sshUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="ssh_password", type="string", nullable=true)
     */
    private $sshPassword;

    /**
     * @var integer
     *
     * @ORM\Column(name="pid_web_socket", type="integer", nullable=true)
     */
    private $pidWebSocket;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actualCaseNumber
     *
     * @param integer $actualCaseNumber
     *
     * @return Setting
     */
    public function setActualCaseNumber($actualCaseNumber)
    {
        $this->actualCaseNumber = $actualCaseNumber;

        return $this;
    }

    /**
     * Get actualCaseNumber
     *
     * @return integer
     */
    public function getActualCaseNumber()
    {
        return $this->actualCaseNumber;
    }

    public function updateCaseNumber(){
        $this->actualCaseNumber++;
    }

    /**
     * Set googleMapsApiKey
     *
     * @param string $googleMapsApiKey
     *
     * @return Setting
     */
    public function setGoogleMapsApiKey($googleMapsApiKey)
    {
        $this->googleMapsApiKey = $googleMapsApiKey;

        return $this;
    }

    /**
     * Get googleMapsApiKey
     *
     * @return string
     */
    public function getGoogleMapsApiKey()
    {
        return $this->googleMapsApiKey;
    }

    /**
     * Set sshUsername
     *
     * @param string $sshUsername
     *
     * @return Setting
     */
    public function setSshUsername($sshUsername)
    {
        $this->sshUsername = $sshUsername;

        return $this;
    }

    /**
     * Get sshUsername
     *
     * @return string
     */
    public function getSshUsername()
    {
        return $this->sshUsername;
    }

    /**
     * Set sshPassword
     *
     * @param string $sshPassword
     *
     * @return Setting
     */
    public function setSshPassword($sshPassword)
    {
        $this->sshPassword = $sshPassword;

        return $this;
    }

    /**
     * Get sshPassword
     *
     * @return string
     */
    public function getSshPassword()
    {
        return $this->sshPassword;
    }

    /**
     * Set pidWebSocket
     *
     * @param integer $pidWebSocket
     *
     * @return Setting
     */
    public function setPidWebSocket($pidWebSocket)
    {
        $this->pidWebSocket = $pidWebSocket;

        return $this;
    }

    /**
     * Get pidWebSocket
     *
     * @return integer
     */
    public function getPidWebSocket()
    {
        return $this->pidWebSocket;
    }
}
