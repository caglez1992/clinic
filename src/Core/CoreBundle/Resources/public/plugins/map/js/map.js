// Create custom map style here: https://mapstyle.withgoogle.com/
//Map initialize function
var map;
var isGoogleMapLoad = false;
var directionsService;
var directionsRendererGlobal;

function initializeMap() {
    
    var mapStyles = [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#444444"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#2196f3"
                }
            ]
        }
    ];
    
    // map center
    var center = new google.maps.LatLng(23.0754418, -82.4667503);
    
    var mapOptions = {
        center: center,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: mapStyles,
        scrollwheel: false
    };
    
    //create a google map instance into the Dom element
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    isGoogleMapLoad = true;

    directionsService = new google.maps.DirectionsService();
    directionsRendererGlobal = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    });
}

// the ajax callback function. Do all the stuff you want to do with the remote data in between this function.
function renderMap(data, oneRoute) {

    if(isGoogleMapLoad) {

        var centerPoint = data[0];
        var center = new google.maps.LatLng(centerPoint.latitude, centerPoint.longitude);
        map.panTo(center);

        //loop through each of the single JSON object obtained from the JSON file.
        var markers = [];
        var infobox = new InfoBox({
            content: 'empty',
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-250, -330),
            zIndex: null,
            closeBoxURL: "",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            isOpen: false,
            pane: "floatPane",
            enableEventPropagation: false
        });
        infobox.addListener('domready', function () {
            $('.infobox-close').on('click', function () {
                infobox.close(map, this);
                infobox.isOpen = false;
            });
        });

        var flightPlanCoordinates = [];
        var test = [];
        $.each(data, function (i, value) {
            var markerCenter = new google.maps.LatLng(value.latitude, value.longitude);

            flightPlanCoordinates.push({
                lat: value.latitude,
                lng: value.longitude
            });

            test.push(markerCenter);

            var verified = '';

            if (value.verified) {
                verified = '<div class="marker-verified"><i class="fa fa-check"></i></div>';
            }

            function getMarkerContent(value) {
                var content =
                    '<div id="marker-' + value.id + '" class="flip-container">' + verified +
                        '<div class="flipper">' +
                            '<div class="front">' +
                                '<p class="center padding-top-5"><strong>' + '<i class="fa fa-eye"></i>' + '</strong></p>' +
                            '</div>' +

                            '<div class="back">' +
                                '<i class="fa fa-eye"></i>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                return content;
            }

            var markerContent = getMarkerContent(value);
            var marker = new RichMarker({
                id: value.id,
                data: value,
                flat: true,
                position: markerCenter,
                map: map,
                shadow: 0,
                content: markerContent,
                title: "Fábrica de Porcelana da Vista Alegre",
                is_logged_in: value.is_logged_in
            });
            markers.push(marker);

            // This event expects a click on a marker
            // When this event is fired the Info Window is opened.
            google.maps.event.addListener(marker, 'click', function () {

                var content = '<div class="infobox-close"><i class="fa fa-close"></i></div>' +
                    '<a class="list-link" href="listing-details-left.html">' +
                    '<div id="iw-container">' +
                    '<div class="iw-content">' +
                    /*'<ul class="list-inline rating">' +
                     '<li><i class="fa fa-star" aria-hidden="true"></i></li>' +
                     '<li><i class="fa fa-star" aria-hidden="true"></i></li>' +
                     '<li><i class="fa fa-star" aria-hidden="true"></i></li>' +
                     '<li><i class="fa fa-star" aria-hidden="true"></i></li>' +
                     '<li><i class="fa fa-star" aria-hidden="true"></i></li>' +
                     '</ul>' +*/
                    '<div class="iw-subTitle">' + marker.data.title + '</div>' +
                    '<p>' + marker.data.address + '</p>' +
                    '<p>' + marker.data.text2 + '</p>' +
                    '<p>' + marker.data.latitude + '</p>' +
                    '<p>' + marker.data.longitude + '</p>' +
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                    '</div>' +
                    '</a>';

                if (!infobox.isOpen) {
                    infobox.setContent(content);
                    infobox.open(map, this);
                    infobox.isOpen = true;
                    infobox.markerId = marker.id;
                } else {
                    if (infobox.markerId == marker.id) {
                        infobox.close(map, this);
                        infobox.isOpen = false;
                    } else {
                        infobox.close(map, this);
                        infobox.isOpen = false;

                        infobox.setContent(content);
                        infobox.open(map, this);
                        infobox.isOpen = true;
                        infobox.markerId = marker.id;
                    }
                }
            });
        });

        //Render Polilines
        /*var flightPath = new google.maps.Polyline({
         path: flightPlanCoordinates,
         geodesic: true,
         //strokeColor: '#FF0000',
         strokeColor: '#46bcec',
         strokeOpacity: 1.0,
         strokeWeight: 2
         });

         flightPath.setMap(map);*/

        //Render routes streets.
        function requestDirections(start, end) {
            directionsService.route({
                origin: start,
                destination: end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            }, function(result) {
                renderDirections(result);
            });
        }

        function renderDirections(result) {
            var directionsRenderer = directionsRendererGlobal;
            if(!oneRoute) {
                directionsRenderer = new google.maps.DirectionsRenderer({
                    suppressMarkers: true
                });
            }

            directionsRenderer.setMap(map);
            directionsRenderer.setDirections(result);
        }

        if(test.length > 1) {
            for (var i = 0; i < test.length - 1; i++) {
                requestDirections(test[i], test[i + 1]);
            }
        }
    } else {
        console.log("Can not render map google maps api is not loaded");
    }
}
