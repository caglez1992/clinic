<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\Entity\Setting;
use Core\CoreBundle\Manager\BaseManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Install command for install arch.
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class RunGosCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:run:gos')
            ->setDescription('run cron command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Help");
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $d = $this->getContainer()->get('doctrine');
        $config = $d->getRepository(Setting::class)->find(1);
        /** @var BaseManager $bm */
        $bm = $this->getContainer()->get(BaseManager::class);
        
        $dirVar = $this->getContainer()->getParameter('dir.var');
        
        $stream = new StreamHandler($dirVar.'/logs/gosCommand.log', Logger::INFO);
        $logger = new Logger('gosCommand');
        $logger->pushHandler($stream);
        
        try {
            $output->writeln('<comment>Run gos web.</comment>');

            $dirApp = $this->getContainer()->getParameter('dir.app');
            $dirBin = $this->getContainer()->getParameter('dir.bin');
            
            $gosLog = $dirVar . '/logs/gos.log';
            
            $pid = $config->getPidWebSocket();

            $logger->info("PID WebSocket: $pid");

            if ($pid != '0') {
                $checkCommand = 'ps -p ' . $pid;
                exec($checkCommand, $response);

                if (isset($response[1]))
                    $logger->info("Response: " . $response[1]);

                if (count($response) == 2 && strpos($response[1], 'php') !== false) {
                    $logger->info("Nothing to do, already running");
                    $logger->info("End Command");
                    $output->writeln('<comment>All good.</comment>');
                    return;
                }
            }

            $command = 'php ' . $dirBin . '/console gos:websocket:server --env=prod > ' . $gosLog . ' 2>&1 & echo $!';
            exec($command, $op);

            $pid = $op[0];

            $logger->info("Execute gos web socket with new pid: $pid");

            $config->setPidWebSocket($pid);
            $bm->save($config);

            $output->writeln('<comment>All good.</comment>');
            $logger->info("End Command");
            
        }catch (Exception $e){
            $logger->critical("WebSocket Command Error: " . $e->getMessage());
        }
    }
}
