<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Patient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().
 *
 */
class FixDbContentCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:fix:db')
            ->setDescription('Fix Content in Database')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Fix Content");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Command Started...</comment>');

        $application = $this->getApplication();

        $d = $this->getContainer()->get('doctrine');
        $bm = $this->getContainer()->get(BaseManager::class);

        //$d->getRepository(Patient::class)->tempQuery();
        /*$stateFl = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATE_TYPE_FL);
        $patients = $d->getRepository(Patient::class)->findAll();
        foreach ($patients as $patient){
            $patient->setState($stateFl);
            $bm->sa
        }*/

        $output->writeln('<comment>Command Ended...</comment>');
    }
}

