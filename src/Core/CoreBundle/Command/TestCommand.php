<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Manager\PatientManager;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Test command for staff.
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class TestCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:test')
            ->setDescription('test command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Test");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Test Command...</comment>');

        $application = $this->getApplication();

        $d = $this->getContainer()->get('doctrine');
        /** @var BaseManager $bm */
        $bm = $this->getContainer()->get(BaseManager::class);

        //Update Authorizations automatically.

        $d = $this->getContainer()->get('doctrine');
        $openPatients = $d->getRepository(Patient::class)->getOpenPatients();
        foreach ($openPatients as $patient) {
            $this->getContainer()->get(PatientManager::class)->updateAuthorizationStatus($patient);
        }
        
    }
}

