<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\ServiceModifier;
use Core\PatientBundle\Manager\PatientManager;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Test command for staff.
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class UpdateUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:update:users')
            ->setDescription('test command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Test");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Update Users...</comment>');

        $application = $this->getApplication();

        $d = $this->getContainer()->get('doctrine');
        $smRepo = $d->getRepository(ServiceModifier::class);

        $hpModifier = $smRepo->find(1);
        $hrModifier = $smRepo->find(7);
        $hnModifier = $smRepo->find(5);
        $tsModifier = $smRepo->find(4);
        $hoModifier = $smRepo->find(2);
        $hqModifier = $smRepo->find(8);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('93');
        $user->setName("Zaira");
        $user->setLastname("Mendez");
        $user->setUsername("zaira.mendez");
        $user->setPlainPassword("zaira.mendez93@FHC2019");
        $user->setEmail("mendez9134@aol.com");
        $user->addRole(User::ROLE_AUTHORIZATION_CHECKER);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('118');
        $user->setName("Antonio");
        $user->setLastname("Morales");
        $user->setUsername("antonio.morales");
        $user->setPlainPassword("antonio.morales118@FHC2019");
        $user->setEmail("moralesaguilantonio@hotmail.com");
        $user->addRole(User::ROLE_BILLING);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('136');
        $user->setName("Alvaro L.");
        $user->setLastname("Zamora");
        $user->setUsername("alvaro.zamora");
        $user->setPlainPassword("alvaro.zamora136@FHC2019");
        $user->setEmail("aleozamora@yahoo.com");
        $user->addRole(User::ROLE_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hrModifier);
        $user->addModifier($hnModifier);
        $user->addModifier($tsModifier);
        $user->addModifier($hoModifier);
        $user->addModifier($hqModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('184');
        $user->setName("Annabel");
        $user->setLastname("Cardet");
        $user->setUsername("annabel.cardet");
        $user->setPlainPassword("annabel.cardet184@FHC2019");
        $user->setEmail("cardetannabel@gmail.com");
        $user->addRole(User::ROLE_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hrModifier);
        $user->addModifier($hnModifier);
        $user->addModifier($tsModifier);
        $user->addModifier($hoModifier);
        $user->addModifier($hqModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('186');
        $user->setName("Ariadna");
        $user->setLastname("Montero");
        $user->setUsername("ariadna.montero186@FHC2019");
        $user->setPlainPassword("ariadna.montero");
        $user->setEmail("ariadnamon@yahoo.com");
        $user->addRole(User::ROLE_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hrModifier);
        $user->addModifier($hnModifier);
        $user->addModifier($tsModifier);
        $user->addModifier($hoModifier);
        $user->addModifier($hqModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('129');
        $user->setName("Maria T.");
        $user->setLastname("Rodriguez");
        $user->setUsername("maria.rodriguez");
        $user->setPlainPassword("maria.rodriguez129@FHC2019");
        $user->setEmail("Maiterw80@gmail.com");
        $user->addRole(User::ROLE_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hrModifier);
        $user->addModifier($hnModifier);
        $user->addModifier($tsModifier);
        $user->addModifier($hoModifier);
        $user->addModifier($hqModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('122');
        $user->setName("Rudy");
        $user->setLastname("Cepero");
        $user->setUsername("rudy.cepero");
        $user->setPlainPassword("rudy.cepero122@FHC2019");
        $user->setEmail("rfcepero.machado@gmail.com");
        $user->addRole(User::ROLE_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hrModifier);
        $user->addModifier($hnModifier);
        $user->addModifier($tsModifier);
        $user->addModifier($hoModifier);
        $user->addModifier($hqModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('149');
        $user->setName("Mariely");
        $user->setLastname("Herrera");
        $user->setUsername("mariely.herrera");
        $user->setPlainPassword("mariely.herrera149@FHC2019");
        $user->setEmail("mariely2921@gmail.com");
        $user->addRole(User::ROLE_FRONT_DESK);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('194');
        $user->setName("Mario");
        $user->setLastname("Rodriguez");
        $user->setUsername("mario.rodriguez");
        $user->setPlainPassword("mario.rodriguez194@FHC2019");
        $user->setEmail("mayitorg1997@gmail.com");
        $user->addRole(User::ROLE_FRONT_DESK);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('120');
        $user->setName("Zandra");
        $user->setLastname("Artiles");
        $user->setUsername("zandra.artiles");
        $user->setPlainPassword("zandra.artiles120@FHC2019");
        $user->setEmail("zartiles@yahoo.com");
        $user->addRole(User::ROLE_FRONT_DESK);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('51');
        $user->setName("Yaser");
        $user->setLastname("Rodriguez");
        $user->setUsername("yaser.rodriguez");
        $user->setPlainPassword("yaser.rodriguez51@FHC2019");
        $user->setEmail("yaserrs07@yahoo.com");
        $user->addRole(User::ROLE_SUPERVISOR_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('160');
        $user->setName("Dayme");
        $user->setLastname("Rodriguez");
        $user->setUsername("dayme.rodriguez");
        $user->setPlainPassword("dayme.rodriguez160@FHC2019");
        $user->setEmail("dfaime@yahoo.com");
        $user->addRole(User::ROLE_QA_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('154');
        $user->setName("Vilma");
        $user->setLastname("Valdes");
        $user->setUsername("vilma.valdes");
        $user->setPlainPassword("vilma.valdes154@FHC2019");
        $user->setEmail("vilmacvaldes@gmail.com");
        $user->addRole(User::ROLE_QA_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('204');
        $user->setName("Laritza");
        $user->setLastname("Pantoja");
        $user->setUsername("laritza.pantoja");
        $user->setPlainPassword("laritza.pantoja204@FHC2019");
        $user->setEmail("laritzapantoja@gmail.com");
        $user->addRole(User::ROLE_QA_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('162');
        $user->setName("Yaimy");
        $user->setLastname("Perez");
        $user->setUsername("yaimy.perez");
        $user->setPlainPassword("yaimy.perez162@FHC2019");
        $user->setEmail("yaimita2008@yahoo.es");
        $user->addRole(User::ROLE_QA_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('150');
        $user->setName("Jorge E.");
        $user->setLastname("Fernandez");
        $user->setUsername("jorge.fernandez");
        $user->setPlainPassword("jorge.fernandez150@FHC2019");
        $user->setEmail("noemail11@yahoo.es");
        $user->addRole(User::ROLE_SPECIALIST);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hpModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('189');
        $user->setName("Sylma");
        $user->setLastname("Millares");
        $user->setUsername("sylma.millares");
        $user->setPlainPassword("sylma.millares189@FHC2019");
        $user->setEmail("noemail1@yahoo.es");
        $user->addRole(User::ROLE_SPECIALIST);
        $user->addRole(User::ROLE_DEFAULT);
        $user->addModifier($hoModifier);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('81');
        $user->setName("Dalgys");
        $user->setLastname("Perez");
        $user->setUsername("dalgys.perez");
        $user->setPlainPassword("dalgys.perez81@FHC2019");
        $user->setEmail("ops.mgmt1@hcsflorida.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('75');
        $user->setName("Carlos");
        $user->setLastname("Rodriguez");
        $user->setUsername("carlos.rodriguez");
        $user->setPlainPassword("carlos.rodriguez75@FHC2019");
        $user->setEmail("ops.mgmt@hcsflorida.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('143');
        $user->setName("Leduan");
        $user->setLastname("Lopez");
        $user->setUsername("leduan.lopez");
        $user->setPlainPassword("leduan.lopez143@FHC2019");
        $user->setEmail("leduanlc@gmail.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('2');
        $user->setName("Michael");
        $user->setLastname("Ruiz");
        $user->setUsername("michael.ruiz");
        $user->setPlainPassword("michael.ruiz2@FHC2019");
        $user->setEmail("chaclmi76@yahoo.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('7');
        $user->setName("Jessica");
        $user->setLastname("Pujals");
        $user->setUsername("jessica.pujals");
        $user->setPlainPassword("jessica.pujals7@FHC2019");
        $user->setEmail("jessicapujals@yahoo.com");
        $user->addRole(User::ROLE_SUPERVISOR_CMH);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('145');
        $user->setName("Barbaro H.");
        $user->setLastname("Gutierrez");
        $user->setUsername("barbaro.gutierrez");
        $user->setPlainPassword("barbaro.gutierrez145@FHC2019");
        $user->setEmail("dr.humberto77@yahoo.es");
        $user->addRole(User::ROLE_SUPERVISOR_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('22');
        $user->setName("Maria");
        $user->setLastname("Acosta");
        $user->setUsername("maria.acosta");
        $user->setPlainPassword("maria.acosta22@FHC2019");
        $user->setEmail("acosta-m@bellsouth.net");
        $user->addRole(User::ROLE_SUPERVISOR_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('34');
        $user->setName("Alain");
        $user->setLastname("Pineda");
        $user->setUsername("alain.pineda");
        $user->setPlainPassword("alain.pineda34@FHC2019");
        $user->setEmail("alainpineda76@yahoo.es");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('197');
        $user->setName("Alexei");
        $user->setLastname("Alfonso");
        $user->setUsername("alexei.alfonso");
        $user->setPlainPassword("alexei.alfonso197@FHC2019");
        $user->setEmail("alexeiafonso@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('144');
        $user->setName("Alina M.");
        $user->setLastname("Regueiro");
        $user->setUsername("alina.regueiro");
        $user->setPlainPassword("alina.regueiro144@FHC2019");
        $user->setEmail("alinaregueiro67@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('49');
        $user->setName("Anaisy");
        $user->setLastname("Exposito");
        $user->setUsername("anaisy.exposito");
        $user->setPlainPassword("anaisy.exposito49@FHC2019");
        $user->setEmail("anaisy14@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('152');
        $user->setName("Annia");
        $user->setLastname("Arnaiz");
        $user->setUsername("annia.arnaiz");
        $user->setPlainPassword("annia.arnaiz152@FHC2019");
        $user->setEmail("annia-arnaiz17@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('60');
        $user->setName("Antonio");
        $user->setLastname("Pantoja");
        $user->setUsername("antonio.pantoja");
        $user->setPlainPassword("antonio.pantoja60@FHC2019");
        $user->setEmail("pantojacardio@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('206');
        $user->setName("Dorlys");
        $user->setLastname("Gonzalez");
        $user->setUsername("dorlys.gonzalez");
        $user->setPlainPassword("dorlys.gonzalez206@FHC2019");
        $user->setEmail("dorlyz061291@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('188');
        $user->setName("Dunia");
        $user->setLastname("Lopez");
        $user->setUsername("dunia.lopez");
        $user->setPlainPassword("dunia.lopez188@FHC2019");
        $user->setEmail("defptly@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('91');
        $user->setName("Eduardo");
        $user->setLastname("Cusido");
        $user->setUsername("eduardo.cusido");
        $user->setPlainPassword("eduardo.cusido91@FHC2019");
        $user->setEmail("escusido72@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('202');
        $user->setName("Elayne");
        $user->setLastname("Gonzalez");
        $user->setUsername("elayne.gonzalez");
        $user->setPlainPassword("elayne.gonzalez202@FHC2019");
        $user->setEmail("elayne_ggonzalez@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('196');
        $user->setName("Janet");
        $user->setLastname("Oliva");
        $user->setUsername("janet.oliva");
        $user->setPlainPassword("janet.oliva196@FHC2019");
        $user->setEmail("jnodarse1970@live.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('170');
        $user->setName("Jorge Luis");
        $user->setLastname("Pérez");
        $user->setUsername("jorge.perez");
        $user->setPlainPassword("jorge.perez170@FHC2019");
        $user->setEmail("jorgeluis1976@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('203');
        $user->setName("Leda Lena");
        $user->setLastname("Setien");
        $user->setUsername("leda.setien");
        $user->setPlainPassword("leda.setien203@FHC2019");
        $user->setEmail("noemail3@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('180');
        $user->setName("Lisette Valdes");
        $user->setLastname("Oliva");
        $user->setUsername("lisette.oliva");
        $user->setPlainPassword("lisette.oliva180@FHC2019");
        $user->setEmail("lisy0791@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('198');
        $user->setName("Lourdes");
        $user->setLastname("Ramos");
        $user->setUsername("lourdes.ramos");
        $user->setPlainPassword("lourdes.ramos198@FHC2019");
        $user->setEmail("lourdesramos2003@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('171');
        $user->setName("Maria C.");
        $user->setLastname("Córdova");
        $user->setUsername("maria.cordova");
        $user->setPlainPassword("maria.cordova171@FHC2019");
        $user->setEmail("mariacaridad83@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('135');
        $user->setName("Napenai");
        $user->setLastname("Montero-S");
        $user->setUsername("napenai.montero");
        $user->setPlainPassword("napenai.montero135@FHC2019");
        $user->setEmail("alnape06@yahoo.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('201');
        $user->setName("Noel A.");
        $user->setLastname("Mora");
        $user->setUsername("noel.mora");
        $user->setPlainPassword("noel.mora201@FHC2019");
        $user->setEmail("noah1293@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('111');
        $user->setName("Pedro E.");
        $user->setLastname("Leon");
        $user->setUsername("pedro.leon");
        $user->setPlainPassword("pedro.leon111@FHC2019");
        $user->setEmail("pedri537@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('209');
        $user->setName("Yaima");
        $user->setLastname("Planas");
        $user->setUsername("yaima.planas");
        $user->setPlainPassword("yaima.planas209@FHC2019");
        $user->setEmail("yplanas80@yahoo.es");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('159');
        $user->setName("Yasiel");
        $user->setLastname("Corzo");
        $user->setUsername("yasiel.corzo");
        $user->setPlainPassword("yasiel.corzo159@FHC2019");
        $user->setEmail("yasielcorzo@yahoo.es");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('175');
        $user->setName("Yatali");
        $user->setLastname("Montero-S");
        $user->setUsername("yatali.montero");
        $user->setPlainPassword("yatali.montero175@FHC2019");
        $user->setEmail("yatalimontero29@gmail.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('205');
        $user->setName("Yudeisy");
        $user->setLastname("Alvarez");
        $user->setUsername("yudeisy.alvarez");
        $user->setPlainPassword("yudeisy.alvarez205@FHC2019");
        $user->setEmail("yudeisyalvarez@outlook.com");
        $user->addRole(User::ROLE_TCM);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('183');
        $user->setName("Elizabeth");
        $user->setLastname("Jimenez");
        $user->setUsername("elizabeth.jimenez");
        $user->setPlainPassword("elizabeth.jimenez183@FHC2019");
        $user->setEmail("elizabeth.jimenez009@gmail.com");
        $user->addRole(User::ROLE_UTILIZATION_MANAGER);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setBirthDate(DateTime::createFromFormat('m/d/Y', '11/15/1982'));
        $user->setEnabled(true);
        $this->getContainer()->get('fos_user.user_manager')->updateUser($user);
    }
}

