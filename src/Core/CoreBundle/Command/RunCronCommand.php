<?php

namespace Core\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunCronCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:cron')
            ->setDescription('run cron command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Help");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Registing crontab every minute.</comment>');

        $application = $this->getApplication();

        $dirApp = $this->getContainer()->getParameter('dir.app');
        $dirBin = $this->getContainer()->getParameter('dir.bin');
        $dirVar = $this->getContainer()->getParameter('dir.var');

        $cronLog = $dirVar . '/logs/cron.log';

        $file = $dirApp.'/command';

        $commandPath = '* * * * * sudo -H -u www-data php '.$dirBin.'/console core:run:gos --env=prod' . "\n";

        if(file_exists($file)){
            file_put_contents($file, $commandPath);
            exec("crontab $file > $cronLog 2>&1 & echo $!");
            $output->writeln('<comment>Register cron tasks.</comment>');
        }
    }
}
