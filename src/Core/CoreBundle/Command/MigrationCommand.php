<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\City;
use Core\PatientBundle\Entity\DiagnosisMigration;
use Core\PatientBundle\Entity\DiagnosisPatient;
use Core\PatientBundle\Entity\DiagnosisTemplate;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Medicaid;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\RegistryPatient;
use Core\PatientBundle\Entity\ServiceCode;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Migration command.
 *
 * @author Cesar A Glez
 */
class MigrationCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:migration')
            ->setDescription('Migration data clinic')
            //->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Migration");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Starting Migration ...</comment>');

        $application = $this->getApplication();

        $d = $this->getContainer()->get('doctrine');
        $migrationPath = $this->getContainer()->getParameter('dir.app') . '/migration/';
        /** @var BaseManager $bm */
        $bm = $this->getContainer()->get(BaseManager::class);

        $sexMale = NomUtil::getNomenclatorsByName(NomUtil::NOM_SEX_TYPE_M);
        $sexFemale = NomUtil::getNomenclatorsByName(NomUtil::NOM_SEX_TYPE_F);

        $errorId = -1;
        $rawEligibility = [];
        try{
            /*$output->writeln('<comment>Loading Eligibility Raw Data...</comment>');
            $filePath = $migrationPath . 'eligibility_medicaid.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $ie = [
                        'id' => $parseLine[0],
                        'patient_id' => $parseLine[1],
                        'hmo_id' => $parseLine[2] == '16' ? 9 : $parseLine[2], //Stywall and wellcare
                        'date' => DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[3]),
                    ];
                    $rawEligibility[] = $ie;
                }
            }
            fclose($fd);*/

            /*$output->writeln('<comment>Loading City ...</comment>');
            $filePath = $migrationPath . 'list_city.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $city = new City();
                    $city->setName($parseLine[1]);
                    $bm->save($city);
                }
            }
            fclose($fd);*/

            /*$output->writeln('<comment>Loading Diagnosis Templates Json ...</comment>');
            $jsonDiagnosis = file_get_contents($migrationPath . 'diagnosis.json');
            $serializer = $this->getContainer()->get('jms_serializer');
            $diagnosisMigrations = $serializer->deserialize($jsonDiagnosis, 'array<Core\PatientBundle\Entity\DiagnosisMigration>', 'json');
            foreach($diagnosisMigrations as $dm){
                $errorId = $dm;
                if($dm->getCode1() != '' || $dm->getCode2() != ''){
                    $diagnosisTemplate = new DiagnosisTemplate();
                    $diagnosisTemplate->setPrimaryCode($dm->getCode1());
                    $diagnosisTemplate->setSecondaryCode($dm->getCode2());
                    $diagnosisTemplate->setDescription($dm->getDiagnosis());
                    $bm->save($diagnosisTemplate);
                }
            }*/


            /*$output->writeln('<comment>Loading Patients ...</comment>');
            $filePath = $migrationPath . 'patients.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $lastIe = $this->getLastEligibility($rawEligibility, $parseLine[0]);

                    $patient = new Patient();
                    $patient->setMigrationId($parseLine[0]);
                    $patient->setCaseNumber($parseLine[0]);
                    $patient->setName($parseLine[1]);
                    $patient->setLastname($parseLine[2]);
                    $patient->setDob(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[3]));
                    $patient->setGender($parseLine[4] == '0' ? $sexMale : $sexFemale);
                    $patient->setSocialSecurity(sprintf('%s-%s-%s',
                        substr($parseLine[5], 0, 3),
                        substr($parseLine[5], 3, 2),
                        substr($parseLine[5], 5, 4)
                    ));
                    $patient->setHomePhone(sprintf('(%s) %s-%s',
                        substr($parseLine[6], 0, 3),
                        substr($parseLine[6], 3, 3),
                        substr($parseLine[6], 6, 4)
                    ));
                    $patient->setAddress($parseLine[7]);
                    $city = $d->getRepository(City::class)->find($parseLine[8]);
                    $patient->setCity($city);
                    $patient->setState('FL');
                    $patient->setZipCode($parseLine[10]);


                    $hmo = $d->getRepository(Hmo::class)->findOneBy(['migrationId' => $lastIe['hmo_id']]);
                    if(is_null($hmo)){
                        $hmo = $d->getRepository(Hmo::class)->find(26);
                    }
                    $medicaid = new Medicaid();
                    $medicaid->setHmo($hmo);
                    $medicaid->setNumber($parseLine[12]);
                    $bm->save($medicaid);

                    $patient->setMedicaid($medicaid);
                    $bm->save($patient);

                    $ie = new InsuranceEligibility();
                    $ie->setPatient($patient);
                    $ie->setNumber($patient->getMedicaid()->getNumber());
                    $ie->setHmo($patient->getMedicaid()->getHmo());
                    $ie->setType(NomUtil::MEDICAID);
                    $ie->setRunDate($lastIe['date']);
                    $bm->save($ie);
                }
            }
            fclose($fd);*/

            /*$output->writeln('<comment>Loading Entry Records for make patient inactive o active ...</comment>');
            $filePath = $migrationPath . 'date_entry.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $registry = new RegistryPatient();
                    $patient = $d->getRepository(Patient::class)->findOneBy([
                        'migrationId' => $parseLine[1],
                    ]);
                    $registry->setPatient($patient);
                    $registry->setEntryDate(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[2]));
                    $isDischarge = (boolean) $parseLine[5];
                    if($isDischarge){
                        $registry->setIsOpen(false);
                        $registry->setDischargeCmh(true);
                        $registry->setDischargeTcm(true);
                        if($parseLine[6] != '') {
                            $registry->setDischargeCmhDate(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[6]));
                            $registry->setDischargeTcmDate(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[6]));
                        } else {
                            $registry->setDischargeCmhDate($registry->getEntryDate());
                            $registry->setDischargeTcmDate($registry->getEntryDate());
                        }
                    } else {
                        $registry->setIsOpen(true);
                        $inactiveTcm = (boolean) $parseLine[7];
                        $inactiveCmh = (boolean) $parseLine[8];
                        $registry->setOpenByTcm(!$inactiveTcm);
                        $registry->setOpenByCmh(!$inactiveCmh);

                        $patient->setPatientStatus(NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN));
                        $patient->setOpenDate($registry->getEntryDate());
                        $bm->save($patient);
                    }
                    $bm->save($registry);
                }
            }
            fclose($fd);*/

            /*$output->writeln('<comment>Loading Authorizations...</comment>');
            $filePath = $migrationPath . 'billing_auth_units.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $authorization = new Authorization();
                    $authorization->setMigrationId($parseLine[0]);
                    $authorization->setNumber($parseLine[1]);

                    $patient = $d->getRepository(Patient::class)->findOneBy([
                        'migrationId' => $parseLine[2],
                    ]);

                    $authorization->setPatient($patient);
                    $authorization->setBeginDate(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[4]));
                    $authorization->setEndDate(DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[5]));
                    $authorization->setUnitsApproved($parseLine[6]);
                    $authorization->setUnitsConsumed($parseLine[7]);

                    $hmo = $d->getRepository(Hmo::class)->findOneBy(['migrationId' => $parseLine[3] == '16' ? 9 : $parseLine[3]]);
                    if(is_null($hmo)){
                        $hmo = $d->getRepository(Hmo::class)->find(26);
                    }

                    $authorization->setHmo($hmo);

                    //Check status of autorization
                    //Check Dates
                    $now = new DateTime();
                    if($authorization->getBeginDate()->getTimestamp() <= $now->getTimestamp() && $now->getTimestamp() <= $authorization->getEndDate()->getTimestamp() ){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    //Check Insurance Patient
                    $authHmo = $authorization->getHmo()->getId();
                    $medicarePatient = $authorization->getPatient()->getMedicare();
                    $medicaidPatient = $authorization->getPatient()->getMedicaid();

                    if( (!is_null($medicarePatient) && $authHmo == $medicarePatient->getHmo()->getId()) || (!is_null($medicaidPatient) && $authHmo == $medicaidPatient->getHmo()->getId()) ){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    //Check amount units
                    if($authorization->getRemainedUnits() > 0){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    $bm->save($authorization);
                }
            }
            fclose($fd);*/

            
            /*$output->writeln('<comment>Fixed Stywell mistake...</comment>');
            $staywellHmo = $d->getRepository(Hmo::class)->find(14);
            $filePath = $migrationPath . 'billing_auth_units.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);

                    $authorization = $d->getRepository(Authorization::class)->findOneBy([
                        'migrationId' => $parseLine[0],
                        'hmo' => 26,
                    ]);

                    if(is_null($authorization) || $parseLine[3] != '16')
                        continue;

                    $authorization->setHmo($staywellHmo);

                    //Check status of $authorization
                    //Check Dates
                    $now = new DateTime();
                    if($authorization->getBeginDate()->getTimestamp() <= $now->getTimestamp() && $now->getTimestamp() <= $authorization->getEndDate()->getTimestamp() ){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    //Check Insurance Patient
                    $authHmo = $authorization->getHmo()->getId();
                    $medicarePatient = $authorization->getPatient()->getMedicare();
                    $medicaidPatient = $authorization->getPatient()->getMedicaid();

                    if( (!is_null($medicarePatient) && $authHmo == $medicarePatient->getHmo()->getId()) || (!is_null($medicaidPatient) && $authHmo == $medicaidPatient->getHmo()->getId()) ){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    //Check amount units
                    if($authorization->getRemainedUnits() > 0){
                        $authorization->setActive(true);
                    } else {
                        $authorization->setActive(false);
                        $bm->save($authorization);
                        continue;
                    }

                    $bm->save($authorization);
                }
            }
            fclose($fd);*/

            /*$output->writeln('<comment>Update authorization factor weight order number...</comment>');
            $patients = $d->getRepository(Patient::class)->findAll();
            foreach ($patients as $patient) {
                $patient->updateAmountAuth();
                $bm->save($patient);
            }*/

            /*$output->writeln('<comment>Update authorization service codes...</comment>');
            $serviceCodeRepo = $d->getRepository(ServiceCode::class);
            //Migration ID => Real ServiceCode DB
            $serviceCodes = [
                1 => $serviceCodeRepo->find(8),
                2 => $serviceCodeRepo->find(9),
                3 => $serviceCodeRepo->find(12),
                4 => $serviceCodeRepo->find(3),
                5 => $serviceCodeRepo->find(4),
                6 => $serviceCodeRepo->find(15),
                8 => $serviceCodeRepo->find(5),
                10 => $serviceCodeRepo->find(11),
                11 => $serviceCodeRepo->find(10),
                12 => $serviceCodeRepo->find(16),
            ];
            $filePath = $migrationPath . 'billing_auth_units_service.txt';
            $fd = fopen ($filePath, "r");
            $authorization = null;
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);

                    if(is_null($authorization) || $authorization->getMigrationId() != $parseLine[1]) {
                        $authorization = $d->getRepository(Authorization::class)->findOneBy([
                            'migrationId' => $parseLine[1],
                        ]);
                    }

                    $serviceCode = $serviceCodes[$parseLine[2]];

                    if(!$authorization->getServiceCodes()->contains($serviceCode)){
                        $authorization->addServiceCode($serviceCode);
                        $bm->save($authorization);
                    }
                }
            }
            fclose($fd);*/
            
            //Remove all diagnosis patient
            $d->getRepository(DiagnosisPatient::class)->removeAll();
            
            //Remove all diagnosis template
            $d->getRepository(DiagnosisTemplate::class)->removeAll();

            $output->writeln('<comment>Loading list diagnosis templates...</comment>');
            $filePath = $migrationPath . 'list_diagnosis.txt';
            $fd = fopen ($filePath, "r");
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $codes = explode('(', $parseLine[1]);

                    $primaryCode = rtrim($codes[0]);
                    $secondaryCode = str_replace(')', '', $codes[1]);

                    $diagnosisTemplate = new DiagnosisTemplate();
                    $diagnosisTemplate->setPrimaryCode($primaryCode);
                    $diagnosisTemplate->setSecondaryCode($secondaryCode);
                    $diagnosisTemplate->setMigrationId($parseLine[0]);
                    $bm->save($diagnosisTemplate);
                    
                }
            }
            fclose($fd);

            $output->writeln('<comment>Loading diagnosis of patients...</comment>');
            $filePath = $migrationPath . 'diagnostic_patient.txt';
            $fd = fopen ($filePath, "r");
            $diagnosisTemplateRepo = $d->getRepository(DiagnosisTemplate::class);
            $patientRepo = $d->getRepository(Patient::class);
            $admin = $d->getRepository(User::class)->findOneBy(['username' => 'admin']);
            while (!feof($fd)) {
                $buffer = fgets($fd);
                if(strlen($buffer) > 0) {
                    $parseLine = $this->parserLine($buffer);
                    $errorId = $parseLine[0];

                    $diagnosisTemplate = $diagnosisTemplateRepo->findOneBy(['migrationId' => $parseLine[2]]);
                    $patient = $patientRepo->findOneBy(['migrationId' => $parseLine[1]]);
                    $date = DateTime::createFromFormat('n/j/Y G:i:s', $parseLine[3]);
                    $ip = $parseLine[4];
                    $bio = $parseLine[5];
                    $pcp = $parseLine[6];

                    if(is_null($diagnosisTemplate) || is_null($patient))
                        continue;

                    $dp = new DiagnosisPatient();
                    $dp->setDiagnosisTemplate($diagnosisTemplate);
                    $dp->setPatient($patient);
                    $dp->setCreatedAt($date);
                    $dp->setDiagnosisDate($date);
                    $dp->setCreatedBy($admin);
                    $dp->setIp((bool)$ip);
                    $dp->setBio((bool)$bio);
                    $dp->setPcp((bool)$pcp);

                    $bm->save($dp);
                }
            }
            fclose($fd);
            
        }catch (\Exception $e){
            echo sprintf('Error on item %s: %s', $errorId, $e);
            die;
        }
    }

    private function getLastEligibility($list, $patientId){
        for($i = count($list)-1; $i >= 0 ; $i--){
            $ie = $list[$i];
            if($ie['patient_id'] == $patientId)
                return $ie;
        }
        return null;
    }

    private function parserLine($line){
        $line = str_replace("\n", '', $line);
        $line = str_replace("\r", '', $line);
        $a = explode(';', $line);
        return $a;
    }
}

