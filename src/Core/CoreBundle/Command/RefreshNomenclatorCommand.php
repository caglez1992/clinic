<?php

namespace Core\CoreBundle\Command;

use Core\CoreBundle\DataFixtures\NomFixtures;
use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Util\NomUtil;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * RefreshNomenclator.
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().        
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class RefreshNomenclatorCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:nom:refresh')
            ->setDescription('refresh nomenclators command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Refresh nomenclators");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $application = $this->getApplication();
        $output->writeln('<comment>Refreshing nomenclators...</comment>');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $nom = new NomFixtures();
        $parameters = $nom->getParameters();

        foreach ($parameters as $parameter) {
            foreach ($parameter['nom'] as $key => $nom) {
                $name = $nom;
                if(is_array($nom)){
                    $name = $nom['name'];
                }

                $nomenclator = NomUtil::getNomenclatorsByName($name);

                if(is_null($nomenclator)) {
                    $output->writeln("<comment>Create new nomencaltor $name</comment>");
                    $nomenclator = new Nomenclator();
                }

                $output->writeln("<comment>Editing nomenclator $name</comment>");

                $nomenclator->setName($name);
                $nomenclator->setType($parameter['type']);

                if(is_array($nom)){
                    if(isset($nom['pdfName']))
                        $nomenclator->setPdfName($nom['pdfName']);
                    if(isset($nom['cssClass']))
                        $nomenclator->setClassCss($nom['cssClass']);
                }

                $em->persist($nomenclator);
                $em->flush();
            }
        }
        $output->writeln('<comment>Done</comment>');
    }
}

