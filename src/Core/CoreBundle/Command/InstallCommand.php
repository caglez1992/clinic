<?php

namespace Core\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Install command for install arch.
 *
 * You could also extend from Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 * to get access to the container via $this->getContainer().        
 *
 * @author Tobias Schultze <http://tobion.de>
 */
class InstallCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('core:install')
            ->setDescription('install command')
            ->addArgument('who', InputArgument::OPTIONAL, 'Who to greet.', 'World')
            ->setHelp("Help");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Installing the platform...</comment>');

        $application = $this->getApplication();

        $output->writeln('<comment>Delete Database...</comment>');
        $command = $application->find('doctrine:database:drop');
        $input = new ArrayInput(
            array(
                'command' => 'doctrine:database:drop',
                '--force' => true,
                '--if-exists' => true,
            )
        );
        $command->run($input, $output);

        $output->writeln('<comment>Create New Database...</comment>');
        $command = $application->find('doctrine:database:create');
        $input = new ArrayInput(
            array(
                'command' => 'doctrine:database:create',
            )
        );
        $command->run($input, $output);

        $output->writeln('<comment>Update schema ...</comment>');
        $command = $application->find('doctrine:schema:update');
        $input = new ArrayInput(
            array(
                'command' => 'doctrine:schema:update',
                '--force' => true,
            )
        );
        $command->run($input, $output);

        $output->writeln('<comment>Publish Assets With --symlink ...</comment>');
        $command = $application->find('assets:install');
        $input = new ArrayInput(
            array(
                'command' => 'assets:install web',
                '--symlink' => true,
            )
        );
        $command->run($input, $output);

        $output->writeln('<comment>Load Fixtures...</comment>');
        $command = $application->find('doctrine:fixtures:load');
        $input = new ArrayInput(
            array(
                'command' => 'doctrine:fixtures:load',
            )
        );
        $input->setInteractive(false);
        $command->run($input, $output);


    }
}
