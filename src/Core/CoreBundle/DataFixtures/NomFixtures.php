<?php
/**
 * Created by PhpStorm.
 * User: LocurA
 * Date: 22/01/15
 */

namespace Core\CoreBundle\DataFixtures;

use Core\CoreBundle\Entity\Nomenclator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Core\CoreBundle\Util\NomUtil;

class NomFixtures extends Fixture implements OrderedFixtureInterface
{
    private $parameters = array(

        array(
            'type' => NomUtil::NOM_INSURANCE_TYPE,
            'nom' => array(
                NomUtil::MEDICARE,
                NomUtil::MEDICAID,
            ),
        ),

        array(
            'type' => NomUtil::NOM_INTAKE_TYPE,
            'nom' => array(
                NomUtil::NOM_INTAKE_TYPE_TCM,
                NomUtil::NOM_INTAKE_TYPE_CMH,
            ),
        ),

        array(
            'type' => NomUtil::NOM_STATUS,
            'nom' => array(
                ['name' => NomUtil::NOM_STATUS_OPEN, 'cssClass' => 'label label-info'],
                ['name' => NomUtil::NOM_STATUS_CLOSE, 'cssClass' => 'label label-default'],
                ['name' => NomUtil::NOM_STATUS_PENDING, 'cssClass' => 'label label-warning'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_SEX_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_SEX_TYPE_M, 'pdfName' => 'Choice1'],
                ['name' => NomUtil::NOM_SEX_TYPE_F, 'pdfName' => 'Choice2'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_RACE_TYPE,
            'nom' => array(
                NomUtil::NOM_RACE_TYPE_W,
                NomUtil::NOM_RACE_TYPE_B,
                NomUtil::NOM_RACE_TYPE_AA,
                NomUtil::NOM_RACE_TYPE_NA,
                NomUtil::NOM_RACE_TYPE_AS,
                NomUtil::NOM_RACE_TYPE_MR,
                NomUtil::NOM_RACE_TYPE_UN,
            ),
        ),

        array(
            'type' => NomUtil::NOM_ETHNICITY_TYPE,
            'nom' => array(
                NomUtil::NOM_ETHNICITY_TYPE_HL,
                NomUtil::NOM_ETHNICITY_TYPE_NHL,

            ),
        ),

        array(
            'type' => NomUtil::NOM_MARITAL_STATUS_TYPE,
            'nom' => array(
                NomUtil::NOM_MARITAL_STATUS_TYPE_SINGLE,
                NomUtil::NOM_MARITAL_STATUS_TYPE_MARRIED,
                NomUtil::NOM_MARITAL_STATUS_TYPE_COHABITING,
                NomUtil::NOM_MARITAL_STATUS_TYPE_DIVORCED,
                NomUtil::NOM_MARITAL_STATUS_TYPE_SEPARATED,
                NomUtil::NOM_MARITAL_STATUS_TYPE_WIDOWED,
                NomUtil::NOM_MARITAL_STATUS_TYPE_UNKNOWN,
            ),
        ),

        array(
            'type' => NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE,
            'nom' => array(
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_NO_EDUCATION,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_ELEMENTARY,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_MIDDLE_SCHOOL,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_HIGH_SCHOOL,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_TECH,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_COLLEGE,
                NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE_PHD,

            ),
        ),

        array(
            'type' => NomUtil::NOM_LANGUAGE_TYPE,
            'nom' => array(
                NomUtil::NOM_LANGUAGE_TYPE_S,
                NomUtil::NOM_LANGUAGE_TYPE_E,
                NomUtil::NOM_LANGUAGE_TYPE_C,

            ),
        ),

        array(
            'type' => NomUtil::NOM_EMPLOYMENT_TYPE,
            'nom' => array(
                NomUtil::NOM_EMPLOYMENT_TYPE_FT,
                NomUtil::NOM_EMPLOYMENT_TYPE_PT,
                NomUtil::NOM_EMPLOYMENT_TYPE_RETIRED,
                NomUtil::NOD_EMPLOYMENT_TYPE_DISABLED,
                NomUtil::NOM_EMPLOYMENT_TYPE_HM,
                NomUtil::NOM_EMPLOYMENT_TYPE_STUDENT,
                NomUtil::NOM_EMPLOYMENT_TYPE_UNEMPLOYED,
                NomUtil::NOM_EMPLOYMENT_TYPE_NA,

            ),
        ),

        array(
            'type' => NomUtil::NOM_RESIDENTIAL_TYPE,
            'nom' => array(
                NomUtil::NOM_RESIDENTIAL_TYPE_A,
                NomUtil::NOM_RESIDENTIAL_TYPE_R,
                NomUtil::NOM_RESIDENTIAL_TYPE_NR,
                NomUtil::NOM_RESIDENTIAL_TYPE_ALF,
                NomUtil::NOM_RESIDENTIAL_TYPE_FC,
                NomUtil::NOM_RESIDENTIAL_TYPE_H,
                NomUtil::NOM_RESIDENTIAL_TYPE_SIPP,
                NomUtil::NOM_RESIDENTIAL_TYPE_C,
                NomUtil::NOM_RESIDENTIAL_TYPE_HL,

            ),
        ),

        array(
            'type' => NomUtil::NOM_REFERRED_TYPE,
            'nom' => array(
                NomUtil::NOM_REFERRED_TYPE_S,
                NomUtil::NOM_REFERRED_TYPE_A,

            ),
        ),

        array(
            'type' => NomUtil::NOM_AGENCY_TYPE,
            'nom' => array(
                NomUtil::NOM_AGENCY_TYPE_COLONIA,
                NomUtil::NOM_AGENCY_TYPE_LEON,
                NomUtil::NOM_AGENCY_TYPE_PASTEUR,
                NomUtil::NOM_AGENCY_TYPE_CAC,
                NomUtil::NOM_AGENCY_TYPE_DOCTORS,
                NomUtil::NOM_AGENCY_TYPE_WELLMAX,
                NomUtil::NOM_AGENCY_TYPE_ULTRACARE,
                NomUtil::NOM_AGENCY_TYPE_MERCEDES,
                NomUtil::NOM_AGENCY_TYPE_INTERAMERICAN,
                NomUtil::NOM_AGENCY_TYPE_UNIVERSITY,
                NomUtil::NOM_AGENCY_TYPE_OTHER,

            ),
        ),

        array(
            'type' => NomUtil::NOM_REFERRAL_REASONS_TYPE,
            'nom' => array(      
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_SA, 'pdfName' => 'alcohol.0.0'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_PS, 'pdfName' => 'alcohol.1.0'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_HP, 'pdfName' => 'alcohol.0.1.0.0.0.0'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_CR, 'pdfName' => 'alcohol.1.1.0.0.0.0'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_PH, 'pdfName' => 'alcohol.0.1.0.0.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_EN, 'pdfName' => 'alcohol.1.1.0.0.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_HO, 'pdfName' => 'alcohol.0.1.1.0.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_DL, 'pdfName' => 'alcohol.1.1.1.0.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_CS, 'pdfName' => 'alcohol.0.1.0.0.1.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_LI, 'pdfName' => 'alcohol.0.1.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_SS, 'pdfName' => 'alcohol.1.1.0.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_CMH, 'pdfName' => 'alcohol.0.1.1.1'],      
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_AS, 'pdfName' => 'alcohol.1.1.1.1'],
                ['name' => NomUtil::NOM_REFERRAL_REASONS_TYPE_US, 'pdfName' => 'alcohol.1.1.1.0.1.1'],    
            ),
        ),

        array(
            'type' => NomUtil::NOM_SCHOOL_PROGRAM_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_REGULAR, 'pdfName' => 'Choice1'],
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_ESE, 'pdfName' => 'Choice2'],
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_EBD, 'pdfName' => 'Choice3'],
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_ESOL, 'pdfName' => 'Choice4'],
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_HHIP, 'pdfName' => 'Choice5'],
                ['name' => NomUtil::NOM_SCHOOL_PROGRAM_OTHER, 'pdfName' => 'Choice6'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_RESIDENTIAL_STATUS_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_RESIDENTIAL_STATUS_TYPE_CITIZEN, 'pdfName' => 'Choice6'],
                ['name' => NomUtil::NOM_RESIDENTIAL_STATUS_TYPE_RESIDENT, 'pdfName' => 'Choice1'],
                ['name' => NomUtil::NOM_RESIDENTIAL_STATUS_TYPE_OTHER, 'pdfName' => 'Choice2'],
            ),
        ),


        array(
            'type' => NomUtil::NOM_ACTION_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_ACTION_TYPE_CREATE, 'cssClass' => 'label label-success'],
                ['name' => NomUtil::NOM_ACTION_TYPE_UPDATE, 'cssClass' => 'label label-warning'],
                ['name' => NomUtil::NOM_ACTION_TYPE_DELETE, 'cssClass' => 'label label-danger']
            ),
        ),

        array(
            'type' => NomUtil::NOM_SERVICE_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_SERVICE_TYPE_EVALUATION, 'cssClass' => 'label label-success'],
                ['name' => NomUtil::NOM_SERVICE_TYPE_INTERVENTION, 'cssClass' => 'label label-info'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_SERVICE_SESSION_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_SERVICE_SESSION_TYPE_AM, 'cssClass' => 'label label-info'],
                ['name' => NomUtil::NOM_SERVICE_SESSION_TYPE_PM, 'cssClass' => 'label label-success'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_SERVICE_PRESENCE_TYPE,
            'nom' => array(
                NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF,
                NomUtil::NOM_SERVICE_PRESENCE_TYPE_OB,
            ),
        ),

        array(
            'type' => NomUtil::NOM_MODIFIER_AMOUNT_TYPE,
            'nom' => array(
                NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT,
                NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS,
                NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS,
            ),
        ),

        array(
            'type' => NomUtil::NOM_FREQUENCY_TYPE,
            'nom' => array(
                NomUtil::NOM_FREQUENCY_TYPE_YEAR,
                NomUtil::NOM_FREQUENCY_TYPE_MONTH,
                NomUtil::NOM_FREQUENCY_TYPE_WEEK,
                NomUtil::NOM_FREQUENCY_TYPE_DAY,
            ),
        ),

        array(
            'type' => NomUtil::NOM_ACTION_REGISTRY_PATIENT_TYPE,
            'nom' => array(
                ['name' => NomUtil::NOM_ACTION_REGISTRY_PATIENT_TYPE_ADMITTED, 'cssClass' => 'label label-success'],
                ['name' => NomUtil::NOM_ACTION_REGISTRY_PATIENT_TYPE_MODIFY, '  cssClass' => 'label label-warning'],
                ['name' => NomUtil::NOM_ACTION_REGISTRY_PATIENT_TYPE_DISCHARGE, 'cssClass' => 'label label-default'],
            ),
        ),

        array(
            'type' => NomUtil::NOM_REGISTRY_PD_ACTION,
            'nom' => array(
                NomUtil::NOM_REGISTRY_PD_ACTION_ASSESSMENT,
                NomUtil::NOM_REGISTRY_PD_ACTION_INTERVENTION,
            ),
        ),

        array(
            'type' => NomUtil::NOM_PARENT_TYPE,
            'nom' => array(
                NomUtil::NOM_PARENT_TYPE_AUNT,
                NomUtil::NOM_PARENT_TYPE_BROTHER,
                NomUtil::NOM_PARENT_TYPE_BROTHER_LAW,
                NomUtil::NOM_PARENT_TYPE_BROTHER_WIDOWER,
                NomUtil::NOM_PARENT_TYPE_COUSIN,
                NomUtil::NOM_PARENT_TYPE_DAUGHTER,
                NomUtil::NOM_PARENT_TYPE_DAUGHTER_LAW,
                NomUtil::NOM_PARENT_TYPE_DAUGHTER_WIDOWER,
                NomUtil::NOM_PARENT_TYPE_FATHER,
                NomUtil::NOM_PARENT_TYPE_FATHER_LAW,
                NomUtil::NOM_PARENT_TYPE_FATHER_DAUGHTER,
                NomUtil::NOM_PARENT_TYPE_FATHER_SON,
                NomUtil::NOM_PARENT_TYPE_FATHER_WIDOW,
                NomUtil::NOM_PARENT_TYPE_GDH,
                NomUtil::NOM_PARENT_TYPE_GDW,
                NomUtil::NOM_PARENT_TYPE_GF,
                NomUtil::NOM_PARENT_TYPE_GFW,
                NomUtil::NOM_PARENT_TYPE_GFWI,
                NomUtil::NOM_PARENT_TYPE_GM,
                NomUtil::NOM_PARENT_TYPE_GMH,
                NomUtil::NOM_PARENT_TYPE_GMW,
                NomUtil::NOM_PARENT_TYPE_GS,
                NomUtil::NOM_PARENT_TYPE_GSW,
                NomUtil::NOM_PARENT_TYPE_GSWI,
                NomUtil::NOM_PARENT_TYPE_GGD,
                NomUtil::NOM_PARENT_TYPE_GGF,
                NomUtil::NOM_PARENT_TYPE_GGM,
                NomUtil::NOM_PARENT_TYPE_GGS,
                NomUtil::NOM_PARENT_TYPE_HB,
                NomUtil::NOM_PARENT_TYPE_HSH,
                NomUtil::NOM_PARENT_TYPE_HBW,
                NomUtil::NOM_PARENT_TYPE_HS,
                NomUtil::NOM_PARENT_TYPE_H,
                NomUtil::NOM_PARENT_TYPE_MIL,
                NomUtil::NOM_PARENT_TYPE_MD,
                NomUtil::NOM_PARENT_TYPE_MS,
                NomUtil::NOM_PARENT_TYPE_MW,
                NomUtil::NOM_PARENT_TYPE_N,
                NomUtil::NOM_PARENT_TYPE_NI,
                NomUtil::NOM_PARENT_TYPE_S,
                NomUtil::NOM_PARENT_TYPE_SIL,
                NomUtil::NOM_PARENT_TYPE_SON,
                NomUtil::NOM_PARENT_TYPE_SONIL,
                NomUtil::NOM_PARENT_TYPE_SONW,
                NomUtil::NOM_PARENT_TYPE_SB,
                NomUtil::NOM_PARENT_TYPE_SD,
                NomUtil::NOM_PARENT_TYPE_SDH,
                NomUtil::NOM_PARENT_TYPE_SF,
                NomUtil::NOM_PARENT_TYPE_SM,
                NomUtil::NOM_PARENT_TYPE_SS,
                NomUtil::NOM_PARENT_TYPE_SSON,
                NomUtil::NOM_PARENT_TYPE_SSONWI,
                NomUtil::NOM_PARENT_TYPE_U,
                NomUtil::NOM_PARENT_TYPE_W,
            ),
        ),

        array(
            'type' => NomUtil::NOM_STATE_TYPE,
            'nom' => array(
                NomUtil::NOM_STATE_TYPE_AL,
                NomUtil::NOM_STATE_TYPE_AK,
                NomUtil::NOM_STATE_TYPE_AS,
                NomUtil::NOM_STATE_TYPE_AZ,
                NomUtil::NOM_STATE_TYPE_AR,
                NomUtil::NOM_STATE_TYPE_CA,
                NomUtil::NOM_STATE_TYPE_CO,
                NomUtil::NOM_STATE_TYPE_CT,
                NomUtil::NOM_STATE_TYPE_DE,
                NomUtil::NOM_STATE_TYPE_DC,
                NomUtil::NOM_STATE_TYPE_FM,
                NomUtil::NOM_STATE_TYPE_FL,
                NomUtil::NOM_STATE_TYPE_GA,
                NomUtil::NOM_STATE_TYPE_GU,
                NomUtil::NOM_STATE_TYPE_HI,
                NomUtil::NOM_STATE_TYPE_ID,
                NomUtil::NOM_STATE_TYPE_IL,
                NomUtil::NOM_STATE_TYPE_IN,
                NomUtil::NOM_STATE_TYPE_IA,
                NomUtil::NOM_STATE_TYPE_KS,
                NomUtil::NOM_STATE_TYPE_KY,
                NomUtil::NOM_STATE_TYPE_LA,
                NomUtil::NOM_STATE_TYPE_ME,
                NomUtil::NOM_STATE_TYPE_MH,
                NomUtil::NOM_STATE_TYPE_MD,
                NomUtil::NOM_STATE_TYPE_MA,
                NomUtil::NOM_STATE_TYPE_MI,
                NomUtil::NOM_STATE_TYPE_MN,
                NomUtil::NOM_STATE_TYPE_MS,
                NomUtil::NOM_STATE_TYPE_MO,
                NomUtil::NOM_STATE_TYPE_MT,
                NomUtil::NOM_STATE_TYPE_NE,
                NomUtil::NOM_STATE_TYPE_NV,
                NomUtil::NOM_STATE_TYPE_NH,
                NomUtil::NOM_STATE_TYPE_NJ,
                NomUtil::NOM_STATE_TYPE_NM,
                NomUtil::NOM_STATE_TYPE_NY,
                NomUtil::NOM_STATE_TYPE_NC,
                NomUtil::NOM_STATE_TYPE_ND,
                NomUtil::NOM_STATE_TYPE_MP,
                NomUtil::NOM_STATE_TYPE_OH,
                NomUtil::NOM_STATE_TYPE_OK,
                NomUtil::NOM_STATE_TYPE_OR,
                NomUtil::NOM_STATE_TYPE_PW,
                NomUtil::NOM_STATE_TYPE_PA,
                NomUtil::NOM_STATE_TYPE_PR,
                NomUtil::NOM_STATE_TYPE_RI,
                NomUtil::NOM_STATE_TYPE_SC,
                NomUtil::NOM_STATE_TYPE_SD,
                NomUtil::NOM_STATE_TYPE_TN,
                NomUtil::NOM_STATE_TYPE_TX,
                NomUtil::NOM_STATE_TYPE_UT,
                NomUtil::NOM_STATE_TYPE_VT,
                NomUtil::NOM_STATE_TYPE_VI,
                NomUtil::NOM_STATE_TYPE_VA,
                NomUtil::NOM_STATE_TYPE_WA,
                NomUtil::NOM_STATE_TYPE_WV,
                NomUtil::NOM_STATE_TYPE_WI,
                NomUtil::NOM_STATE_TYPE_WY,
            ),
        ),
    );

    public function load(ObjectManager $manager)
    {
        foreach ($this->parameters as $parameter) {
            $type = $parameter['type'];

            foreach ($parameter['nom'] as $key => $value) {
                $nomenclator = new Nomenclator();
                $nomenclator->setType($type);
                if (is_array($value)) {
                    $nomenclator->setName($value['name']);
                    if (isset($value['cssClass'])) {
                        $nomenclator->setClassCss($value['cssClass']);
                    }
                    if (isset($value['pdfName'])) {
                        $nomenclator->setPdfName($value['pdfName']);
                    }
                } else {
                    $nomenclator->setName($value);
                }
                $manager->persist($nomenclator);
                $manager->flush();
            }
        }
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    public function getOrder()
    {
        return 0;
    }
}
