<?php
/**
 * Created by PhpStorm.
 * User: ealopez@uci.cu
 * Date: 22/01/15
 */

namespace Core\CoreBundle\DataFixtures;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\DiagnosisTemplate;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\HmoHandler;
use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\ServiceCode;
use Core\PatientBundle\Entity\ServiceModifier;
use Core\PatientBundle\Entity\ServiceParent;
use Core\WorkerBundle\Entity\ServiceAcl;
use Core\WorkerBundle\Entity\SettingCode;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PatientFixtures extends Fixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null.
     *
     * @return void
     */

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $now = new DateTime();
        $year = (int)$now->format('Y');
        $this->container->get('background.tasks')->generateWeeks($year);
        
        //Diagnosis Templates
        $diagnosis = new DiagnosisTemplate();
        $diagnosis->setPrimaryCode('305.00');
        $diagnosis->setSecondaryCode('F10.10');
        $diagnosis->setDescription('Alcohol use disorder, Mild');
        $manager->persist($diagnosis);
        $manager->flush();

        $diagnosis = new DiagnosisTemplate();
        $diagnosis->setPrimaryCode('303.90');
        $diagnosis->setSecondaryCode('F10.20');
        $diagnosis->setDescription('Alcohol use disorder, Moderate');
        $manager->persist($diagnosis);
        $manager->flush();

        $diagnosis = new DiagnosisTemplate();
        $diagnosis->setPrimaryCode('291.81');
        $diagnosis->setDescription('Alcohol withdrawal');
        $manager->persist($diagnosis);
        $manager->flush();

        $diagnosis = new DiagnosisTemplate();
        $diagnosis->setPrimaryCode('291.0');
        $diagnosis->setSecondaryCode('f10.231');
        $diagnosis->setDescription('Alcohol withdrawal delirium');
        $manager->persist($diagnosis);
        $manager->flush();

        //HANDLERS-----------------------------------------------------------
        $hMedicare = new HmoHandler();
        $hMedicare->setName('Medicare');
        $manager->persist($hMedicare);
        $manager->flush();

        $hMedicaid = new HmoHandler();
        $hMedicaid->setName('Medicaid');
        $manager->persist($hMedicaid);
        $manager->flush();

        $hBHO = new HmoHandler();
        $hBHO->setName('Beacon Health Options');
        $manager->persist($hBHO);
        $manager->flush();

        $hCBH = new HmoHandler();
        $hCBH->setName('Concordia Behavioral Health ');
        $manager->persist($hCBH);
        $manager->flush();

        $hWF = new HmoHandler();
        $hWF->setName('Wellcare of Florida');
        $manager->persist($hWF);
        $manager->flush();

        $hC = new HmoHandler();
        $hC->setName('Centene');
        $manager->persist($hC);
        $manager->flush();

        $hMCC = new HmoHandler();
        $hMCC->setName('Magellan Complete Care');
        $manager->persist($hMCC);
        $manager->flush();

        $hCMS = new HmoHandler();
        $hCMS->setName('Children\'s Medical Services');
        $manager->persist($hCMS);
        $manager->flush();

        $hUnited = new HmoHandler();
        $hUnited->setName('United');
        $manager->persist($hUnited);
        $manager->flush();

        $hNA = new HmoHandler();
        $hNA->setName('NA');
        $manager->persist($hNA);
        $manager->flush();

        //HMO----------------------------------------------------------------
        $fullHmo = new Hmo();
        $fullHmo->setName('Medicare Free');
        $fullHmo->setMedicare(true);
        $fullHmo->setHandler($hMedicare);
        $fullHmo->setFree(true);
        $manager->persist($fullHmo);
        $manager->flush();

        $fullHmo = new Hmo();
        $fullHmo->setName('Full Medicaid');
        $fullHmo->setMigrationId(4);
        $fullHmo->setMedicaid(true);
        $fullHmo->setFree(true);
        $fullHmo->setHandler($hMedicaid);
        $manager->persist($fullHmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Simply Amerigroup');
        $hmo->setMigrationId(2);
        $hmo->setMedicaid(true);
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Positive Healthcare');
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Simply Medicare');
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Humana');
        $hmo->setMigrationId(3);
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Care Plus HP');
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Freedom Health');
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Optimum Healthcare');
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Molina Healthcare');
        $hmo->setMigrationId(14);
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Coventry Health Care');
        $hmo->setMigrationId(12);
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Coventry Summit');
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Healthsun');
        $hmo->setMedicare(true);
        $hmo->setHandler($hCBH);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Staywell');
        $hmo->setMigrationId(9);
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hWF);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Allwell from Sunshine Health');
        $hmo->setMedicare(true);
        $hmo->setHandler($hC);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Simply Healthcare Plans');
        $hmo->setMigrationId(6);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Clear Health Alliance');
        $hmo->setMedicaid(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Magellan Complete Care');
        $hmo->setMigrationId(19);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hMCC);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('CMS');
        $hmo->setMedicaid(true);
        $hmo->setHandler($hCMS);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Sunshine Health');
        $hmo->setMigrationId(5);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hC);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('United');
        $hmo->setMigrationId(13);
        $hmo->setMedicaid(true);
        $hmo->setMedicare(true);
        $hmo->setHandler($hUnited);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Prestige HC');
        $hmo->setMigrationId(15);
        $hmo->setMedicaid(true);
        $hmo->setMedicare(true);
        $hmo->setHandler($hUnited);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Better');
        $hmo->setMigrationId(18);
        $hmo->setMedicaid(true);
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Staywell Kids Florida');
        $hmo->setMigrationId(20);
        $hmo->setMedicare(true);
        $hmo->setMedicaid(true);
        $hmo->setHandler($hWF);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('Beacon');
        $hmo->setMigrationId(21);
        $hmo->setMedicaid(true);
        $hmo->setMedicare(true);
        $hmo->setHandler($hBHO);
        $manager->persist($hmo);
        $manager->flush();

        $hmo = new Hmo();
        $hmo->setName('NA');
        $hmo->setHandler($hNA);
        $manager->persist($hmo);
        $manager->flush();

        //MODIFIERS ----------------------------------------------------------
        $modifierHp = new ServiceModifier();
        $modifierHp->setCode('HP');
        $manager->persist($modifierHp);
        $manager->flush();

        $modifierHo = new ServiceModifier();
        $modifierHo->setCode('HO');
        $manager->persist($modifierHo);
        $manager->flush();

        $modifierGt = new ServiceModifier();
        $modifierGt->setCode('GT');
        $manager->persist($modifierGt);
        $manager->flush();

        $modifierTs = new ServiceModifier();
        $modifierTs->setCode('TS');
        $manager->persist($modifierTs);
        $manager->flush();

        $modifierHn = new ServiceModifier();
        $modifierHn->setCode('HN');
        $manager->persist($modifierHn);
        $manager->flush();

        $modifierHe = new ServiceModifier();
        $modifierHe->setCode('HE');
        $manager->persist($modifierHe);
        $manager->flush();

        $modifierHr = new ServiceModifier();
        $modifierHr->setCode('HR');
        $manager->persist($modifierHr);
        $manager->flush();

        $modifierHq = new ServiceModifier();
        $modifierHq->setCode('HQ');
        $manager->persist($modifierHq);
        $manager->flush();

        $modifierHf = new ServiceModifier();
        $modifierHf->setCode('HF');
        $manager->persist($modifierHf);
        $manager->flush();

        //Services Codes --------------------------------------------------------------------------
        $serviceCodeH0001 = new ServiceCode();
        $serviceCodeH0001->setCode('H0001');
        $manager->persist($serviceCodeH0001);
        $manager->flush();

        $serviceCodeH0020 = new ServiceCode();
        $serviceCodeH0020->setCode('H0020');
        $manager->persist($serviceCodeH0020);
        $manager->flush();

        $serviceCodeH0031 = new ServiceCode();
        $serviceCodeH0031->setCode('H0031');
        $manager->persist($serviceCodeH0031);
        $manager->flush();

        $serviceCodeH0032 = new ServiceCode();
        $serviceCodeH0032->setCode('H0032');
        $manager->persist($serviceCodeH0032);
        $manager->flush();

        $serviceCodeH0046 = new ServiceCode();
        $serviceCodeH0046->setCode('H0046');
        $manager->persist($serviceCodeH0046);
        $manager->flush();

        $serviceCodeH0047 = new ServiceCode();
        $serviceCodeH0047->setCode('H0047');
        $manager->persist($serviceCodeH0047);
        $manager->flush();

        $serviceCodeH0048 = new ServiceCode();
        $serviceCodeH0048->setCode('H0048');
        $manager->persist($serviceCodeH0048);
        $manager->flush();

        $serviceCodeH2000 = new ServiceCode();
        $serviceCodeH2000->setCode('H2000');
        $manager->persist($serviceCodeH2000);
        $manager->flush();

        $serviceCodeH2010 = new ServiceCode();
        $serviceCodeH2010->setCode('H2010');
        $manager->persist($serviceCodeH2010);
        $manager->flush();

        $serviceCodeH2012 = new ServiceCode();
        $serviceCodeH2012->setCode('H2012');
        $manager->persist($serviceCodeH2012);
        $manager->flush();

        $serviceCodeH2017 = new ServiceCode();
        $serviceCodeH2017->setCode('H2017');
        $manager->persist($serviceCodeH2017);
        $manager->flush();

        $serviceCodeH2019 = new ServiceCode();
        $serviceCodeH2019->setCode('H2019');
        $manager->persist($serviceCodeH2019);
        $manager->flush();

        $serviceCodeH2030 = new ServiceCode();
        $serviceCodeH2030->setCode('H2030');
        $manager->persist($serviceCodeH2030);
        $manager->flush();

        $serviceCodeT1007 = new ServiceCode();
        $serviceCodeT1007->setCode('T1007');
        $manager->persist($serviceCodeT1007);
        $manager->flush();

        $serviceCodeT1015 = new ServiceCode();
        $serviceCodeT1015->setCode('T1015');
        $manager->persist($serviceCodeT1015);
        $manager->flush();

        $serviceCodeT1017 = new ServiceCode();
        $serviceCodeT1017->setCode('T1017');
        $manager->persist($serviceCodeT1017);
        $manager->flush();

        //PDF forms---------------------------------------------------------------------------------
        $dischargeCmh = new PdfForm();
        $dischargeCmh->setName('Discharge Summary v1.7');
        $dischargeCmh->setFilename('psychiatric_evaluation.pdf');
        $dischargeCmh->setSize('424018');
        $dischargeCmh->setMimeType('application/pdf');
        $manager->persist($dischargeCmh);
        $manager->flush();

        $dischargeTcm = new PdfForm();
        $dischargeTcm->setName('Discharge Summary v1.4');
        $dischargeTcm->setFilename('discharge_tcm.pdf');
        $dischargeTcm->setSize('386744');
        $dischargeTcm->setMimeType('application/pdf');
        $manager->persist($dischargeTcm);
        $manager->flush();

        $pe = new PdfForm();
        $pe->setName('Psychiatric Evaluation v1.1');
        $pe->setFilename('psychiatric_evaluation.pdf');
        $pe->setSize('595336');
        $pe->setMimeType('application/pdf');
        $manager->persist($pe);
        $manager->flush();

        $bio = new PdfForm();
        $bio->setName('Biopsychosocial v1.9');
        $bio->setFilename('biopsychosocial_assessment.pdf');
        $bio->setSize('1314933');
        $bio->setMimeType('application/pdf');
        $manager->persist($bio);
        $manager->flush();

        $brief = new PdfForm();
        $brief->setName('Brief new (Recovered)');
        $brief->setFilename('brief.pdf');
        $brief->setSize('188094');
        $brief->setMimeType('application/pdf');
        $manager->persist($brief);
        $manager->flush();

        $farsBlank = new PdfForm();
        $farsBlank->setName('FARS Blank');
        $farsBlank->setFilename('fars_blank.pdf');
        $farsBlank->setSize('1053399');
        $farsBlank->setMimeType('application/pdf');
        $manager->persist($farsBlank);
        $manager->flush();

        $mtp = new PdfForm();
        $mtp->setName('Master Treatment Plan v2.4');
        $mtp->setFilename('master_treatment_plan.pdf');
        $mtp->setSize('1314876');
        $mtp->setMimeType('application/pdf');
        $manager->persist($mtp);
        $manager->flush();

        $mtpa = new PdfForm();
        $mtpa->setName('MTP Addendum v1.4');
        $mtpa->setFilename('mtp_addendum.pdf');
        $mtpa->setSize('173967');
        $mtpa->setMimeType('application/pdf');
        $manager->persist($mtpa);
        $manager->flush();

        $tpr = new PdfForm();
        $tpr->setName('TPR v1.5');
        $tpr->setFilename('tpr.pdf');
        $tpr->setSize('408568');
        $tpr->setMimeType('application/pdf');
        $manager->persist($tpr);
        $manager->flush();

        $spa = new PdfForm();
        $spa->setName('Service Plan Addendum v1.6');
        $spa->setFilename('service_plan_addendum.pdf');
        $spa->setSize('868615');
        $spa->setMimeType('application/pdf');
        $manager->persist($spa);
        $manager->flush();

        $spog = new PdfForm();
        $spog->setName('Service Plan Opening Goal v1.2');
        $spog->setFilename('service_plan_opening_goal.pdf');
        $spog->setSize('981971');
        $spog->setMimeType('application/pdf');
        $manager->persist($spog);
        $manager->flush();

        //Services Parents--------------------------------------------------------------------------
        $spH2000Hp = new ServiceParent();
        $spH2000Hp->setName('Psychiatric evaluation by physician');
        $spH2000Hp->setCode($serviceCodeH2000);
        $spH2000Hp->addModifier($modifierHp);
        $spH2000Hp->setMedicare(true);
        $spH2000Hp->setMedicaid(true);
        $spH2000Hp->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2000Hp->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2000Hp->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2000Hp->setFeePerUnits(210);
        $spH2000Hp->setTimePerUnits(45);
        $spH2000Hp->setMaxUnits(1);
        $spH2000Hp->addForm($pe);
        $spH2000Hp->setAlwaysMaxDuration(true);
        if(!is_null($spH2000Hp->getMaxUnits()) && !is_null($spH2000Hp->getTimePerUnits())){
            $spH2000Hp->setMaxDuration($spH2000Hp->getMaxUnits() * $spH2000Hp->getTimePerUnits());
        }
        $spH2000Hp->setAmount(2);
        $spH2000Hp->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH2000Hp->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2000Hp);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2000Ho = new ServiceParent();
        $spH2000Ho->setName('Psychiatric evaluation by nonphysician');
        $spH2000Ho->setCode($serviceCodeH2000);
        $spH2000Ho->addModifier($modifierHo);
        $spH2000Ho->setMedicare(true);
        $spH2000Ho->setMedicaid(true);
        $spH2000Ho->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2000Ho->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2000Ho->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2000Ho->setFeePerUnits(150);
        $spH2000Ho->setTimePerUnits(45);
        $spH2000Ho->setMaxUnits(1);
        $spH2000Ho->addForm($pe);
        $spH2000Ho->setAlwaysMaxDuration(true);
        if(!is_null($spH2000Ho->getMaxUnits()) && !is_null($spH2000Ho->getTimePerUnits())){
            $spH2000Ho->setMaxDuration($spH2000Ho->getMaxUnits() * $spH2000Ho->getTimePerUnits());
        }
        $spH2000Ho->setAmount(2);
        $spH2000Ho->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH2000Ho->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2000Ho);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2010Ho = new ServiceParent();
        $spH2010Ho->setName('Brief behavioral health status exam');
        $spH2010Ho->setCode($serviceCodeH2010);
        $spH2010Ho->addModifier($modifierHo);
        $spH2010Ho->setMedicaid(true);
        $spH2010Ho->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2010Ho->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2010Ho->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2010Ho->setFeePerUnits(14.66);
        $spH2010Ho->setTimePerUnits(15);
        $spH2010Ho->setMaxUnits(2);
        $spH2010Ho->setPayRateCmh(14.66);
        $spH2010Ho->addForm($brief);
        if(!is_null($spH2010Ho->getMaxUnits()) && !is_null($spH2010Ho->getTimePerUnits())){
            $spH2010Ho->setMaxDuration($spH2010Ho->getMaxUnits() * $spH2010Ho->getTimePerUnits());
        }
        $spH2010Ho->setAmount(2.5);
        $spH2010Ho->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2010Ho->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2010Ho);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2010Gt = new ServiceParent();
        $spH2010Gt->setName('Brief behavioral health status exam');
        $spH2010Gt->setCode($serviceCodeH2010);
        $spH2010Gt->addModifier($modifierGt);
        $spH2010Gt->setMedicaid(true);
        $spH2010Gt->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2010Gt->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2010Gt->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2010Gt->setFeePerUnits(14.66);
        $spH2010Gt->setTimePerUnits(15);
        $spH2010Gt->setMaxUnits(2);
        $spH2010Gt->addForm($brief);
        if(!is_null($spH2010Gt->getMaxUnits()) && !is_null($spH2010Gt->getTimePerUnits())){
            $spH2010Gt->setMaxDuration($spH2010Gt->getMaxUnits() * $spH2010Gt->getTimePerUnits());
        }
        $spH2010Gt->setAmount(2.5);
        $spH2010Gt->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2010Gt->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2010Gt);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2000 = new ServiceParent();
        $spH2000->setName('Psychiatric review of records');
        $spH2000->setCode($serviceCodeH2000);
        $spH2000->setMedicaid(true);
        $spH2000->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2000->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2000->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_OB));
        $spH2000->setFeePerUnits(26);
        $spH2000->setFeePerService(true);
        if(!is_null($spH2000->getMaxUnits()) && !is_null($spH2000->getTimePerUnits())){
            $spH2000->setMaxDuration($spH2000->getMaxUnits() * $spH2000->getTimePerUnits());
        }
        $spH2000->setAmount(2);
        $spH2000->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH2000->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2000);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0031HoGt = new ServiceParent();
        $spH0031HoGt->setName('In-depth assessment, new patient, mental health');
        $spH0031HoGt->setCode($serviceCodeH0031);
        $spH0031HoGt->addModifier($modifierHo);
        $spH0031HoGt->addModifier($modifierGt);
        $spH0031HoGt->setMedicaid(true);
        $spH0031HoGt->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0031HoGt->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0031HoGt->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0031HoGt->setFeePerUnits(125);
        $spH0031HoGt->setFeePerService(true);
        $spH0031HoGt->addForm($bio);
        if(!is_null($spH0031HoGt->getMaxUnits()) && !is_null($spH0031HoGt->getTimePerUnits())){
            $spH0031HoGt->setMaxDuration($spH0031HoGt->getMaxUnits() * $spH0031HoGt->getTimePerUnits());
        }
        $spH0031HoGt->setAmount(1);
        $spH0031HoGt->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0031HoGt->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0031HoGt);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0031Ts = new ServiceParent();
        $spH0031Ts->setName('In-depth assessment, established patient, mental health');
        $spH0031Ts->setCode($serviceCodeH0031);
        $spH0031Ts->addModifier($modifierTs);
        $spH0031Ts->setMedicaid(true);
        $spH0031Ts->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0031Ts->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0031Ts->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0031Ts->setFeePerUnits(100);
        $spH0031Ts->setFeePerService(true);
        $spH0031Ts->addForm($bio);
        if(!is_null($spH0031Ts->getMaxUnits()) && !is_null($spH0031Ts->getTimePerUnits())){
            $spH0031Ts->setMaxDuration($spH0031Ts->getMaxUnits() * $spH0031Ts->getTimePerUnits());
        }
        $spH0031Ts->setAmount(1);
        $spH0031Ts->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0031Ts->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0031Ts);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0001Ho = new ServiceParent();
        $spH0001Ho->setName('In-depth assessment, new patient, substance abuse');
        $spH0001Ho->setCode($serviceCodeH0001);
        $spH0001Ho->addModifier($modifierHo);
        $spH0001Ho->setMedicaid(true);
        $spH0001Ho->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0001Ho->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0001Ho->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0001Ho->setFeePerUnits(125);
        $spH0001Ho->setFeePerService(true);
        if(!is_null($spH0001Ho->getMaxUnits()) && !is_null($spH0001Ho->getTimePerUnits())){
            $spH0001Ho->setMaxDuration($spH0001Ho->getMaxUnits() * $spH0001Ho->getTimePerUnits());
        }
        $spH0001Ho->setAmount(1);
        $spH0001Ho->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0001Ho->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0001Ho);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0001Ts = new ServiceParent();
        $spH0001Ts->setName('In-depth assessment, established patient, substance abuse');
        $spH0001Ts->setCode($serviceCodeH0001);
        $spH0001Ts->addModifier($modifierTs);
        $spH0001Ts->setMedicaid(true);
        $spH0001Ts->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0001Ts->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0001Ts->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0001Ts->setFeePerUnits(100);
        $spH0001Ts->setFeePerService(true);
        if(!is_null($spH0001Ts->getMaxUnits()) && !is_null($spH0001Ts->getTimePerUnits())){
            $spH0001Ts->setMaxDuration($spH0001Ts->getMaxUnits() * $spH0001Ts->getTimePerUnits());
        }
        $spH0001Ts->setAmount(1);
        $spH0001Ts->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0001Ts->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0001Ts);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0031Hn = new ServiceParent();
        $spH0031Hn->setName('Bio-psychosocial Evaluation');
        $spH0031Hn->setCode($serviceCodeH0031);
        $spH0031Hn->addModifier($modifierHn);
        $spH0031Hn->setMedicaid(true);
        $spH0031Hn->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0031Hn->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0031Hn->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0031Hn->setFeePerUnits(48);
        $spH0031Hn->setTimePerUnits(45);
        $spH0031Hn->setMaxUnits(1);
        $spH0031Hn->setAlwaysMaxDuration(true);
        $spH0031Hn->setPayRateCmh(50);
        $spH0031Hn->addForm($bio);
        $spH0031Hn->addForm($farsBlank);
        if(!is_null($spH0031Hn->getMaxUnits()) && !is_null($spH0031Hn->getTimePerUnits())){
            $spH0031Hn->setMaxDuration($spH0031Hn->getMaxUnits() * $spH0031Hn->getTimePerUnits());
        }
        $spH0031Hn->setAmount(1);
        $spH0031Hn->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0031Hn->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0031Hn);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2019 = new ServiceParent();
        $spH2019->setName('Psychological testing');
        $spH2019->setCode($serviceCodeH2019);
        $spH2019->setMedicaid(true);
        $spH2019->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2019->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH2019->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2019->setFeePerUnits(15);
        $spH2019->setTimePerUnits(15);
        if(!is_null($spH2019->getMaxUnits()) && !is_null($spH2019->getTimePerUnits())){
            $spH2019->setMaxDuration($spH2019->getMaxUnits() * $spH2019->getTimePerUnits());
        }
        $spH2019->setAmount(10);
        $spH2019->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2019->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2019);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0031 = new ServiceParent();
        $spH0031->setName('Limited functional assessment, mental health');
        $spH0031->setCode($serviceCodeH0031);
        $spH0031->setMedicaid(true);
        $spH0031->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0031->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0031->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0031->setFeePerUnits(15);
        $spH0031->setFeePerService(true);
        if(!is_null($spH0031->getMaxUnits()) && !is_null($spH0031->getTimePerUnits())){
            $spH0031->setMaxDuration($spH0031->getMaxUnits() * $spH0031->getTimePerUnits());
        }
        $spH0031->setAmount(3);
        $spH0031->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0031->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0031);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0001 = new ServiceParent();
        $spH0001->setName('Limited functional assessment, substance abuse');
        $spH0001->setCode($serviceCodeH0001);
        $spH0001->setMedicaid(true);
        $spH0001->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0001->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0001->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0001->setFeePerUnits(15);
        $spH0001->setFeePerService(true);
        if(!is_null($spH0001->getMaxUnits()) && !is_null($spH0001->getTimePerUnits())){
            $spH0001->setMaxDuration($spH0001->getMaxUnits() * $spH0001->getTimePerUnits());
        }
        $manager->persist($spH0001);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0032 = new ServiceParent();
        $spH0032->setName('Treatment plan development, new and established patient, mental health');
        $spH0032->setCode($serviceCodeH0032);
        $spH0032->setMedicaid(true);
        $spH0032->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0032->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION));
        $spH0032->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0032->setFeePerUnits(97);
        $spH0032->setTimePerUnits(45);
        $spH0032->setMaxUnits(1);
        $spH0032->setAlwaysMaxDuration(true);
        $spH0032->setPayRateCmh(40);
        $spH0032->addForm($mtp);
        $spH0032->addForm($mtpa);
        if(!is_null($spH0032->getMaxUnits()) && !is_null($spH0032->getTimePerUnits())){
            $spH0032->setMaxDuration($spH0032->getMaxUnits() * $spH0032->getTimePerUnits());
        }
        $spH0032->setAmount(2);
        $spH0032->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0032->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0032);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1007 = new ServiceParent();
        $spT1007->setName('Treatment plan development, new and established patient, substance abuse');
        $spT1007->setCode($serviceCodeT1007);
        $spT1007->setMedicaid(true);
        $spT1007->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spT1007->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1007->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1007->setFeePerUnits(97);
        $spT1007->setTimePerUnits(45);
        $spT1007->setMaxUnits(1);
        $spT1007->setAlwaysMaxDuration(true);
        if(!is_null($spT1007->getMaxUnits()) && !is_null($spT1007->getTimePerUnits())){
            $spT1007->setMaxDuration($spT1007->getMaxUnits() * $spT1007->getTimePerUnits());
        }
        $spT1007->setAmount(2);
        $spT1007->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spT1007->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spT1007);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0032Ts = new ServiceParent();
        $spH0032Ts->setName('Treatment plan review, mental health');
        $spH0032Ts->setCode($serviceCodeH0032);
        $spH0032Ts->addModifier($modifierTs);
        $spH0032Ts->setMedicaid(true);
        $spH0032Ts->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0032Ts->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH0032Ts->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0032Ts->setFeePerUnits(48.50);
        $spH0032Ts->setTimePerUnits(45);
        $spH0032Ts->setMaxUnits(1);
        $spH0032Ts->setAlwaysMaxDuration(true);
        $spH0032Ts->setPayRateCmh(20);
        $spH0032Ts->addForm($tpr);
        if(!is_null($spH0032Ts->getMaxUnits()) && !is_null($spH0032Ts->getTimePerUnits())){
            $spH0032Ts->setMaxDuration($spH0032Ts->getMaxUnits() * $spH0032Ts->getTimePerUnits());
        }
        $spH0032Ts->setAmount(4);
        $spH0032Ts->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH0032Ts->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0032Ts);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1007Ts = new ServiceParent();
        $spT1007Ts->setName('Treatment plan review, substance abuse');
        $spT1007Ts->setCode($serviceCodeT1007);
        $spT1007Ts->addModifier($modifierTs);
        $spT1007Ts->setMedicaid(true);
        $spT1007Ts->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spT1007Ts->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1007Ts->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1007Ts->setFeePerUnits(48.50);
        $spT1007Ts->setTimePerUnits(45);
        $spT1007Ts->setAlwaysMaxDuration(false);
        if(!is_null($spT1007Ts->getMaxUnits()) && !is_null($spT1007Ts->getTimePerUnits())){
            $spT1007Ts->setMaxDuration($spT1007Ts->getMaxUnits() * $spT1007Ts->getTimePerUnits());
        }
        $spT1007Ts->setAmount(4);
        $spT1007Ts->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spT1007Ts->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spT1007Ts);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1015 = new ServiceParent();
        $spT1015->setName('Medication management');
        $spT1015->setCode($serviceCodeT1015);
        $spT1015->setMedicaid(true);
        $spT1015->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spT1015->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1015->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1015->setFeePerUnits(60);
        $spT1015->setTimePerUnits(15);
        $spT1015->setMaxUnits(1);
        $spT1015->setAlwaysMaxDuration(true);
        if(!is_null($spT1015->getMaxUnits()) && !is_null($spT1015->getTimePerUnits())){
            $spT1015->setMaxDuration($spT1015->getMaxUnits() * $spT1015->getTimePerUnits());
        }
        $manager->persist($spT1015);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2010HeMh = new ServiceParent();
        $spH2010HeMh->setName('Brief individual medical psychotherapy, mental health');
        $spH2010HeMh->setCode($serviceCodeH2010);
        $spH2010HeMh->addModifier($modifierHe);
        $spH2010HeMh->setMedicaid(true);
        $spH2010HeMh->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2010HeMh->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2010HeMh->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2010HeMh->setFeePerUnits(15);
        $spH2010HeMh->setTimePerUnits(15);
        $spH2010HeMh->setMaxUnits(2);
        $spH2010HeMh->setAlwaysMaxDuration(false);
        if(!is_null($spH2010HeMh->getMaxUnits()) && !is_null($spH2010HeMh->getTimePerUnits())){
            $spH2010HeMh->setMaxDuration($spH2010HeMh->getMaxUnits() * $spH2010HeMh->getTimePerUnits());
        }
        $spH2010HeMh->setAmount(4);
        $spH2010HeMh->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH2010HeMh->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2010HeMh);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2010HeSa = new ServiceParent();
        $spH2010HeSa->setName('Brief individual medical psychotherapy, substance abuse');
        $spH2010HeSa->setCode($serviceCodeH2010);
        $spH2010HeSa->addModifier($modifierHe);
        $spH2010HeSa->setMedicaid(true);
        $spH2010HeSa->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2010HeSa->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2010HeSa->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2010HeSa->setFeePerUnits(15);
        $spH2010HeSa->setTimePerUnits(15);
        $spH2010HeSa->setMaxUnits(2);
        $spH2010HeSa->setAlwaysMaxDuration(false);
        if(!is_null($spH2010HeSa->getMaxUnits()) && !is_null($spH2010HeSa->getTimePerUnits())){
            $spH2010HeSa->setMaxDuration($spH2010HeSa->getMaxUnits() * $spH2010HeSa->getTimePerUnits());
        }
        $spH2010HeSa->setAmount(4);
        $spH2010HeSa->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spH2010HeSa->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2010HeSa);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0046 = new ServiceParent();
        $spH0046->setName('Behavioral health-related medical services: verbal interaction, mental health');
        $spH0046->setCode($serviceCodeH0046);
        $spH0046->setMedicaid(true);
        $spH0046->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0046->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH0046->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0046->setFeePerUnits(15);
        $spH0046->setTimePerUnits(15);
        $spH0046->setMaxUnits(1);
        $spH0046->setAlwaysMaxDuration(true);
        if(!is_null($spH0046->getMaxUnits()) && !is_null($spH0046->getTimePerUnits())){
            $spH0046->setMaxDuration($spH0046->getMaxUnits() * $spH0046->getTimePerUnits());
        }
        $spH0046->setAmount(52);
        $spH0046->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS));
        $spH0046->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0046);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0047 = new ServiceParent();
        $spH0047->setName('Behavioral health-related medical services: verbal interaction, substance abuse');
        $spH0047->setCode($serviceCodeH0047);
        $spH0047->setMedicaid(true);
        $spH0047->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0047->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH0047->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0047->setFeePerUnits(15);
        $spH0047->setTimePerUnits(15);
        $spH0047->setMaxUnits(1);
        $spH0047->setAlwaysMaxDuration(true);
        if(!is_null($spH0047->getMaxUnits()) && !is_null($spH0047->getTimePerUnits())){
            $spH0047->setMaxDuration($spH0047->getMaxUnits() * $spH0047->getTimePerUnits());
        }
        $spH0047->setAmount(52);
        $spH0047->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS));
        $spH0047->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0047);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1015HeMh = new ServiceParent();
        $spT1015HeMh->setName('Behavioral health-related medical services: medical procedures, mental health');
        $spT1015HeMh->setCode($serviceCodeT1015);
        $spT1015HeMh->addModifier($modifierHe);
        $spT1015HeMh->setMedicaid(true);
        $spT1015HeMh->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spT1015HeMh->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1015HeMh->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1015HeMh->setFeePerUnits(10);
        $spT1015HeMh->setTimePerUnits(15);
        $spT1015HeMh->setMaxUnits(1);
        $spT1015HeMh->setAlwaysMaxDuration(true);
        if(!is_null($spT1015HeMh->getMaxUnits()) && !is_null($spT1015HeMh->getTimePerUnits())){
            $spT1015HeMh->setMaxDuration($spT1015HeMh->getMaxUnits() * $spT1015HeMh->getTimePerUnits());
        }
        $spT1015HeMh->setAmount(52);
        $spT1015HeMh->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS));
        $spT1015HeMh->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spT1015HeMh);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1015HeSa = new ServiceParent();
        $spT1015HeSa->setName('Behavioral health-related medical services: medical procedures, substance abuse');
        $spT1015HeSa->setCode($serviceCodeT1015);
        $spT1015HeSa->addModifier($modifierHe);
        $spT1015HeSa->setMedicaid(true);
        $spT1015HeSa->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spT1015HeSa->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1015HeSa->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1015HeSa->setFeePerUnits(10);
        $spT1015HeSa->setTimePerUnits(15);
        $spT1015HeSa->setMaxUnits(1);
        $spT1015HeSa->setAlwaysMaxDuration(true);
        if(!is_null($spT1015HeSa->getMaxUnits()) && !is_null($spT1015HeSa->getTimePerUnits())){
            $spT1015HeSa->setMaxDuration($spT1015HeSa->getMaxUnits() * $spT1015HeSa->getTimePerUnits());
        }
        $spT1015HeSa->setAmount(52);
        $spT1015HeSa->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS));
        $spT1015HeSa->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spT1015HeSa);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH0048 = new ServiceParent();
        $spH0048->setName('Behavioral health-related medical services: alcohol and other drug screening specimen collection');
        $spH0048->setCode($serviceCodeH0048);
        $spH0048->setMedicaid(true);
        $spH0048->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH0048->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH0048->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH0048->setFeePerUnits(10);
        $spH0048->setTimePerUnits(15);
        $spH0048->setMaxUnits(1);
        $spH0048->setAlwaysMaxDuration(true);
        if(!is_null($spH0048->getMaxUnits()) && !is_null($spH0048->getTimePerUnits())){
            $spH0048->setMaxDuration($spH0048->getMaxUnits() * $spH0048->getTimePerUnits());
        }
        $spH0048->setAmount(52);
        $spH0048->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS));
        $spH0048->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH0048);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2019Hr = new ServiceParent();
        $spH2019Hr->setName('Individual and family therapy');
        $spH2019Hr->setCode($serviceCodeH2019);
        $spH2019Hr->addModifier($modifierHr);
        $spH2019Hr->setMedicaid(true);
        $spH2019Hr->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2019Hr->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2019Hr->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2019Hr->setFeePerUnits(18.33);
        $spH2019Hr->setTimePerUnits(15);
        $spH2019Hr->setMaxUnits(4);
        $spH2019Hr->setAlwaysMaxDuration(true);
        $spH2019Hr->setPayRateCmh(30);
        $spH2019Hr->addForm($dischargeCmh);
        $spH2019Hr->setHasDischarge(true);
        if(!is_null($spH2019Hr->getMaxUnits()) && !is_null($spH2019Hr->getTimePerUnits())){
            $spH2019Hr->setMaxDuration($spH2019Hr->getMaxUnits() * $spH2019Hr->getTimePerUnits());
        }
        $spH2019Hr->setAmount(26);
        $spH2019Hr->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2019Hr->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2019Hr);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2019Hq = new ServiceParent();
        $spH2019Hq->setName('Group therapy');
        $spH2019Hq->setCode($serviceCodeH2019);
        $spH2019Hq->addModifier($modifierHq);
        $spH2019Hq->setMedicaid(true);
        $spH2019Hq->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2019Hq->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2019Hq->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2019Hq->setFeePerUnits(6.67);
        $spH2019Hq->setTimePerUnits(15);
        $spH2019Hq->setMaxUnits(4);
        $spH2019Hq->setAlwaysMaxDuration(true);
        $spH2019Hq->setGroup(true);
        $spH2019Hq->addForm($dischargeCmh);
        $spH2019Hq->setHasDischarge(true);
        if(!is_null($spH2019Hq->getMaxUnits()) && !is_null($spH2019Hq->getTimePerUnits())){
            $spH2019Hq->setMaxDuration($spH2019Hq->getMaxUnits() * $spH2019Hq->getTimePerUnits());
        }
        $spH2019Hq->setAmount(39);
        $spH2019Hq->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2019Hq->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2019Hq);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2012 = new ServiceParent();
        $spH2012->setName('Behavioral health day services, mental health');
        $spH2012->setCode($serviceCodeH2012);
        $spH2012->setMedicaid(true);
        $spH2012->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2012->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2012->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2012->setFeePerUnits(12.50);
        $spH2012->setTimePerUnits(60);
        $spH2012->setAlwaysMaxDuration(false);
        if(!is_null($spH2012->getMaxUnits()) && !is_null($spH2012->getTimePerUnits())){
            $spH2012->setMaxDuration($spH2012->getMaxUnits() * $spH2012->getTimePerUnits());
        }
        $spH2012->setAmount(190);
        $spH2012->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2012->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2012);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2012Hf = new ServiceParent();
        $spH2012Hf->setName('Behavioral health day services, substance abuse');
        $spH2012Hf->setCode($serviceCodeH2012);
        $spH2012Hf->addModifier($modifierHf);
        $spH2012Hf->setMedicaid(true);
        $spH2012Hf->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2012Hf->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2012Hf->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2012Hf->setFeePerUnits(12.50);
        $spH2012Hf->setTimePerUnits(60);
        $spH2012Hf->setAlwaysMaxDuration(false);
        if(!is_null($spH2012Hf->getMaxUnits()) && !is_null($spH2012Hf->getTimePerUnits())){
            $spH2012Hf->setMaxDuration($spH2012Hf->getMaxUnits() * $spH2012Hf->getTimePerUnits());
        }
        $spH2012Hf->setAmount(190);
        $spH2012Hf->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS));
        $spH2012Hf->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_YEAR));
        $manager->persist($spH2012Hf);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2017 = new ServiceParent();
        $spH2017->setName('Psychosocial rehabilitation services');
        $spH2017->setCode($serviceCodeH2017);
        $spH2017->setMedicaid(true);
        $spH2017->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2017->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2017->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2017->setFeePerUnits(9);
        $spH2017->setTimePerUnits(15);
        $spH2017->setMaxUnits(16);
        $spH2017->setAlwaysMaxDuration(true);
        $spH2017->setGroup(true);
        $spH2017->setPayRateCmh(150);
        $spH2017->addForm($dischargeCmh);
        $spH2017->setHasDischarge(true);
        if(!is_null($spH2017->getMaxUnits()) && !is_null($spH2017->getTimePerUnits())){
            $spH2017->setMaxDuration($spH2017->getMaxUnits() * $spH2017->getTimePerUnits());
        }
        $manager->persist($spH2017);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spH2030 = new ServiceParent();
        $spH2030->setName('Clubhouse services');
        $spH2030->setCode($serviceCodeH2030);
        $spH2030->setMedicaid(true);
        $spH2030->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
        $spH2030->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spH2030->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spH2030->setFeePerUnits(5);
        $spH2030->setTimePerUnits(15);
        $spH2030->setMaxUnits(16);
        $spH2030->setAlwaysMaxDuration(false);
        if(!is_null($spH2030->getMaxUnits()) && !is_null($spH2030->getTimePerUnits())){
            $spH2030->setMaxDuration($spH2030->getMaxUnits() * $spH2030->getTimePerUnits());
        }
        $manager->persist($spH2030);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1017 = new ServiceParent();
        $spT1017->setName('Targeted Case Management');
        $spT1017->setCode($serviceCodeT1017);
        $spT1017->setMedicaid(true);
        $spT1017->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_TCM));
        $spT1017->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1017->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1017->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_OB));
        $spT1017->setFeePerUnits(12);
        $spT1017->addForm($spa);
        $spT1017->addForm($spog);
        $spT1017->addForm($dischargeTcm);
        $spT1017->setHasDischarge(true);
        if(!is_null($spT1017->getMaxUnits()) && !is_null($spT1017->getTimePerUnits())){
            $spT1017->setMaxDuration($spT1017->getMaxUnits() * $spT1017->getTimePerUnits());
        }
        $spT1017->setAmount(344);
        $spT1017->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spT1017->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_MONTH));
        $manager->persist($spT1017);
        $manager->flush();

        //------------------------------------------------------------------------------------------
        $spT1017Hk = new ServiceParent();
        $spT1017Hk->setName('Intensive Team Targeted Case Management for Adults (18 years or older)');
        $spT1017Hk->setCode($serviceCodeT1017);
        $spT1017Hk->setMedicaid(true);
        $spT1017Hk->setSpecialty(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_TCM));
        $spT1017Hk->setType(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION));
        $spT1017Hk->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_FTF));
        $spT1017Hk->addPresence(NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_PRESENCE_TYPE_OB));
        $spT1017Hk->setFeePerUnits(12);
        $spT1017Hk->addForm($spa);
        $spT1017Hk->addForm($spog);
        $spT1017Hk->addForm($dischargeTcm);
        $spT1017Hk->setHasDischarge(true);
        if(!is_null($spT1017Hk->getMaxUnits()) && !is_null($spT1017Hk->getTimePerUnits())){
            $spT1017Hk->setMaxDuration($spT1017Hk->getMaxUnits() * $spT1017Hk->getTimePerUnits());
        }
        $spT1017Hk->setAmount(48);
        $spT1017Hk->setModifierAmount(NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT));
        $spT1017Hk->setFrequency(NomUtil::getNomenclatorsByName(NomUtil::NOM_FREQUENCY_TYPE_DAY));
        $manager->persist($spT1017Hk);
        $manager->flush();

        //Setting Codes ------------------------------------------------------------------------------
        $sc = new SettingCode();
        $sc->setCode('03');
        $sc->setPlace('School');
        $manager->persist($sc);
        $manager->flush();

        $sc = new SettingCode();
        $sc->setCode('11');
        $sc->setPlace('Office');
        $manager->persist($sc);
        $manager->flush();

        $sc = new SettingCode();
        $sc->setCode('12');
        $sc->setPlace('Home');
        $manager->persist($sc);
        $manager->flush();

        $sc = new SettingCode();
        $sc->setCode('13');
        $sc->setPlace('ALF');
        $manager->persist($sc);
        $manager->flush();

        $sc = new SettingCode();
        $sc->setCode('53');
        $sc->setPlace('CMHC');
        $manager->persist($sc);
        $manager->flush();

        $sc = new SettingCode();
        $sc->setCode('99');
        $sc->setPlace('Other');
        $manager->persist($sc);
        $manager->flush();

        //Role by ServiceParent -----------------------------------------------------------------
        $serviceAcl = new ServiceAcl();
        $serviceAcl->setRole(User::ROLE_SPECIALIST);
        $serviceAcl->addService($spH2000Hp);
        $serviceAcl->addService($spH2000Ho);
        $serviceAcl->addService($spH2010Ho);
        $serviceAcl->addService($spH2010Gt);
        $serviceAcl->addService($spH2000);
        $serviceAcl->addService($spH0031HoGt);
        $serviceAcl->addService($spH0031Ts);
        $serviceAcl->addService($spH0001Ho);
        $serviceAcl->addService($spH0001Ts);
        $serviceAcl->addService($spH0031Hn);
        $serviceAcl->addService($spH2019);
        $serviceAcl->addService($spH0031);
        $serviceAcl->addService($spH0001);
        $serviceAcl->addService($spH0032);
        $serviceAcl->addService($spT1007);
        $serviceAcl->addService($spH0032Ts);
        $serviceAcl->addService($spT1015);
        $serviceAcl->addService($spH2010HeMh);
        $serviceAcl->addService($spH0046);
        $serviceAcl->addService($spH0047);
        $serviceAcl->addService($spT1015HeMh);
        $serviceAcl->addService($spH2019Hq);
        $serviceAcl->addService($spH2012);
        $serviceAcl->addService($spH2012Hf);
        $serviceAcl->addService($spH2030);
        $serviceAcl->addService($spT1007Ts);
        $serviceAcl->addService($spH2010HeSa);
        $serviceAcl->addService($spT1015HeSa);
        $serviceAcl->addService($spH0048);
        $manager->persist($serviceAcl);
        $manager->flush();

        $serviceAcl = new ServiceAcl();
        $serviceAcl->setRole(User::ROLE_CMH);
        $serviceAcl->addService($spH2019Hq);
        $serviceAcl->addService($spH2019Hr);
        $serviceAcl->addService($spH2017);
        $serviceAcl->addService($spH2010Ho);
        $serviceAcl->addService($spH2010Gt);
        $serviceAcl->addService($spH0031HoGt);
        $serviceAcl->addService($spH0031Ts);
        $serviceAcl->addService($spH0001Ho);
        $serviceAcl->addService($spH0001Ts);
        $serviceAcl->addService($spH0031Hn);
        $serviceAcl->addService($spH2019);
        $serviceAcl->addService($spH0031);
        $serviceAcl->addService($spH0001);
        $serviceAcl->addService($spH0032);
        $serviceAcl->addService($spT1007);
        $serviceAcl->addService($spH0032Ts);
        $serviceAcl->addService($spH2012);
        $serviceAcl->addService($spH2012Hf);
        $serviceAcl->addService($spH2030);
        $serviceAcl->addService($spT1007Ts);
        $manager->persist($serviceAcl);
        $manager->flush();

        $serviceAcl = new ServiceAcl();
        $serviceAcl->setRole(User::ROLE_TCM);
        $serviceAcl->addService($spT1017);
        $serviceAcl->addService($spT1017Hk);
        $manager->persist($serviceAcl);
        $manager->flush();
    }

    public function getOrder()
    {
        return 100;
    }
}
