<?php
/**
 * Created by PhpStorm.
 * User: caglez1992@gmail.com
 */

namespace Core\CoreBundle\DataFixtures;


use Core\CoreBundle\Entity\Setting;
use Core\CoreBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CoreFixtures extends Fixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null.
     *
     * @return void
     */

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('0');
        $user->setName("Admin");
        $user->setLastname("Worker");
        $user->setUsername("admin");
        $user->setPlainPassword("Florid@HC2019!!!");
        $user->setEmail("admin@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('a1');
        $user->setName("Admin1");
        $user->setLastname("Worker");
        $user->setUsername("admin1");
        $user->setPlainPassword("admin1@FHC2019!!!");
        $user->setEmail("admin1@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('a2');
        $user->setName("Admin2");
        $user->setLastname("Worker");
        $user->setUsername("admin2");
        $user->setPlainPassword("admin2@FHC2019!!!");
        $user->setEmail("admin2@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('a3');
        $user->setName("Admin3");
        $user->setLastname("Worker");
        $user->setUsername("admin3");
        $user->setPlainPassword("admin3@FHC2019!!!");
        $user->setEmail("admin3@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('a4');
        $user->setName("Admin4");
        $user->setLastname("Worker");
        $user->setUsername("admin4");
        $user->setPlainPassword("admin4@FHC2019!!!");
        $user->setEmail("admin4@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('a5');
        $user->setName("Admin5");
        $user->setLastname("Worker");
        $user->setUsername("admin5");
        $user->setPlainPassword("admin5@FHC2019!!!");
        $user->setEmail("admin5@admin.com");
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        /*$user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1001');
        $user->setName("Operation");
        $user->setLastname("Worker");
        $user->setUsername("op");
        $user->setPlainPassword("op");
        $user->setEmail("op@op.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_OPERATION_MANAGER);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1002');
        $user->setName("Therapist");
        $user->setLastname("Worker");
        $user->setUsername("therapist");
        $user->setPlainPassword("therapist");
        $user->setEmail("therapist@therapist.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_CMH);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1003');
        $user->setName("TCM");
        $user->setLastname("Worker");
        $user->setUsername("tcm");
        $user->setPlainPassword("tcm");
        $user->setEmail("tcm@tcm.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_TCM);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1004');
        $user->setName("Specialist");
        $user->setLastname("Worker");
        $user->setUsername("specialist");
        $user->setPlainPassword("specialist");
        $user->setEmail("specialist@specialist.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_SPECIALIST);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1005');
        $user->setName("Frontdesk");
        $user->setLastname("Worker");
        $user->setUsername("frontdesk");
        $user->setPlainPassword("frontdesk");
        $user->setEmail("front@front.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_FRONT_DESK);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1006');
        $user->setName("Supervisor");
        $user->setLastname("TCM");
        $user->setUsername("stcm");
        $user->setPlainPassword("stcm");
        $user->setEmail("stcm@stcm.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_SUPERVISOR_TCM);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1007');
        $user->setName("Supervisor");
        $user->setLastname("CMH");
        $user->setUsername("scmh");
        $user->setPlainPassword("scmh");
        $user->setEmail("scmh@scmh.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_SUPERVISOR_CMH);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1008');
        $user->setName("Billing");
        $user->setLastname("Worker");
        $user->setUsername("billing");
        $user->setPlainPassword("billing");
        $user->setEmail("billing@fhc.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_BILLING);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1009');
        $user->setName("QA TCM");
        $user->setLastname("Worker");
        $user->setUsername("qtcm");
        $user->setPlainPassword("qtcm");
        $user->setEmail("qtcm@fhc.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_QA_TCM);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1010');
        $user->setName("QA CMH");
        $user->setLastname("Worker");
        $user->setUsername("qcmh");
        $user->setPlainPassword("qcmh");
        $user->setEmail("qcmh@fhc.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_QA_CMH);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1011');
        $user->setName("AUTHORIZATION CHECKER");
        $user->setLastname("Worker");
        $user->setUsername("auth");
        $user->setPlainPassword("auth");
        $user->setEmail("auth@fhc.com");
        $user->addRole(User::ROLE_DEFAULT);
        $user->addRole(User::ROLE_AUTHORIZATION_CHECKER);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $user = $this->container->get('fos_user.user_manager')->createUser();
        $user->setWorkerNumber('w1012');
        $user->setName("Utilization Manager");
        $user->setLastname("Worker");
        $user->setUsername("utilization");
        $user->setPlainPassword("utilization");
        $user->setEmail("utilization@fhc.com");
        $user->addRole(User::ROLE_UTILIZATION_MANAGER);
        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(true);
        $this->container->get('fos_user.user_manager')->updateUser($user);*/

        $setting = new Setting();
        $setting->setActualCaseNumber(2520);
        $manager->persist($setting);
        $manager->flush();        
    }

    public function getOrder()
    {
        return 1;
    }
}
