<?php

namespace Core\CoreBundle\Controller;

use Core\CoreBundle\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class WelcomeController extends Controller
{
    public function indexAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $userReal = $this->get(UserManager::class)->getRealUser();

        $confirmed = $request->get('confirmed');

        if(!is_null($confirmed) && !is_null($userReal) && $confirmed){
            $message = $this->get('translator')->trans('registration.confirmed', array('%username%' => $userReal->getName()), 'app');
            $this->addFlash('success', $message);
        }
        
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }
}
    