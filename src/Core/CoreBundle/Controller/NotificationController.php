<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\CoreBundle\Controller;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Manager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    
    /**
     * @Route("/unseen", name="notifications_unseen_json", options={"expose"=true})
     * @return Response
     */
    public function getNotificationsUnSeenAction()
    {
        $doctrine = $this->get('doctrine');
        $notificationRepo = $doctrine->getRepository(Notification::class);

        $userReal = $this->get(UserManager::class)->getRealUser();

        $notifications = $notificationRepo->getUnseenNotifications($userReal);

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($notifications, 'json');

        //$notificationRepo->updateRaisedStatus($notifications);

        return new Response($json, 200, array(
            'Content-Type' => 'application/json',
        ));
    }

    /**
     * @Route("/render", name="render_notifications", options={"expose"=true})
     * @return Response
     */
    public function renderNotificationsAction()
    {
        $doctrine = $this->get('doctrine');
        $notificationRepo = $doctrine->getRepository(Notification::class);

        $userReal = $this->get(UserManager::class)->getRealUser();

        $notifications = $notificationRepo->getMyNotifications($userReal, false, 10);
        $amountNonRead = $notificationRepo->countNotifications($userReal, true);

        $unRaisedNotifications = $notificationRepo->getUnseenNotifications($userReal);
        $notificationRepo->updateRaisedStatus($unRaisedNotifications);

        return $this->render('@BackEnd/structures/admin/notification_ajax.html.twig', array(
            'notifications' => $notifications,
            'amountNonRead' => $amountNonRead,
            'hasUnread' => $amountNonRead != 0,
            'userReal' => $userReal,
        ));
    }
        
    /**
     * @Route("/read/all", name="read_notifications", options={"expose"=true})
     * @return Response
     */
    public function readNotificationsAction()
    {
        $userReal = $this->get(UserManager::class)->getRealUser();
        $this->get('doctrine')->getRepository(Notification::class)->readNotificationsByUser($userReal);
        return new Response('ok');
    }
}