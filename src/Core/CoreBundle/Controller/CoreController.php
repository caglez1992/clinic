<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\CoreBundle\Controller;

use Core\CoreBundle\Entity\Setting;
use DateTime;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CoreController extends Controller
{
    /**
     * @Route("/d/{filename}", name="d_d", options={"expose"=true})
     * @param string $filename
     * @return Response
     */
    public function dAction($filename)
    {
        $dirWeb = $this->container->getParameter('dir.web');
        $content = file_get_contents(sprintf('%s/d/%s.zip', $dirWeb, $filename));

        $response = new Response();
        $filename = sprintf('%s.zip', $filename);
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $filename);
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        $response->setContent($content);
        return $response;
    }

    /**
     * @Route("/automatic/deploy", name="automatic_deploy", options={"expose"=true})
     * @return Response
     */
    public function automaticDeployAction()
    {
        $d = $this->getDoctrine();
        $config = $d->getRepository(Setting::class)->find(1);
        $request = $this->get('request_stack')->getCurrentRequest();
        $emailUtil = $this->get('email.utils');

        $SECRET = 'Clinic2019'; // random string of characters; must match the "Secret" defined in your GitHub webhook
        $GITHUB_BRANCH = 'master'; // name of the git branch that you're deploying
        $EMAIL_RECIPIENT = 'caglez1992@gmail.com'; // your email address, where you'll receive notices of deploy successes/failures

        try {
            $serverHeaders = $request->headers->all();
            $event = isset($serverHeaders['x-gitlab-event']) ? $serverHeaders['x-gitlab-event'] : null;

            if (is_null($event) || $event[0] != 'Push Hook') {
                throw new Exception("400 Bad Request: go away X-Gitlab-Event");
            }

            $payload = file_get_contents('php://input');

            $secret = $serverHeaders['x-gitlab-token'];

            if($secret[0] != $SECRET){
                throw new Exception("403 Bad Secret");
            }

            // read the payload
            $data = json_decode($payload);
            if (is_null($data)) {
                throw new Exception('Failed to decode JSON payload');
            }

            // make sure it's the right branch
            $branchRef = $data->ref;
            if ($branchRef != 'refs/heads/'.$GITHUB_BRANCH) {
                die("Ignoring push to '$branchRef'");
            }

            $output = '';
            $errors = '';

            $rootProject = $this->getParameter('kernel.root_dir') . '/..';

            //DateBase Backup
            $now = new DateTime();
            $dbUser = $this->getParameter('database_user');
            $dbPass = $this->getParameter('database_password');
            $this->exec(sprintf('mysqldump -u %s -p%s clinic > %s/backups/clinic%s.sql',
                $dbUser,
                $dbPass,
                $rootProject,
                $now->format('Ymd')
            ), $output, $errors);

            //GosWebKill
            $this->exec(sprintf('crontab -r', $rootProject), $output);

            $pid = $config->getPidWebSocket();

            $output .= "PID WebSocket: $pid \n";

            if ($pid != '0') {
                $checkCommand = 'ps -p ' . $pid;
                exec($checkCommand, $response);

                if (isset($response[1]))
                    $output .= "Response: " . $response[1] . "\n";

                if (count($response) == 2 && strpos($response[1], 'php') !== false) {
                    $this->exec(sprintf('kill %s', $pid), $output, $errors);
                } else {
                    $output .= "Pid not found running \n";
                }
            }

            //Git Pull
            $this->exec(sprintf('cd %s && git reset --hard HEAD', $rootProject), $output);
            $this->exec(sprintf('cd %s && git pull', $rootProject), $output, $errors);

            $this->exec(sprintf('php %s/bin/console cache:clear --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('php %s/bin/console cache:warmup --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('php %s/bin/console doctrine:schema:update --force --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('php %s/bin/console core:fix:db --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('php %s/bin/console assets:install web --symlink --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('php %s/bin/console core:cron --env=prod', $rootProject), $output, $errors);
            $this->exec(sprintf('chmod 777 -R %s/var/cache/prod/', $rootProject), $output, $errors);


            echo sprintf('<pre>%s</pre>', $output);

            echo sprintf('<pre>%s</pre>', $errors);

            $mailBody = "Output of bash" . "</br>"
                . sprintf('<pre>%s</pre>', $output) . "</br>"
                . "Output of Errors:" . "</br>"
                . sprintf('<pre>%s</pre>', $errors) . "</br>"
                . 'That\'s all!';

            $emailUtil->sendGeneralEmail($EMAIL_RECIPIENT, 'Clinic FHS Automatic Deploy', $mailBody, null, false);

        } catch (Exception $e) {
            $emailUtil->sendGeneralEmail($EMAIL_RECIPIENT, 'Clinic FHS Automatic Deploy Error', sprintf('<pre>%s</pre>', strval($e)), null, false);
            echo sprintf('<pre>%s</pre>', strval($e));
        }

        return new Response('Finish Autodeploy');
    }

    public function exec($cmd, &$output = '', &$error = '')
    {
        $d = $this->getDoctrine();
        $config = $d->getRepository(Setting::class)->find(1);
        $SSH_USERNAME = $config->getSshUsername();
        $SSH_PASSWORD = $config->getSshPassword();

        // ssh into the local server
        $connection = ssh2_connect('127.0.0.1', 22);
        $authSuccess = ssh2_auth_password(
            $connection,
            $SSH_USERNAME,
            $SSH_PASSWORD
        );
        if (!$authSuccess) {
            throw new Exception('SSH authentication failure');
        }

        $output .= sprintf('Execute cmd: %s%s', $cmd, "\n");

        $stream = ssh2_exec($connection, $cmd);
        $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        stream_set_blocking($errorStream, TRUE);
        stream_set_blocking($stream, TRUE);

        $output .= stream_get_contents($stream);
        $error .= stream_get_contents($errorStream);

        fclose($stream);
    }


}