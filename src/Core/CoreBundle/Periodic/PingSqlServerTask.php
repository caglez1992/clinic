<?php

namespace Core\CoreBundle\Periodic;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Service("ping.sql.server.tasks", public=true)
 * @Tag("gos_web_socket.periodic")
 */
class PingSqlServerTasks implements PeriodicInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @InjectParams({
     *     "em" = @Inject("doctrine.orm.entity_manager"),
     *     "logger" = @Inject("logger")
     * })
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $em = null, LoggerInterface $logger = null)
    {
        $this->em = $em;
        $this->logger = null === $logger ? new NullLogger() : $logger;
    }

    /**
     * This function is executed every 1 minute.
     *
     * For more advanced functionality, try injecting a Topic Service to perform actions on your connections every x seconds.
     */
    public function tick()
    {
        if (null === $this->em) {
            $this->logger->critical('Unable to ping sql server, service pdo is unavailable');
            return;
        }

        try {
            //$startTime = microtime(true);
            $this->em->getConnection()->query('SELECT 1');
            //$endTime = microtime(true);
            //$this->logger->info(sprintf('Successfully ping sql server (~%s ms)', round(($endTime - $startTime) * 100000), 2));
        } catch (\PDOException $e) {
            $this->logger->critical('Sql server is gone, and unable to reconnect');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        return 120;
    }
}