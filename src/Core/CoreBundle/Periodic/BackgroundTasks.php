<?php

namespace Core\CoreBundle\Periodic;

use Core\CoreBundle\Manager\BaseManager;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use DateTimeZone;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Service("background.tasks", public=true)
 * @Tag("gos_web_socket.periodic")
 */
class BackgroundTasks implements PeriodicInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @InjectParams({
     *     "container" = @Inject("service_container")
     * })
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null){
        $this->container = $container;
    }

    /**
     * This function is executed every 1 minute.
     *
     * For more advanced functionality, try injecting a Topic Service to perform actions on your connections every x seconds.
     */
    public function tick()
    {
        $doctrine = $this->container->get('doctrine');

        //Update weeks
        $now = new DateTime();
        $year = (int)$now->format('Y');
        $hasWeek = $doctrine->getRepository('WorkerBundle:Week')->hasYearWeeks($year);
        if(!$hasWeek){
            $this->generateWeeks($year);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        //return 1 minute;
        return 60*60;
    }

    /**
     * @param integer $year
     */
    public function generateWeeks($year){

        $bm = $this->container->get(BaseManager::class);
        $tz = new DateTimeZone('UTC');

        $initWeek = new DateTime("$year-01-01 00:00:00", $tz);

        $dayOfWeek = (int) $initWeek->format('N');

        //No interesa el primer dia, sino la primera semana del anno. Lleva a la primera semana del anno.
        if($dayOfWeek != 1){
            $initWeek->setTimestamp($initWeek->getTimestamp() + (86400 * (8 - $dayOfWeek)));
        }

        $number = 1;
        while( (int)$initWeek->format('Y') == $year ){
            $week = new Week();
            $week->setNumber($number);
            $week->setYear($year);
            $week->setFrom($initWeek);
            $to = new DateTime('now', $tz);
            $to->setTimestamp($initWeek->getTimestamp() + (86400 * 6) + 86399);
            $week->setTo($to);

            $bm->save($week);

            $number++;
            //$initWeek->setTimestamp($initWeek->getTimestamp() + 86400 * 7);
            $initWeek->modify('+1 week');
        }
    }
}