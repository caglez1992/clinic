<?php

namespace Core\CoreBundle\Periodic;

use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Patient;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Service("background.eligibility.tasks", public=true)
 * @Tag("gos_web_socket.periodic")
 */
class BackgroundEligibilityTasks implements PeriodicInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @InjectParams({
     *     "container" = @Inject("service_container"),
     *     "logger" = @Inject("logger")
     * })
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container = null, LoggerInterface $logger = null){
        $this->container = $container;
        $this->logger = null === $logger ? new NullLogger() : $logger;
    }
    
    
    public function tick()
    {
        $doctrine = $this->container->get('doctrine');

        try {
            $openPatients = $doctrine->getRepository(Patient::class)->getOpenPatients();
            $now = new DateTime();

            foreach ($openPatients as $patient) {
                $lastRunDate = $doctrine->getRepository(InsuranceEligibility::class)->getLastRunDateByPatient($patient);
                $lastRunDate = DateTime::createFromFormat('Y-m-d H:i:s', $lastRunDate);

                $date = $lastRunDate->format('Y-m-d');
                $date = explode('-', $date);

                $date[1]++;
                if ($date[1] == 13) {
                    $date[1] = 1;
                    $date[0]++;
                }

                $lastRunDate->setDate($date[0], $date[1], $date[2]);

                //TODO: Fix role ROLE_SUPER_ADMIN in notification.
                if ($lastRunDate->format('Y-m-d') == $now->format('Y-m-d')) {
                    $url = $this->container->get('sonata.admin.pool')->getAdminByClass(Patient::class)->generateObjectUrl('edit', $patient);
                    $this->container->get(NotificationManager::class)->createNotification(
                        [User::ROLE_SUPER_ADMIN, User::ROLE_OPERATION_MANAGER, User::ROLE_FRONT_DESK],
                        sprintf('Is necessary to run eligibility on patient %s', $patient->getFullName()),
                        $url,
                        'success', true, [], false
                    );
                }

            }
        }catch (\Exception $e){
            $this->logger->critical('Eligibility Background Periodic Alerts errors', $e);
        }
        
    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        //return 1 day;
        return 86400;
    }
}