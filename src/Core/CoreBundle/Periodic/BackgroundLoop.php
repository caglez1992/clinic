<?php

namespace Core\CoreBundle\Periodic;

use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Manager\PatientManager;
use DateTime;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Service("background.eligibility.tasks", public=true)
 * @Tag("gos_web_socket.periodic")
 */
class BackgroundLoop implements PeriodicInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @InjectParams({
     *     "container" = @Inject("service_container"),
     *     "logger" = @Inject("logger")
     * })
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container = null, LoggerInterface $logger = null){
        $this->container = $container;
        $this->logger = null === $logger ? new NullLogger() : $logger;
    }
    
    public function tick()
    {
        //Update Authorizations automatically.
        try {
            $now = new DateTime();
            if($now->format('H:i') == '00:01'){
                $d = $this->container->get('doctrine');
                $openPatients = $d->getRepository(Patient::class)->getOpenPatients();
                foreach ($openPatients as $patient) {
                    $this->container->get(PatientManager::class)->updateAuthorizationStatus($patient);
                }
            }
        }catch (\Exception $e){
            $this->logger->critical('Background loop error: can not update authorizations', $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        //return 1 minute;
        return 120;
    }
}