<?php

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Util\NomUtil;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class LogAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('remove');
        $collection->remove('show');
        $collection->remove('batch');
        $collection->remove('edit');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('classLabel', null, array(
                'label' => 'form.log.class.name',
            ))
            ->add('identifier', null, array(
                'label' => 'form.log.identifier',
            ))
            ->add('field', null, array(
                'label' => 'form.log.field',
            ))
            ->add('user', null, array(
                'label' => 'form.log.user',
            ))
            ->add('action', null, ['label' => 'form.log.action'], 'entity', [
                //'class' => 'Core\CoreBundle\Entity\Nomenclator',
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' => function(EntityRepository $er){
                    return  $er->createQueryBuilder('n')
                        ->where('n.type = :type')
                        ->setParameter('type', NomUtil::NOM_ACTION_TYPE);
                },
            ])
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'form.log.created.at',
                'field_type' => 'sonata_type_date_picker',
            ), null, array('format' => 'd/MM/yyyy'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            /*->add('id', null, array(
                'label' => 'general.code',
            ))*/
            ->add('classLabel', null, array(
                'label' => 'form.log.class.name',
            ))
            ->add('identifier', null, array(
                'label' => 'form.log.identifier',
            ))
            ->add('field', null, array(
                'label' => 'form.log.field',
            ))
            ->add('previousValue', null, array(
                'label' => 'form.log.previous.value',
            ))
            ->add('actualValue', null, array(
                'label' => 'form.log.actual.value',
            ))
            ->add('user', null, array(
                'label' => 'form.log.user',
            ))
            ->add('action', null, array(
                'label' => 'form.log.action',
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.log.created.at',
            ))
            /*->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ))*/
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /*$showMapper
            ->add('className', null, array(
                'label' => 'form.log.className',
            ))
            ->add('identifier', null, array(
                'label' => 'form.log.identifier',
            ))
            ->add('field', null, array(
                'label' => 'form.log.field',
            ))
            ->add('previousValue', null, array(
                'label' => 'form.log.previousValue',
            ))
            ->add('actualValue', null, array(
                'label' => 'form.log.actualValue',
            ))
            ->add('id', null, array(
                'label' => 'form.log.id',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.log.createdAt',
            ))
            ->add('updatedAt', null, array(
                'label' => 'form.log.updatedAt',
            ))
        ;*/
    }

    public function toString($object)
    {
        if($object instanceof LogAdmin && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.log',array(),'BackEndBundle');
    }
}
