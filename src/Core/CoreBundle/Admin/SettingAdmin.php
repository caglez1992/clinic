<?php

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SettingAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'form.setting.id',
            ))
            ->add('actualCaseNumber', null, array(
                'label' => 'form.setting.actualCaseNumber',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'form.setting.id',
            ))
            ->add('actualCaseNumber', null, array(
                'label' => 'form.setting.actualCaseNumber',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('actualCaseNumber', null, array(
                'label' => 'Actual Case Number',
            ))
            ->add('googleMapsApiKey')
            ->add('sshUsername')
            ->add('sshPassword')
            ->add('pidWebSocket', null, [
                'label' => 'PID WS'
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.setting.id',
            ))
            ->add('actualCaseNumber', null, array(
                'label' => 'form.setting.actualCaseNumber',
            ))
        ;
    }

    public function toString($object)
    {
        return $this->trans('menu.entity.setting',array(),'BackEndBundle');
    }
}
