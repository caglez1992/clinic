<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/4/17
 * Time: 3:47 PM
 */

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Patient;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\DependencyInjection\ContainerInterface;


class BaseAdmin extends AbstractAdmin
{
    /*protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
        $collection->remove('create');
        $collection->remove('delete');
    }*/

    /*protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );*/

    /*public function createQuery($context = 'list')
    {
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);

        if($userReal->isServiceWorker()){
            $root = $query->getRootAliases()[0];
            $query->andWhere("$root.worker = :userReal");
            $query->setParameter('userReal', $userReal);
        }
        return $query;
    }*/

    protected $divideList = false;

    protected $fieldsetForm = [];

    protected $customLabelMenu = false;

    protected $createRoute = true;

    /**
     * Predefined per page options.
     *
     * @var array
     */
    protected $perPageOptions = [16, 32, 64];

    /*protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
    }*/

    /**
     * @return boolean
     */
    public function isDivideList()
    {
        return $this->divideList;
    }

    /**
     * @param boolean $divideList
     */
    public function setDivideList($divideList)
    {
        $this->divideList = $divideList;
    }

    /**
     * @return mixed
     */
    public function getFieldsetForm()
    {
        return $this->fieldsetForm;
    }

    /**
     * @return boolean
     */
    public function isCreateRoute()
    {
        return $this->createRoute;
    }

    /**
     * @param $field
     * @param $value
     * @param null $label
     * @return array
     */
    public function addFieldsetForm($field, $value, $label = null)
    {
        $content = array(
            'value' => $value,
            'label' => $label,
        );
        $this->fieldsetForm[$field] = $content;
    }

    /**
     * @return boolean
     */
    public function getCustomLabelMenu()
    {
        if(!$this->customLabelMenu)
            return $this->label;
        return $this->customLabelMenu;
    }

    /**
     * @param boolean $customLabelMenu
     */
    public function setCustomLabelMenu($customLabelMenu)
    {
        $this->customLabelMenu = $customLabelMenu;
    }
    
    /**
     * Query CallBack to Filter fields dateTime.
     * @param QueryBuilder $queryBuilder
     * @param string $alias
     * @param string $field
     * @param array $value
     * @return bool|void
     */
    public function filterDate($queryBuilder, $alias, $field, $value)
    {
        $date = $value['value'];
        if (!$date)
            return;

        $dateEndFilter = new DateTime();
        $dateEndFilter->setTimestamp($date->getTimestamp() + 86400 - 1);

        $queryBuilder->andWhere("$alias.$field >= :date")
            ->andWhere("$alias.$field <= :dateEndFilter")
            ->setParameter('date', $date)
            ->setParameter('dateEndFilter', $dateEndFilter);
        return true;
    }

    /**
     * Devuelve todas las entidades de las relaciones One to Many filtrdas por al libres y las que le pertenece a la entidad.
     * Usada para no sobrescribir las llaves foranes de als entidades del Many ya usadas por otras relaciones.
     * @param string $bundle
     * @param string $entity
     * @param string $property
     * @param string $fqdn
     *
     * @return mixed
     */
    public function getQueryOneToMany($bundle, $entity, $property, $fqdn){
        $dql = 'SELECT obj FROM '.$bundle.':'.$entity.' obj WHERE obj.'.$property.' IS null';
        if($this->getSubject() != null && $this->getSubject()->getId() != null)
            $dql .= ' OR obj.'.$property.' = :object';

        $em = $this->modelManager->getEntityManager($fqdn);
        $query = $em->createQuery($dql);

        if($this->getSubject() != null && $this->getSubject()->getId() != null)
            $query->setParameter('object', $this->getSubject());

        return $query;
    }
    
    /**
     * Devuelve todos los nomencladores filtrados por un tipo de nomenclador espesifico.
     * @param string $nomType
     * @return mixed
     */
    public function getQueryNomenclators($nomType){
        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(Nomenclator::class);
        $query = $em->createQuery('SELECT n FROM CoreBundle:Nomenclator n WHERE n.type = :nom_type');
        return $query->setParameter('nom_type', $nomType);
    }

    /**
     * @param EntityRepository $er
     * @param string $nomType
     * @return QueryBuilder
     */
    public function getQueryBuilderNomenclators(EntityRepository $er, $nomType){
        return $er->createQueryBuilder('n')
            ->where('n.type = :nom_type')
            ->setParameter('nom_type', $nomType);
    }

    /**
     * @param array $roles
     * @return \Doctrine\ORM\Query
     */
    public function getQueryUserByRoles($roles = []){
        $sql = 'SELECT u FROM CoreBundle:User u';

        if(count($roles) > 0){
            $sql .= ' WHERE ';
        }

        foreach ($roles as $rol){
            $sql .= "u.roles LIKE '%$rol%' OR ";
        }

        if(count($roles) > 0){
            $sql = substr("$sql",0,strrpos($sql,' OR '));
        }

        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(User::class);
        return $em->createQuery($sql);
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getPatientsActive(){
        $nomStatusOpen = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN);
        $sql = 'SELECT p FROM PatientBundle:Patient p WHERE p.patientStatus = :status';

        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(Patient::class);
        return $em->createQuery($sql)->setParameter('status', $nomStatusOpen);
    }

    /**
     * {@inheritdoc}
     */

    public function getRequest()
    {
        if (!$this->request) {
            //  throw new \RuntimeException('The Request object has not been set');
            $this->request = $this->getConfigurationPool()->getContainer()->get('Request');
        }
        return $this->request;
    }

    public function isCreateMode(){
        return ($this->id($this->getSubject()))?(false):(true);
    }

    public function container(){
        return $this->getConfigurationPool()->getContainer();
    }

    public function get($service){
        return $this->getConfigurationPool()->getContainer()->get($service);
    }

    /**
     * @return User
     */
    public function getRealUser(){
        return  $this->container()->get(UserManager::class)->getRealUser();
    }

    public function save($object){
        $this->container()->get(BaseManager::class)->save($object);
    }

    public function g($role){
        return $this->container()->get('security.authorization_checker')->isGranted($role);
    }
}