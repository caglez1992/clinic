<?php

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Form\Type\MyFileType;
use Core\CoreBundle\Form\Type\XalixFileType;
use Core\CoreBundle\Manager\NotificationManager;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Core\CoreBundle\Util\NomUtil;
use Symfony\Component\Validator\Constraints\Count;

class UserAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);
        $root = $query->getRootAliases()[0];

        if ($userReal->isSupervisor()) {

            $query->andWhere("$root IN (:assignedWorkers)");
            $query->setParameter('assignedWorkers', $userReal->getAssignedWorkers());
        }

        if ($userReal->hasRole(User::ROLE_QA_CMH)){
            $query->andWhere("$root.roles LIKE :rol1 OR $root.roles LIKE :rol2 OR $root.roles LIKE :rol3");
            $query->setParameter('rol1', '%' . User::ROLE_SUPERVISOR_CMH . '%');
            $query->setParameter('rol2', '%' . User::ROLE_CMH . '%');
            $query->setParameter('rol3', '%' . User::ROLE_SPECIALIST . '%');
        }

        if ($userReal->hasRole(User::ROLE_QA_TCM)){
            $query->andWhere("$root.roles LIKE :rol1 OR $root.roles LIKE :rol2");
            $query->setParameter('rol1', '%' . User::ROLE_SUPERVISOR_TCM . '%');
            $query->setParameter('rol2', '%' . User::ROLE_TCM . '%');
        }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = (!$this->getSubject() || is_null($this->getSubject()->getId())) ? 'Registration' : 'Profile';
        //$options['validation_groups'] = array('Default', 'Registration');

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        // avoid security field to be exported
        return array_filter(parent::getExportFields(), function ($v) {
            return !in_array($v, array('password', 'salt'));
        });
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager')->updateCanonicalFields($user);
        $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager')->updatePassword($user);;
    }

    /**
     * @param User $object
     */
    public function postPersist($object)
    {
        $url = $this->generateUrl('list');
        $this->container()->get(NotificationManager::class)->createNotification([User::ROLE_SUPER_ADMIN, User::ROLE_OPERATION_MANAGER], 'notification.add.new.user', $url, Notification::SUCCESS);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('workerNumber')
            ->add('name')
            ->add('lastname')
            ->add('username')
            ->add('email')
        ;
    }

    public function configureBatchActions($actions)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('workerNumber', null, array(
                'label' => 'Number',
            ))
            /*->add('avatar', 'image', array(
                'height' => 45,
                'label' => 'general.picture',
            ))*/
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('username', null, array(
                'label' => 'general.username',
            ))
            ->add('roles', null, array(
                'label' => 'Roles',
                'template' => '@BackEnd/structures/show_field_array.html.twig'
            ))
            /*->add('email', null, array(
                'label' => 'general.email',
            ))*/
            ->add('enabled', null, array(
                'label' => 'general.enabled',
            ))
            ->add('lastLogin', null, array(
                'label' => 'form.user.last.login',
                'format'=>'d/M/Y',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'show_worker' => ['template' => '@BackEnd/btn_actions/worker/show_worker.html.twig'],
                    'edit' => ['label' => false],
                    'delete' => ['label' => false],
                    'impersonate' => ['template' => '@BackEnd/btn_actions/worker/impersonation.html.twig'],
                )
            ))
        ;
    }



    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $rolesOptions = [
            'label' => 'general.roles',
            'multiple' => true,
            'constraints' => [
                new Count(['min' => 1, 'max' => 1, 'groups' => ['Registration', 'Profile']]),
            ]
        ];

        $rolesOptions['choices'] = array(
            //'User' => 'ROLE_USER',
            'Front Desk' => 'ROLE_FRONT_DESK',
            'Human Resource' => 'ROLE_HR',
            'Authorization Checker' => 'ROLE_AUTHORIZATION_CHECKER',
            'TCM' => 'ROLE_TCM',
            'CMH' => 'ROLE_CMH',
            'Specialist' => 'ROLE_SPECIALIST',
            'Supervisor TCM' => 'ROLE_SUPERVISOR_TCM',
            'Supervisor CMH' => 'ROLE_SUPERVISOR_CMH',
            'QA TCM' => 'ROLE_QA_TCM',
            'QA CMH' => 'ROLE_QA_CMH',
            'Billing' => 'ROLE_BILLING',
            'Utilization Manager' => 'ROLE_UTILIZATION_MANAGER',
            'Operation Manager' => 'ROLE_OPERATION_MANAGER',
            'Super Admin' => 'ROLE_SUPER_ADMIN',
        );

        $formMapper
            ->add('username', null, array(
                'label' => 'general.username',
            ))
            ->add('email', null, array(
                'label' => 'general.email',
            ))
            ->add('plainPassword', 'password', array(
                'required' => false,
                'label' => 'general.password',
                'constraints' => null,
            ))
            ->add('roles', 'choice', $rolesOptions)
            ->add('assignedWorkers', 'sonata_type_model', array(
                'placeholder' => 'placeholder.select',
                'query' => $this->getQueryUserByRoles([User::ROLE_TCM, User::ROLE_CMH, User::ROLE_SPECIALIST]),
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ))
            ->add('modifiers')
            ->add('workerNumber', null, [
                'label' => 'Clinic Identifier'
            ])
            ->add('enabled', null, array(
                'required' => false,
                'label' => 'general.enabled',
            ))


            ->add('mediaFile', MyFileType::class, array(
                'label' => 'block.user.avatar',
                'required' => false,
            ))
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('lastname', null, array(
                'label' => 'general.lastname',
            ))
            ->add('phone', null, array(
                'label' => 'general.phone',
                'required' => false,
                'attr' => ['class' => 'mask-phone'],
            ))
            ->add('birthDate', 'sonata_type_date_picker', array(
                'label' => 'Birth Date',
                'format' => 'MM/dd/yyyy',
            ))
            ->add('address', null, array(
                'label' => 'general.address',
            ))
            ->add('socialNumber', null, array(
                'label' => 'general.social.number',
            ))

            ->add('contractionDate', 'sonata_type_date_picker', array(
                'label' => 'form.user.contraction.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
            ->add('firedDate', 'sonata_type_date_picker', array(
                'label' => 'form.user.fired.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
            ->add('workPermission', null, array(
                'label' => 'form.user.work.permission',
                'required' => false,
            ))
            ->add('workPermissionExpiredDate', 'sonata_type_date_picker', array(
                'label' => 'form.user.work.permission.expired.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
            ->add('residence', null, array(
                'label' => 'form.user.residence',
            ))
            ->add('citizenship', null, array(
                'label' => 'form.user.citizenship',
            ))
            ->add('driveLicence', null, array(
                'label' => 'form.user.drive.licence',
            ))
            ->add('driveLicenceExpiredDate', 'sonata_type_date_picker', array(
                'label' => 'form.user.drive.licence.expired.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
            ->add('proLicence', null, array(
                'label' => 'form.user.pro.licence',
            ))
            ->add('proLicenceExpiredDate', 'sonata_type_date_picker', array(
                'label' => 'form.user.pro.licence.expired.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))

            ->add('providerNumber', null, array(
                'label' => 'form.user.provider.number',
            ))
            ->add('dcfFarsId', null, array(
                'label' => 'form.user.dcf.fars.id',
            ))
            ->add('ahcaBackgroundDate', 'sonata_type_date_picker', array(
                'label' => 'AHCA Background Date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
        ;

        $this->addFieldsetForm('username', 'init', 'block.user.system.info');
        $this->addFieldsetForm('enabled', 'end');

        $this->addFieldsetForm('mediaFile', 'init', 'block.user.personal.info');
        $this->addFieldsetForm('socialNumber', 'end');

        $this->addFieldsetForm('contractionDate', 'init', 'block.user.work.info');
        $this->addFieldsetForm('proLicenceExpiredDate', 'end');

        $this->addFieldsetForm('providerNumber', 'init', 'block.user.other.info');
        $this->addFieldsetForm('ahcaBackgroundDate', 'end');
    }

        /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if($this->request->isXmlHttpRequest()){
            $showMapper
                ->add('avatar', 'image', array(
                    'width' => 150,
                    'height' => 150,
                    'label' => 'form.user.picture',
                ))
                ->add('username', null, array(
                    'label' => 'general.username',
                ))
                ->add('name', null, array(
                    'label' => 'general.name',
                ))
                ->add('lastname', null, array(
                    'label' => 'general.lastname',
                ))
               /* ->add('email', null, array(
                    'label' => 'general.email',
                ))*/
                ->add('phone', null, array(
                    'label' => 'general.phone',
                ))
                ->add('enabled', null, array(
                    'label' => 'general.enabled',
                ))
            ;
            return;
        }


        $showMapper
            ->with('block.user.avatar', array('class' => 'col-md-4'))
                ->add('avatar', 'image', array(
                    'prefix' => '/uploads/users/',
                    'width' => 150,
                    'height' => 150,
                    'label' => 'form.user.picture',
                ))
            ->end()

            ->with('block.user.personal.info', array('class' => 'col-md-4'))
                ->add('name', null, array(
                    'label' => 'general.name',
                ))
                ->add('lastname', null, array(
                    'label' => 'general.lastname',
                ))
                ->add('email', null, array(
                    'label' => 'general.email',
                ))
                ->add('phone', null, array(
                    'label' => 'general.phone',
                ))
                ->add('enabled', null, array(
                    'label' => 'general.enabled',
                ))
            ->end()

            ->with('block.user.system.info', array('class' => 'col-md-4'))
                ->add('username', null, array(
                    'label' => 'general.username',
                ))
                ->add('roles', 'choice', array(
                    'label' => 'general.roles',
                    'multiple' => true,
                    'choices' => null,
                ))
            ->end()
        ;
    }

    public function toString($object)
    {
        if($object instanceof User && $object->getUsername() != null)
            return $object->getUsername();
        return $this->trans('menu.entity.user',array(),'BackEndBundle');
    }
}

