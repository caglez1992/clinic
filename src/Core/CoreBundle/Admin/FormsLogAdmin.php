<?php

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class FormsLogAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
        $collection->remove('create');
        $collection->remove('delete');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('formName', null, array(
                'label' => 'Form Name',
            ))
            ->add('container', null, array(
                'label' => 'Contain In',
            ))
            ->add('identifier', null, array(
                'label' => 'Container Identifier',
            ))
            ->add('field')
            ->add('value')
            ->add('worker')
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'Generate At',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('formName', null, array(
                'label' => 'Form Name',
            ))
            ->add('container', null, array(
                'label' => 'Contain In',
            ))
            ->add('identifier', null, array(
                'label' => 'Container Id',
            ))
            ->add('field')
            ->add('value')
            ->add('worker')
            ->add('createdAt', null, array(
                'label' => 'Generated At',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof FormsLogAdmin && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.formslogadmin',array(),'BackEndBundle');
    }
}
