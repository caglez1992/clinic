<?php

namespace Core\CoreBundle\Admin;

use Core\CoreBundle\Entity\Media;
use Core\CoreBundle\Form\Type\MyFileType;
use Core\CoreBundle\Form\Type\XalixFileType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class MediaAdmin extends BaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
//        $collection->remove('edit');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'general.name'
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('media', 'string', array(
                'label' => 'form.media.media',
                'template' => 'BackEndBundle:sonata:list_field_image.html.twig',
            ))
            ->addIdentifier('name', null, array(
                'label' => 'general.name'
            ))

            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    //'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('block.general.name', array('class' => 'col-md-9'))
                ->add('name', null, array(
                    'label' => false,
                ))
            ->end()

            ->with('block.media.media', array('class' => 'col-md-3'))
                ->add('mediaFile', MyFileType::class, array(
                    'label' => false,
                ))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        //$mediaPath = $this->getConfigurationPool()->getContainer()->getParameter('param.medias');
//        $imagePath = $mediaPath.'/'.$this->getSubject()->getMedia();
        $showMapper
            ->add('name', null, array(
                'label' => 'general.name'
            ))
            ->add('media', 'image', array(
                'prefix' => '/uploads/medias/',
                'width' => 120,
                'height' => 90,
                'label' => 'form.media.media',
            ))

        ;
    }

    public function toString($object)
    {
        if($object instanceof Media && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.media',array(),'BackEndBundle');
    }
}
