<?php

namespace Core\CoreBundle\Twig\Extension;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Service("boolean.twig.extension", public=true)
 * @Tag("twig.extension")
 */
class BooleanExtension extends \Twig_Extension {

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @InjectParams({
     *     "translator" = @Inject("translator")
     * })
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('toString', array($this, 'toStringFilter')),
        );
    }

    public function toStringFilter($bool){
        $yes = $this->translator->trans('general.yes', [], 'BackEndBundle');
        $no = $this->translator->trans('general.no', [], 'BackEndBundle');
        return $bool ? $yes : $no;
    }

    public function getName()
    {
        return 'country_extension';
    }
}