<?php
/**
 * Created by PhpStorm.
 * User: ned
 * Date: 1/12/15
 * Time: 15:57
 */

namespace Core\CoreBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;
use \Twig_Extension;

class FileExistExtension extends Twig_Extension
{
    protected $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getName()
    {
        return 'arch.asset_exists.extension';
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('asset_exists',
                array($this, 'asset_exists')
            ),
        );
    }

    /**
     * @param string
     *
     * @return mixed
     */
    public function asset_exists($path)
    {
        return file_exists($path);
    }
}