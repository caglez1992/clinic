<?php

namespace Core\CoreBundle\Twig\Extension;

use DateTime;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Core\PatientBundle\Entity\Service as ServiceEntity;
use Twig_Extension;
use Twig_SimpleFilter;

/**
 * @Service("calendar.service.color.twig.extension", public=true)
 * @Tag("twig.extension")
 */
class CalendarServiceColor extends Twig_Extension {

    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('eventTextColor', array($this, 'colorFilter')),
            new Twig_SimpleFilter('eventBackgroundColor', array($this, 'backgroundColorFilter')),
        );
    }

    /**
     * @param ServiceEntity $service
     * @return mixed
     */
    public function colorFilter($service){
        $now =  new DateTime();
        if($service->getScheduleEnd()->format('d-m-Y') == $now->format('d-m-Y'))
            return '#fff';

        if($service->getScheduleEnd() < $now)
            return '#666';

        return '#fff';
    }

    /**
     * @param ServiceEntity $service
     * @return mixed
     */
    public function backgroundColorFilter($service){
        $now =  new DateTime();
        if($service->getScheduleEnd()->format('d-m-Y') == $now->format('d-m-Y'))
            return '#e5603b';

        if($service->getScheduleEnd() < $now)
            return '#ddd';

        return '#79A5F0';
    }

    public function getName()
    {
        return 'calendar.service.color.extension';
    }
}