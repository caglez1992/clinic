<?php

namespace Core\CoreBundle\Twig\Extension;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Service("money.twig.extension", public=true)
 * @Tag("twig.extension")
 */
class MoneyExtension extends \Twig_Extension {

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('money', array($this, 'toUsdMoney')),
        );
    }

    public function toUsdMoney($number){
        return '$' . number_format($number, 2, '.', ',');
    }

    public function getName()
    {
        return 'money_extension';
    }
}