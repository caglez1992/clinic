<?php

/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 14/11/16
 * Time: 16:18
 */

namespace Core\CoreBundle\Twig;

use ReflectionClass;
use Twig_Extension;
use Twig_SimpleFilter;

class GetClassExtension extends Twig_Extension
{
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('class', array($this, 'getClass')),
        );
    }

    public function getClass($object)
    {
        return $classname = (new ReflectionClass($object))->getShortName();
    }

    public function getName()
    {
        return 'get.class.twig_extension';
    }
}