<?php

namespace Core\CoreBundle;

use Core\CoreBundle\DependencyInjection\Compile\MailerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new MailerPass());
    }
}
