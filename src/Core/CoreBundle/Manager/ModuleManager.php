<?php
namespace Core\CoreBundle\Manager;

use Core\CoreBundle\Repository\ModuleRepository;

class ModuleManager extends BaseManager
{
    /**
     * @return ModuleRepository
     */
    public function getRepo(){
        return $this->getEm()->getRepository('CoreBundle:Module');
    }

    public function getActiveModules(){
        return $this->getRepo()->findBy(array(
            'enable' => true,
        ));
    }

    public function getArrayActiveModules(){
        return $this->getRepo()->getArrayEnabledModules();
    }
}
