<?php
namespace Core\CoreBundle\Manager;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Exception;

class NotificationManager extends BaseManager
{
    /**
     * @return \Core\CoreBundle\Repository\NotificationRepository
     */
    public function getRepo(){
        return $this->getEm()->getRepository('CoreBundle:Notification');
    }

    /**
     * @param User[]|User|string $to
     * @param string $msg
     * @param string $url
     * @param string $type
     * @param bool $important
     * @param array $dataMsg
     * @param bool $pusher
     * @param bool $persist
     */
    public function createNotification($to, $msg, $url = null, $type = null, $important = false, $dataMsg = array(), $pusher = true, $persist = true){
        $mainLocale = $this->container->getParameter('locale');
        $locales = $this->container->getParameter('locales');
        $locales = explode('|', $locales);
        $translator = $this->container->get('translator');

        $users = [];
        if(is_string($to)){
            $users = $this->getEm()->getRepository(User::class)->getUsersByRole($to);
        }

        if(!is_array($to) && $to instanceof User) {
            $users[] = $to;
        }

        if(is_array($to) && count($to) > 0 && $to[0] instanceof User){
            $users = $to;
        }

        if(is_array($to) && count($to) > 0 && is_string($to[0])){
            foreach ($to as $t){
                $usersByRole = $this->getEm()->getRepository(User::class)->getUsersByRole($t);
                $users = array_merge($users, $usersByRole);
            }
        }

        foreach ($users as $u) {
            $notification = new Notification();
            $notification->setOwner($u);
            $notification->setUrl($url);
            $notification->setType($type);
            $notification->setImportant($important);
            $notification->setMsg($translator->trans($msg, $dataMsg, 'BackEndBundle', $mainLocale));
            $notification->setLocale($mainLocale);

            if($persist)
                $this->save($notification);

            foreach ($locales as $locale){
                if($locale != $mainLocale) {
                    $notification->setMsg($translator->trans($msg, $dataMsg, 'BackEndBundle', $locale));
                    $notification->setLocale($locale);
                    $this->save($notification);
                }
            }

            if($important){
                $this->container->get('email.utils')->sendGeneralEmail($u, 'Notification: ', $notification->getMsg());
            }

            if($pusher) {
                $pusher = $this->container->get('gos_web_socket.wamp.pusher');
                try {
                    $pusher->push(['msg' => 'new.notification'], 'notification_by_user', ['user_id' => $u->getId()]);
                } catch (Exception $e) {
                    $this->container->get('logger')->critical('Websocket is down can not notify to user');
                }
            }
        }
    }

    /**
     * @param Notification $notification
     * @param string $locale
     * @return Notification
     */
    public function refresh($notification, $locale){
        $notification->setLocale($locale);
        $this->getEm()->refresh($notification);
        return $notification;
    }
}
