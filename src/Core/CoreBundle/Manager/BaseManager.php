<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 26/01/16
 */


namespace Core\CoreBundle\Manager;


use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseManager {

    protected $em;
    protected $container;

    /**
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $em = null, ContainerInterface $container = null)
    {
        $this->em = $em;
        $this->container = $container;
    }


    /**
     * Save an object in database.
     *
     * @param mixed $object Instancia de una entidad de doctrine.
     *
     * @return boolean Resultado de la operacion.
     */
    public function save($object)
    {

        if (is_object($object) and !empty($object)) {

            // suspend auto-commit
            $this->em->getConnection()->beginTransaction();
            // Try and make the transaction
            try {

                // Save account
                $this->em->persist($object);
                $this->em->flush();

                // Try and commit the transaction
                $this->em->getConnection()->commit();
                return true;

            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $this->em->getConnection()->rollBack();
                dump($e);die;
                return false;
            }
        }
    }


    /**
     * Remove an object of the database.
     *
     * @param mixed $object Instancia de una entidad de doctrine.
     *
     * @return boolean Resultado de la operacion.
     */
    public function remove($object)
    {

        if (is_object($object) and !empty($object)) {

            // suspend auto-commit
            $this->em->getConnection()->beginTransaction();
            // Try and make the transaction
            try {

                // remove object
                $this->em->remove($object);
                $this->em->flush();

                // Try and commit the transaction
                $this->em->getConnection()->commit();
                return true;

            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $this->em->getConnection()->rollBack();
                return false;
            }
        }
    }


    /**
     * Persisting an object.
     *
     * @param mixed $object Instancia de una entidad de doctrine.
     *
     * @return boolean Resultado de la operacion.
     */
    public function persist($object)
    {

        if (is_object($object) and !empty($object)) {

            // suspend auto-commit
            $this->em->getConnection()->beginTransaction();
            // Try and make the transaction
            try {

                // persist object
                $this->em->persist($object);


                // Try and commit the transaction
                $this->em->getConnection()->commit();
                return true;

            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $this->em->getConnection()->rollBack();
                return false;
            }
        }
    }

    /**
     * Flush an object.
     *
     * @param mixed $object Instancia de una entidad de doctrine.
     *
     * @return boolean Resultado de la operacion.
     */
    public function flush($object)
    {

        if (is_object($object) and !empty($object)) {

            // suspend auto-commit
            $this->em->getConnection()->beginTransaction();
            // Try and make the transaction
            try {

                // flush object
                $this->em->flush($object);


                // Try and commit the transaction
                $this->em->getConnection()->commit();
                return true;

            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $this->em->getConnection()->rollBack();
                return false;
            }
        }
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

}
