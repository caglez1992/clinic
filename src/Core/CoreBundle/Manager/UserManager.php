<?php
namespace Core\CoreBundle\Manager;

use Core\CoreBundle\Entity\User;

class UserManager extends BaseManager
{

    public function getRepository(){
        return $this->getEm()->getRepository('CoreBundle:User');
    }

    /**
     * Returns the real user authenticated in the system.
     *
     * @return User
     */
    public function getRealUser()
    {
        $tokenStorage = $this->container->get('security.token_storage')->getToken();
        $user = (!is_null($tokenStorage)) ? $tokenStorage->getUser() : null;
        if (is_object($user)) {
            $userReal = $this->container->get('fos_user.user_manager')->findUserByUsername($user->getUsername());
            return (!is_null($userReal) ? $userReal : null);
        }
        return null;
    }
}
