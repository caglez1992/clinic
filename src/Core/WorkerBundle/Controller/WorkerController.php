<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\WorkerBundle\Controller;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\ServiceForm;
use Core\PatientBundle\Entity\ServiceParent;
use Core\PatientBundle\Entity\Task;
use Core\PatientBundle\Entity\Service;
use Core\WorkerBundle\Entity\Note;
use Core\WorkerBundle\Entity\ServiceAcl;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WorkerController extends Controller
{

    /**
     * @Route("/show/{id}", name="show_user", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function showWorkerAction($id)
    {
        $d = $this->getDoctrine();
        $worker = $d->getRepository(User::class)->find($id);

        $userReal = $this->get(UserManager::class)->getRealUser();

        $isMyProfile = $worker->getId() == $userReal->getId();
        $notifications = $isMyProfile ? $d->getRepository(Notification::class)->getMyNotifications($userReal, false) : null;

        $pendingAssessment = null;
        $pendingServices = null;
        
        $pendingOrders = $d->getRepository(Task::class)->getTasks($userReal);
        
        $patientsHistory = $d->getRepository(Patient::class)->getPatientHistory($worker);
        $serviceHistory = $d->getRepository(Service::class)->getByDoctor($worker);

        if($this->get('security.authorization_checker')->isGranted(User::ROLE_OPERATION_MANAGER)){
            $pendingAssessment = $d->getRepository(Patient::class)->getPendingForTasks();
        }

        return $this->render('@BackEnd/custom/worker/show/show_worker.html.twig', [
            'user' => $worker,
            'isMyProfile' => $isMyProfile,
            'notifications' => $notifications,
            'pendingAssessment' => $pendingAssessment,
            'pendingOrders' => $pendingOrders,
            'patientsHistory' => $patientsHistory,
            'serviceHistory' => $serviceHistory,
            'services' => $d->getRepository(Service::class)->getByDoctor($worker),
            'servicesForCalendar' => $d->getRepository(Service::class)->getForCalendar($worker),
        ]);
    }

    /**
     * @Route("/show/my/profile", name="show_my_profile", options={"expose"=true})
     * @return Response
     */
    public function showMyProfileAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $userReal = $this->get(UserManager::class)->getRealUser();

        $url = $this->generateUrl('show_user', ['id'=>$userReal->getId()]);
        if($request->query->has('tab')){
            $tab = $request->query->get('tab');
            $url .= "?tab=$tab";
        }

        return $this->redirect($url);
    }

    /**
     * @Route("/ajax/services/by/worker/{workerId}", name="ajax_services_by_worker", options={"expose"=true})
     * @param integer $workerId
     * @return Response
     */
    public function ajaxServicesByWorkerAction($workerId)
    {
        $d = $this->getDoctrine();
        $worker = $d->getRepository(User::class)->find($workerId);
        $services = new ArrayCollection();

        foreach ($worker->getRoles() as $rol){
            /** @var ServiceAcl $serviceAcl */
            $serviceAcl = $d->getRepository(ServiceAcl::class)->findOneBy(['role' => $rol]);
            if(!is_null($serviceAcl)) {
                /** @var ServiceParent $service */
                foreach ($serviceAcl->getServices() as $service) {
                    if (!$service->isGroup() && !$services->contains($service))
                        $services->add($service);
                }
            }
        }

        $html = ""; // HTML as response
        foreach($services as $service){
            $html .= '<option value="' . $service->getId().'" >' . $service . '</option>';
        }
        return new Response($html, 200);
    }

    /**
     * @Route("/close/order/{id}", name="close_order", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function closeOrderAction($id)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();

        $taskAdmin = $this->get('sonata.admin.pool')->getAdminByClass(Task::class);
        $task = $d->getRepository(Task::class)->find($id);

        if($task->getWorker()->getId() != $userReal->getId() || $task->getStatus()->getName() == NomUtil::NOM_STATUS_CLOSE){
            $this->addFlash(Notification::ERROR, 'You can not close this task.');
            return $this->redirect($taskAdmin->generateUrl('list'));
        }

        $task->setCloseAt(new DateTime());
        $task->setStatus(NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_CLOSE));
        $this->get(BaseManager::class)->save($task);

        $this->addFlash(Notification::SUCCESS, sprintf('The Task #%s has been closed successfully.', $task->getId()));

        $url = $taskAdmin->generateObjectUrl('show', $task);
        $this->get(NotificationManager::class)->createNotification($task->getCreatedBy(), sprintf('%s close the task with code %s', $task->getWorker(), $task->getId()), $url, Notification::SUCCESS, true);

        return $this->redirect($this->get('sonata.admin.pool')->getAdminByClass(Task::class)->generateUrl('list'));
    }

    /**
     * @Route("/close/service/{idService}", name="close_service", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_CLOSE')")
     * @param integer $idService
     * @return Response
     */
    public function closeService($idService){
        $d = $this->getDoctrine();
        //$userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $serviceAdmin = $this->get('sonata.admin.pool')->getAdminByClass(Service::class);

        $service = $d->getRepository(Service::class)->find($idService);
        $supervisors = $d->getRepository(User::class)->getSupervisorsByWorker($service->getWorker());

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SERVICE_CLOSE', $service)) {
            $this->addFlash(Notification::ERROR, 'You can not close this service.');
            return $this->redirect($serviceAdmin->generateUrl('list'));
        }

        $service->setStatus(NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_CLOSE));
        $this->get(BaseManager::class)->save($service);

        if($service->isTcm() && !$service->getDischarge()){
            //Find note
            $note = $d->getRepository(Note::class)->getNoteBySimilarService($service);
            if(is_null($note)) {
                //Create new note
                $note = new Note();
                $note->setServiceParent($service->getServiceParent());
                $note->setSettingCode($service->getSettingCode());
                $note->setWorker($service->getWorker());
                $note->setPatient($service->getPatient());
                $note->setWeek($service->getWeek());
                $note->setHmo($service->getHmo());
                $note->setDiagnosisTemplate($service->getDiagnosisTemplate());
                $note->setServiceDate($service->getServiceDate());
                $note->setCanEdit(true);
                $this->get(BaseManager::class)->save($note);

                //Generate pdf note if null
                $this->get('pdf.util')->copyNote($note, 'tcm_note.pdf', 'tcm');
            }

            if(!$note->getRevised()) {
                if($service->isBillingRetroActive() && $note->getWeek() != $service->getWeek()){
                    $this->addFlash(Notification::ERROR, 'You can not close this service. Note of this service is already created in a week diferent that the week of this service, please contact your supervisor if you think that this is a mistake.');
                    return $this->redirect($serviceAdmin->generateUrl('list'));
                }
                //Add this service to note created
                $service->setNote($note);
                $this->get(BaseManager::class)->save($service);

                //Update billing zero status
                $note->setBillingZero($service->getBillingZero());
                $this->get(BaseManager::class)->save($note);

                //Fill Pdf
                $this->get('pdf.util')->fillTcmNote($note);
                $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been updated', $note->getId()));
            } else {
                $this->addFlash(Notification::ERROR, 'You can not close this service. Note of this service is already revised, please contact your supervisor.');
                return $this->redirect($serviceAdmin->generateUrl('list'));
            }
        }

        if( ($service->getCode() == 'H2017' || $service->getCode() == 'H2019') && !$service->getDischarge()) {
            //Create new note
            $note = new Note();
            $note->setServiceParent($service->getServiceParent());
            $note->setSettingCode($service->getSettingCode());
            $note->setWorker($service->getWorker());
            $note->setPatient($service->getPatient());
            $note->setWeek($service->getWeek());
            $note->setHmo($service->getHmo());
            $note->setDiagnosisTemplate($service->getDiagnosisTemplate());
            $note->setServiceDate($service->getServiceDate());
            $note->setCanEdit(true);
            $this->get(BaseManager::class)->save($note);

            //Add this service to note created
            $service->setNote($note);
            $this->get(BaseManager::class)->save($service);

            if($service->getCode() == 'H2017'){
                //Generate PDF note
                $this->get('pdf.util')->copyNote($note, 'cmh_note_psr.pdf', 'cmh');
                //Fill Note
                $this->get('pdf.util')->fillPsrNote($note, $service);
            }

            if($service->getCode() == 'H2019'){
                //Generate PDF note
                $this->get('pdf.util')->copyNote($note, 'cmh_note_h2019.pdf', 'cmh');
                //Fill Note
                $this->get('pdf.util')->fillH2019Note($note, $service);
            }

            $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been created', $note->getId()));
        }

        $this->addFlash(Notification::SUCCESS, sprintf('The service %s has been closed successfully', $service->getId()));

        $serviceAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('patient.bundle.admin.service');

        //Notify to the right Supervisor
        $url = $serviceAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $service->getWeek()->getId(), $service->getWorker()->getId());
        foreach ($supervisors as $supervisor) {
            $nm->createNotification($supervisor, sprintf('%s close the service with code %s', $service->getWorker()->getFullName(), $service->getId()), $url, Notification::SUCCESS, true);
        }

        return $this->redirect($serviceAdmin->generateUrl('list'));
    }


    /**
     * @Route("/close/note/{idNote}", name="close_note", options={"expose"=true})
     * @Security("has_role('ROLE_NOTE_CLOSE')")
     * @param integer $idNote
     * @return Response
     */
    public function closeNote($idNote)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');

        $note = $d->getRepository(Note::class)->find($idNote);

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_NOTE_CLOSE', $note)) {
            $this->addFlash(Notification::ERROR, 'You can not close this service.');
            return $this->redirect($noteAdmin->generateUrl('list'));
        }

        //Check if note can be close
        $services = $d->getRepository(Service::class)->getServiceForNote($note->getWorker(), $note->getPatient(), $note->getServiceDate(), $note->getCode(), NomUtil::NOM_STATUS_OPEN);
        if(count($services) > 0){
            $this->addFlash(Notification::ERROR, sprintf('Can not close this note, exist %s service(s) OPEN that fit with this note', count($services)));
            return $this->redirect($noteAdmin->generateUrl('list'));
        }

        $note->setStatus(NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_CLOSE));
        $this->get(BaseManager::class)->save($note);

        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');
        $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
        $supervisors = $d->getRepository(User::class)->getSupervisorsByWorker($userReal);
        foreach ($supervisors as $supervisor) {
            $nm->createNotification($supervisor, sprintf('%s close the note with code %s', $userReal->getFullName(), $note->getId()), $url, Notification::SUCCESS, true);
        }

        $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been closed successfully', $note->getId()));
        return $this->redirect($url);
    }

    /**
     * @Route("/review/service/{idService}", name="review_service", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_REVIEW')")
     * @param integer $idService
     * @return Response
     */
    public function reviewService($idService)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $serviceAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('patient.bundle.admin.service');

        $service = $d->getRepository(Service::class)->find($idService);

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SERVICE_REVIEW', $service)) {
            $this->addFlash(Notification::ERROR, 'You can not review this service.');
            return $this->redirect($serviceAdmin->generateUrl('list'));
        }

        $service->setRevised(true);
        $this->get(BaseManager::class)->save($service);

        if($service->getType() == 'tcm')
            $roles = [User::ROLE_QA_TCM, User::ROLE_OPERATION_MANAGER];
        else
            $roles = [User::ROLE_QA_CMH, User::ROLE_OPERATION_MANAGER];
        $url = $serviceAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $service->getWeek()->getId(), $service->getWorker()->getId());
        $nm->createNotification($roles, sprintf('%s reviewed the service with code %s', $userReal->getFullName(), $service->getId()), $url, Notification::SUCCESS, true);

        $flashMsg = sprintf('The service %s has been reviewed correctly', $service->getId());
        $this->addFlash(Notification::SUCCESS, $flashMsg);

        return $this->redirect($url);
    }

    /**
     * @Route("/review/note/{idNote}", name="review_note", options={"expose"=true})
     * @Security("has_role('ROLE_NOTE_REVIEW')")
     * @param integer $idNote
     * @return Response
     */
    public function reviewNote($idNote)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');

        $note = $d->getRepository(Note::class)->find($idNote);
        $note->setRevised(true);
        $this->get(BaseManager::class)->save($note);

        //Review all service that fit with this note
        /** @var Service $service */
        foreach($note->getServices() as $service){
            $service->setRevised(true);
            $this->get(BaseManager::class)->save($service);
        }

        $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
        $nm->createNotification($note->getRolesToNotify($userReal), sprintf('%s reviewed the note with code %s', $userReal, $note->getId()), $url, Notification::SUCCESS, true);

        $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been reviewed correctly', $note->getId()));
        return $this->redirect($url);
    }

    /**
     * @Route("/unreview/note/{idNote}", name="un_review_note", options={"expose"=true})
     * @Security("has_role('ROLE_NOTE_UNREVIEW')")
     * @param integer $idNote
     * @return Response
     */
    public function unReviewNote($idNote)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');

        $note = $d->getRepository(Note::class)->find($idNote);

        if($note->getApproved() && !($userReal->hasRole(User::ROLE_QA_CMH) || $userReal->hasRole(User::ROLE_QA_TCM)) ){
            $this->addFlash(Notification::ERROR, 'Can not perform this operation, note is already approved by QA');
            return $this->redirect($noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId()));
        }

        $note->setRevised(false);
        if($userReal->hasRole(User::ROLE_QA_CMH) || $userReal->hasRole(User::ROLE_QA_TCM)){
            $note->setApproved(false);
        }
        $this->get(BaseManager::class)->save($note);

        //Un Review all service that fit with this note
        /** @var Service $service */
        foreach($note->getServices() as $service){
            $service->setRevised(false);
            $this->get(BaseManager::class)->save($service);
        }

        $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
        $nm->createNotification($note->getRolesToNotify($userReal), sprintf('%s uncheck the note with code %s', $userReal, $note->getId()), $url, Notification::SUCCESS, true);

        $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been unckecked correctly', $note->getId()));
        return $this->redirect($url);
    }

    /**
     * @Route("/approve/service/{idService}", name="approve_service", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_APPROVE')")
     * @param integer $idService
     * @return Response
     */
    public function approveService($idService)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $serviceAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('patient.bundle.admin.service');

        $service = $d->getRepository(Service::class)->find($idService);

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SERVICE_APPROVE', $service)) {
            $this->addFlash(Notification::ERROR, 'You can not approve this service.');
            return $this->redirect($serviceAdmin->generateUrl('list'));
        }

        $service->setApproved(true);
        $service->setApprovedBy($userReal);

        if($service->getDischarge()){
            //Do discharge patient
            $patient = $service->getPatient();
            $openRegistry = $patient->getOpenRegistry();
            if($service->isTcm()){
                $openRegistry->setDischargeTcm(true);
                $openRegistry->setDischargeTcmDate(new DateTime());
            }
            if($service->isCmh()){
                $openRegistry->setDischargeCmh(true);
                $openRegistry->setDischargeCmhDate(new DateTime());
            }

            //Close patient if match conditions
            $tcmCondition = !$openRegistry->getOpenByTcm();
            $cmhCondition = !$openRegistry->getOpenByCmh();
            if($openRegistry->getOpenByTcm() && $openRegistry->getDischargeTcm())
                $tcmCondition = true;
            if($openRegistry->getOpenByCmh() && $openRegistry->getDischargeCmh())
                $cmhCondition = true;
            if($tcmCondition && $cmhCondition){
                //Close Registry
                $openRegistry->setIsOpen(false);
                //Close patient
                $patient->closePatient();
                $this->get(BaseManager::class)->save($patient);
                $url = $this->generateUrl('show_patient', ['id' => $patient->getId()]);
                $nm->createNotification([User::ROLE_OPERATION_MANAGER, User::ROLE_BILLING], sprintf('Patient %s has been closed.', $patient), $url, Notification::SUCCESS, true);
            }

            $this->get(BaseManager::class)->save($openRegistry);
        }

        $this->get(BaseManager::class)->save($service);

        $url = $serviceAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $service->getWeek()->getId(), $service->getWorker()->getId());

        $roles = [User::ROLE_BILLING, User::ROLE_OPERATION_MANAGER];
        $nm->createNotification($roles, sprintf('%s approved the service with code %s', $userReal->getFullName(), $service->getId()), $url, Notification::SUCCESS, true);

        $flashMsg = sprintf('The service %s has been approved correctly', $service->getId());
        $this->addFlash(Notification::SUCCESS, $flashMsg);
        return $this->redirect($url);
    }

    /**
     * @Route("/approve/note/{idNote}", name="approve_note", options={"expose"=true})
     * @Security("has_role('ROLE_NOTE_APPROVE')")
     * @param integer $idNote
     * @return Response
     */
    public function approveNote($idNote)
    {
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');

        $note = $d->getRepository(Note::class)->find($idNote);
        $note->setApproved(true);
        $note->setApprovedBy($userReal);
        $this->get(BaseManager::class)->save($note);

        //Review all service that fit with this note
        /** @var Service $service */
        foreach($note->getServices() as $service){
            $service->setApproved(true);
            $service->setApprovedBy($userReal);
            $this->get(BaseManager::class)->save($service);
        }

        $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
        $nm->createNotification($note->getRolesToNotify($userReal), sprintf('%s approved the note with code %s', $userReal, $note->getId()), $url, Notification::SUCCESS, true);

        $this->addFlash(Notification::SUCCESS, sprintf('The note %s has been approved correctly', $note->getId()));
        return $this->redirect($url);
    }

    /**
     * @Route("/ajax/select/services/by/week/{weekId}/{patientId}", name="ajax_select_services_by_week", options={"expose"=true})
     * @param integer $weekId
     * @param integer $patientId
     * @return Response
     */
    public function ajaxSelectServicesByWeekAction($weekId, $patientId)
    {
        $d = $this->getDoctrine();
        $services = $d->getRepository(Service::class)->findBy([
            'week' => $weekId,
            'patient' => $patientId,
        ]);

        $html = ""; // HTML as response
        foreach($services as $service){
            $html = sprintf('<option value="%d">%s, Duration: %s --- %s</option>',
                $service->getId(),
                $service,
                $service->getScheduleStart()->format('M d, Y H:i'),
                $service->getScheduleEnd()->format('M d, Y H:i')
            );
        }
        return new Response($html, 200);
    }

    /**
     * @Route("/add/form/to/service", name="add_form_to_service", options={"expose"=true})
     * @return Response
    */
    public function addFormToServiceAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $d = $this->getDoctrine();
        $data = $request->request->all();
        $bm = $this->get(BaseManager::class);

        $service = $d->getRepository(Service::class)->find($data['service']);
        $originForm = $d->getRepository(PdfForm::class)->find($data['formTcm']);

        $serviceForm = new ServiceForm();
        $serviceForm->setName($originForm->getName());
        $serviceForm->setFilename($originForm->getFilename());
        $serviceForm->setService($service);
        $bm->save($serviceForm);
        $this->get('pdf.util')->copyOriginForm($originForm, $serviceForm, $service);

        return $this->redirect($this->generateUrl('show_patient', [
            'id' => $service->getPatient()->getId(),
            'tab' => 'tabTcmForms',
        ]));
    }
}