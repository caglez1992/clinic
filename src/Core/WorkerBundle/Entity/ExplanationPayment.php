<?php

namespace Core\WorkerBundle\Entity;

use Core\CoreBundle\Entity\User;
use Core\PatientBundle\Entity\BillingRegistry;
use Core\PatientBundle\Entity\Patient;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\WorkerBundle\Repository\ExplanationPaymentRepository")
 * @Vich\Uploadable
 */
class ExplanationPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\BillingRegistry", inversedBy="payments")
     * @ORM\JoinColumn(name="billing_registry_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $billingRegistry;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_number", type="string", nullable=true)
     */
    private $paymentNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="payment_amount", type="float", nullable=true)
     */
    private $paymentAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="reversal_payment_amount", type="float", nullable=true)
     */
    private $reversalPaymentAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="interest", type="float", nullable=true)
     */
    private $interest;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $patient;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentNumber
     *
     * @param string $paymentNumber
     *
     * @return ExplanationPayment
     */
    public function setPaymentNumber($paymentNumber)
    {
        $this->paymentNumber = $paymentNumber;

        return $this;
    }

    /**
     * Get paymentNumber
     *
     * @return string
     */
    public function getPaymentNumber()
    {
        return $this->paymentNumber;
    }

    /**
     * Set paymentAmount
     *
     * @param float $paymentAmount
     *
     * @return ExplanationPayment
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * Set reversalPaymentAmount
     *
     * @param float $reversalPaymentAmount
     *
     * @return ExplanationPayment
     */
    public function setReversalPaymentAmount($reversalPaymentAmount)
    {
        $this->reversalPaymentAmount = $reversalPaymentAmount;

        return $this;
    }

    /**
     * Get reversalPaymentAmount
     *
     * @return float
     */
    public function getReversalPaymentAmount()
    {
        return $this->reversalPaymentAmount;
    }

    /**
     * Set interest
     *
     * @param float $interest
     *
     * @return ExplanationPayment
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Get interest
     *
     * @return float
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ExplanationPayment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set billingRegistry
     *
     * @param BillingRegistry $billingRegistry
     *
     * @return ExplanationPayment
     */
    public function setBillingRegistry(BillingRegistry $billingRegistry = null)
    {
        $this->billingRegistry = $billingRegistry;

        return $this;
    }

    /**
     * Get billingRegistry
     *
     * @return BillingRegistry
     */
    public function getBillingRegistry()
    {
        return $this->billingRegistry;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return ExplanationPayment
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return ExplanationPayment
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function checkAssert(ExecutionContextInterface $context)
    {
        $totalPayment = $this->getBillingRegistry()->getTotalPayment();
        $totalPayment += $this->getPaymentAmount();
        $totalPayment -= $this->getReversalPaymentAmount();

        if($totalPayment > $this->getBillingRegistry()->getAmount()){
            $context->buildViolation('Check Payment/Reverse amount is high that the billed amount')->atPath('paymentAmount')->addViolation();
        }

    }
}
