<?php

namespace Core\WorkerBundle\Entity;

use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Service;
use Core\PatientBundle\Entity\ServiceMapper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\WorkerBundle\Repository\NoteRepository")
 */
class Note extends ServiceMapper
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @var string
     *
     * @ORM\Column(name="time", type="integer", nullable=true)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="units", type="integer", nullable=true)
     */
    private $units;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\Service", mappedBy="note", cascade={"all"})
     */
    private $services;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", nullable=true)
     */
    private $observations;

    /**
     * @var bool
     *
     * @ORM\Column(name="no_billable", type="boolean", nullable=true)
     */
    private $noBillable;
    
    /**
     * Note constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->services = new ArrayCollection();
        $this->observations = '<Click to edit>';
        $this->noBillable = false;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return Note
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set units
     *
     * @param integer $units
     *
     * @return Note
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * Get units
     *
     * @return integer
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return Note
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Add service
     *
     * @param Service $service
     *
     * @return Note
     */
    public function addService(Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param Service $service
     */
    public function removeService(Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return string
     */
    public function getFullPath(){
        $folder = $this->getCode() == 'T1017' ? 'tcm' : 'cmh';
        return "/pdf/notes/$folder/note" . $this->getId() . '.pdf';
    }

    /**
     * @return string
     */
    public function __toString() {
        return (string) ('Note ' . $this->getId());
    }

    /**
     * @return int
     */
    public function getUnitsUsed()
    {
        return $this->getUnits();
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return Note
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return is_null($this->observations) ? '<Click to edit>' : $this->observations;
    }

    /**
     * Set noBillable
     *
     * @param boolean $noBillable
     *
     * @return Note
     */
    public function setNoBillable($noBillable)
    {
        $this->noBillable = $noBillable;

        return $this;
    }

    /**
     * Get noBillable
     *
     * @return boolean
     */
    public function getNoBillable()
    {
        return is_null($this->noBillable) ? false : $this->noBillable;
    }

    /**
     * Get canEdit
     *
     * @return boolean
     */
    public function getCanEdit()
    {
        return $this->canEdit;
    }
}
