<?php

namespace Core\WorkerBundle\Entity;

use Core\PatientBundle\Entity\ServiceParent;
use Core\PatientBundle\Entity\ServiceModifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\WorkerBundle\Repository\ServiceAclRepository")
 */
class ServiceAcl
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rolee", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\ServiceParent")
     */
    private $services;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return ServiceAcl
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add service
     *
     * @param ServiceParent $service
     *
     * @return ServiceAcl
     */
    public function addService(ServiceParent $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param ServiceParent $service
     */
    public function removeService(ServiceParent $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }
}
