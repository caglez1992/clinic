<?php

namespace Core\WorkerBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\User;
use Core\PatientBundle\Repository\PatientRepository;
use Core\WorkerBundle\Entity\Note;
use Core\WorkerBundle\Entity\Week;
use Core\WorkerBundle\Repository\WeekRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class NoteAdmin extends BaseAdmin
{
    protected $createRoute = false;

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'serviceDate',
    );

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);

        $root = $query->getRootAliases()[0];

        if($userReal->isServiceWorker()){
            $query->andWhere("$root.worker = :userReal");
            $query->setParameter('userReal', $userReal);
        }

        if($userReal->isSupervisor()){
            $query->andWhere("$root.worker IN (:assignedWorkers)");
            $query->setParameter('assignedWorkers', $userReal->getAssignedWorkers());
        }

        if($userReal->isQa()){
            $condition = $userReal->hasRole(User::ROLE_QA_TCM) ? "sc.code = 'T1017'" : "sc.code != 'T1017'";
            $query->join("$root.serviceParent", 'sp')
                ->join("sp.code", 'sc')
                ->andWhere($condition);
        }
        
        return $query;
    }
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $userReal = $this->getRealUser();
        $d = $this->container()->get('doctrine');

        $primaryOptionsWorker = [
            'label' => 'Worker',
            'show_filter' => !$userReal->isServiceWorker(),
        ];
        $secondaryOptionsWorker = [
            'choices' => $d->getRepository(User::class)->getUsersByRole([User::ROLE_TCM, User::ROLE_CMH, User::ROLE_SPECIALIST])
        ];

        if($userReal->isSupervisor()){
            $secondaryOptionsWorker['choices'] = $userReal->getAssignedWorkers();
        }

        if($userReal->hasRole(User::ROLE_QA_TCM)){
            $secondaryOptionsWorker['choices'] = $d->getRepository(User::class)->getUsersByRole([User::ROLE_TCM]);
        }

        if($userReal->hasRole(User::ROLE_QA_CMH)){
            $secondaryOptionsWorker['choices'] = $d->getRepository(User::class)->getUsersByRole([User::ROLE_CMH, User::ROLE_SPECIALIST]);
        }

        $datagridMapper
            ->add('id', null, array(
                'label' => 'Note Code',
            ))
            ->add('patient', null, [], 'entity', [
                'query_builder' => function(PatientRepository $pr){
                    return $pr->getOpenPatients(true);
                },
            ])
            ->add('serviceDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'Date of Service',
            ))
            ->add('time', null, array(
                'label' => 'Total Time (min)',
            ))
            ->add('units')
            ->add('revised', null, array(
                'label' => 'Approved',
            ))
            ->add('approved', null, [
                'label' => 'Revised'
            ])
            ->add('week', null, ['show_filter' => true], 'entity', [
                'query_builder' => function(WeekRepository $wr){
                    return $wr->getWeeksForSelect(true);
                },
            ])
            ->add('worker', null, $primaryOptionsWorker, 'entity', $secondaryOptionsWorker)
            ->add('observations')
        ;
    }

    public function configureDefaultFilterValues(array &$filterValues)
    {
        $thisWeek = $this->container()->get('doctrine')->getRepository(Week::class)->getWeekByDay(new \DateTime());
        $filterValues['week'] = array(
            //'type'  => ChoiceFilter::TYPE_CONTAINS,
            'value' => $thisWeek->getId(),
        );
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'general.code',
            ))
            ->add('patient', null, array(
                'label' => 'Patient',
                'template' => '@BackEnd/structures/list_field_patient.html.twig'
            ))
            ->add('serviceDate', null, array(
                'label' => 'Date of Services',
                'format' => 'M d, Y'
            ))
            ->add('fullServiceCode', null, array(
                'label' => 'Service Code',
            ))
            ->add('worker')
            ->add('time', null, array(
                'label' => 'Total Time (min)',
            ))
            ->add('units', null, array(
                'label' => 'Units',
            ))
            ->add('status', null, array(
                'label' => 'Status',
                'template' => '@BackEnd/structures/list_field_nom.html.twig'
            ))
            ->add('revised', null, array(
                'label' => 'Approved',
            ))
            ->add('approved', null, [
                'label' => 'Revised'
            ])
            ->add('noBillable', null, [
                'label' => 'No Billeable',
            ])
        ;

        if($this->getRealUser()->hasRole(User::ROLE_TCM)){
            $listMapper->add('observations', 'text', [
                'editable' => true
            ]);
        }

        $actions = [
            'open_note'      => ['template' => '@BackEnd/btn_actions/note/open_pdf_note.html.twig'],
            'edit_pdf_note'  => ['template' => '@BackEnd/btn_actions/note/edit_pdf_note.html.twig'],
            'close_note'     => ['template' => '@BackEnd/btn_actions/note/close_note.html.twig'],
            'review_note'    => ['template' => '@BackEnd/btn_actions/note/review_note.html.twig'],
            'un_review_note' => ['template' => '@BackEnd/btn_actions/note/un_review_note.html.twig'],
            'approve_note'   => ['template' => '@BackEnd/btn_actions/note/approve_note.html.twig'],
            'update_billable'=> ['template' => '@BackEnd/btn_actions/note/update_billable_note.twig'],
        ];

        if($this->isGranted(User::ROLE_OPERATION_MANAGER)){
            $actions['edit'] = [];
            $actions['delete'] = [];
        }

        $listMapper->add('_action', null, array(
            'actions' => $actions,
        ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if(!$this->getRealUser()->hasRole(User::ROLE_TCM)) {
            $formMapper
                ->add('time', null, array(
                    'label' => 'Duration',
                ))
                ->add('units', null, array(
                    'label' => 'Units',
                ))
                ->add('revised', null, array(
                    'label' => 'Revised',
                ))
                ->add('dateOfService', null, array(
                    'label' => 'Date Of Service',
                ))
                ->add('patient', null, array(
                    'label' => 'Patient',
                ))
                ->add('worker', null, array(
                    'label' => 'Created By',
                ));
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.note.id',
            ))
            ->add('time', null, array(
                'label' => 'form.note.time',
            ))
            ->add('units', null, array(
                'label' => 'form.note.units',
            ))
            ->add('revised', null, array(
                'label' => 'form.note.revised',
            ))
            ->add('filename', null, array(
                'label' => 'form.note.filename',
            ))
            ->add('dateOfService', null, array(
                'label' => 'form.note.dateOfService',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.note.createdAt',
            ))
        ;
    }


    public function toString($object)
    {
        return 'Note';
    }
}
