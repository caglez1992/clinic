<?php

namespace Core\WorkerBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Validator\Constraints\Count;

class ServiceAclAdmin extends BaseAdmin
{
    protected $createRoute = false;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'form.serviceacl.id',
            ))
            ->add('role', null, array(
                'label' => 'form.serviceacl.role',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('role', null, array(
                'label' => 'Role',
            ))
            ->add('services', null, array(
                'label' => 'Services that can perform',
                'template' => '@BackEnd/structures/list_field_ul_many_to_many.html.twig',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('services', null, array(
                'label' => 'Services that can perform',
                'required' => true,
                'constraints' => [
                    new Count(['min' => 1]),
                ]
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.serviceacl.id',
            ))
            ->add('role', null, array(
                'label' => 'form.serviceacl.role',
            ))
        ;
    }

    public function toString($object)
    {
        return 'Role Management';
    }
}
