<?php

namespace Core\WorkerBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\WorkerBundle\Entity\ExplanationPayment;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\NotBlank;

class ExplanationPaymentAdmin extends BaseAdmin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'Record #',
            ))
            ->add('billingRegistry', null, array(
            ))
            ->add('patient', null, array(
            ))
            ->add('paymentNumber', null, array(

            ))
            ->add('paymentAmount', null, array(

            ))
            ->add('reversalPaymentAmount', null, array(

            ))
            ->add('interest', null, array(

            ))
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'Record #',
            ))
            ->add('billingRegistry', null, array(
            ))
            ->add('paymentNumber', null, array(
            ))
            ->add('paymentAmount', null, array(
            ))
            ->add('reversalPaymentAmount', null, array(
            ))
            ->add('interest', null, array(
            ))
            ->add('createdAt', null, array(
                'format' => 'M d, Y'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('billingRegistry', 'sonata_type_model', array(
                'btn_add' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('paymentNumber', null, array(

            ))
            ->add('paymentAmount', null, array(

            ))
            ->add('reversalPaymentAmount', null, array(

            ))
            ->add('interest', null, array(

            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.explanationpayment.id',
            ))
            ->add('paymentNumber', null, array(
                'label' => 'form.explanationpayment.paymentNumber',
            ))
            ->add('paymentAmount', null, array(
                'label' => 'form.explanationpayment.paymentAmount',
            ))
            ->add('reversalPaymentAmount', null, array(
                'label' => 'form.explanationpayment.reversalPaymentAmount',
            ))
            ->add('interest', null, array(
                'label' => 'form.explanationpayment.interest',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.explanationpayment.createdAt',
            ))
        ;
    }

    /**
     * @param ExplanationPayment $object
     */
    public function prePersist($object)
    {
        $object->setPatient($object->getBillingRegistry()->getPatient());
        $object->setCreatedBy($this->getRealUser());
    }

    public function preUpdate($object)
    {
        $this->prePersist($object);
    }

    public function toString($object)
    {
        return $this->trans('menu.entity.explanation.payment',array(),'BackEndBundle');
    }
}
