<?php

namespace Core\WorkerBundle\Repository;
use Core\CoreBundle\Entity\User;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Service;
use Core\WorkerBundle\Entity\Note;
use DateTime;
use Doctrine\ORM\EntityRepository;

/**
 * NoteRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NoteRepository extends EntityRepository
{
    /**
     * @param Service $service
     * @return Note|null
     */
    public function getNoteBySimilarService($service){
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->join('n.serviceParent', 'sp')
            ->join('sp.code', 'sc')
            ->where('n.patient = :patient')
            ->andWhere('n.worker = :worker')
            ->andWhere('n.serviceDate = :serviceDate')
            ->andWhere('sc.code = :serviceCode')
            ->setParameter('patient', $service->getPatient())
            ->setParameter('worker', $service->getWorker())
            ->setParameter('serviceDate', $service->getServiceDate())
            ->setParameter('serviceCode', $service->getCode());


        $result = $qb->getQuery()->getResult();
        return count($result) == 0 ? null : $result[0];
    }

    /**
     * @param integer $weekId
     * @param integer $workId
     * @param integer $patientId
     * @param bool $onlyBillingZero
     * @return Note[]
     */
    public function getNotesTcmForBilling($weekId, $workId, $patientId, $onlyBillingZero = false){
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->join('n.services', 's')
            ->join('s.week', 'w')
            ->join('n.serviceParent', 'sp')
            ->join('sp.code', 'sc')
            ->where('n.revised = true')
            ->andWhere('n.approved = true')
            ->andWhere('sc.code = :serviceCode')
            ->andWhere('w.id = :weekId')
            ->setParameter('weekId', $weekId)
            ->setParameter('serviceCode', 'T1017')
            ->orderBy('n.serviceDate', 'DESC');

        if($workId != -1){
            $qb->join('n.worker', 'u')
                ->andWhere('u.id = :workId')
                ->setParameter('workId', $workId);
        }

        if($patientId != -1){
            $qb->join('n.patient', 'p')
                ->andWhere('p.id = :patientId')
                ->setParameter('patientId', $patientId);
        }

        if($onlyBillingZero){
            $qb->andWhere('n.billingZero = 1');
        }

        return $qb->getQuery()->getResult();
    }

    public function getUnReviewedNotes(){
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->where('n.revised = false');
        return $qb->getQuery()->getResult();
    }
}
