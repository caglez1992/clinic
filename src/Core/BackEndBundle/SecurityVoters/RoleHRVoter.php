<?php
namespace Core\BackEndBundle\SecurityVoters;
 
 
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Service;
use Core\WorkerBundle\Entity\Note;
use JMS\DiExtraBundle\Annotation\Service as ServiceJMS;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


/**
 * @ServiceJMS("role.hr.voter", public=false)
 * @Tag("security.voter")
 */
class RoleHRVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            'ROLE_CORE_BUNDLE_ADMIN_USER_DELETE',
        ]);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param $object
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $object, TokenInterface $token)
    {
        /** @var User $userReal */
        $userReal = $token->getUser();

        if($userReal->hasRole(User::ROLE_HR) and $object instanceof User and ($object->hasRole(User::ROLE_SUPER_ADMIN) || $object->hasRole(User::ROLE_OPERATION_MANAGER)))
            return false;

        return true;
    }
}