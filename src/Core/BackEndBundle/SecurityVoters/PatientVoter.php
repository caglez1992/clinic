<?php
namespace Core\BackEndBundle\SecurityVoters;
 
 
use Core\CoreBundle\Entity\User;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


/*
 * @Service("patient.voter", public=false)
 * @Tag("security.voter")
 */
class PatientVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            'ROLE_PATIENT_BUNDLE_ADMIN_PATIENT_CREATE',
            'ROLE_PATIENT_BUNDLE_ADMIN_PATIENT_EDIT',
        ]);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $userReal */
        $userReal = $token->getUser();
        return $userReal->hasRole(User::ROLE_OPERATION_MANAGER) || $userReal->hasRole(User::ROLE_FRONT_DESK);
    }
}