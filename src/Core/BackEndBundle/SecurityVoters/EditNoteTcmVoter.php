<?php
namespace Core\BackEndBundle\SecurityVoters;
 
 
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\ServiceForm;
use Core\WorkerBundle\Entity\Note;
use JMS\DiExtraBundle\Annotation\Service as ServiceJMS;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


/**
 * @ServiceJMS("edit.note.tcm.voter", public=false)
 * @Tag("security.voter")
 */
class EditNoteTcmVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            'ROLE_NOTE_WEB_PDF_VIEWER',
        ]) && ($subject instanceof Note || $subject instanceof ServiceForm);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param Note|ServiceForm $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $userReal */
        $userReal = $token->getUser();
        
        if($userReal->hasRole(User::ROLE_OPERATION_MANAGER) || $userReal->hasRole(User::ROLE_SUPERVISOR))
            return true;

        if($subject instanceof Note){
            if(is_null($subject->getStatus()))
                return true;
            if(!$subject->getRevised())
                return true;
        }

        if($subject instanceof ServiceForm){
            return true;
        }
        
        return false;
    }
}