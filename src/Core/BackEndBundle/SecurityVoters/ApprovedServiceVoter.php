<?php
namespace Core\BackEndBundle\SecurityVoters;
 
 
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Service;
use JMS\DiExtraBundle\Annotation\Service as ServiceJMS;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


/**
 * @ServiceJMS("approved.service.voter", public=false)
 * @Tag("security.voter")
 */
class ApprovedServiceVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            'ROLE_SERVICE_APPROVE',
        ]) && $subject instanceof Service;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param Service $service
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $service, TokenInterface $token)
    {
        /** @var User $userReal */
        $userReal = $token->getUser();

        if($service->getStatus()->getName() != NomUtil::NOM_STATUS_CLOSE || !$service->getRevised() || $service->getApproved() || !is_null($service->getNote()))
            return false;

        if($userReal->hasRole(User::ROLE_OPERATION_MANAGER))
            return true;

        if($service->getType() == 'cmh' && $userReal->hasRole(User::ROLE_QA_CMH))
            return true;

        if($service->getType() == 'tcm' && $userReal->hasRole(User::ROLE_QA_TCM))
            return true;
        
        return false;
    }
}