<?php
/**
 * Created by PhpStorm.
 * User: Cesar A Glez
 * Date: 18/01/17
 * Time: 8:49
 */

namespace Core\BackEndBundle\Util;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 *
 * @DI\Service("admin.util")
 * 
 */
class AdminUtil
{
    private $container;

    /**
     * @param ContainerInterface $container
     * @DI\InjectParams({
     *     "container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function getAdminConfig(){
        $file = __DIR__.'/../Resources/config/admin.yml';
        if (is_file($file)) {
            $config = Yaml::parse(file_get_contents($file));
            return $config['entity'];
        }
        return null;
    }
}
