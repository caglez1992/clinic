<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\BackEndBundle\Controller;

use Core\CoreBundle\Entity\FormsLog;
use Core\CoreBundle\Entity\Log;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use Core\CoreBundle\Util\StaticUtil;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Service;
use Core\PatientBundle\Entity\ServiceForm;
use Core\WorkerBundle\Entity\Note;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Exception;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use mikehaertl\pdftk\Pdf;

class PdfController extends Controller
{

    /**
     * @Inject
     */
    private $translator;

    /**
     * Services Forms Viewer.
     * @Route("/service/form/viewer/{idServiceForm}", name="service_form_viewer", options={"expose"=true})
     * @param integer $idServiceForm
     * @return Response
     */
    public function serviceFormViewerAction($idServiceForm)
    {
        $d = $this->getDoctrine();
        $serviceForm = $d->getRepository(ServiceForm::class)->find($idServiceForm);
        $userReal = $this->get(UserManager::class)->getRealUser();
        $service = $serviceForm->getService();

        if($service->getWorker() != $userReal || $service->getRevised()){
            throw $this->createAccessDeniedException('Unable to access this page!');
        }

        $pdfSavePath = $this->generateUrl('ajax_save_pdf', [
            'type' => 'serviceForm',
            'idServiceForm' => $idServiceForm,
            'pdfPath' => $serviceForm->getFullPath()
        ]);
        
        $logsSaveUrl = $this->generateUrl('ajax_save_form_logs', [
            'pdfName' => $serviceForm->getName(),
            'container' => $serviceForm->getService()->__toString(),
            'identifier' => $serviceForm->getService()->getId(),
        ]);

        return $this->render('@BackEnd/custom/pdf_viewer/web_pdf_viewer.html.twig', array(
            'pdfPath' => $serviceForm->getFullPath(),
            'type' => 'serviceForm',
            'logsSaveUrl' => $logsSaveUrl,
            'pdfSavePath' => $pdfSavePath,
            'patient' => $serviceForm->getService()->getPatient(),
        ));
    }

    /**
     * Notes Viewer.
     * @Route("/note/viewer/{noteId}", name="note_viewer", options={"expose"=true})
     * @Security("has_role('ROLE_NOTE_WEB_PDF_VIEWER')")
     * @param integer $noteId
     * @return Response
     */
    public function noteViewerAction($noteId)
    {
        $d = $this->getDoctrine();
        $note = $d->getRepository(Note::class)->find($noteId);
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();

        if($userReal->isServiceWorker() && ($note->getWorker() != $userReal || $note->getRevised())){
            throw $this->createAccessDeniedException('Unable to access this page!');
        }

        $pdfSavePath = $this->generateUrl('ajax_save_pdf', [
            'type' => 'note',
            'pdfPath' => $note->getFullPath(),
            'noteId' => $note->getId(),
        ]);

        $logsSaveUrl = $this->generateUrl('ajax_save_form_logs', [
            'pdfName' => $note->__toString(),
            'container' => $note->__toString(),
            'identifier' => $note->getId(),
        ]);

        return $this->render('@BackEnd/custom/pdf_viewer/web_pdf_viewer.html.twig', array(
            'pdfPath' => $note->getFullPath(),
            'type' => 'note',
            'logsSaveUrl' => $logsSaveUrl,
            'pdfSavePath' => $pdfSavePath,
            'patient' => $note->getPatient(),
        ));
    }

    /**
     * Generic save pdf action.
     * @Route("/ajax/save/pdf", name="ajax_save_pdf", options={"expose"=true})
     * @return Response
     */
    public function ajaxSavePdfAction()
    {
        /** @var NotificationManager $nm */
        $nm = $this->get(NotificationManager::class);
        
        $request = $this->get('request_stack')->getCurrentRequest();
        $d = $this->getDoctrine();
        $dirWeb = $this->getParameter('dir.web');

        $type = $request->query->get('type');
        $pdfPath = $request->query->get('pdfPath');

        if($type == 'serviceForm'){
            $idServiceForm = $request->query->get('idServiceForm');
            $serviceForm = $d->getRepository(ServiceForm::class)->find($idServiceForm);
            $serviceForm->setFilled(true);
            $this->get(BaseManager::class)->save($serviceForm);
        }

        if($type == 'note'){
            $idNote = $request->query->get('noteId');
            $note = $d->getRepository(Note::class)->find($idNote);
            $noteAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('worker.bundle.admin.note');
            $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
            $supervisors = $d->getRepository(User::class)->getSupervisorsByWorker($note->getWorker());
            foreach ($supervisors as $supervisor) {
                $nm->createNotification($supervisor, sprintf('%s modify the note %s', $note->getWorker()->getFullName(), $note->getId()), $url, Notification::SUCCESS, true);
            }
        }

        // pull the raw binary data from the POST
        $data = $request->request->get('data');
        $data = substr($data, strpos($data, ",") + 1);

        // decode it
        $decodedData = base64_decode($data);

        // write the data out to the file
        $fp = fopen($dirWeb . $pdfPath, 'wb');
        fwrite($fp, $decodedData);
        fclose($fp);

        return new Response($dirWeb . $pdfPath);
    }

    /**
     * Save logs pdf modifications.
     * @Route("/ajax/save/logs/form", name="ajax_save_form_logs", options={"expose"=true})
     * @return Response
     */
    public function ajaxSaveFormLogsAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $pdfName = $request->query->get('pdfName');
        $containerObject = $request->query->get('container');
        $identifier = $request->query->get('identifier');
        $logs = $request->request->all();

        $userReal = $this->get(UserManager::class)->getRealUser();
        $baseManager = $this->get(BaseManager::class);

        foreach ($logs as $field => $value) {
            $formLog = new FormsLog();
            $formLog->setFormName($pdfName);
            $formLog->setContainer($containerObject);
            $formLog->setWorker($userReal);
            $formLog->setField($field);
            $formLog->setValue($value);
            $formLog->setIdentifier($identifier);
            $baseManager->save($formLog);
        }

        return new Response('ok');
    }

    /**
     * @Route("/intake/{type}/patient/{id}", name="generate_intake_pdf", options={"expose"=true})
     * @param string $type
     * @param integer $id
     * @return Response
     */
    public function generateIntakePdfAction($type, $id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);
        
        $dirPdf = $this->getParameter('dir.pdf');
        $originPDF = "$dirPdf/originals/intake_$type.pdf";

        $pdf = new Pdf($originPDF);
        $pdf->fillForm($patient->getArrayPdf($this->translator, $type))
            ->needAppearances()
            ->send('Intake' . strtoupper($type) . $patient->getId().'.pdf', true);

        return new Response('You most install pdftk to generate this pdf.');
    }

    /**
     * @Route("/generate/time/sheet/tcm/{weekId}/{workerId}", name="generate_time_sheet_tcm", options={"expose"=true})
     * @param integer $workerId
     * @param integer $weekId
     * @return Response
     */
    public function generateTimeSheetTcmAction($weekId, $workerId)
    {
        $d = $this->getDoctrine();

        $worker = $d->getRepository(User::class)->find($workerId);
        $week = $d->getRepository(Week::class)->find($weekId);
        $services = $d->getRepository(Service::class)->getServicesForBilling($weekId, $workerId, -1, NomUtil::NOM_INTAKE_TYPE_TCM);

        $totalUnits = 0;
        $totalTime = 0;
        /** @var Service $service */
        foreach ($services as $service) {
            $totalUnits += $service->getTcmTimeSheetUnits();
            $totalTime += $service->getServiceDurationMin();
        }

        $html = $this->renderView('@BackEnd/custom/billing/time_sheet_tcm.html.twig', [
            'worker' => $worker,
            'week' => $week,
            'services' => $services,
            'totalUnits' => $totalUnits,
            'totalTime' => $totalTime,
            'totalPay' => StaticUtil::tcmPayFormula($totalUnits),
        ]);
        //return new Response($html);

        $route = '/tmp/clinic/time_sheet_tcm' . $weekId . $workerId . '.pdf';

        try {
            $footer = $this->renderView('@BackEnd/custom/billing/time_sheet_footer.html.twig', ['now' => new DateTime()]);
            $this->get('knp_snappy.pdf')->generateFromHtml($html, $route, ['footer-html' => $footer], true);
        }catch (Exception $e){
            $this->get('logger')->critical(sprintf('TIME SHEET TCM Worker %s and Week %S ERROR: ', $workerId, $weekId) . $e->getMessage());
        }

        $content = file_get_contents($route);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename="' . 'time_sheet' . $weekId . $workerId . '.pdf');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Expires', '0');
        $response->headers->set('Content-Length', filesize($route));
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        $response->setContent($content);

        @unlink($route);

        return $response;
    }

    /**
     * @Route("/generate/time/sheet/cmh/{weekId}/{workerId}", name="generate_time_sheet_cmh", options={"expose"=true})
     * @param integer $workerId
     * @param integer $weekId
     * @return Response
     */
    public function generateTimeSheetCmhAction($weekId, $workerId)
    {
        $d = $this->getDoctrine();

        $worker = $d->getRepository(User::class)->find($workerId);
        $week = $d->getRepository(Week::class)->find($weekId);
        $services = $d->getRepository(Service::class)->getServicesForBilling($weekId, $workerId, -1, NomUtil::NOM_INTAKE_TYPE_CMH);

        //Group By Service Parent
        $groupedServices = [];
        $groupData = [];
        /** @var Service $service */
        foreach ($services as $service) {
            $idServiceParent = $service->getServiceParent()->getId();
            if($service->getDischarge())
                $idServiceParent = $idServiceParent.'discharge';
            $serviceDate = $service->getServiceDate()->getTimestamp();

            if(!isset($groupedServices[$idServiceParent])){
                $groupedServices[$idServiceParent] = [];
            }

            if(!isset($groupData[$idServiceParent])){
                $groupData[$idServiceParent] = [];
            }

            if(!isset($groupedServices[$idServiceParent][$serviceDate])){
                $groupedServices[$idServiceParent][$serviceDate] = [
                    'payInfo' => [
                        'count' => 0,
                        'payRate' => 0,
                        'perSession' => 0,
                        NomUtil::NOM_SERVICE_SESSION_TYPE_AM => 0,
                        NomUtil::NOM_SERVICE_SESSION_TYPE_PM => 0
                    ],
                    'serviceData' => [],
                ];
            }

            $groupedServices[$idServiceParent][$serviceDate]['serviceData'][] = $service;
            $groupData[$idServiceParent]['serviceParentName'] = $service->getServiceParent()->getName() . ($service->getDischarge() ? ' (Discharge)' : '');
            $groupData[$idServiceParent]['serviceCode'] = $service->getFullServiceCode() . ($service->getDischarge() ? ' (D)' : '');

            //update pay info
            $payInfo = $groupedServices[$idServiceParent][$serviceDate]['payInfo'];
            if($service->getCode() == 'H2017'){
                $session = $service->getSessionType()->getName();
                if($payInfo[$session] == 0){
                    $payInfo[$session] = 1;
                    $payInfo['count']++;
                }
            } else {
                $payInfo['count']++;
            }
            $payInfo['payRate'] = !$service->getDischarge() ? $service->getServiceParent()->getPayRateCmh() : 7.5;
            $payInfo['perSession'] = $payInfo['count'] * $payInfo['payRate'];
            $groupedServices[$idServiceParent][$serviceDate]['payInfo'] = $payInfo;
        }

        $totalAmount = 0;
        foreach($groupedServices as $gs) {
            foreach ($gs as $g) {
                $payInfo = $g['payInfo'];
                $totalAmount += $payInfo['perSession'];
            }
        }
        
        //dump($groupedServices, $groupData);die;

        $html = $this->renderView('@BackEnd/custom/billing/time_sheet_cmh.html.twig', [
            'worker' => $worker,
            'week' => $week,
            'groupedServices' => $groupedServices,
            'groupData' => $groupData,
            'totalAmount' => $totalAmount,
        ]);
//        return new Response($html);

        $route = '/tmp/clinic/time_sheet_cmh' . $weekId . $workerId . '.pdf';

        try {
            $footer = $this->renderView('@BackEnd/custom/billing/time_sheet_footer.html.twig', ['now' => new DateTime()]);
            $this->get('knp_snappy.pdf')->generateFromHtml($html, $route, ['footer-html' => $footer], true);
        }catch (Exception $e){
            $this->get('logger')->critical(sprintf('TIME SHEET CMH Worker %s and Week %S ERROR: ', $workerId, $weekId) . $e->getMessage());
        }

        $content = file_get_contents($route);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename="' . 'time_sheet' . $weekId . $workerId . '.pdf');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Expires', '0');
        $response->headers->set('Content-Length', filesize($route));
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        $response->setContent($content);

        @unlink($route);

        return $response;
    }
}