<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\BackEndBundle\Controller;

use Core\CoreBundle\Entity\Notification;
use Core\PatientBundle\Controller\PatientController;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\Service;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ServiceCRUDController extends BaseController
{
    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionCloseService(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $this->admin->checkAccess('edit');

        $services = $selectedModelQuery->execute();
        $patientController = new PatientController();
        $patientController->setContainer($this->container);

        // do the work here
        try {
            /** @var Service $service */
            foreach ($services as $service) {
                if(!$service->isClose()){
                    $patientController->closeService($service->getId());
                }
            }
        } catch (\Exception $e) {
            $this->addFlash(Notification::ERROR, 'Could not complete action');

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }
}