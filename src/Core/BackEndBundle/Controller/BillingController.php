<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\BackEndBundle\Controller;


use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\BillingRegistry;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Service;
use Core\PatientBundle\Entity\ServiceMapper;
use Core\WorkerBundle\Entity\ExplanationPayment;
use Core\WorkerBundle\Entity\Note;
use Core\WorkerBundle\Entity\Week;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BillingController extends Controller
{

    /**
     * @Route("/{type}", name="show_billing", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_WORKER', 'ROLE_BILLING')")
     * @param string $type
     * @return Response
     */
    public function showBillingAction($type)
    {
        $d = $this->getDoctrine();

        if($type == 'tcm')
            $roles = User::ROLE_TCM;
        else
            $roles = [User::ROLE_CMH, User::ROLE_SPECIALIST];
        $workers = $d->getRepository(User::class)->getUsersByRole($roles);

        $weeks = $d->getRepository(Week::class)->getWeeksForSelect();

        return $this->render('@BackEnd/custom/billing/show_billing.html.twig', [
            'type' => $type,
            'workers' => $workers,
            'weeks' => $weeks,
        ]);
    }

    /**
     * @Route("/ajax/patients/{type}/{weekId}/{workId}", name="ajax_show_billing_patients", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_WORKER', 'ROLE_BILLING')")
     * @param string $type
     * @param integer $weekId
     * @param integer $workId
     * @return Response
     */
    public function ajaxShowBillingPatientsAction($type, $weekId, $workId)
    {
        $d = $this->getDoctrine();

        $speciality = $type == 'tcm' ? NomUtil::NOM_INTAKE_TYPE_TCM : NomUtil::NOM_INTAKE_TYPE_CMH;
        $patients = $d->getRepository(Patient::class)->getPatientForBilling($weekId, $workId, $speciality);

        return $this->render('@BackEnd/custom/billing/patients_billing.html.twig', [
            'patients' => $patients,
        ]);
    }

    /**
     * @Route("/ajax/services/tcm/{weekId}/{workId}/{patientId}", name="ajax_show_billing_services_tcm", options={"expose"=true})
     * @Security("has_role('ROLE_TCM', 'ROLE_BILLING')")
     * @param integer $weekId
     * @param integer $workId
     * @param integer $patientId
     * @return Response
     */
    public function ajaxShowBillingServicesTcmAction($weekId, $workId, $patientId)
    {
        $d = $this->getDoctrine();
        $allNotes = $d->getRepository(Note::class)->getNotesTcmForBilling($weekId, $workId, $patientId);
        $billingZeroNotes = $d->getRepository(Note::class)->getNotesTcmForBilling($weekId, $workId, $patientId, true);
        return $this->render('@BackEnd/custom/billing/services_billing_tcm.html.twig', [
            'allNotes' => $allNotes,
            'billingZeroNotes' => $billingZeroNotes,
            'actualStats' => $this->noteTotals($allNotes),
        ]);
    }

    /**
     * @Route("/ajax/services/cmh/{weekId}/{workId}/{patientId}", name="ajax_show_billing_services_cmh", options={"expose"=true})
     * @Security("has_role('ROLE_CMH', 'ROLE_BILLING')")
     * @param integer $weekId
     * @param integer $workId
     * @param integer $patientId
     * @return Response
     */
    public function ajaxShowBillingServicesCmhAction($weekId, $workId, $patientId)
    {
        $d = $this->getDoctrine();
        $allServices = $d->getRepository(Service::class)->getServicesForBilling($weekId, $workId, $patientId, NomUtil::NOM_INTAKE_TYPE_CMH);
        $billingZeroServices = $d->getRepository(Service::class)->getServicesForBilling($weekId, $workId, $patientId, NomUtil::NOM_INTAKE_TYPE_CMH, true);
        
        return $this->render('@BackEnd/custom/billing/services_billing_cmh.html.twig', [
            'allServices' => $allServices,
            'billingZeroServices' => $billingZeroServices,
            'actualStats' => $this->serviceTotals($allServices),
        ]);
    }

    /**
     * @Route("/ajax/view/services/note/{noteId}", name="ajax_view_services_note", options={"expose"=true})
     * @param integer $noteId
     * @return Response
     */
    public function ajaxViewServicesNoteAction($noteId)
    {
        $d = $this->getDoctrine();
        $note = $d->getRepository(Note::class)->find($noteId);
        return $this->render('@BackEnd/custom/billing/ajax_modal_body_service_note.html.twig', [
            'note' => $note,
        ]);
    }

    /**
     * @Route("/ajax/toggle/boolean/{property}/{objectType}/{objectId}", name="ajax_toggle_billing_property", options={"expose"=true})
     * @Security("has_role('ROLE_BILLING')")
     * @param string $property
     * @param string $objectType
     * @param integer $objectId
     * @return Response
     */
    public function ajaxTogglePropertyAction($property, $objectType, $objectId)
    {
        $d = $this->getDoctrine();
        $bm = $this->get(BaseManager::class);
        $nm = $this->get(NotificationManager::class);
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();

        $response = new Response('ok');

        $class = NomUtil::REL[$objectType];
        /** @var ServiceMapper $object */
        $object = $d->getRepository($class)->find($objectId);
        
        $serviceParent = $object->getServiceParent();
        $hmo = $object->getHmo();
        $serviceCode = $object->getCode();
        $patient = $object->getPatient();

        //Service not billed, make all operation to billed it, discount units to authorization if not hmo free.
        if($property == 'billed' && !$object->getBilled()){
            //Create a new billed registry
            $billingRegistry = new BillingRegistry();
            $billingRegistry->setCreatedBy($userReal);
            $billingRegistry->setHmo($hmo);

            if(!$hmo->getFree() && $serviceParent->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_INTERVENTION){
                $authorization = $d->getRepository(Authorization::class)->findAuthorization($patient, $hmo, $serviceCode);
                if(is_null($authorization)){
                    $nm->createNotification($userReal, 'Service can not be billed, authorization not found.', $url = null, Notification::ERROR, $important = true, $dataMsg = array(), $pusher = true, false);
                    return $response;
                }
                $billingRegistry->setAuthorization($authorization);
            }

            $msgNotification = '';
            if($objectType == 'note'){
                $billingRegistry->setNote($object);
                $billingRegistry->setUnitsConsumed($object->getUnitsUsed());
                $billingRegistry->setAmount($object->getAmount());

                /** @var Service $s */
                foreach ($object->getServices() as $s) {
                    $s->setBilled(true);
                    $bm->save($s);
                }

                $msgNotification = sprintf('Billed note %s successfully', $object->getId());
            } else {
                $billingRegistry->setService($object);
                $billingRegistry->setUnitsConsumed($object->getUnitsUsed());
                $billingRegistry->setAmount($object->getAmount());

                $msgNotification = sprintf('Billed service %s successfully', $object->getId());
            }

            $object->setBilled(true);
            $bm->save($object);

            $bm->save($billingRegistry);

            //Update record units consumed authorization
            if(!is_null($billingRegistry->getAuthorization())){
                $authorization = $billingRegistry->getAuthorization();
                $authorization->setUnitsConsumed($authorization->getTotalUnitsConsumed());
                $bm->save($authorization);
            }

            $nm->createNotification($userReal, $msgNotification, $url = null, Notification::SUCCESS, $important = false, $dataMsg = array(), $pusher = true, false);
        }

        if($property == 'payed' && !$object->getPayed()){
            if(!$object->getBilled()){
                $nm->createNotification($userReal, 'Can not check as payed, because is not billed yet.', $url = null, Notification::ERROR, $important = false, $dataMsg = array(), $pusher = true, false);
                return $response;
            }

            if($objectType == 'note'){
                $note = $object;
                /** @var Service $s */
                foreach ($note->getServices() as $s) {
                    $s->setPayed(true);
                    $bm->save($s);
                }
                $nm->createNotification($userReal, sprintf('Check as payed note %s successfully', $note->getId()), null, Notification::SUCCESS, false, [], true, false);
            } else {
                $nm->createNotification($userReal, sprintf('Check as payed service %s successfully', $object->getId()), $url = null, Notification::SUCCESS, $important = false, $dataMsg = array(), $pusher = true, false);
            }

            $object->setPayed(true);
            $bm->save($object);

            //Update billing record payed row.
            $bRecord = $d->getRepository(BillingRegistry::class)->findByObject($object);
            if(!is_null($bRecord)){
                $bRecord->setPayed(true);
                $bm->save($bRecord);
            }
        }

        return $response;
    }

    private function noteTotals($notes){
        $stats = [
            'amountTotal' => 0,
            'unitsTotal' => 0,
            'insurances' => [],
            'desglose' => [],
        ];

        /** @var Note $note */
        foreach ($notes as $note) {
            if (!$note->getBillingZero()) {
                $stats['unitsTotal'] += $note->getUnits();

                /** @var Hmo $hmo */
                $hmo = $note->getHmo();
                $billed = $note->getBilled();
                $payed = $note->getPayed();

                if (!isset($stats['insurances'][$hmo->getId()])) {
                    $stats['insurances'][$hmo->getId()] = $hmo;
                }

                if (!isset($stats['desglose'][$hmo->getId()])) {
                    $stats['desglose'][$hmo->getId()] = [
                        'units' => 0,
                        'unitsBilled' => 0,
                        'unitsPayed' => 0,
                        'amount' => 0,
                        'amountBilled' => 0,
                        'amountPayed' => 0,
                        'percent' => 0,
                    ];
                }

                $stats['desglose'][$hmo->getId()]['units'] += $note->getUnits();
                $stats['desglose'][$hmo->getId()]['amount'] = $stats['desglose'][$hmo->getId()]['units'] * 12;

                if ($billed) {
                    $stats['desglose'][$hmo->getId()]['unitsBilled'] += $note->getUnits();
                    $stats['desglose'][$hmo->getId()]['amountBilled'] = $stats['desglose'][$hmo->getId()]['unitsBilled'] * 12;
                }

                if ($payed) {
                    $stats['desglose'][$hmo->getId()]['unitsPayed'] += $note->getUnits();
                    $stats['desglose'][$hmo->getId()]['amountPayed'] = $stats['desglose'][$hmo->getId()]['unitsPayed'] * 12;
                }

                $pTotal = $stats['desglose'][$hmo->getId()]['amount'];
                $stats['desglose'][$hmo->getId()]['percent'] = $pTotal != 0 ? $stats['desglose'][$hmo->getId()]['amountPayed'] * 100 / $pTotal : 0;
            }
        }

        $stats['amountTotal'] = $stats['unitsTotal'] * 12;
        return $stats;
    }

    private function serviceTotals($services){
        $stats = [
            'amountTotal' => 0,
            'unitsTotal' => 0,
            'insurances' => [],
            'desglose' => [],
        ];

        /** @var Service $service */
        foreach ($services as $service) {
            if (!$service->getBillingZero()) {
                $stats['unitsTotal'] += $service->getUnitsUsed();
                $stats['amountTotal'] += $service->getAmount();

                /** @var Hmo $hmo */
                $hmo = $service->getHmo();
                $billed = $service->getBilled();
                $payed = $service->getPayed();

                if (!isset($stats['insurances'][$hmo->getId()])) {
                    $stats['insurances'][$hmo->getId()] = $hmo;
                }

                if (!isset($stats['desglose'][$hmo->getId()])) {
                    $stats['desglose'][$hmo->getId()] = [
                        'units' => 0,
                        'unitsBilled' => 0,
                        'unitsPayed' => 0,
                        'amount' => 0,
                        'amountBilled' => 0,
                        'amountPayed' => 0,
                        'percent' => 0,
                    ];
                }

                $stats['desglose'][$hmo->getId()]['units'] += $service->getUnitsUsed();
                $stats['desglose'][$hmo->getId()]['amount'] += $service->getAmount();

                if ($billed) {
                    $stats['desglose'][$hmo->getId()]['unitsBilled'] += $service->getUnitsUsed();
                    $stats['desglose'][$hmo->getId()]['amountBilled'] += $service->getAmount();
                }

                if ($payed) {
                    $stats['desglose'][$hmo->getId()]['unitsPayed'] += $service->getUnitsUsed();
                    $stats['desglose'][$hmo->getId()]['amountPayed'] += $service->getAmount();
                }

                $pTotal = $stats['desglose'][$hmo->getId()]['amount'];
                $stats['desglose'][$hmo->getId()]['percent'] = $pTotal != 0 ? $stats['desglose'][$hmo->getId()]['amountPayed'] * 100 / $pTotal : 0;
            }
        }

        return $stats;
    }

    /**
     * @Route("/ajax/show/consume/authorization/{id}", name="ajax_view_consume_auth", options={"expose"=true})
     * @Security("has_role('ROLE_BILLING')")
     * @param integer $id
     * @return Response
     */
    public function ajaxViewConsumeAuth($id)
    {
        $d = $this->getDoctrine();
        $desglose = $d->getRepository(BillingRegistry::class)->getByAuthId($id);
        return $this->render('@BackEnd/authorization_admin/ajax_show_consume.html.twig', [
            'desglose' => $desglose,
        ]);
    }

    /**
     * @Route("/finance/view", name="finance", options={"expose"=true})
     * @Security("has_role('ROLE_BILLING')")
     * @return Response
     */
    public function showFinanceAction()
    {
        $d = $this->getDoctrine();
        $patients = $d->getRepository(Patient::class)->findAll();
        return $this->render('@BackEnd/custom/finance/show_finance.html.twig', [
            'patients' => $patients,
        ]);
    }

    /**
     * @Route("/show/patient/details/{id}", name="show_patient_details", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function showPatientDetailsAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);
        return $this->render('@BackEnd/custom/finance/show_patient_details_section.html.twig', [
            'patient' => $patient,
        ]);
    }

    /**
     * @Route("/show/patient/finances/{id}", name="show_patient_finances", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function showPatientFinancesAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);
        $billingRegistry = $d->getRepository(BillingRegistry::class)->findByPatient($patient);
        $payments = $d->getRepository(ExplanationPayment::class)->findBy(['patient' => $patient]);
        return $this->render('@BackEnd/custom/finance/show_patient_finances_section.html.twig', [
            'patient' => $patient,
            'billingRegistry' => $billingRegistry,
            'payments' => $payments,
        ]);
    }
}