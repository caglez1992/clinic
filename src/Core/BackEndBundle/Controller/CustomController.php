<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\BackEndBundle\Controller;

use Core\CoreBundle\Util\NomUtil;
use Core\WorkerBundle\Entity\Note;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CustomController extends Controller
{

    /**
     * @Route("/{element}/{id}", name="show_element", options={"expose"=true})
     * @param string $element
     * @param integer $id
     * @return Response
     */
    public function showElementAction($element, $id)
    {
        return $this->redirect($this->generateUrl('show_'.$element, ['id' => $id]));
    }

    /**
     * @Route("/update/billable/note/{idNote}", name="update_billable_note", options={"expose"=true})
     * @Security("has_role('ROLE_SERVICE_WORKER')")
     * @param $idNote
     * @return Response
     */
    public function updateBillableNoteAction($idNote)
    {
        $noteAdmin = $this->get('sonata.admin.pool')->getAdminByClass(Note::class);
        $d = $this->getDoctrine();
        
        $note = $d->getRepository(Note::class)->find($idNote);
        $this->get('pdf.util')->updateTamNoteBillableStatus($note);

        $url = $noteAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $note->getWeek()->getId(), $note->getWorker()->getId());
        return $this->redirect($url);
    }
}