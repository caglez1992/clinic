<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\BackEndBundle\Controller;

use Core\PatientBundle\Entity\Authorization;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorizationCRUDController extends MyCRUDController
{
    /**
     * This method can be overloaded in your custom CRUD controller.
     * It's called from createAction.
     *
     * @param Request $request
     * @param Authorization $object
     * @return null|Response
     */
    protected function preCreate(Request $request, $object)
    {
        //Check all conditions of the new authorization.
        $isAuthOk = true;
        $d = $this->getDoctrine();

        $olderAuthorizations = $d->getRepository(Authorization::class)->olderAuthorizations($object->getPatient(), $object->getHmo(), $object->getServiceCodes());

        $rangeA = [
            'begin' => $object->getBeginDate(),
            'end' => $object->getEndDate(),
        ];

        $msgError = '';
        /** @var Authorization $authorization */
        foreach($olderAuthorizations as $authorization){
            //Check date ranges
            $rangeB = [
                'begin' => $authorization->getBeginDate(),
                'end' => $authorization->getEndDate(),
            ];
            if(!$this->get('main.util')->datesFreeIntersection($rangeA, $rangeB)){
                $isAuthOk = false;
                $msgError = sprintf(
                    'Can not create authorization. Already exist a similar authorization with unique code (%s) schedule for %s to %s',
                    $authorization->getId(),
                    $authorization->getBeginDate()->format('M d, Y'),
                    $authorization->getEndDate()->format('M d, Y')
                );
                break;
            }
        }

        if($isAuthOk)
            return null;
        else{
            $this->addFlash('sonata_flash_error', $msgError);
        }

        $form = $this->admin->getForm();
        $formView = $form->createView();
        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFormTheme());
        // NEXT_MAJOR: Remove this line and use commented line below it instead
        $template = $this->admin->getTemplate('edit');
        // $template = $this->templateRegistry->getTemplate($templateKey);
        return $this->renderWithExtraParams($template, [
            'action' => 'create',
            'form' => $formView,
            'object' => $object,
            'objectId' => null,
            'r' => $request->query->has('r') ? $request->query->get('r') : null,
        ], null);
    }
}