<?php

namespace Core\BackEndBundle\Block;

use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @author LocurA <caglez1992@gmail.com>
 * @DI\Service("dashboard.block.chart", public=true)
 * @DI\Tag("sonata.block")
 */
class ChartBlockService extends AbstractBlockService
{
    private $pool;

    private $container;

    /**
     * @param string          $name
     * @param EngineInterface $templating
     * @param Pool            $pool
     * @param Container $container
     *
     * @DI\InjectParams({
     *     "name" = @DI\Inject("templating"),
     *     "templating" = @DI\Inject("templating"),
     *     "pool" = @DI\Inject("sonata.admin.pool"),
     *     "container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct($name, EngineInterface $templating, Pool $pool, Container $container)
    {
        $name = 'dashboard.block.chart';
        parent::__construct($name, $templating);

        $this->pool = $pool;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderPrivateResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin.test';
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'template' => '@BackEnd/structures/blocks/chart_block.html.twig',
        ));
    }
}
