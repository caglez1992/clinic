function initWidgster(){
    $('.widget').widgster();
}

function initNotifications() {
    var theme = 'air';
    var classes = 'messenger-fixed messenger-on-top messenger-on-right';
    $.globalMessenger({ extraClasses: classes, theme: theme });
    Messenger.options = { extraClasses: classes, theme: theme };

    $('#notifications').on('click', function (evt) {
        $('#notifications span.count').addClass('hidden');
        $.getq('main', urlReadNotifications);
    });
}

function initDataTables(customSelector){
    /* Set the defaults for DataTables initialisation */
    $.extend( true, $.fn.dataTable.defaults, {
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        }
    } );


    /* Default class modification */
    $.extend( $.fn.dataTableExt.oStdClasses, {
        "sWrapper": "dataTables_wrapper form-inline"
    } );


    /* API method to get paging information */
    $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
    {
        return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
        };
    };


    /* Bootstrap style pagination control */
    $.extend( $.fn.dataTableExt.oPagination, {
        "bootstrap": {
            "fnInit": function( oSettings, nPaging, fnDraw ) {
                var oLang = oSettings.oLanguage.oPaginate;
                var fnClickHandler = function ( e ) {
                    e.preventDefault();
                    if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                        fnDraw( oSettings );
                    }
                };

                $(nPaging).append(
                    '<ul class="pagination no-margin">'+
                    '<li class="prev disabled"><a href="#">'+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+'</a></li>'+
                    '</ul>'
                );
                var els = $('a', nPaging);
                $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
                $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
            },

            "fnUpdate": function ( oSettings, fnDraw ) {
                var iListLength = 5;
                var oPaging = oSettings.oInstance.fnPagingInfo();
                var an = oSettings.aanFeatures.p;
                var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

                if ( oPaging.iTotalPages < iListLength) {
                    iStart = 1;
                    iEnd = oPaging.iTotalPages;
                }
                else if ( oPaging.iPage <= iHalf ) {
                    iStart = 1;
                    iEnd = iListLength;
                } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                    iStart = oPaging.iTotalPages - iListLength + 1;
                    iEnd = oPaging.iTotalPages;
                } else {
                    iStart = oPaging.iPage - iHalf + 1;
                    iEnd = iStart + iListLength - 1;
                }

                for ( i=0, ien=an.length ; i<ien ; i++ ) {
                    // Remove the middle elements
                    $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                    // Add the new list items and their event handlers
                    for ( j=iStart ; j<=iEnd ; j++ ) {
                        sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                        $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                            .insertBefore( $('li:last', an[i])[0] )
                            .bind('click', function (e) {
                                e.preventDefault();
                                oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                                fnDraw( oSettings );
                            } );
                    }

                    // Add / remove disabled classes from the static elements
                    if ( oPaging.iPage === 0 ) {
                        $('li:first', an[i]).addClass('disabled');
                    } else {
                        $('li:first', an[i]).removeClass('disabled');
                    }

                    if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                        $('li:last', an[i]).addClass('disabled');
                    } else {
                        $('li:last', an[i]).removeClass('disabled');
                    }
                }
            }
        }
    } );

    var selector = $('.datatable-jquery');
    if(customSelector)
       selector = customSelector;
    
    //var unsortableColumns = [];
    /*$('.datatable-jquery').find('thead th').each(function(){
        if ($(this).hasClass('no-sort')){
            unsortableColumns.push({"bSortable": false});
        } else {
            unsortableColumns.push(null);
        }
    });*/

    selector.each(function () {
        var unsortableColumns = [];

        $(this).find('thead th').each(function () {
            if ($(this).hasClass('no-sort')) {
                unsortableColumns.push({"bSortable": false});
            } else {
                unsortableColumns.push(null);
            }
        });

        $(this).dataTable({
            "sDom": "<'row'<'col-md-6 hidden-xs'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "oLanguage": {
                "sLengthMenu": "_MENU_",
                "sInfo": "Showing <strong>_START_ to _END_</strong> of _TOTAL_ entries",
                'sSearch': '',
                "sSearchPlaceholder": "Search"
            },
            "oClasses": {
                "sFilter": "pull-right",
                "sFilterInput": "form-control input-transparent ml-sm"
            },
            "aoColumns": unsortableColumns,
            "aaSorting": [],
            "bProcessing": true
        });
    });

    /*$(".datatable-jquery").dataTable({
        "sDom": "<'row'<'col-md-6 hidden-xs'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_",
            "sInfo": "Showing <strong>_START_ to _END_</strong> of _TOTAL_ entries",
            'sSearch': '',
            "sSearchPlaceholder": "Search"
        },
        "oClasses": {
            "sFilter": "pull-right",
            "sFilterInput": "form-control input-transparent ml-sm"
        },
        "aoColumns": unsortableColumns,
        "aaSorting": [],
        "bProcessing": true
    });*/

    $(".dataTables_length select").selectpicker({
        width: 'auto'
    });
}

function initCopyField() {

    /* Get the text field */
    $('.copy-field').on('click', function () {
        var selectorId = $(this).data('id');

        var copyText = document.getElementById(selectorId);

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        //alert("Copied the text: " + copyText.value);
        Messenger().post("Copied text: " + copyText.value);
    });
}

function initSlimScroll(){
    $(".myScroll").each(function (evt) {
        var h = $(this).data('h');
        $(this).slimscroll({
            height: h, //auto
            size: '5px',
            alwaysVisible: false,
            railVisible: true
        });
    });
}

function initJqueryMask(){
    $(".mask-phone").mask("(999) 999-9999");
    $(".mask-social-security").mask("999-99-9999");
}

function initSideMenuAddElement(){
    $('.addNewElement').on('click', function (evt) {
        evt.preventDefault();
        window.location.href = $(this).data('url');
    })
}

function initTemplate() {
    initWidgster();
    initNotifications();
    initDataTables();
    initCopyField();
    initSlimScroll();
    initJqueryMask();
    initSideMenuAddElement();
    $('.dropdown-toggle').dropdown();
}
