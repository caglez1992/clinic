//Connect to WebSocket
var webSocket;
var webSocketConnect = false;

function socketConnect() {
    webSocket = WS.connect(_WS_URI);

    webSocket.on("socket/connect", function(session){
        //session is an Autobahn JS WAMP session.
        console.log("WebSocket Connection Succesufuly");
        webSocketConnect = true;

        //Create a event to notify that websocket is connected.
        var evt = $.Event('onSocketConnect');
        evt.state = session;
        $(window).trigger(evt);
    });

    webSocket.on("socket/disconnect", function(error){
        console.log("Disconnected for " + error.reason + " with code " + error.code);
        webSocketConnect = false;
    });
}

function setIntervalReconnectWS(time) {
    //Reconnect Web Socket
    setInterval(function() {
        if(!webSocketConnect){
            console.log('Try to connect to server');
            socketConnect();
        }
    }, time);
}