<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Loggable;
use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Util\NomUtil;
use Core\WorkerBundle\Entity\Note;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\ServiceRepository")
 */
class Service extends ServiceMapper implements Loggable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="services", cascade={"persist"})
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $patient;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="schedule_start", type="datetime", nullable=true)
     */
    private $scheduleStart;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="schedule_end", type="datetime", nullable=true)
     */
    private $scheduleEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="justification", type="text", length=255, nullable=true)
     */
    private $justification;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\ServiceForm", mappedBy="service", cascade={"all"})
     */
    private $forms;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="presence_id", referencedColumnName="id")
     */
    private $presence;

    /**
     * @ORM\ManyToOne(targetEntity="Core\WorkerBundle\Entity\Note", inversedBy="services")
     * @ORM\JoinColumn(name="note_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $note;

    /**
     * @var bool
     *
     * @ORM\Column(name="discharge", type="boolean", nullable=true)
     */
    private $discharge;

    private $pdfForms;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\ServiceAnnex", mappedBy="service", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $annexes;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->forms = new ArrayCollection();
        $this->annexes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPdfForms()
    {
        return $this->pdfForms;
    }

    /**
     * @param mixed $pdfForms
     */
    public function setPdfForms($pdfForms)
    {
        $this->pdfForms = $pdfForms;
    }

    /**
     * @param Patient $patient
     * @return Service
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        if(!is_null($this->getServiceParent())){
            if($this->getServiceParent()->getMedicare() && !is_null($this->getPatient()->getMedicare()) ){
                $this->setHmo($this->getPatient()->getMedicare()->getHmo());
            } else if($this->getServiceParent()->getMedicaid() && !is_null($this->getPatient()->getMedicaid())){
                $this->setHmo($this->getPatient()->getMedicaid()->getHmo());
            }
        }

        if(!is_null($this->getPatient()->getActiveDiagnosis())){
            $this->setDiagnosisTemplate($this->getPatient()->getActiveDiagnosis()->getDiagnosisTemplate());
        }

        return $this;
    }

    /**
     * @return Patient
     */
    public function getPatient(){
        return $this->patient;
    }

    /**
     * Set scheduleStart
     *
     * @param \DateTime $scheduleStart
     *
     * @return Service
     */
    public function setScheduleStart($scheduleStart)
    {
        $this->scheduleStart = $scheduleStart;

        return $this;
    }

    /**
     * Get scheduleStart
     *
     * @return \DateTime
     */
    public function getScheduleStart()
    {
        return $this->scheduleStart;
    }

    /**
     * Set scheduleEnd
     *
     * @param \DateTime $scheduleEnd
     *
     * @return Service
     */
    public function setScheduleEnd($scheduleEnd)
    {
        $this->scheduleEnd = $scheduleEnd;

        return $this;
    }

    /**
     * Get scheduleEnd
     *
     * @return \DateTime
     */
    public function getScheduleEnd()
    {
        return $this->scheduleEnd;
    }

    /**
     * Set justification
     *
     * @param string $justification
     *
     * @return Service
     */
    public function setJustification($justification)
    {
        $this->justification = $justification;

        return $this;
    }

    /**
     * Get justification
     *
     * @return string
     */
    public function getJustification()
    {
        return $this->justification;
    }
    
    /**
     * Add form
     *
     * @param ServiceForm $form
     *
     * @return Service
     */
    public function addForm(ServiceForm $form)
    {
        $this->forms[] = $form;

        return $this;
    }

    /**
     * Remove form
     *
     * @param ServiceForm $form
     */
    public function removeForm(ServiceForm $form)
    {
        $this->forms->removeElement($form);
    }

    /**
     * Set presence
     *
     * @param Nomenclator $presence
     *
     * @return Service
     */
    public function setPresence(Nomenclator $presence = null)
    {
        $this->presence = $presence;

        return $this;
    }

    /**
     * Get presence
     *
     * @return Nomenclator
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Get forms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForms()
    {
        return $this->forms;
    }

    /**
     * Set note
     *
     * @param Note $note
     *
     * @return Service
     */
    public function setNote(Note $note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return Note
     */
    public function getNote()
    {
        return $this->note;
    }

    public function getLabel()
    {
        return 'menu.entity.service';
    }

    /**
     * @return float|int
     */
    public function getServiceDurationMin(){
        if($this->getServiceParent()->hasPreDuration())
            return $this->getServiceParent()->getMaxDuration();

        $duration = $this->getScheduleEnd()->getTimestamp() - $this->getScheduleStart()->getTimestamp();
        return $duration/60;
    }

    /**
     * @return int|string
     */
    public function getUnitsUsed(){
        if($this->getServiceParent()->isFeePerService())
            return 1;
        if($this->getServiceParent()->getSpecialty() == NomUtil::NOM_INTAKE_TYPE_TCM)
            return $this->getTcmTimeSheetUnits();

        if($this->getServiceParent()->getTimePerUnits() == 0)
            return 0;

        $units = $this->getServiceDurationMin() / $this->getServiceParent()->getTimePerUnits();
        return (int) $units;
    }

    /**
     * @return int
     */
    public function getTcmTimeSheetUnits(){
        return (int)(($this->getServiceDurationMin() + 7 ) / 15);
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function checkDateAndTimes($context)
    {
        if(!$this->getServiceParent()->hasPreDuration()){
            $ss = $this->getScheduleStart();
            $se = $this->getScheduleEnd();

            if(is_null($ss) || is_null($se)){
                return;
            }

            if($ss->getTimestamp() >= $se->getTimestamp()){
                $context->buildViolation('Schedule end can not be less or equal than schedule start')->atPath('scheduleEnd')->addViolation();
            }

            $duration = ($se->getTimestamp() - $ss->getTimestamp())/60;

            if(!is_null($this->getServiceParent()->getMaxDuration()) && $this->getServiceParent()->getMaxDuration() < $duration){
                $context->buildViolation(sprintf('Can not create this service, the high duration for this service is %s minutes.', $this->getServiceParent()->getMaxDuration()))->atPath('scheduleEnd')->addViolation();
            }
        }

        if(is_null($this->getScheduleStart())){
            $context->buildViolation('This value should not be blank.')->atPath('scheduleStart')->addViolation();
            return;
        }

        $serviceDateSS = DateTime::createFromFormat('Y-m-d H:i', $this->getScheduleStart()->format('Y-m-d') . ' 00:00');

        if(!$this->getServiceParent()->hasPreDuration()) {
            $serviceDateSE = DateTime::createFromFormat('Y-m-d H:i', $this->getScheduleEnd()->format('Y-m-d') . ' 00:00');

            //Check that the service has the same serviceDate
            if ($serviceDateSS->getTimestamp() != $serviceDateSE->getTimestamp()) {
                $context->buildViolation('Can not create this service, the duration for a service can not exceed to another day.')->atPath('scheduleEnd')->addViolation();
            }
        }

        $this->serviceDate = $serviceDateSS;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s [Record #%s]', $this->getFullServiceCode(), $this->getId());
    }

    /**
     * Set discharge
     *
     * @param boolean $discharge
     *
     * @return Service
     */
    public function setDischarge($discharge)
    {
        $this->discharge = $discharge;

        return $this;
    }

    /**
     * Get discharge
     *
     * @return boolean
     */
    public function getDischarge()
    {
        return $this->discharge;
    }

    /**
     * Add annex
     *
     * @param ServiceAnnex $annex
     *
     * @return Service
     */
    public function addAnnex(ServiceAnnex $annex)
    {
        $this->annexes[] = $annex;

        return $this;
    }

    /**
     * Remove annex
     *
     * @param ServiceAnnex $annex
     */
    public function removeAnnex(ServiceAnnex $annex)
    {
        $this->annexes->removeElement($annex);
    }

    /**
     * Get annexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnnexes()
    {
        return $this->annexes;
    }

    /**
     * Set canEdit
     *
     * @param boolean $canEdit
     *
     * @return Service
     */
    public function setCanEdit($canEdit)
    {
        $this->canEdit = $canEdit;

        return $this;
    }

    /**
     * Get canEdit
     *
     * @return boolean
     */
    public function getCanEdit()
    {
        return $this->canEdit;
    }

    /**
     * Get billingRetroActive
     *
     * @return boolean
     */
    public function getBillingRetroActive()
    {
        return $this->billingRetroActive;
    }
}
