<?php

namespace Core\PatientBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\GroupPatientRegistryRepository")
 */
class GroupPatientRegistry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="groupPatientRecords")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $group;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="in_date", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $inDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="out_date", type="datetime", nullable=true)
     */
    private $outDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * GroupPatient constructor.
     */
    public function __construct()
    {
        $this->inDate = new DateTime();
        $this->active = true;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inDate
     *
     * @param \DateTime $inDate
     *
     * @return GroupPatientRegistry
     */
    public function setInDate($inDate)
    {
        $this->inDate = $inDate;

        return $this;
    }

    /**
     * Get inDate
     *
     * @return \DateTime
     */
    public function getInDate()
    {
        return $this->inDate;
    }

    /**
     * Set outDate
     *
     * @param \DateTime $outDate
     *
     * @return GroupPatientRegistry
     */
    public function setOutDate($outDate)
    {
        $this->outDate = $outDate;

        return $this;
    }

    /**
     * Get outDate
     *
     * @return \DateTime
     */
    public function getOutDate()
    {
        return $this->outDate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return GroupPatientRegistry
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return GroupPatientRegistry
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set group
     *
     * @param Group $group
     *
     * @return GroupPatientRegistry
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }
}
