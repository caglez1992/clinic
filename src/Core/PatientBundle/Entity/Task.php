<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\TaskRepository")
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="tasks")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     */
    private $worker;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="responsible_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\ServiceParent")
     * @ORM\JoinColumn(name="service_parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $serviceParent;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $status;

    /**
     * @var DateTime $createdAt
     *
     * @ORM\Column(name="close_at", type="datetime", nullable=true)
     */
    private $closeAt;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->status = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Task
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Task
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return Task
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set worker
     *
     * @param User $worker
     *
     * @return Task
     */
    public function setWorker(User $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return User
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Task
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set status
     *
     * @param Nomenclator $status
     *
     * @return Task
     */
    public function setStatus(Nomenclator $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Nomenclator
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set closeAt
     *
     * @param \DateTime $closeAt
     *
     * @return Task
     */
    public function setCloseAt($closeAt)
    {
        $this->closeAt = $closeAt;

        return $this;
    }

    /**
     * Get closeAt
     *
     * @return \DateTime
     */
    public function getCloseAt()
    {
        return $this->closeAt;
    }

    /**
     * Set serviceParent
     *
     * @param ServiceParent $serviceParent
     *
     * @return Task
     */
    public function setServiceParent(ServiceParent $serviceParent = null)
    {
        $this->serviceParent = $serviceParent;

        return $this;
    }

    /**
     * Get serviceParent
     *
     * @return ServiceParent
     */
    public function getServiceParent()
    {
        return $this->serviceParent;
    }
}
