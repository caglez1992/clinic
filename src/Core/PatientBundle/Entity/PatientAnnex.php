<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\BaseFile;
use Core\CoreBundle\Entity\Loggable;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\PatientAnnexRepository")
 * @Vich\Uploadable
 */
class PatientAnnex extends BaseFile implements Loggable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="form_date", type="datetime", nullable=true)
     */
    private $formDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\WorkerBundle\Entity\SettingCode")
     * @ORM\JoinColumn(name="setting_code_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $settingCode;

    /**
     * @Vich\UploadableField(mapping="annexes", fileNameProperty="filename")
     * @var File
     * @Assert\File(mimeTypes={"application/pdf"})
     */
    private $formFile;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="annexes")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setFormFile(File $file = null)
    {
        $this->formFile = $file;

        if($file && $file instanceof UploadedFile){
            if(is_null($this->name))
                $this->name = $file->getClientOriginalName();
            $this->size = $file->getSize();
        }

        if ($file) {
            $this->mimeType = $file->getMimeType();
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getFormFile()
    {
        return $this->formFile;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function checkNewMedia($context)
    {
        if(is_null($this->filename) && is_null($this->formFile)){
            $context->buildViolation('empty.file')->atPath('formFile')->addViolation();
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return '/pdf/annexes/';
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return $this->getPath() . $this->filename;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'menu.entity.pdf.form';
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return PatientAnnex
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set formDate
     *
     * @param \DateTime $formDate
     *
     * @return PatientAnnex
     */
    public function setFormDate($formDate)
    {
        $this->formDate = $formDate;

        return $this;
    }

    /**
     * Get formDate
     *
     * @return \DateTime
     */
    public function getFormDate()
    {
        return $this->formDate;
    }

    /**
     * Set settingCode
     *
     * @param \Core\WorkerBundle\Entity\SettingCode $settingCode
     *
     * @return PatientAnnex
     */
    public function setSettingCode(\Core\WorkerBundle\Entity\SettingCode $settingCode = null)
    {
        $this->settingCode = $settingCode;

        return $this;
    }

    /**
     * Get settingCode
     *
     * @return \Core\WorkerBundle\Entity\SettingCode
     */
    public function getSettingCode()
    {
        return $this->settingCode;
    }

    /**
     * Set createdBy
     *
     * @param \Core\CoreBundle\Entity\User $createdBy
     *
     * @return PatientAnnex
     */
    public function setCreatedBy(\Core\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Core\CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
