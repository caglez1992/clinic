<?php

namespace Core\PatientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\DiagnosisTemplateRepository")
 */
class DiagnosisTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="migrationId", type="integer", nullable=true)
     */
    private $migrationId;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_code", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $primaryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="secondary_code", type="string", nullable=true)
     */
    private $secondaryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set primaryCode
     *
     * @param string $primaryCode
     *
     * @return DiagnosisTemplate
     */
    public function setPrimaryCode($primaryCode)
    {
        $this->primaryCode = strtoupper($primaryCode);

        return $this;
    }

    /**
     * Get primaryCode
     *
     * @return string
     */
    public function getPrimaryCode()
    {
        return $this->primaryCode;
    }

    /**
     * Set secondaryCode
     *
     * @param string $secondaryCode
     *
     * @return DiagnosisTemplate
     */
    public function setSecondaryCode($secondaryCode)
    {
        $this->secondaryCode = strtoupper($secondaryCode);
        return $this;
    }

    /**
     * Get secondaryCode
     *
     * @return string
     */
    public function getSecondaryCode()
    {
        return $this->secondaryCode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DiagnosisTemplate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $str = $this->primaryCode;
        if(!is_null($this->secondaryCode)){
            $str .= sprintf(' (%s)', $this->secondaryCode);
        }
        return (string) $str;
    }

    /**
     * Set migrationId
     *
     * @param integer $migrationId
     *
     * @return DiagnosisTemplate
     */
    public function setMigrationId($migrationId)
    {
        $this->migrationId = $migrationId;

        return $this;
    }

    /**
     * Get migrationId
     *
     * @return integer
     */
    public function getMigrationId()
    {
        return $this->migrationId;
    }
}
