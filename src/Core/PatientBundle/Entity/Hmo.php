<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\HmoRepository")
 */
class Hmo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="migration_id", type="integer", nullable=true)
     */
    private $migrationId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medicare", type="boolean", nullable=true)
     */
    private $medicare;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medicaid", type="boolean", nullable=true)
     */
    private $medicaid;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\HmoHandler")
     * @ORM\JoinColumn(name="handler_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $handler;

    /**
     * @var boolean
     *
     * @ORM\Column(name="free", type="boolean", nullable=true)
     */
    private $free;

    public function __construct()
    {
        $this->free = false;
        $this->medicare = false;
        $this->medicaid = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Hmo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set handler
     *
     * @param HmoHandler $handler
     *
     * @return Hmo
     */
    public function setHandler(HmoHandler $handler = null)
    {
        $this->handler = $handler;

        return $this;
    }

    /**
     * Get handler
     *
     * @return HmoHandler
     */
    public function getHandler()
    {
        return $this->handler;
    }

    public function __toString()
    {
        $str = $this->name;

        if(!is_null($this->handler)){
            $str .= ' (' . $this->getHandler()->getName() . ')';
        }

        return (string) $str;
    }
    
    /**
     * Set free
     *
     * @param boolean $free
     *
     * @return Hmo
     */
    public function setFree($free)
    {
        $this->free = $free;

        return $this;
    }

    /**
     * Get free
     *
     * @return boolean
     */
    public function getFree()
    {
        return $this->free;
    }

    /**
     * @return boolean
     */
    public function isMedicare()
    {
        return $this->medicare;
    }

    /**
     * @param boolean $medicare
     */
    public function setMedicare($medicare)
    {
        $this->medicare = $medicare;
    }

    /**
     * @return boolean
     */
    public function isMedicaid()
    {
        return $this->medicaid;
    }

    /**
     * @param boolean $medicaid
     */
    public function setMedicaid($medicaid)
    {
        $this->medicaid = $medicaid;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function checkType($context)
    {
        if(!$this->isMedicaid() && !$this->isMedicare()){
            $context->buildViolation('Mark a least one Insurance Program')->atPath('medicare')->addViolation();
        }
    }

    /**
     * Get medicare
     *
     * @return boolean
     */
    public function getMedicare()
    {
        return $this->medicare;
    }

    /**
     * Get medicaid
     *
     * @return boolean
     */
    public function getMedicaid()
    {
        return $this->medicaid;
    }

    /**
     * Set migrationId
     *
     * @param integer $migrationId
     *
     * @return Hmo
     */
    public function setMigrationId($migrationId)
    {
        $this->migrationId = $migrationId;

        return $this;
    }

    /**
     * Get migrationId
     *
     * @return integer
     */
    public function getMigrationId()
    {
        return $this->migrationId;
    }
}
