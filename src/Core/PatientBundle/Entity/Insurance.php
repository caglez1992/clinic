<?php

namespace Core\PatientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\InsuranceRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="insurance_type", type="string")
 * @ORM\DiscriminatorMap({"medicare"="Medicare", "medicaid"="Medicaid"})
 */
abstract class Insurance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Hmo")
     * @ORM\JoinColumn(name="hmo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $hmo;
    
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="number", type="string", length=255)
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="alternate_id", type="string", length=255)
     */
    protected $alternateId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Insurance
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set hmo
     *
     * @param Hmo $hmo
     *
     * @return Insurance
     */
    public function setHmo(Hmo $hmo = null)
    {
        $this->hmo = $hmo;

        return $this;
    }

    /**
     * Get hmo
     *
     * @return Hmo
     */
    public function getHmo()
    {
        return $this->hmo;
    }

    public function __toString(){
        return (string) $this->number . ' [' . $this->getHmo()->__toString() . ']';
    }

    /**
     * Set alternateId
     *
     * @param string $alternateId
     *
     * @return Insurance
     */
    public function setAlternateId($alternateId)
    {
        $this->alternateId = $alternateId;

        return $this;
    }

    /**
     * Get alternateId
     *
     * @return string
     */
    public function getAlternateId()
    {
        return $this->alternateId;
    }
}
