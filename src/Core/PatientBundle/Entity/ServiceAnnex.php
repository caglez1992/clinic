<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\BaseFile;
use Core\CoreBundle\Entity\Loggable;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\ServiceAnnexRepository")
 * @Vich\Uploadable
 */
class ServiceAnnex extends BaseFile implements Loggable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="annexes", fileNameProperty="filename")
     * @var File
     */
    private $formFile;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Service", inversedBy="annexes")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $service;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setFormFile(File $file = null)
    {
        $this->formFile = $file;

        if($file && $file instanceof UploadedFile){
            if(is_null($this->name))
                $this->name = $file->getClientOriginalName();
            $this->size = $file->getSize();
        }

        if ($file) {
            $this->mimeType = $file->getMimeType();
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getFormFile()
    {
        return $this->formFile;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function checkNewMedia($context)
    {
        if(is_null($this->filename) && is_null($this->formFile)){
            $context->buildViolation('empty.file')->atPath('formFile')->addViolation();
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return '/pdf/annexes/';
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return $this->getPath() . $this->filename;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'menu.entity.service.annexes';
    }

    /**
     * Set service
     *
     * @param Service $service
     *
     * @return ServiceAnnex
     */
    public function setService(Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}
