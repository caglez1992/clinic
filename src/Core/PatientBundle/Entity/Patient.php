<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Loggable;
use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\PatientRepository")
 * @HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Patient implements Loggable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="migration_id", type="integer", nullable=true)
     */
    private $migrationId;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @Assert\NotBlank()
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var int
     * @ORM\Column(name="case_number", type="integer", nullable=true)
     */
    private $caseNumber;

    /**
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinTable(name="intake_type_patient")
     */
    private $intakeTypes;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="patient_status_id", referencedColumnName="id")
     */
    private $patientStatus;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="open_date", type="datetime", nullable=true)
     */
    private $openDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Medicare")
     * @ORM\JoinColumn(name="medicare_id", referencedColumnName="id")
     */
    private $medicare;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Medicaid")
     * @ORM\JoinColumn(name="medicaid_id", referencedColumnName="id")
     */
    private $medicaid;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security", type="string", nullable=true)
     */
    private $socialSecurity;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="state_us_id", referencedColumnName="id")
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(name="home_phone", type="string", length=255, nullable=true)
     */
    private $homePhone;

    /**
     * @var string
     * @ORM\Column(name="cell_phone", type="string", length=255, nullable=true)
     */
    private $cellPhone;

    /**
     * @var string
     * @ORM\Column(name="other_phone", type="string", length=255, nullable=true)
     */
    private $otherPhone;

    /**
     * @var string
     * @Assert\Regex("/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}$/", message = "Please a valid email address.")
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dob", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $dob;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="ssn", type="string", length=255, nullable=true)
     * @Assert\Regex("/^([0-9]){3}-([0-9]){2}-([0-9]){4}$/", message = "Invalid SSN.")
     */
    private $ssn;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="gender_id", referencedColumnName="id")
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="race_id", referencedColumnName="id")
     */
    private $race;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="ethnicity_id", referencedColumnName="id")
     */
    private $ethnicity;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="marital_status_id", referencedColumnName="id")
     */
    private $maritalStatus;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="educational_level_id", referencedColumnName="id")
     */
    private $educationalLevel;

    /**
     * @var string
     * @Assert\Regex("/\D+/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="religion", type="string", length=255, nullable=true)
     */
    private $religion;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="primary_language_id", referencedColumnName="id")
     */
    private $primaryLanguage;

    /**
     * @var string
     * @Assert\Regex("/\D+/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="other_language", type="string", length=255, nullable=true)
     */
    private $otherLanguage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="readd", type="boolean", nullable=true)
     */
    private $read;

    /**
     * @var boolean
     *
     * @ORM\Column(name="speak", type="boolean", nullable=true)
     */
    private $speak;

    /**
     * @var boolean
     *
     * @ORM\Column(name="understand", type="boolean", nullable=true)
     */
    private $understand;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="employment_status_id", referencedColumnName="id")
     */
    private $employmentStatus;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="residential_status_id", referencedColumnName="id")
     */
    private $residentialStatus;

    /**
     * @var float
     * @Assert\Regex("/\d+(\.(0-9)+)?/", message = "Please enter a correct money value.")
     * @ORM\Column(name="family_income", type="float", nullable=true)
     */
    private $familyIncome;

    /**
     * @var string
     *
     * @ORM\Column(name="income_source", type="string", length=255, nullable=true)
     */
    private $incomeSource;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="guardian_name", type="string", length=255, nullable=true)
     */
    private $guardianName;

    /**
     * @var string
     * @ORM\Column(name="guardian_phone", type="string", length=255, nullable=true)
     */
    private $guardianPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="guardian_address", type="text", length=255, nullable=true)
     */
    private $guardianAddress;

    /**
     * @var string
     * @ORM\Column(name="guardian_city", type="string", length=255, nullable=true)
     */
    private $guardianCity;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="referred_by_id", referencedColumnName="id")
     */
    private $referredBy;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="referred_by_name", type="string", length=255, nullable=true)
     */
    private $referredByName;

    /**
     * @var string
     * @ORM\Column(name="referred_by_phone", type="string", length=255, nullable=true)
     */
    private $referredByPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="referred_by_address", type="text", length=255, nullable=true)
     */
    private $referredByAddress;

    /**
     * @var string
     * @ORM\Column(name="referred_by_city", type="string", length=255, nullable=true)
     */
    private $referredByCity;

    /**
     * @var string
     *
     * @ORM\Column(name="referred_by_title", type="string", length=255, nullable=true)
     */
    private $referredByTitle;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id")
     */
    private $agency;

    /**
     * @var string
     *
     * @ORM\Column(name="referred_by_other", type="string", length=255, nullable=true)
     */
    private $referredByOther;

    /**
     * @var boolean
     *
     * @ORM\Column(name="self_referral_walk_in", type="boolean", nullable=true)
     */
    private $selfReferralWalkIn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="self_referral_phone_call", type="boolean", nullable=true)
     */
    private $selfReferralPhoneCall;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mental_health_prov", type="boolean", nullable=true)
     */
    private $mentalHealthProv;

    /**
     * @var string
     *
     * @ORM\Column(name="mh_specification", type="text", length=255, nullable=true)
     */
    private $mhSpecification;

    /**
     * @var string
     *
     * @ORM\Column(name="eligibility_criteria", type="text", length=255, nullable=true)
     */
    private $eligibilityCriteria;

    /**
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinTable(name="patient_referral_reasons")
     */
    private $referralReasons;

    /**
     * @var string
     *
     * @ORM\Column(name="school", type="string", length=255, nullable=true)
     */
    private $school;

    /**
     * @var string
     *
     * @ORM\Column(name="grade", type="string", length=255, nullable=true)
     */
    private $grade;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinTable(name="patient_school_program")
     */
    private $schoolProgram;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="teacher_name", type="string", length=255, nullable=true)
     */
    private $teacherName;

    /**
     * @var string
     * @ORM\Column(name="teacher_phone", type="string", length=255, nullable=true)
     */
    private $teacherPhone;

    /**
     * @var string
     *
     * @Assert\Country()
     * @ORM\Column(name="country_birth", type="string", length=255, nullable=true)
     */
    private $countryBirth;

    /**
     * @var int
     * @Assert\Regex("/^(\d){4}$/", message = "Please enter a valid year.")
     * @ORM\Column(name="year_usa", type="integer", nullable=true)
     */
    private $yearUSA;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="status_explination", type="text", length=255, nullable=true)
     */
    private $statusExplination;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="primary_contact_name", type="string", length=255, nullable=true)
     */
    private $primaryContactName;

    /**
     * @var string
     * @ORM\Column(name="primary_contact_phone", type="string", length=255, nullable=true)
     */
    private $primaryContactPhone;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="primary_contact_type_id", referencedColumnName="id")
     */
    private $primaryContactType;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="secondary_contact_name", type="string", length=255, nullable=true)
     */
    private $secondaryContactName;

    /**
     * @var string
     * @ORM\Column(name="secondary_contact_phone", type="string", length=255, nullable=true)
     */
    private $secondaryContactPhone;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="secondary_contact_type_id", referencedColumnName="id")
     */
    private $secondaryContactType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contact_permit", type="boolean", nullable=true)
     */
    private $contactPermit;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="psychiatrist_name", type="string", length=255, nullable=true)
     */
    private $psychiatristName;

    /**
     * @var string
     * @ORM\Column(name="psychiatrist_phone", type="string", length=255, nullable=true)
     */
    private $psychiatristPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="psychiatrist_address", type="text", length=255, nullable=true)
     */
    private $psychiatristAddress;

    /**
     * @var string
     * @Assert\Regex("/\D+/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="psychiatrist_city", type="string", length=255, nullable=true)
     */
    private $psychiatristCity;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="pcp_name", type="string", length=255, nullable=true)
     */
    private $pcpName;

    /**
     * @var string
     * @ORM\Column(name="pcp_phone", type="string", length=255, nullable=true)
     */
    private $pcpPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="pcp_address", type="text", length=255, nullable=true)
     */
    private $pcpAddress;

    /**
     * @var string
     * @Assert\Regex("/\D+/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="pcp_city", type="string", length=255, nullable=true)
     */
    private $pcpCity;

    /**
     * @var string
     * @Assert\Regex("/^([a-zA-ZáéíóúÁÉÍÓÚñÑ ])+$/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="other_provider_name", type="string", length=255, nullable=true)
     */
    private $otherProviderName;

    /**
     * @var string
     * @ORM\Column(name="other_provider_phone", type="string", length=255, nullable=true)
     */
    private $otherProviderPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="other_provider_address", type="text", length=255, nullable=true)
     */
    private $otherProviderAddress;

    /**
     * @var string
     * @Assert\Regex("/\D+/", message = "This field doesn't allow digits.")
     * @ORM\Column(name="other_provider_city", type="string", length=255, nullable=true)
     */
    private $otherProviderCity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="spetial_accommodation", type="boolean", nullable=true)
     */
    private $spetialAccommodation;

    /**
     * @var string
     *
     * @ORM\Column(name="accommodation_specification", type="text", length=255, nullable=true)
     */
    private $accommodationSpecification;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="case_manager", referencedColumnName="id")
     */
    private $caseManager;

    /**
     * @var string
     *
     * @ORM\Column(name="case_manager_credentials", type="string", length=255, nullable=true)
     */
    private $caseManagerCredentials;

    /**
     * @var string
     *
     * @ORM\Column(name="case_manager_notes", type="text", length=255, nullable=true)
     */
    private $caseManagerNotes;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\Service", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"scheduleStart" = "DESC"})
     */
    private $services;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\RegistryPatient", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"entryDate" = "ASC"})
     */
    private $registrys;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\Task", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $tasks;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\DiagnosisPatient", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"diagnosisDate" = "ASC"})
     */
    private $diagnosisEntrys;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\InsuranceEligibility", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"runDate" = "ASC"})
     */
    private $eligibilityEntrys;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\Authorization", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $authorizations;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\GroupPatientRegistry", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"inDate" = "ASC"})
     */
    private $groupPatientRecords;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\PatientAnnex", mappedBy="patient", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $annexes;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\User", mappedBy="patients")
     */
    private $workers;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255, nullable = true)
     */
    private $media;

    /**
     * @Vich\UploadableField(mapping="patient_images", fileNameProperty="media")
     * @var File
     */
    private $mediaFile;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount_auth", type="integer", nullable=true)
     */
    private $amountAuth;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->referralReasons = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->registrys = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->diagnosisEntrys = new ArrayCollection();
        $this->eligibilityEntrys = new ArrayCollection();
        $this->authorizations = new ArrayCollection();
        $this->groupPatientRecords = new ArrayCollection();

        $this->patientStatus = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_PENDING);
        $this->race = NomUtil::getNomenclatorsByName(NomUtil::NOM_RACE_TYPE_UN);

        $this->state = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATE_TYPE_FL);

        $this->amountAuth = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Patient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Patient
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set caseNumber
     *
     * @param string $caseNumber
     *
     * @return Patient
     */
    public function setCaseNumber($caseNumber)
    {
        $this->caseNumber = $caseNumber;

        return $this;
    }

    /**
     * Get caseNumber
     *
     * @return string
     */
    public function getCaseNumber()
    {
        return $this->caseNumber;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Patient
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return Patient
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set homePhone
     *
     * @param string $homePhone
     *
     * @return Patient
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Set cellPhone
     *
     * @param string $cellPhone
     *
     * @return Patient
     */
    public function setCellPhone($cellPhone)
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cellPhone;
    }

    /**
     * Set otherPhone
     *
     * @param string $otherPhone
     *
     * @return Patient
     */
    public function setOtherPhone($otherPhone)
    {
        $this->otherPhone = $otherPhone;

        return $this;
    }

    /**
     * Get otherPhone
     *
     * @return string
     */
    public function getOtherPhone()
    {
        return $this->otherPhone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Patient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dob
     *
     * @param DateTime $dob
     *
     * @return Patient
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set ssn
     *
     * @param string $ssn
     *
     * @return Patient
     */
    public function setSsn($ssn)
    {
        $this->ssn = $ssn;

        return $this;
    }

    /**
     * Get ssn
     *
     * @return string
     */
    public function getSsn()
    {
        return $this->ssn;
    }

    /**
     * Set religion
     *
     * @param string $religion
     *
     * @return Patient
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;

        return $this;
    }

    /**
     * Get religion
     *
     * @return string
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * Set otherLanguage
     *
     * @param string $otherLanguage
     *
     * @return Patient
     */
    public function setOtherLanguage($otherLanguage)
    {
        $this->otherLanguage = $otherLanguage;

        return $this;
    }

    /**
     * Get otherLanguage
     *
     * @return string
     */
    public function getOtherLanguage()
    {
        return $this->otherLanguage;
    }

    /**
     * Set read
     *
     * @param boolean $read
     *
     * @return Patient
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set speak
     *
     * @param boolean $speak
     *
     * @return Patient
     */
    public function setSpeak($speak)
    {
        $this->speak = $speak;

        return $this;
    }

    /**
     * Get speak
     *
     * @return boolean
     */
    public function getSpeak()
    {
        return $this->speak;
    }

    /**
     * Set understand
     *
     * @param boolean $understand
     *
     * @return Patient
     */
    public function setUnderstand($understand)
    {
        $this->understand = $understand;

        return $this;
    }

    /**
     * Get understand
     *
     * @return boolean
     */
    public function getUnderstand()
    {
        return $this->understand;
    }

    /**
     * Set familyIncome
     *
     * @param float $familyIncome
     *
     * @return Patient
     */
    public function setFamilyIncome($familyIncome)
    {
        $this->familyIncome = $familyIncome;

        return $this;
    }

    /**
     * Get familyIncome
     *
     * @return float
     */
    public function getFamilyIncome()
    {
        return $this->familyIncome;
    }

    /**
     * Set incomeSource
     *
     * @param string $incomeSource
     *
     * @return Patient
     */
    public function setIncomeSource($incomeSource)
    {
        $this->incomeSource = $incomeSource;

        return $this;
    }

    /**
     * Get incomeSource
     *
     * @return string
     */
    public function getIncomeSource()
    {
        return $this->incomeSource;
    }

    /**
     * Set guardianName
     *
     * @param string $guardianName
     *
     * @return Patient
     */
    public function setGuardianName($guardianName)
    {
        $this->guardianName = $guardianName;

        return $this;
    }

    /**
     * Get guardianName
     *
     * @return string
     */
    public function getGuardianName()
    {
        return $this->guardianName;
    }

    /**
     * Set guardianPhone
     *
     * @param string $guardianPhone
     *
     * @return Patient
     */
    public function setGuardianPhone($guardianPhone)
    {
        $this->guardianPhone = $guardianPhone;

        return $this;
    }

    /**
     * Get guardianPhone
     *
     * @return string
     */
    public function getGuardianPhone()
    {
        return $this->guardianPhone;
    }

    /**
     * Set guardianAddress
     *
     * @param string $guardianAddress
     *
     * @return Patient
     */
    public function setGuardianAddress($guardianAddress)
    {
        $this->guardianAddress = $guardianAddress;

        return $this;
    }

    /**
     * Get guardianAddress
     *
     * @return string
     */
    public function getGuardianAddress()
    {
        return $this->guardianAddress;
    }

    /**
     * Set guardianCity
     *
     * @param string $guardianCity
     *
     * @return Patient
     */
    public function setGuardianCity($guardianCity)
    {
        $this->guardianCity = $guardianCity;

        return $this;
    }

    /**
     * Get guardianCity
     *
     * @return string
     */
    public function getGuardianCity()
    {
        return $this->guardianCity;
    }

    /**
     * Set referredByOther
     *
     * @param string $referredByOther
     *
     * @return Patient
     */
    public function setReferredByOther($referredByOther)
    {
        $this->referredByOther = $referredByOther;

        return $this;
    }

    /**
     * Get referredByOther
     *
     * @return string
     */
    public function getReferredByOther()
    {
        return $this->referredByOther;
    }

    /**
     * Set selfReferralWalkIn
     *
     * @param boolean $selfReferralWalkIn
     *
     * @return Patient
     */
    public function setSelfReferralWalkIn($selfReferralWalkIn)
    {
        $this->selfReferralWalkIn = $selfReferralWalkIn;

        return $this;
    }

    /**
     * Get selfReferralWalkIn
     *
     * @return boolean
     */
    public function getSelfReferralWalkIn()
    {
        return $this->selfReferralWalkIn;
    }

    /**
     * Set selfReferralPhoneCall
     *
     * @param boolean $selfReferralPhoneCall
     *
     * @return Patient
     */
    public function setSelfReferralPhoneCall($selfReferralPhoneCall)
    {
        $this->selfReferralPhoneCall = $selfReferralPhoneCall;

        return $this;
    }

    /**
     * Get selfReferralPhoneCall
     *
     * @return boolean
     */
    public function getSelfReferralPhoneCall()
    {
        return $this->selfReferralPhoneCall;
    }

    /**
     * @param boolean $mentalHealthProv
     *
     * @return Patient
     */
    public function setMentalHealthProv($mentalHealthProv)
    {
        $this->mentalHealthProv = $mentalHealthProv;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getMentalHealthProv()
    {
        return $this->mentalHealthProv;
    }

    /**
     * Set school
     *
     * @param string $school
     *
     * @return Patient
     */
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return string
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set grade
     *
     * @param string $grade
     *
     * @return Patient
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set teacherName
     *
     * @param string $teacherName
     *
     * @return Patient
     */
    public function setTeacherName($teacherName)
    {
        $this->teacherName = $teacherName;

        return $this;
    }

    /**
     * Get teacherName
     *
     * @return string
     */
    public function getTeacherName()
    {
        return $this->teacherName;
    }

    /**
     * Set teacherPhone
     *
     * @param string $teacherPhone
     *
     * @return Patient
     */
    public function setTeacherPhone($teacherPhone)
    {
        $this->teacherPhone = $teacherPhone;

        return $this;
    }

    /**
     * Get teacherPhone
     *
     * @return string
     */
    public function getTeacherPhone()
    {
        return $this->teacherPhone;
    }

    /**
     * Set countryBirth
     *
     * @param string $countryBirth
     *
     * @return Patient
     */
    public function setCountryBirth($countryBirth)
    {
        $this->countryBirth = $countryBirth;

        return $this;
    }

    /**
     * Get countryBirth
     *
     * @return string
     */
    public function getCountryBirth()
    {
        return $this->countryBirth;
    }

    /**
     * Set yearUSA
     *
     * @param integer $yearUSA
     *
     * @return Patient
     */
    public function setYearUSA($yearUSA)
    {
        $this->yearUSA = $yearUSA;

        return $this;
    }

    /**
     * Get yearUSA
     *
     * @return integer
     */
    public function getYearUSA()
    {
        return $this->yearUSA;
    }

    /**
     * Set statusExplination
     *
     * @param string $statusExplination
     *
     * @return Patient
     */
    public function setStatusExplination($statusExplination)
    {
        $this->statusExplination = $statusExplination;

        return $this;
    }

    /**
     * Get statusExplination
     *
     * @return string
     */
    public function getStatusExplination()
    {
        return $this->statusExplination;
    }

    /**
     * Set primaryContactName
     *
     * @param string $primaryContactName
     *
     * @return Patient
     */
    public function setPrimaryContactName($primaryContactName)
    {
        $this->primaryContactName = $primaryContactName;

        return $this;
    }

    /**
     * Get primaryContactName
     *
     * @return string
     */
    public function getPrimaryContactName()
    {
        return $this->primaryContactName;
    }

    /**
     * Set primaryContactPhone
     *
     * @param string $primaryContactPhone
     *
     * @return Patient
     */
    public function setPrimaryContactPhone($primaryContactPhone)
    {
        $this->primaryContactPhone = $primaryContactPhone;

        return $this;
    }

    /**
     * Get primaryContactPhone
     *
     * @return string
     */
    public function getPrimaryContactPhone()
    {
        return $this->primaryContactPhone;
    }

    /**
     * Set secondaryContactName
     *
     * @param string $secondaryContactName
     *
     * @return Patient
     */
    public function setSecondaryContactName($secondaryContactName)
    {
        $this->secondaryContactName = $secondaryContactName;

        return $this;
    }

    /**
     * Get secondaryContactName
     *
     * @return string
     */
    public function getSecondaryContactName()
    {
        return $this->secondaryContactName;
    }

    /**
     * Set secondaryContactPhone
     *
     * @param string $secondaryContactPhone
     *
     * @return Patient
     */
    public function setSecondaryContactPhone($secondaryContactPhone)
    {
        $this->secondaryContactPhone = $secondaryContactPhone;

        return $this;
    }

    /**
     * Get secondaryContactPhone
     *
     * @return string
     */
    public function getSecondaryContactPhone()
    {
        return $this->secondaryContactPhone;
    }

    /**
     * Set contactPermit
     *
     * @param boolean $contactPermit
     *
     * @return Patient
     */
    public function setContactPermit($contactPermit)
    {
        $this->contactPermit = $contactPermit;

        return $this;
    }

    /**
     * Get contactPermit
     *
     * @return boolean
     */
    public function getContactPermit()
    {
        return $this->contactPermit;
    }

    /**
     * Set psychiatristName
     *
     * @param string $psychiatristName
     *
     * @return Patient
     */
    public function setPsychiatristName($psychiatristName)
    {
        $this->psychiatristName = $psychiatristName;

        return $this;
    }

    /**
     * Get psychiatristName
     *
     * @return string
     */
    public function getPsychiatristName()
    {
        return $this->psychiatristName;
    }

    /**
     * Set psychiatristPhone
     *
     * @param string $psychiatristPhone
     *
     * @return Patient
     */
    public function setPsychiatristPhone($psychiatristPhone)
    {
        $this->psychiatristPhone = $psychiatristPhone;

        return $this;
    }

    /**
     * Get psychiatristPhone
     *
     * @return string
     */
    public function getPsychiatristPhone()
    {
        return $this->psychiatristPhone;
    }

    /**
     * Set psychiatristAddress
     *
     * @param string $psychiatristAddress
     *
     * @return Patient
     */
    public function setPsychiatristAddress($psychiatristAddress)
    {
        $this->psychiatristAddress = $psychiatristAddress;

        return $this;
    }

    /**
     * Get psychiatristAddress
     *
     * @return string
     */
    public function getPsychiatristAddress()
    {
        return $this->psychiatristAddress;
    }

    /**
     * Set psychiatristCity
     *
     * @param string $psychiatristCity
     *
     * @return Patient
     */
    public function setPsychiatristCity($psychiatristCity)
    {
        $this->psychiatristCity = $psychiatristCity;

        return $this;
    }

    /**
     * Get psychiatristCity
     *
     * @return string
     */
    public function getPsychiatristCity()
    {
        return $this->psychiatristCity;
    }

    /**
     * Set pcpName
     *
     * @param string $pcpName
     *
     * @return Patient
     */
    public function setPcpName($pcpName)
    {
        $this->pcpName = $pcpName;

        return $this;
    }

    /**
     * Get pcpName
     *
     * @return string
     */
    public function getPcpName()
    {
        return $this->pcpName;
    }

    /**
     * Set pcpPhone
     *
     * @param string $pcpPhone
     *
     * @return Patient
     */
    public function setPcpPhone($pcpPhone)
    {
        $this->pcpPhone = $pcpPhone;

        return $this;
    }

    /**
     * Get pcpPhone
     *
     * @return string
     */
    public function getPcpPhone()
    {
        return $this->pcpPhone;
    }

    /**
     * Set pcpAddress
     *
     * @param string $pcpAddress
     *
     * @return Patient
     */
    public function setPcpAddress($pcpAddress)
    {
        $this->pcpAddress = $pcpAddress;

        return $this;
    }

    /**
     * Get pcpAddress
     *
     * @return string
     */
    public function getPcpAddress()
    {
        return $this->pcpAddress;
    }

    /**
     * Set pcpCity
     *
     * @param string $pcpCity
     *
     * @return Patient
     */
    public function setPcpCity($pcpCity)
    {
        $this->pcpCity = $pcpCity;

        return $this;
    }

    /**
     * Get pcpCity
     *
     * @return string
     */
    public function getPcpCity()
    {
        return $this->pcpCity;
    }

    /**
     * Set otherProviderName
     *
     * @param string $otherProviderName
     *
     * @return Patient
     */
    public function setOtherProviderName($otherProviderName)
    {
        $this->otherProviderName = $otherProviderName;

        return $this;
    }

    /**
     * Get otherProviderName
     *
     * @return string
     */
    public function getOtherProviderName()
    {
        return $this->otherProviderName;
    }

    /**
     * Set otherProviderPhone
     *
     * @param string $otherProviderPhone
     *
     * @return Patient
     */
    public function setOtherProviderPhone($otherProviderPhone)
    {
        $this->otherProviderPhone = $otherProviderPhone;

        return $this;
    }

    /**
     * Get otherProviderPhone
     *
     * @return string
     */
    public function getOtherProviderPhone()
    {
        return $this->otherProviderPhone;
    }

    /**
     * Set otherProviderAddress
     *
     * @param string $otherProviderAddress
     *
     * @return Patient
     */
    public function setOtherProviderAddress($otherProviderAddress)
    {
        $this->otherProviderAddress = $otherProviderAddress;

        return $this;
    }

    /**
     * Get otherProviderAddress
     *
     * @return string
     */
    public function getOtherProviderAddress()
    {
        return $this->otherProviderAddress;
    }

    /**
     * Set otherProviderCity
     *
     * @param string $otherProviderCity
     *
     * @return Patient
     */
    public function setOtherProviderCity($otherProviderCity)
    {
        $this->otherProviderCity = $otherProviderCity;

        return $this;
    }

    /**
     * Get otherProviderCity
     *
     * @return string
     */
    public function getOtherProviderCity()
    {
        return $this->otherProviderCity;
    }

    /**
     * Set spetialAccommodation
     *
     * @param boolean $spetialAccommodation
     *
     * @return Patient
     */
    public function setSpetialAccommodation($spetialAccommodation)
    {
        $this->spetialAccommodation = $spetialAccommodation;

        return $this;
    }

    /**
     * Get spetialAccommodation
     *
     * @return boolean
     */
    public function getSpetialAccommodation()
    {
        return $this->spetialAccommodation;
    }

    /**
     * Set accommodationSpecification
     *
     * @param string $accommodationSpecification
     *
     * @return Patient
     */
    public function setAccommodationSpecification($accommodationSpecification)
    {
        $this->accommodationSpecification = $accommodationSpecification;

        return $this;
    }

    /**
     * Get accommodationSpecification
     *
     * @return string
     */
    public function getAccommodationSpecification()
    {
        return $this->accommodationSpecification;
    }

    /**
     * Set gender
     *
     * @param Nomenclator $gender
     *
     * @return Patient
     */
    public function setGender(Nomenclator $gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return Nomenclator
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set race
     *
     * @param Nomenclator $race
     *
     * @return Patient
     */
    public function setRace(Nomenclator $race = null)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return Nomenclator
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Set ethnicity
     *
     * @param Nomenclator $ethnicity
     *
     * @return Patient
     */
    public function setEthnicity(Nomenclator $ethnicity = null)
    {
        $this->ethnicity = $ethnicity;

        return $this;
    }

    /**
     * Get ethnicity
     *
     * @return Nomenclator
     */
    public function getEthnicity()
    {
        return $this->ethnicity;
    }

    /**
     * Set maritalStatus
     *
     * @param Nomenclator $maritalStatus
     *
     * @return Patient
     */
    public function setMaritalStatus(Nomenclator $maritalStatus = null)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return Nomenclator
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set educationalLevel
     *
     * @param Nomenclator $educationalLevel
     *
     * @return Patient
     */
    public function setEducationalLevel(Nomenclator $educationalLevel = null)
    {
        $this->educationalLevel = $educationalLevel;

        return $this;
    }

    /**
     * Get educationalLevel
     *
     * @return Nomenclator
     */
    public function getEducationalLevel()
    {
        return $this->educationalLevel;
    }

    /**
     * Set primaryLanguage
     *
     * @param Nomenclator $primaryLanguage
     *
     * @return Patient
     */
    public function setPrimaryLanguage(Nomenclator $primaryLanguage = null)
    {
        $this->primaryLanguage = $primaryLanguage;

        return $this;
    }

    /**
     * Get primaryLanguage
     *
     * @return Nomenclator
     */
    public function getPrimaryLanguage()
    {
        return $this->primaryLanguage;
    }

    /**
     * Set employmentStatus
     *
     * @param Nomenclator $employmentStatus
     *
     * @return Patient
     */
    public function setEmploymentStatus(Nomenclator $employmentStatus = null)
    {
        $this->employmentStatus = $employmentStatus;

        return $this;
    }

    /**
     * Get employmentStatus
     *
     * @return Nomenclator
     */
    public function getEmploymentStatus()
    {
        return $this->employmentStatus;
    }

    /**
     * Set residentialStatus
     *
     * @param Nomenclator $residentialStatus
     *
     * @return Patient
     */
    public function setResidentialStatus(Nomenclator $residentialStatus = null)
    {
        $this->residentialStatus = $residentialStatus;

        return $this;
    }

    /**
     * Get residentialStatus
     *
     * @return Nomenclator
     */
    public function getResidentialStatus()
    {
        return $this->residentialStatus;
    }

    /**
     * Set referredBy
     *
     * @param Nomenclator $referredBy
     *
     * @return Patient
     */
    public function setReferredBy(Nomenclator $referredBy = null)
    {
        $this->referredBy = $referredBy;

        return $this;
    }

    /**
     * Get referredBy
     *
     * @return Nomenclator
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }

    /**
     * Set agency
     *
     * @param Nomenclator $agency
     *
     * @return Patient
     */
    public function setAgency(Nomenclator $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Nomenclator
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Add referralReason
     *
     * @param Nomenclator $referralReason
     *
     * @return Patient
     */
    public function addReferralReason(Nomenclator $referralReason)
    {
        $this->referralReasons[] = $referralReason;

        return $this;
    }

    /**
     * Remove referralReason
     *
     * @param Nomenclator $referralReason
     */
    public function removeReferralReason(Nomenclator $referralReason)
    {
        $this->referralReasons->removeElement($referralReason);
    }

    /**
     * Get referralReasons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferralReasons()
    {
        return $this->referralReasons;
    }

    /**
     * Set status
     *
     * @param Nomenclator $status
     *
     * @return Patient
     */
    public function setStatus(Nomenclator $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Nomenclator
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mhSpecification
     *
     * @param string $mhSpecification
     *
     * @return Patient
     */
    public function setMhSpecification($mhSpecification)
    {
        $this->mhSpecification = $mhSpecification;

        return $this;
    }

    /**
     * Get mhSpecification
     *
     * @return string
     */
    public function getMhSpecification()
    {
        return $this->mhSpecification;
    }

    /**
     * Set patientStatus
     *
     * @param Nomenclator $patientStatus
     *
     * @return Patient
     */
    public function setPatientStatus(Nomenclator $patientStatus = null)
    {
        $this->patientStatus = $patientStatus;

        return $this;
    }

    /**
     * Get patientStatus
     *
     * @return Nomenclator
     */
    public function getPatientStatus()
    {
        return $this->patientStatus;
    }

    /**
     * Add service
     *
     * @param Service $service
     *
     * @return Patient
     */
    public function addService(Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param Service $service
     */
    public function removeService(Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Patient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Patient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set schoolProgram
     *
     * @param Nomenclator $schoolProgram
     *
     * @return Patient
     */
    public function setSchoolProgram(Nomenclator $schoolProgram = null)
    {
        $this->schoolProgram = $schoolProgram;

        return $this;
    }

    /**
     * Get schoolProgram
     *
     * @return Nomenclator
     */
    public function getSchoolProgram()
    {
        return $this->schoolProgram;
    }

    /**
     * Set referredByName
     *
     * @param string $referredByName
     *
     * @return Patient
     */
    public function setReferredByName($referredByName)
    {
        $this->referredByName = $referredByName;

        return $this;
    }

    /**
     * Get referredByName
     *
     * @return string
     */
    public function getReferredByName()
    {
        return $this->referredByName;
    }

    /**
     * Set referredByPhone
     *
     * @param string $referredByPhone
     *
     * @return Patient
     */
    public function setReferredByPhone($referredByPhone)
    {
        $this->referredByPhone = $referredByPhone;

        return $this;
    }

    /**
     * Get referredByPhone
     *
     * @return string
     */
    public function getReferredByPhone()
    {
        return $this->referredByPhone;
    }

    /**
     * Set referredByAddress
     *
     * @param string $referredByAddress
     *
     * @return Patient
     */
    public function setReferredByAddress($referredByAddress)
    {
        $this->referredByAddress = $referredByAddress;

        return $this;
    }

    /**
     * Get referredByAddress
     *
     * @return string
     */
    public function getReferredByAddress()
    {
        return $this->referredByAddress;
    }

    /**
     * Set referredByCity
     *
     * @param string $referredByCity
     *
     * @return Patient
     */
    public function setReferredByCity($referredByCity)
    {
        $this->referredByCity = $referredByCity;

        return $this;
    }

    /**
     * Get referredByCity
     *
     * @return string
     */
    public function getReferredByCity()
    {
        return $this->referredByCity;
    }

    /**
     * Set referredByTitle
     *
     * @param string $referredByTitle
     *
     * @return Patient
     */
    public function setReferredByTitle($referredByTitle)
    {
        $this->referredByTitle = $referredByTitle;

        return $this;
    }

    /**
     * Get referredByTitle
     *
     * @return string
     */
    public function getReferredByTitle()
    {
        return $this->referredByTitle;
    }

    /**
     * Set eligibilityCriteria
     *
     * @param string $eligibilityCriteria
     *
     * @return Patient
     */
    public function setEligibilityCriteria($eligibilityCriteria)
    {
        $this->eligibilityCriteria = $eligibilityCriteria;

        return $this;
    }

    /**
     * Get eligibilityCriteria
     *
     * @return string
     */
    public function getEligibilityCriteria()
    {
        return $this->eligibilityCriteria;
    }

    /**
     * Set caseManagerCredentials
     *
     * @param string $caseManagerCredentials
     *
     * @return Patient
     */
    public function setCaseManagerCredentials($caseManagerCredentials)
    {
        $this->caseManagerCredentials = $caseManagerCredentials;

        return $this;
    }

    /**
     * Get caseManagerCredentials
     *
     * @return string
     */
    public function getCaseManagerCredentials()
    {
        return $this->caseManagerCredentials;
    }

    /**
     * Set caseManagerNotes
     *
     * @param string $caseManagerNotes
     *
     * @return Patient
     */
    public function setCaseManagerNotes($caseManagerNotes)
    {
        $this->caseManagerNotes = $caseManagerNotes;

        return $this;
    }

    /**
     * Get caseManagerNotes
     *
     * @return string
     */
    public function getCaseManagerNotes()
    {
        return $this->caseManagerNotes;
    }

    /**
     * Set caseManager
     *
     * @param User $caseManager
     *
     * @return Patient
     */
    public function setCaseManager(User $caseManager = null)
    {
        $this->caseManager = $caseManager;

        return $this;
    }

    /**
     * Get caseManager
     *
     * @return User
     */
    public function getCaseManager()
    {
        return $this->caseManager;
    }

    /**
     * Add intakeType
     *
     * @param Nomenclator $intakeType
     *
     * @return Patient
     */
    public function addIntakeType(Nomenclator $intakeType)
    {
        $this->intakeTypes[] = $intakeType;

        return $this;
    }

    /**
     * Remove intakeType
     *
     * @param Nomenclator $intakeType
     */
    public function removeIntakeType(Nomenclator $intakeType)
    {
        $this->getIntakeTypes()->removeElement($intakeType);
    }

    /**
     * Get intakeTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntakeTypes()
    {
        return $this->intakeTypes;
    }

    /**
     * Add registry
     *
     * @param RegistryPatient $registry
     *
     * @return Patient
     */
    public function addRegistry(RegistryPatient $registry)
    {
        $this->registrys[] = $registry;

        return $this;
    }

    /**
     * Remove registry
     *
     * @param RegistryPatient $registry
     */
    public function removeRegistry(RegistryPatient $registry)
    {
        $this->registrys->removeElement($registry);
    }

    /**
     * Get registrys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistrys()
    {
        return $this->registrys;
    }

    /**
     * Add diagnosisEntry
     *
     * @param DiagnosisPatient $diagnosisEntry
     *
     * @return Patient
     */
    public function addDiagnosisEntry(DiagnosisPatient $diagnosisEntry)
    {
        $this->diagnosisEntrys[] = $diagnosisEntry;

        return $this;
    }

    /**
     * Remove diagnosisEntry
     *
     * @param DiagnosisPatient $diagnosisEntry
     */
    public function removeDiagnosisEntry(DiagnosisPatient $diagnosisEntry)
    {
        $this->diagnosisEntrys->removeElement($diagnosisEntry);
    }

    /**
     * Get diagnosisEntrys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiagnosisEntrys()
    {
        $sort = new Criteria(null, ['createdAt' => Criteria::DESC]);
        return $this->diagnosisEntrys->matching($sort);
        //return $this->diagnosisEntrys;
    }

    /**
     * Add eligibilityEntry
     *
     * @param InsuranceEligibility $eligibilityEntry
     *
     * @return Patient
     */
    public function addEligibilityEntry(InsuranceEligibility $eligibilityEntry)
    {
        $this->eligibilityEntrys[] = $eligibilityEntry;

        return $this;
    }

    /**
     * Remove eligibilityEntry
     *
     * @param InsuranceEligibility $eligibilityEntry
     */
    public function removeEligibilityEntry(InsuranceEligibility $eligibilityEntry)
    {
        $this->eligibilityEntrys->removeElement($eligibilityEntry);
    }

    /**
     * Get eligibilityEntrys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEligibilityEntrys()
    {
        return $this->eligibilityEntrys;
    }


    /**
     * Add task
     *
     * @param Task $task
     *
     * @return Patient
     */
    public function addTask(Task $task)
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param Task $task
     */
    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param Medicare $medicare
     * @return Patient
     */
    public function setMedicare(Medicare $medicare = null)
    {
        $this->medicare = $medicare;

        return $this;
    }

    /**
     * @return Medicare
     */
    public function getMedicare()
    {
        return $this->medicare;
    }

    /**
     * Set medicaid
     *
     * @param Medicaid $medicaid
     *
     * @return Patient
     */
    public function setMedicaid(Medicaid $medicaid = null)
    {
        $this->medicaid = $medicaid;

        return $this;
    }

    /**
     * Get medicaid
     *
     * @return Medicaid
     */
    public function getMedicaid()
    {
        return $this->medicaid;
    }

    /**
     * Set openDate
     *
     * @param \DateTime $openDate
     *
     * @return Patient
     */
    public function setOpenDate($openDate)
    {
        $this->openDate = $openDate;

        return $this;
    }

    /**
     * Get openDate
     *
     * @return \DateTime
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }

    /**
     * Add authorization
     *
     * @param Authorization $authorization
     *
     * @return Patient
     */
    public function addAuthorization(Authorization $authorization)
    {
        $this->authorizations[] = $authorization;

        return $this;
    }

    /**
     * Remove authorization
     *
     * @param Authorization $authorization
     */
    public function removeAuthorization(Authorization $authorization)
    {
        $this->authorizations->removeElement($authorization);
    }

    /**
     * Get authorizations
     *
     * @param bool $onlyActive
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizations($onlyActive = false)
    {
        return $this->authorizations->filter(function (Authorization $auth){
            return $auth->getActive();
        });
    }


    /**
     * Add worker
     *
     * @param User $worker
     *
     * @return Patient
     */
    public function addWorker(User $worker)
    {
        $this->workers[] = $worker;

        return $this;
    }

    /**
     * Remove worker
     *
     * @param User $worker
     */
    public function removeWorker(User $worker)
    {
        $this->workers->removeElement($worker);
    }

    /**
     * Get workers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkers()
    {
        return $this->workers;
    }

    /**
     * Add groupPatientRecord
     *
     * @param GroupPatientRegistry $groupPatientRecord
     *
     * @return Patient
     */
    public function addGroupPatientRecord(GroupPatientRegistry $groupPatientRecord)
    {
        $this->groupPatientRecords[] = $groupPatientRecord;

        return $this;
    }

    /**
     * Remove groupPatientRecord
     *
     * @param GroupPatientRegistry $groupPatientRecord
     */
    public function removeGroupPatientRecord(GroupPatientRegistry $groupPatientRecord)
    {
        $this->groupPatientRecords->removeElement($groupPatientRecord);
    }

    /**
     * Get groupPatientRecords
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupPatientRecords()
    {
        return $this->groupPatientRecords;
    }

    /**
     * Set primaryContactType
     *
     * @param Nomenclator $primaryContactType
     *
     * @return Patient
     */
    public function setPrimaryContactType(Nomenclator $primaryContactType = null)
    {
        $this->primaryContactType = $primaryContactType;

        return $this;
    }

    /**
     * Get primaryContactType
     *
     * @return Nomenclator
     */
    public function getPrimaryContactType()
    {
        return $this->primaryContactType;
    }

    /**
     * Set secondaryContactType
     *
     * @param Nomenclator $secondaryContactType
     *
     * @return Patient
     */
    public function setSecondaryContactType(Nomenclator $secondaryContactType = null)
    {
        $this->secondaryContactType = $secondaryContactType;

        return $this;
    }

    /**
     * Get secondaryContactType
     *
     * @return Nomenclator
     */
    public function getSecondaryContactType()
    {
        return $this->secondaryContactType;
    }

    /**
     * @param File|UploadedFile $media
     */
    public function setMediaFile(File $media = null)
    {
        $this->mediaFile = $media;

        if (null !== $media) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getMediaFile()
    {
        return $this->mediaFile;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return string
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        $path = '/images/patients/';
        if(is_null($this->media)) {
            if(!is_null($this->getGender()) && $this->getGender()->getName() == NomUtil::NOM_SEX_TYPE_F)
                return $path . 'default_patient_woman.png';
            return $path . 'default_patient_men.png';
        }
        return $path.$this->getMedia();
    }

    /**
     * Add annex
     *
     * @param PatientAnnex $annex
     *
     * @return Patient
     */
    public function addAnnex(PatientAnnex $annex)
    {
        $this->annexes[] = $annex;

        return $this;
    }

    /**
     * Remove annex
     *
     * @param PatientAnnex $annex
     */
    public function removeAnnex(PatientAnnex $annex)
    {
        $this->annexes->removeElement($annex);
    }

    /**
     * Get annexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnnexes()
    {
        return $this->annexes;
    }

    /**
     * @return DiagnosisPatient|null
     */
    public function getActiveDiagnosis(){
        /** @var DiagnosisPatient $diag */
        foreach($this->getDiagnosisEntrys() as $diag){
            if($diag->getCurrentDiagnosis())
                return $diag;
        }

        if($this->getDiagnosisEntrys()->count() > 0){
            return $this->getDiagnosisEntrys()->get(0);
        }

        return null;
    }

    public function getDiagnosisString(){
        return !is_null($this->getActiveDiagnosis()) ? $this->getActiveDiagnosis()->getDiagnosisTemplate() : null;
    }

    /**
     * @param TranslatorInterface $trans
     * @param string $type
     * @return array
     */
    public function getArrayPdf($trans, $type) {

        $strSpec = $this->fixedStringAccommodationSpecification();
        $strNotes = $this->fixedStringCasemanagerNotes();

        $elements = [
            'Intake Date' => $this->getCreatedAt()->format('m/d/Y'),
            'Recipient' => $this->getFullName(),
            'Case No' => $this->getCaseNumber(),
            'Address' => $this->getAddress(),
            'City State Zip Code' => $this->getCity(),
            'Phone Home' => $this->getHomePhone(),
            'Phone Cell' => $this->getCellPhone(),
            'Phone Other' => $this->getOtherPhone(),
            'Email Address' => $this->getEmail(),
            'DOB' => $this->getDob()->format('m/d/Y'),
            'Age' => $this->getAge(),
            'SSN' => $this->getSsn(),
            'Gender' => !is_null($this->getGender()) ? $this->getGender()->getPdfName() : '',
            'Ethnicity' => !is_null($this->getEthnicity()) ? $trans->trans($this->getEthnicity()->getName(), [], 'BackEndBundle') : '',
            'Marital Status' => !is_null($this->getMaritalStatus()) ? $trans->trans($this->getMaritalStatus()->getName(), [], 'BackEndBundle') : '',
            'Religion' => $this->getReligion(),
            'Other language' => $this->getOtherLanguage(),
            'Check Box2' => $this->getRead() ? 'Yes' : 'Off',
            'Check Box3' => $this->getSpeak() ? 'Yes' : 'Off',
            'Understand' => $this->getUnderstand() ? 'Yes' : 'Off',
            'Employment Status' => !is_null($this->getEmploymentStatus()) ? $trans->trans($this->getEmploymentStatus()->getName(), [], 'BackEndBundle') : '',
            'Residential Status' => !is_null($this->getResidentialStatus()) ? $trans->trans($this->getResidentialStatus()->getName(), [], 'BackEndBundle') : '',
            'Income' => $this->getFamilyIncome(),
            'sources of income' => $this->getIncomeSource(),
            'Legal Guardian' => $this->getGuardianName(),
            'Legal Guardian Telephone Number' => $this->getGuardianPhone(),
            'Legal Guardian Address' => $this->getGuardianAddress(),
            'Legal Guardian City State Zip code' => $this->getGuardianCity(),
            'Self-Referral_Phone_Call' => $this->getSelfReferralPhoneCall() ? 'Yes' : 'Off',
            'Self-Referral_Walkin' => $this->getSelfReferralWalkIn() ? 'Yes' : 'Off',
            'Is client currently receiving Case Management services through another provider' => $this->getMentalHealthProv() ? 'Choice1' : 'Choice3',
            'School If Child' => $this->getSchool(),
            'Grade' => $this->getGrade(),
            'School Program' => !is_null($this->getSchoolProgram()) ? $this->getSchoolProgram()->getPdfName() : '',
            'Teacher Counselor' => $this->getTeacherName(),
            'Teacher Counselor Phone' => $this->getTeacherPhone(),
            'Year entered USA' => $this->getYearUSA(),
            'Immigration Status' => !is_null($this->getStatus()) ? $this->getStatus()->getPdfName() : '',
            'Other Status' => $this->getStatusExplination(),
            'Primary contact' => $this->getPrimaryContactName(),
            'ContactPhone_1' => $this->getPrimaryContactPhone(),
            'Relationship_2' => !is_null($this->getPrimaryContactType()) ? $trans->trans($this->getPrimaryContactType()->getName(), [], 'BackEndBundle') : '',
            'Secondary contact' => $this->getSecondaryContactName(),
            'ContactPhone_2' => $this->getSecondaryContactPhone(),
            'Relationship_3' => !is_null($this->getSecondaryContactType()) ? $trans->trans($this->getSecondaryContactType()->getName(), [], 'BackEndBundle') : '',
            'Msg Consent' => $this->getContactPermit() ? 'Choice1' : 'Choice2',
            'PCP Name' => $this->getPcpName(),
            'PCP Phone Number' => $this->getPcpPhone(),
            'PCP Address' => $this->getPcpAddress(),
            'PCP City State Zip' => $this->getPcpCity(),
            'Other Name' => $this->getOtherProviderName(),
            'Other Phone Number' => $this->getOtherProviderPhone(),
            'Other Address' => $this->getOtherProviderAddress(),
            'Other City State Zip' => $this->getOtherProviderCity(),
            'Special Accommodations' => $this->getSpetialAccommodation() ? 'Choice3' : 'No',
            'If Yes please specify' => $strSpec['sub1'],
            'If Yes please specifyText2' => $strSpec['sub2'],
        ];

        $tcmElement = [
            'Race' => $trans->trans($this->getRace()->getName(), [], 'BackEndBundle'),
            'Educational Level' => !is_null($this->getEducationalLevel()) ? $trans->trans($this->getEducationalLevel()->getName(), [], 'BackEndBundle') : '',
            'Primary Language' => !is_null($this->getPrimaryLanguage()) ? $trans->trans($this->getPrimaryLanguage()->getName(), [], 'BackEndBundle') : '',
            'Referral Name' => $this->getReferredByName(),
            'Referral Phone Number' => $this->getReferredByPhone(),
            'Referral Address' => $this->getReferredByAddress(),
            'Referral City State Zip' => $this->getReferredByCity(),
            'Referral Title Position' => $this->getReferredByTitle(),
            'Referral Agency' => !is_null($this->getAgency()) ? $trans->trans($this->getAgency()->getName(), [], 'BackEndBundle') : '',
            'Axis I Code1' => !is_null($this->getActiveDiagnosis()) ? $this->getActiveDiagnosis()->getDiagnosisTemplate() : '',
            'Description Diagnostic' => !is_null($this->getActiveDiagnosis()) ? $this->getActiveDiagnosis()->getDiagnosisTemplate()->getDescription() : '',
            'Elegibility Criteria' => $this->getEligibilityCriteria(),
            'Country Birth' => $trans->trans($this->getCountryBirth(), [], 'BackEndBundle'),
            'Psychiatrist Name' => $this->getPsychiatristName(),
            'Psychiatrist Phone Number' => $this->getPsychiatristPhone(),
            'Psychiatrist Address' => $this->getPsychiatristAddress(),
            'Psychiatrist City State Zip' => $this->getPsychiatristCity(),
            'notes1' => $strNotes[0],
            'notes2' => $strNotes[1].'',
            'notes3' => $strNotes[2].'',
            'notes4' => $strNotes[3].'',
            'Case Manager Name' => !is_null($this->getCaseManager()) ? $this->getCaseManager()->getName() : '',
            'Credentials' => $this->getCaseManagerCredentials(),
        ];

        $cmhElements = [
            'Race.0' => $trans->trans($this->getRace()->getName(), [], 'BackEndBundle'),
            'Education Level' => !is_null($this->getEducationalLevel()) ? $trans->trans($this->getEducationalLevel()->getName(), [], 'BackEndBundle') : '',
            'Select Languague' => !is_null($this->getPrimaryLanguage()) ? $trans->trans($this->getPrimaryLanguage()->getName(), [], 'BackEndBundle') : '',
            'referred' => !is_null($this->getReferredBy()) ? $trans->trans($this->getReferredBy()->getName(), [], 'BackEndBundle') : '',
            'Agency Name' => !is_null($this->getAgency()) ? $trans->trans($this->getAgency()->getName(), [], 'BackEndBundle') : '',
            'Other' => $this->getReferredByOther(),
            'mental health provider' => $this->getMhSpecification(),
            'Country of Birth' => $this->getCountryBirth(),
            'Psychiatric Name' => $this->getPsychiatristName(),
            'psychiatrist phone' => $this->getPsychiatristPhone(),
            'psychiatrist address' => $this->getPsychiatristAddress(),
            'psychiatrist city' => $this->getPsychiatristCity(),
        ];

        if($type == 'tcm'){
            return array_merge($elements, $tcmElement);
        }

        foreach ($this->getReferralReasons() as $reason) {
            $cmhElements[$reason->getPdfName()] = 'Yes';
        }

        return array_merge($elements, $cmhElements);
    }

    /**
     * @return array
     */
    public function fixedStringAccommodationSpecification()
    {
        $response = [];
        $text = $this->getAccommodationSpecification();
        if (strlen($text) <= 30) {
            $response['sub1'] = $text;
            $response['sub2'] = '';
        } else {
            for ($i = 0; $i < strlen($text); $i++) {
                if (strcmp($text[$i], ' ') == 0 && $i > 30) {
                    $response['sub1'] = substr($text, 0, $i);
                    $response['sub2'] = substr($text, $i);
                    break;
                }
            }
        }

        return $response;
    }

    /**
     * @return array
     */
    public function fixedStringCasemanagerNotes() {
        $response = [];
        $text = $this->getCaseManagerNotes();
        if (strlen($text) <= 30) {
            $response[0] = $text;
            $response[1] = '';
            $response[2] = '';
            $response[3] = '';
        } else {
            $response[1] = '';
            $response[2] = '';
            $response[3] = '';
            $start = 0;
            $end = 50;
            $num=0;
            for ($i = 0; $i < strlen($text); $i++) {
                if (strcmp($text[$i], ' ') == 0 && $i > $end) {
                    $response[$num] = substr($text, $start, $i);
                    $start = $i;
                    $end= $i + 30;
                    $num++;
                }
            }
        }

        return $response;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'menu.entity.patient';
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->name . ' ' . $this->lastname;
    }

    /**
     * @return string
     */
    public function __toString(){
        $str = !is_null($this->caseNumber) ? '[' . $this->caseNumber . '] ' : '';
        return (string) $str . $this->getFullName();
    }

    public function fullToString(){
        $str = !is_null($this->caseNumber) ? '[' . $this->caseNumber . '] ' : '';
        return (string) $str . $this->getFullName() . ', ' . $this->getIntakeTypesStr();
    }

    public function getIntakeTypesStr(){
        if($this->getIntakeTypes()->count() == 0)
            return '';
        if($this->getIntakeTypes()->count() == 2)
            return 'Open in TCM and CMH';
        return $this->getIntakeTypes()->get(0)->getName() == NomUtil::NOM_INTAKE_TYPE_TCM ? 'Open in TCM' : 'Open in CMH';
    }

    /**
     * @return bool
     */
    public function isActive(){
        return $this->getPatientStatus()->getName() == NomUtil::NOM_STATUS_OPEN;
    }

    /**
     * @return bool
     */
    public function hasTcm(){
        return $this->getIntakeTypes()->contains(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_TCM));
    }

    /**
     * @return bool
     */
    public function hasCmh(){
        return $this->getIntakeTypes()->contains(NomUtil::getNomenclatorsByName(NomUtil::NOM_INTAKE_TYPE_CMH));
    }

    /**
     * @PrePersist
     * @PreUpdate
     */
    public function updateAge()
    {
        if(!is_null($this->getDob())){
            $now = new DateTime();
            $diff = $now->getTimestamp() - $this->getDob()->getTimestamp();
            $diff /= (86400 * 365);
            $this->age = (int) $diff;
        }
    }

    public function getEvaluationServices(){
        return $this->getServices()->filter(function (Service $service){
           return $service->getServiceParent()->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_EVALUATION; 
        });
    }

    public function getInterventionServicesCmh(){
        return $this->getServices()->filter(function (Service $service){
            $c1 = $service->getServiceParent()->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_INTERVENTION;
            $c2 = $service->getServiceParent()->getSpecialty()->getName() == NomUtil::NOM_INTAKE_TYPE_CMH;
            return $c1 && $c2;
        });
    }

    public function getInterventionServicesTcm(){
        return $this->getServices()->filter(function (Service $service){
            $c1 = $service->getServiceParent()->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_INTERVENTION;
            $c2 = $service->getServiceParent()->getSpecialty()->getName() == NomUtil::NOM_INTAKE_TYPE_TCM;
            return $c1 && $c2;
        });
    }

    public function getTitlePreview(){
        $str = 'Medicare: ';
        if(!is_null($this->getMedicare())){
            $str .= $this->getMedicare();
        } else {
            $str .= '-';
        }
        $str .= "\n";
        $str .= 'Medicaid: ';
        if(!is_null($this->getMedicaid())){
            $str .= $this->getMedicaid();
        } else {
            $str .= '-';
        }
        $str .= "\n";
        $str .= 'Diagnosis: ';
        if(!is_null($this->getActiveDiagnosis())){
            $str .= $this->getActiveDiagnosis()->getDiagnosisTemplate();
        } else {
            $str .= '-';
        }
        $str .= "\n";
        $str .= 'Age: ' . $this->getAge();
        return $str;
    }

    /**
     * @return RegistryPatient|null
     */
    public function getOpenRegistry(){
        $registers = $this->getRegistrys()->filter(function (RegistryPatient $rp){
            return $rp->getIsOpen();
        });

        return $registers->count() > 0 ? $registers->get(0) : null;
    }
    
    public function closePatient(){
        $this->patientStatus = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_CLOSE);
    }

    /**
     * Set socialSecurity
     *
     * @param string $socialSecurity
     *
     * @return Patient
     */
    public function setSocialSecurity($socialSecurity)
    {
        $this->socialSecurity = $socialSecurity;

        return $this;
    }

    /**
     * Get socialSecurity
     *
     * @return string
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Patient
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param City $city
     *
     * @return Patient
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set migrationId
     *
     * @param integer $migrationId
     *
     * @return Patient
     */
    public function setMigrationId($migrationId)
    {
        $this->migrationId = $migrationId;

        return $this;
    }

    /**
     * Get migrationId
     *
     * @return integer
     */
    public function getMigrationId()
    {
        return $this->migrationId;
    }

    /**
     * Set amountAuth
     *
     * @param integer $amountAuth
     *
     * @return Patient
     */
    public function setAmountAuth($amountAuth)
    {
        $this->amountAuth = $amountAuth;

        return $this;
    }

    /**
     * Get amountAuth
     *
     * @return integer
     */
    public function getAmountAuth()
    {
        return $this->amountAuth;
    }

    public function updateAmountAuth(){
        $authorizations = $this->getAuthorizations(true);
        if($authorizations->count() == 0){
            $this->setAmountAuth(99999);
            return;
        }
        
        $amountRemained = 0;
        /** @var Authorization $authorization */
        foreach ($authorizations as $authorization) {
            $amountRemained += $authorization->getRemainedUnits();
        }
        $this->setAmountAuth($amountRemained);
    }

    /**
     * Set state
     *
     * @param Nomenclator $state
     *
     * @return Patient
     */
    public function setState(Nomenclator $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return Nomenclator
     */
    public function getState()
    {
        return $this->state;
    }
}
