<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\InsuranceEligibilityRepository")
 */
class InsuranceEligibility
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", nullable=true)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Hmo")
     * @ORM\JoinColumn(name="hmo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $hmo;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="run_date", type="datetime", nullable=true)
     */
    private $runDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="eligibilityEntrys")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="responsible_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $responsible;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set runDate
     *
     * @param \DateTime $runDate
     *
     * @return InsuranceEligibility
     */
    public function setRunDate($runDate)
    {
        $this->runDate = $runDate;

        return $this;
    }

    /**
     * Get runDate
     *
     * @return \DateTime
     */
    public function getRunDate()
    {
        return $this->runDate;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return InsuranceEligibility
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return InsuranceEligibility
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set responsible
     *
     * @param User $responsible
     *
     * @return InsuranceEligibility
     */
    public function setResponsible(User $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible
     *
     * @return User
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set hmo
     *
     * @param Hmo $hmo
     *
     * @return InsuranceEligibility
     */
    public function setHmo(Hmo $hmo = null)
    {
        $this->hmo = $hmo;

        return $this;
    }

    /**
     * Get hmo
     *
     * @return Hmo
     */
    public function getHmo()
    {
        return $this->hmo;
    }

    /**
     * @param Insurance $insurance
     * @return bool
     */
    public function compareWithInsurance($insurance){
        if($this->getHmo()->getId() == $insurance->getHmo()->getId() && $this->getNumber() == $insurance->getNumber())
            return true;
        return false;
    }
}
