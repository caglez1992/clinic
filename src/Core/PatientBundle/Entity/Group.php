<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name = "groupp")
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\GroupRepository")
 */
class Group
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\ServiceParent")
     * @ORM\JoinColumn(name="service_parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $serviceParent;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\User")
     */
    private $therapists;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\Patient")
     * @ORM\JoinTable(name="group_with_patient")
     */
    private $patients;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN);
        $this->therapists = new ArrayCollection();
        $this->patients = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param Nomenclator $status
     *
     * @return Group
     */
    public function setStatus(Nomenclator $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Nomenclator
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set serviceParent
     *
     * @param ServiceParent $serviceParent
     *
     * @return Group
     */
    public function setServiceParent(ServiceParent $serviceParent = null)
    {
        $this->serviceParent = $serviceParent;

        return $this;
    }

    /**
     * Get serviceParent
     *
     * @return ServiceParent
     */
    public function getServiceParent()
    {
        return $this->serviceParent;
    }

    /**
     * Add therapist
     *
     * @param User $therapist
     *
     * @return Group
     */
    public function addTherapist(User $therapist)
    {
        $this->therapists[] = $therapist;

        return $this;
    }

    /**
     * Remove therapist
     *
     * @param User $therapist
     */
    public function removeTherapist(User $therapist)
    {
        $this->therapists->removeElement($therapist);
    }

    /**
     * Get therapists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTherapists()
    {
        return $this->therapists;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Add patient
     *
     * @param Patient $patient
     *
     * @return Group
     */
    public function addPatient(Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param Patient $patient
     */
    public function removePatient(Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }
}
