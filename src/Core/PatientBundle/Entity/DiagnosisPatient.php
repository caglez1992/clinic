<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\DiagnosisPatientRepository")
 */
class DiagnosisPatient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="diagnosis_date", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $diagnosisDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\DiagnosisTemplate")
     * @ORM\JoinColumn(name="diagnosis_template_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $diagnosisTemplate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ip", type="boolean", nullable=true)
     */
    private $ip;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bio", type="boolean", nullable=true)
     */
    private $bio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pcp", type="boolean", nullable=true)
     */
    private $pcp;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User", inversedBy="emittedDiagnosis")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="diagnosisEntrys")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\ServiceForm")
     */
    private $forms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="current_diagnosis", type="boolean", nullable=true)
     */
    private $currentDiagnosis;

    /**
     * DiagnosisPatient constructor.
     */
    public function __construct()
    {
        $this->forms = new ArrayCollection();
        $this->diagnosisDate = new DateTime();
        $this->currentDiagnosis = false;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set diagnosisDate
     *
     * @param \DateTime $diagnosisDate
     *
     * @return DiagnosisPatient
     */
    public function setDiagnosisDate($diagnosisDate)
    {
        $this->diagnosisDate = $diagnosisDate;

        return $this;
    }

    /**
     * Get diagnosisDate
     *
     * @return \DateTime
     */
    public function getDiagnosisDate()
    {
        return $this->diagnosisDate;
    }

    /**
     * Set ip
     *
     * @param boolean $ip
     *
     * @return DiagnosisPatient
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return boolean
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set bio
     *
     * @param boolean $bio
     *
     * @return DiagnosisPatient
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return boolean
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set pcp
     *
     * @param boolean $pcp
     *
     * @return DiagnosisPatient
     */
    public function setPcp($pcp)
    {
        $this->pcp = $pcp;

        return $this;
    }

    /**
     * Get pcp
     *
     * @return boolean
     */
    public function getPcp()
    {
        return $this->pcp;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DiagnosisPatient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return DiagnosisPatient
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set diagnosisTemplate
     *
     * @param DiagnosisTemplate $diagnosisTemplate
     *
     * @return DiagnosisPatient
     */
    public function setDiagnosisTemplate(DiagnosisTemplate $diagnosisTemplate = null)
    {
        $this->diagnosisTemplate = $diagnosisTemplate;

        return $this;
    }

    /**
     * Get diagnosisTemplate
     *
     * @return DiagnosisTemplate
     */
    public function getDiagnosisTemplate()
    {
        return $this->diagnosisTemplate;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return DiagnosisPatient
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Add form
     *
     * @param ServiceForm $form
     *
     * @return DiagnosisPatient
     */
    public function addForm(ServiceForm $form)
    {
        $this->forms[] = $form;

        return $this;
    }

    /**
     * Remove form
     *
     * @param ServiceForm $form
     */
    public function removeForm(ServiceForm $form)
    {
        $this->forms->removeElement($form);
    }

    /**
     * Get forms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForms()
    {
        return $this->forms;
    }

    /**
     * Set currentDiagnosis
     *
     * @param boolean $currentDiagnosis
     *
     * @return DiagnosisPatient
     */
    public function setCurrentDiagnosis($currentDiagnosis)
    {
        $this->currentDiagnosis = $currentDiagnosis;

        return $this;
    }

    /**
     * Get currentDiagnosis
     *
     * @return boolean
     */
    public function getCurrentDiagnosis()
    {
        return $this->currentDiagnosis;
    }
}
