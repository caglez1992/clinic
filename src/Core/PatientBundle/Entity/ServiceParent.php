<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\ServiceParentRepository")
 */
class ServiceParent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\ServiceCode", inversedBy="services")
     * @ORM\JoinColumn(name="service_code_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $code;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\ServiceModifier")
     * @Assert\Count(max=2)
     */
    private $modifiers;

    /**
     * @var string
     *
     * @ORM\Column(name="medicare", type="boolean", nullable=true)
     */
    private $medicare;

    /**
     * @var string
     *
     * @ORM\Column(name="medicaid", type="boolean", nullable=true)
     */
    private $medicaid;

    /**
     * @var string
     *
     * @ORM\Column(name="max_duration", type="integer", nullable=true)
     * @Assert\GreaterThan(0)
     */
    private $maxDuration;
    
    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     */
    private $presence;
    
    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="specialty_id", referencedColumnName="id")
     */
    private $specialty;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\PdfForm")
     */
    private $forms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="generate_note", type="boolean", nullable=true)
     */
    private $generateNote;

    /**
     * @var boolean
     *
     * @ORM\Column(name="billing", type="boolean", nullable=true)
     */
    private $billing;

    /**
     * @var string
     *
     * @ORM\Column(name="fee_per_units", type="float", nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\NotBlank()
     */
    private $feePerUnits;

    /**
     * @var string
     *
     * @ORM\Column(name="time_per_units", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $timePerUnits;

    /**
     * @var string
     *
     * @ORM\Column(name="max_units", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $maxUnits;

    /**
     * @var bool
     *
     * @ORM\Column(name="always_max_duration", type="boolean", nullable=true)
     */
    private $alwaysMaxDuration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fee_per_service", type="boolean", nullable=true)
     */
    private $feePerService;

    /**
     * @var string
     *
     * @ORM\Column(name="pay_rate_cmh", type="float", nullable=true)
     */
    private $payRateCmh;

    /**
     * @var bool
     *
     * @ORM\Column(name="groupp", type="boolean", nullable=true)
     */
    private $group;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_discharge", type="boolean", nullable=true)
     */
    private $hasDischarge;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable", type="boolean", nullable=true)
     */
    private $enable;


    //Limitation
    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="modifier_amount_id", referencedColumnName="id")
     */
    private $modifierAmount;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="frequency_id", referencedColumnName="id")
     */
    private $frequency;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->forms = new ArrayCollection();
        $this->modifiers = new ArrayCollection();
        $this->presence = new ArrayCollection();
        $this->feePerUnits = 0.0;
        $this->payRateCmh = 0.0;
        $this->billing = true;
        $this->group = false;
        $this->hasDischarge = false;
        $this->generateNote = false;
        $this->enable = true;

        $this->modifierAmount = NomUtil::getNomenclatorsByName(NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServiceParent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set specialty
     *
     * @param Nomenclator $specialty
     *
     * @return ServiceParent
     */
    public function setSpecialty(Nomenclator $specialty = null)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return Nomenclator
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Set billing
     *
     * @param boolean $billing
     *
     * @return ServiceParent
     */
    public function setBilling($billing)
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * Get billing
     *
     * @return boolean
     */
    public function hasBilling()
    {
        return $this->billing;
    }

    /**
     * @return boolean
     */
    public function isGenerateNote()
    {
        return $this->generateNote;
    }

    /**
     * @param boolean $generateNote
     */
    public function setGenerateNote($generateNote)
    {
        $this->generateNote = $generateNote;
    }

    /**
     * Set type
     *
     * @param Nomenclator $type
     *
     * @return ServiceParent
     */
    public function setType(Nomenclator $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Nomenclator
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get billing
     *
     * @return boolean
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ServiceParent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ServiceParent
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set timePerUnits
     *
     * @param integer $timePerUnits
     *
     * @return ServiceParent
     */
    public function setTimePerUnits($timePerUnits)
    {
        $this->timePerUnits = $timePerUnits;

        return $this;
    }

    /**
     * Get timePerUnits
     *
     * @return integer
     */
    public function getTimePerUnits()
    {
        return $this->timePerUnits;
    }

    /**
     * Set feePerUnits
     *
     * @param float $feePerUnits
     *
     * @return ServiceParent
     */
    public function setFeePerUnits($feePerUnits)
    {
        $this->feePerUnits = $feePerUnits;

        return $this;
    }

    /**
     * Get feePerUnits
     *
     * @return float
     */
    public function getFeePerUnits()
    {
        return $this->feePerUnits;
    }

    /**
     * Set maxUnits
     *
     * @param integer $maxUnits
     *
     * @return ServiceParent
     */
    public function setMaxUnits($maxUnits)
    {
        $this->maxUnits = $maxUnits;

        return $this;
    }

    /**
     * Get maxUnits
     *
     * @return integer
     */
    public function getMaxUnits()
    {
        return $this->maxUnits;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return ServiceParent
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set modifierAmount
     *
     * @param Nomenclator $modifierAmount
     *
     * @return ServiceParent
     */
    public function setModifierAmount(Nomenclator $modifierAmount = null)
    {
        $this->modifierAmount = $modifierAmount;

        return $this;
    }

    /**
     * Get modifierAmount
     *
     * @return Nomenclator
     */
    public function getModifierAmount()
    {
        return $this->modifierAmount;
    }

    /**
     * Set frequency
     *
     * @param Nomenclator $frequency
     *
     * @return ServiceParent
     */
    public function setFrequency(Nomenclator $frequency = null)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return Nomenclator
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set maxDuration
     *
     * @param integer $maxDuration
     *
     * @return ServiceParent
     */
    public function setMaxDuration($maxDuration)
    {
        $this->maxDuration = $maxDuration;

        return $this;
    }

    /**
     * Get maxDuration
     *
     * @return integer
     */
    public function getMaxDuration()
    {
        return $this->maxDuration;
    }

    /**
     * @return boolean
     */
    public function isFeePerService()
    {
        return $this->feePerService;
    }

    /**
     * @param boolean $feePerService
     */
    public function setFeePerService($feePerService)
    {
        $this->feePerService = $feePerService;
    }

    /**
     * @return bool
     */
    public function hasPreDuration(){
        return !is_null($this->timePerUnits) && !is_null($this->maxUnits) && ($this->maxUnits == 1 || $this->alwaysMaxDuration);
    }

    /**
     * Add modifier
     *
     * @param ServiceModifier $modifier
     *
     * @return ServiceParent
     */
    public function addModifier(ServiceModifier $modifier)
    {
        $this->modifiers[] = $modifier;

        return $this;
    }

    /**
     * Remove modifier
     *
     * @param ServiceModifier $modifier
     */
    public function removeModifier(ServiceModifier $modifier)
    {
        $this->modifiers->removeElement($modifier);
    }

    /**
     * Get modifiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * Set medicare
     *
     * @param boolean $medicare
     *
     * @return ServiceParent
     */
    public function setMedicare($medicare)
    {
        $this->medicare = $medicare;

        return $this;
    }

    /**
     * Get medicare
     *
     * @return boolean
     */
    public function getMedicare()
    {
        return $this->medicare;
    }

    /**
     * Set medicaid
     *
     * @param boolean $medicaid
     *
     * @return ServiceParent
     */
    public function setMedicaid($medicaid)
    {
        $this->medicaid = $medicaid;

        return $this;
    }

    /**
     * Get medicaid
     *
     * @return boolean
     */
    public function getMedicaid()
    {
        return $this->medicaid;
    }

    /**
     * Add form
     *
     * @param PdfForm $form
     *
     * @return ServiceParent
     */
    public function addForm(PdfForm $form)
    {
        $this->forms[] = $form;

        return $this;
    }

    /**
     * Remove form
     *
     * @param PdfForm $form
     */
    public function removeForm(PdfForm $form)
    {
        $this->forms->removeElement($form);
    }

    /**
     * Get forms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForms()
    {
        return $this->forms;
    }

    /**
     * Set code
     *
     * @param ServiceCode $code
     *
     * @return ServiceParent
     */
    public function setCode(ServiceCode $code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return ServiceCode
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add presence
     *
     * @param Nomenclator $presence
     *
     * @return ServiceParent
     */
    public function addPresence(Nomenclator $presence)
    {
        $this->presence[] = $presence;

        return $this;
    }

    /**
     * Remove presence
     *
     * @param Nomenclator $presence
     */
    public function removePresence(Nomenclator $presence)
    {
        $this->presence->removeElement($presence);
    }

    /**
     * Get presence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * @return boolean
     */
    public function isAlwaysMaxDuration()
    {
        return $this->alwaysMaxDuration;
    }

    /**
     * @param boolean $alwaysMaxDuration
     */
    public function setAlwaysMaxDuration($alwaysMaxDuration)
    {
        $this->alwaysMaxDuration = $alwaysMaxDuration;
    }

    /**
     * @return boolean
     */
    public function isGroup()
    {
        return $this->group;
    }

    /**
     * @param boolean $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @param Patient $patient
     * @param User $worker
     * @return array
     */
    public function isValid($patient, $worker){
        //validate insurance
        $medicarePatient = $patient->getMedicare();
        $medicaidPatient = $patient->getMedicaid();

        $hasMedicare = !is_null($medicarePatient);
        $hasMedicaid = !is_null($medicaidPatient);

        $isOK = false;
        if($hasMedicare && in_array(NomUtil::MEDICARE, $this->getValidInsurances()))
            $isOK = true;

        if($hasMedicaid && in_array(NomUtil::MEDICAID, $this->getValidInsurances()))
            $isOK = true;

        if(!$isOK){
            return [
                'valid' => 'invalid',
                'tip' => 'Patient not apply for this service. Do not have a valid insurance.'
            ];
        }

        //validate worker modifiers
        if($this->modifiers->count() != 0){
            $isModOk = false;
            foreach ($worker->getModifiers() as $mod){
                if($this->modifiers->contains($mod)) {
                    $isModOk = true;
                    break;
                }
            }
            if(!$isModOk) {
                return [
                    'valid' => 'invalid',
                    'tip' => 'Your modifier is not valid to this service.'
                ];
            }
        }

        //Validate if service group, then a worker most be a responsible of a group of that service.
        if($this->isGroup()){
            $isGroupOk = false;
            /** @var GroupPatientRegistry $groupPatientRegistry */
            foreach ($patient->getGroupPatientRecords() as $groupPatientRegistry){
                $c1 = $groupPatientRegistry->getActive();
                $c2 = $groupPatientRegistry->getGroup()->getStatus()->getName() == NomUtil::NOM_STATUS_OPEN;
                $c3 = $groupPatientRegistry->getGroup()->getServiceParent()->getId() == $this->getId();
                $c4 = $groupPatientRegistry->getGroup()->getTherapists()->contains($worker);
                if($c1 && $c2 && $c3 && $c4){
                    $isGroupOk = true;
                    break;
                }
            }

            if(!$isGroupOk) {
                return [
                    'valid' => 'invalid',
                    'tip' => 'This patient do not belong to a group assigned to you.'
                ];
            }
        }

        //warning about authorization
        if($this->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_INTERVENTION){
            $isAuthOk = false;
            /** @var Authorization $authorization */
            foreach ($patient->getAuthorizations() as $authorization){
                if($authorization->getActive() && $authorization->getServiceCodes()->contains($this->getCode()) && $authorization->getRemainedUnits() > 0){
                    $isAuthOk = true;
                }
            }

            if($this->getMedicare() && !is_null($patient->getMedicare()) && $patient->getMedicare()->getHmo()->getFree())
                $isAuthOk = true;

            if($this->getMedicaid() && !is_null($patient->getMedicaid()) && $patient->getMedicaid()->getHmo()->getFree())
                $isAuthOk = true;

            if(!$isAuthOk){
                return [
                    'valid' => 'warning',
                    'type' => 'auth',
                    'tip' => 'This patient does not have a valid authorization at this time.'
                ];
            }
        }

        return [
            'valid' => 'ok',
            'tip' => 'This service can be used correctly.'
        ];
    }

    /**
     * @return array
     */
    public function getValidInsurances(){
        $insurances = [];
        if($this->getMedicare())
            $insurances[] = NomUtil::MEDICARE;
        if($this->getMedicaid())
            $insurances[] = NomUtil::MEDICAID;
        return $insurances;
    }

    /**
     * @return bool
     */
    public function existLimitations(){
        return !is_null($this->amount) && !is_null($this->modifierAmount) && !is_null($this->frequency);
    }

    function __toString()
    {
        $str = '';
        if($this->getModifiers()->count() > 0) {
            /** @var ServiceModifier $modifier */
            foreach ($this->getModifiers() as $modifier) {
                $str .= ' [';
                $str .= $this->getCode() . '/' . $modifier->getCode();
                $str .= '] ';
            }
        } else {
            $str = ' [' . $this->getCode() . ']';
        }

        return (string) $this->getName() . $str;
    }

    public function getStringCode(){
        $str = '';
        if($this->getModifiers()->count() > 0) {
            /** @var ServiceModifier $modifier */
            foreach ($this->getModifiers() as $modifier) {
                $str .= ' [';
                $str .= $this->getCode() . '/' . $modifier->getCode();
                $str .= '] ';
            }
        } else {
            $str = ' [' . $this->getCode() . ']';
        }

        return $str;
    }

    /**
     * Get alwaysMaxDuration
     *
     * @return boolean
     */
    public function getAlwaysMaxDuration()
    {
        return $this->alwaysMaxDuration;
    }

    /**
     * Get group
     *
     * @return boolean
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get generateNote
     *
     * @return boolean
     */
    public function getGenerateNote()
    {
        return $this->generateNote;
    }

    /**
     * Get feePerService
     *
     * @return boolean
     */
    public function getFeePerService()
    {
        return $this->feePerService;
    }

    /**
     * Set payRateCmh
     *
     * @param float $payRateCmh
     *
     * @return ServiceParent
     */
    public function setPayRateCmh($payRateCmh)
    {
        $this->payRateCmh = $payRateCmh;

        return $this;
    }

    /**
     * Get payRateCmh
     *
     * @return float
     */
    public function getPayRateCmh()
    {
        return $this->payRateCmh;
    }

    /**
     * Set hasDischarge
     *
     * @param boolean $hasDischarge
     *
     * @return ServiceParent
     */
    public function setHasDischarge($hasDischarge)
    {
        $this->hasDischarge = $hasDischarge;

        return $this;
    }

    /**
     * Get hasDischarge
     *
     * @return boolean
     */
    public function getHasDischarge()
    {
        return $this->hasDischarge;
    }

    /**
     * Set enable
     *
     * @param boolean $enable
     *
     * @return ServiceParent
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable
     *
     * @return boolean
     */
    public function getEnable()
    {
        return $this->enable;
    }
}
