<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\WorkerBundle\Entity\SettingCode;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

abstract class ServiceMapper
{
    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\ServiceParent")
     * @ORM\JoinColumn(name="service_parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $serviceParent;

    /**
     * @ORM\ManyToOne(targetEntity="Core\WorkerBundle\Entity\SettingCode")
     * @ORM\JoinColumn(name="setting_code_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $settingCode;
    
    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $worker;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="approved_by_id", referencedColumnName="id")
     */
    protected $approvedBy;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\Nomenclator")
     * @ORM\JoinColumn(name="session_type_id", referencedColumnName="id")
     */
    protected $sessionType;

    /**
     * @ORM\ManyToOne(targetEntity="Core\WorkerBundle\Entity\Week")
     * @ORM\JoinColumn(name="week_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $week;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Hmo")
     * @ORM\JoinColumn(name="hmo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $hmo;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\DiagnosisTemplate")
     * @ORM\JoinColumn(name="diagnosis_template_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $diagnosisTemplate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    protected $approved;

    /**
     * @var boolean
     *
     * @ORM\Column(name="revised", type="boolean", nullable=true)
     */
    protected $revised;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_edit", type="boolean", nullable=true)
     */
    protected $canEdit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="billed", type="boolean", nullable=true)
     */
    protected $billed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payed", type="boolean", nullable=true)
     */
    protected $payed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="billing_zero", type="boolean", nullable=true)
     */
    protected $billingZero;

    /**
     * @var boolean
     *
     * @ORM\Column(name="billing_retro_active", type="boolean", nullable=true)
     */
    protected $billingRetroActive;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="service_date", type="datetime", nullable=true)
     */
    protected $serviceDate;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN);
        $this->revised = false;
        $this->approved = false;
        $this->billed = false;
        $this->payed = false;
        $this->billingZero = false;
    }

    /**
     * Set serviceParent
     *
     * @param ServiceParent $serviceParent
     *
     * @return Service
     */
    public function setServiceParent(ServiceParent $serviceParent = null)
    {
        $this->serviceParent = $serviceParent;

        return $this;
    }

    /**
     * Get serviceParent
     *
     * @return ServiceParent
     */
    public function getServiceParent()
    {
        return $this->serviceParent;
    }

    /**
     * Set settingCode
     *
     * @param SettingCode $settingCode
     *
     * @return Service
     */
    public function setSettingCode(SettingCode $settingCode = null)
    {
        $this->settingCode = $settingCode;

        return $this;
    }

    /**
     * Get settingCode
     *
     * @return SettingCode
     */
    public function getSettingCode()
    {
        return $this->settingCode;
    }

    /**
     * Set worker
     *
     * @param User $worker
     *
     * @return ServiceMapper
     */
    public function setWorker(User $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return User
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set approvedBy
     *
     * @param User $approvedBy
     *
     * @return Service
     */
    public function setApprovedBy(User $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set status
     *
     * @param Nomenclator $status
     *
     * @return Service
     */
    public function setStatus(Nomenclator $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Nomenclator
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sessionType
     *
     * @param Nomenclator $sessionType
     *
     * @return Service
     */
    public function setSessionType(Nomenclator $sessionType = null)
    {
        $this->sessionType = $sessionType;

        return $this;
    }

    /**
     * Get sessionType
     *
     * @return Nomenclator
     */
    public function getSessionType()
    {
        return $this->sessionType;
    }

    /**
     * Set week
     *
     * @param Week $week
     *
     * @return Service
     */
    public function setWeek(Week $week = null)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return Week
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set hmo
     *
     * @param Hmo $hmo
     *
     * @return Service
     */
    public function setHmo(Hmo $hmo = null)
    {
        $this->hmo = $hmo;

        return $this;
    }

    /**
     * Get hmo
     *
     * @return Hmo
     */
    public function getHmo()
    {
        return $this->hmo;
    }

    /**
     * Set diagnosisTemplate
     *
     * @param DiagnosisTemplate $diagnosisTemplate
     *
     * @return Service
     */
    public function setDiagnosisTemplate(DiagnosisTemplate $diagnosisTemplate = null)
    {
        $this->diagnosisTemplate = $diagnosisTemplate;

        return $this;
    }

    /**
     * Get diagnosisTemplate
     *
     * @return DiagnosisTemplate
     */
    public function getDiagnosisTemplate()
    {
        return $this->diagnosisTemplate;
    }

    /**
     * Set revised
     *
     * @param boolean $revised
     *
     * @return Service
     */
    public function setRevised($revised)
    {
        $this->revised = $revised;

        return $this;
    }

    /**
     * Get revised
     *
     * @return boolean
     */
    public function getRevised()
    {
        return $this->revised;
    }



    /**
     * @return mixed
     */
    public function getBilled()
    {
        return $this->billed;
    }

    /**
     * @param mixed $billed
     */
    public function setBilled($billed)
    {
        $this->billed = $billed;
    }

    /**
     * @return mixed
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @param mixed $payed
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;
    }
    
    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return Service
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set billingZero
     *
     * @param boolean $billingZero
     *
     * @return Service
     */
    public function setBillingZero($billingZero)
    {
        $this->billingZero = $billingZero;

        return $this;
    }

    /**
     * Get billingZero
     *
     * @return boolean
     */
    public function getBillingZero()
    {
        return $this->billingZero;
    }

    /**
     * @return DateTime
     */
    public function getServiceDate()
    {
        return $this->serviceDate;
    }

    /**
     * @param mixed $serviceDate
     */
    public function setServiceDate($serviceDate)
    {
        $this->serviceDate = $serviceDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Service
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Service
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param Patient $patient
     * @return Service
     */
    abstract public function setPatient(Patient $patient = null);

    /**
     * @return Patient
     */
    abstract public function getPatient();

    /**
     * @return int
     */
    abstract public function getUnitsUsed();

    /**
     * @return float
     */
    public function getAmount(){
        return $this->getUnitsUsed() * $this->getServiceParent()->getFeePerUnits();
    }

    /**
     * @param User $workerResponsible
     * @return array
     */
    public function getRolesToNotify($workerResponsible){
        if($this->isCmh()){
            if($workerResponsible->isQa())
                return [User::ROLE_BILLING, User::ROLE_OPERATION_MANAGER];
            if($workerResponsible->isSupervisor())
                return [User::ROLE_QA_CMH];
            if($workerResponsible->isServiceWorker())
                return [User::ROLE_SUPERVISOR_CMH];
        } else {
            if($workerResponsible->isQa())
                return [User::ROLE_BILLING, User::ROLE_OPERATION_MANAGER];
            if($workerResponsible->isSupervisor())
                return [User::ROLE_QA_TCM];
            if($workerResponsible->isServiceWorker())
                return [User::ROLE_SUPERVISOR_TCM];
        }
        return [];
    }

    /**
     * @return string
     */
    public function getFullServiceCode(){
        if (is_null($this->getServiceParent()))
            return '';

        $serviceCode = $this->getServiceParent()->getCode()->getCode();
        $str = $serviceCode;

        if($serviceCode == 'T1017'){
            $age = $this->getPatient()->getAge();
            $strTmp = $age > 17 ? '/' . $this->getPatient()->getAge() : '/HA';
            $str .= $strTmp;
        } else {
            foreach ($this->getServiceParent()->getModifiers() as $serviceModifier){
                if($this->getWorker()->getModifiers()->contains($serviceModifier)){
                    $str .= '/' . $serviceModifier->getCode();
                    return $str;
                }
            }
        }

        return $str;
    }

    /**
     * @return string
     */
    public function getCode(){
        if (is_null($this->getServiceParent()))
            return '';
        
        return $this->getServiceParent()->getCode()->getCode();
    }

    /**
     * @return ServiceCode
     */
    public function getServiceCode(){
        return $this->getServiceParent()->getCode();
    }

    /**
     * @return bool
     */
    public function isClose() {
        return $this->getStatus() != null && $this->getStatus()->getName() == NomUtil::NOM_STATUS_CLOSE;
    }
    
    public function getAuthorization(){
        if($this->getServiceParent()->getType()->getName() == NomUtil::NOM_SERVICE_TYPE_INTERVENTION){

            if($this->getServiceParent()->getMedicare() && !is_null($this->getPatient()->getMedicare()) && $this->getPatient()->getMedicare()->getHmo()->getFree())
                return 'free';

            if($this->getServiceParent()->getMedicaid() && !is_null($this->getPatient()->getMedicaid()) && $this->getPatient()->getMedicaid()->getHmo()->getFree())
                return 'free';

            /** @var Authorization $authorization */
            foreach ($this->getPatient()->getAuthorizations() as $authorization){
                if($authorization->getActive() && $authorization->getServiceCodes()->contains($this->getServiceCode()) && $authorization->getRemainedUnits() >= $this->getUnitsUsed()){
                    return $authorization;
                }
            }

            return 'invalid';
        }
        return null;
    }

    /**
     * @return bool
     */
    public function isTcm(){
        return $this->getCode() == 'T1017';
    }

    /**
     * @return bool
     */
    public function isCmh(){
        return !$this->isTcm();
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->isTcm() ? 'tcm' : 'cmh';
    }

    public function isBillable(){
        $auth = $this->getAuthorization();
        return $auth == 'invalid' ? false : true;
    }

    /**
     * @return boolean
     */
    public function isCanEdit()
    {
        return $this->canEdit;
    }

    /**
     * @param boolean $canEdit
     */
    public function setCanEdit($canEdit)
    {
        $this->canEdit = $canEdit;
    }

    /**
     * @return boolean
     */
    public function isBillingRetroActive()
    {
        return $this->billingRetroActive;
    }

    /**
     * @param boolean $billingRetroActive
     */
    public function setBillingRetroActive($billingRetroActive)
    {
        $this->billingRetroActive = $billingRetroActive;
    }
    
    
}
