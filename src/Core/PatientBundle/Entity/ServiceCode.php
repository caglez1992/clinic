<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Nomenclator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\ServiceCodeRepository")
 * @UniqueEntity(
 *     fields={"code"},
 *     errorPath="code",
 * )
 */
class ServiceCode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\ServiceParent", mappedBy="code", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $services;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ServiceCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add service
     *
     * @param ServiceParent $service
     *
     * @return ServiceCode
     */
    public function addService(ServiceParent $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param ServiceParent $service
     */
    public function removeService(ServiceParent $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    function __toString()
    {
        return (string) $this->code;
    }
}
