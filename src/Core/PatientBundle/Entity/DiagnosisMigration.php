<?php

namespace Core\PatientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

class DiagnosisMigration
{
    /**
     * @Type(name="string")
     */
    private $code1;
    /**
     * @Type(name="string")
     */
    private $code2;
    /**
     * @Type(name="string")
     */
    private $diagnosis;

    /**
     * DiagnosisMigration constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getCode1()
    {
        return $this->code1;
    }

    /**
     * @param mixed $code1
     */
    public function setCode1($code1)
    {
        $this->code1 = $code1;
    }

    /**
     * @return mixed
     */
    public function getCode2()
    {
        return $this->code2;
    }

    /**
     * @param mixed $code2
     */
    public function setCode2($code2)
    {
        $this->code2 = $code2;
    }

    /**
     * @return mixed
     */
    public function getDiagnosis()
    {
        return $this->diagnosis;
    }

    /**
     * @param mixed $diagnosis
     */
    public function setDiagnosis($diagnosis)
    {
        $this->diagnosis = $diagnosis;
    }
}
