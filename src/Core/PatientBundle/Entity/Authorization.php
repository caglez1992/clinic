<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\Loggable;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\AuthorizationRepository")
 * @Vich\Uploadable
 */
class Authorization implements Loggable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="migrationId", type="integer", nullable=true)
     */
    private $migrationId;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="authorizations")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Hmo")
     * @ORM\JoinColumn(name="hmo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $hmo;

    /**
     * @ORM\OneToMany(targetEntity="Core\PatientBundle\Entity\BillingRegistry", mappedBy="authorization", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $bills;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $number;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="begin_date", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $beginDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="units_approved", type="integer", nullable=true)
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    private $unitsApproved;

    /**
     * @var string
     *
     * @ORM\Column(name="units_consumed", type="integer", nullable=true)
     */
    private $unitsConsumed;

    /**
     * @ORM\ManyToMany(targetEntity="Core\PatientBundle\Entity\ServiceCode")
     * @Assert\Count(min=1)
     */
    private $serviceCodes;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", nullable=true)
     */
    private $observations;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255, nullable = true)
     */
    private $media;

    /**
     * @Vich\UploadableField(mapping="authorizations", fileNameProperty="media")
     * @var File
     */
    private $mediaFile;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->serviceCodes = new ArrayCollection();
        $this->bills = new ArrayCollection();
        $this->unitsConsumed = 0;
        $this->active = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Authorization
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     *
     * @return Authorization
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Authorization
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set unitsApproved
     *
     * @param integer $unitsApproved
     *
     * @return Authorization
     */
    public function setUnitsApproved($unitsApproved)
    {
        $this->unitsApproved = $unitsApproved;

        return $this;
    }

    /**
     * Get unitsApproved
     *
     * @return integer
     */
    public function getUnitsApproved()
    {
        return $this->unitsApproved;
    }

    /**
     * Set unitsConsumed
     *
     * @param integer $unitsConsumed
     *
     * @return Authorization
     */
    public function setUnitsConsumed($unitsConsumed)
    {
        $this->unitsConsumed = $unitsConsumed;

        return $this;
    }

    /**
     * Get unitsConsumed
     *
     * @return integer
     */
    public function getUnitsConsumed()
    {
        return $this->unitsConsumed;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return Authorization
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set hmo
     *
     * @param Hmo $hmo
     *
     * @return Authorization
     */
    public function setHmo(Hmo $hmo = null)
    {
        $this->hmo = $hmo;

        return $this;
    }

    /**
     * Get hmo
     *
     * @return Hmo
     */
    public function getHmo()
    {
        return $this->hmo;
    }

    /**
     * @return integer
     */
    public function getRemainedUnits(){
        return $this->unitsApproved - $this->unitsConsumed;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Authorization
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getLabel()
    {
        return 'menu.entity.authorization';
    }

    /**
     * Add serviceCode
     *
     * @param ServiceCode $serviceCode
     *
     * @return Authorization
     */
    public function addServiceCode(ServiceCode $serviceCode)
    {
        $this->serviceCodes[] = $serviceCode;

        return $this;
    }

    /**
     * Remove serviceCode
     *
     * @param ServiceCode $serviceCode
     */
    public function removeServiceCode(ServiceCode $serviceCode)
    {
        $this->serviceCodes->removeElement($serviceCode);
    }

    /**
     * Get serviceCodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceCodes()
    {
        return $this->serviceCodes;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Authorization
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param File|UploadedFile $media
     */
    public function setMediaFile(File $media = null)
    {
        $this->mediaFile = $media;

        if (null !== $media) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getMediaFile()
    {
        return $this->mediaFile;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return Authorization
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return Authorization
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Authorization
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add bill
     *
     * @param BillingRegistry $bill
     *
     * @return Authorization
     */
    public function addBill(BillingRegistry $bill)
    {
        $this->bills[] = $bill;

        return $this;
    }

    /**
     * Remove bill
     *
     * @param BillingRegistry $bill
     */
    public function removeBill(BillingRegistry $bill)
    {
        $this->bills->removeElement($bill);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * Set migrationId
     *
     * @param integer $migrationId
     *
     * @return Authorization
     */
    public function setMigrationId($migrationId)
    {
        $this->migrationId = $migrationId;

        return $this;
    }

    /**
     * Get migrationId
     *
     * @return integer
     */
    public function getMigrationId()
    {
        return $this->migrationId;
    }

    /**
     * @return int
     */
    public function getPercent(){
        return (int)(($this->unitsConsumed * 100) / $this->unitsApproved);
    }

    /**
     * @return int
     */
    public function expireIn(){
        $now = new DateTime();
        return (int)(($this->getEndDate()->getTimestamp() - $now->getTimestamp()) / 60 / 60 / 24);
    }

    /**
     * @return null|string
     */
    public function getDocument()
    {
        $path = '/pdf/authorizations/';
        if(is_null($this->media))
            return null;
        return $path.$this->getMedia();
    }

    public function getTotalUnitsConsumed(){
        $units = 0;
        /** @var BillingRegistry $bill */
        foreach ($this->getBills() as $bill) {
            $units += $bill->getUnitsConsumed();
        }
        return $units;
    }

    function __toString()
    {
        return sprintf('%s [Record #%s]', $this->getNumber(), $this->getId());
    }

    /**
     * @return string
     */
    public function getTitlePreview(){
        $str = 'Unique Code: ' . $this->getId();
        $str .= "\n";
        $str .= 'Old ID: ' . $this->getMigrationId();
        $str .= "\n";
        $str .= 'HMO: ' . $this->getHmo();
        $str .= "\n";
        $str .= 'From: ' . $this->getBeginDate()->format('M d, Y');
        $str .= "\n";
        $str .= 'To: ' . $this->getEndDate()->format('M d, Y');
        return $str;
    }
}
