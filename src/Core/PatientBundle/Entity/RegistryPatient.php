<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\RegistryPatientRepository")
 */
class RegistryPatient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=true)
     * @Assert\NotBlank()
     */
    private $entryDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="therapist_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $therapist;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="case_manager_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $caseManager;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_by_cmh", type="boolean", nullable=true)
     */
    private $openByCmh;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_by_tcm", type="boolean", nullable=true)
     */
    private $openByTcm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discharge_cmh", type="boolean", nullable=true)
     */
    private $dischargeCmh;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="discharge_cmh_date", type="datetime", nullable=true)
     */
    private $dischargeCmhDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discharge_tcm", type="boolean", nullable=true)
     */
    private $dischargeTcm;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="discharge_tcm_date", type="datetime", nullable=true)
     */
    private $dischargeTcmDate;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Patient", inversedBy="registrys")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="responsible_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $responsible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_open", type="boolean", nullable=true)
     */
    private $isOpen;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entryDate = new DateTime();
        $this->openByTcm = false;
        $this->openByCmh = false;
        $this->dischargeTcm = false;
        $this->dischargeCmh = false;
        $this->isOpen = true;
    }

    /**
     * Set openByCmh
     *
     * @param boolean $openByCmh
     *
     * @return RegistryPatient
     */
    public function setOpenByCmh($openByCmh)
    {
        $this->openByCmh = $openByCmh;

        return $this;
    }

    /**
     * Get openByCmh
     *
     * @return boolean
     */
    public function getOpenByCmh()
    {
        return $this->openByCmh;
    }

    /**
     * Set openByTcm
     *
     * @param boolean $openByTcm
     *
     * @return RegistryPatient
     */
    public function setOpenByTcm($openByTcm)
    {
        $this->openByTcm = $openByTcm;

        return $this;
    }

    /**
     * Get openByTcm
     *
     * @return boolean
     */
    public function getOpenByTcm()
    {
        return $this->openByTcm;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     *
     * @return RegistryPatient
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Set dischargeCmh
     *
     * @param boolean $dischargeCmh
     *
     * @return RegistryPatient
     */
    public function setDischargeCmh($dischargeCmh)
    {
        $this->dischargeCmh = $dischargeCmh;

        return $this;
    }

    /**
     * Get dischargeCmh
     *
     * @return boolean
     */
    public function getDischargeCmh()
    {
        return $this->dischargeCmh;
    }

    /**
     * Set dischargeCmhDate
     *
     * @param \DateTime $dischargeCmhDate
     *
     * @return RegistryPatient
     */
    public function setDischargeCmhDate($dischargeCmhDate)
    {
        $this->dischargeCmhDate = $dischargeCmhDate;

        return $this;
    }

    /**
     * Get dischargeCmhDate
     *
     * @return \DateTime
     */
    public function getDischargeCmhDate()
    {
        return $this->dischargeCmhDate;
    }

    /**
     * Set dischargeTcm
     *
     * @param boolean $dischargeTcm
     *
     * @return RegistryPatient
     */
    public function setDischargeTcm($dischargeTcm)
    {
        $this->dischargeTcm = $dischargeTcm;

        return $this;
    }

    /**
     * Get dischargeTcm
     *
     * @return boolean
     */
    public function getDischargeTcm()
    {
        return $this->dischargeTcm;
    }

    /**
     * Set dischargeTcmDate
     *
     * @param \DateTime $dischargeTcmDate
     *
     * @return RegistryPatient
     */
    public function setDischargeTcmDate($dischargeTcmDate)
    {
        $this->dischargeTcmDate = $dischargeTcmDate;

        return $this;
    }

    /**
     * Get dischargeTcmDate
     *
     * @return \DateTime
     */
    public function getDischargeTcmDate()
    {
        return $this->dischargeTcmDate;
    }

    /**
     * Set therapist
     *
     * @param User $therapist
     *
     * @return RegistryPatient
     */
    public function setTherapist(User $therapist = null)
    {
        $this->therapist = $therapist;

        return $this;
    }

    /**
     * Get therapist
     *
     * @return User
     */
    public function getTherapist()
    {
        return $this->therapist;
    }

    /**
     * Set caseManager
     *
     * @param User $caseManager
     *
     * @return RegistryPatient
     */
    public function setCaseManager(User $caseManager = null)
    {
        $this->caseManager = $caseManager;

        return $this;
    }

    /**
     * Get caseManager
     *
     * @return User
     */
    public function getCaseManager()
    {
        return $this->caseManager;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     *
     * @return RegistryPatient
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set responsible
     *
     * @param User $responsible
     *
     * @return RegistryPatient
     */
    public function setResponsible(User $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible
     *
     * @return User
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set isOpen
     *
     * @param boolean $isOpen
     *
     * @return RegistryPatient
     */
    public function setIsOpen($isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get isOpen
     *
     * @return boolean
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }
}
