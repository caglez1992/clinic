<?php

namespace Core\PatientBundle\Entity;

use Core\CoreBundle\Entity\User;
use Core\WorkerBundle\Entity\ExplanationPayment;
use Core\WorkerBundle\Entity\Note;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Core\PatientBundle\Repository\BillingRegistryRepository")
 * @Vich\Uploadable
 */
class BillingRegistry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Authorization", inversedBy="bills")
     * @ORM\JoinColumn(name="authorization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $authorization;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Hmo")
     * @ORM\JoinColumn(name="hmo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $hmo;

    /**
     * @var integer
     *
     * @ORM\Column(name="units_consumed", type="integer", nullable=true)
     */
    private $unitsConsumed;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Core\WorkerBundle\Entity\Note")
     * @ORM\JoinColumn(name="note_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="Core\PatientBundle\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $service;

    /**
     * @var DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Core\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payed", type="boolean", nullable=true)
     */
    private $payed;

    /**
     * @ORM\OneToMany(targetEntity="Core\WorkerBundle\Entity\ExplanationPayment", mappedBy="billingRegistry", cascade={"all"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $payments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->unitsConsumed = 0;
        $this->payed = false;
        $this->payments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unitsConsumed
     *
     * @param integer $unitsConsumed
     *
     * @return BillingRegistry
     */
    public function setUnitsConsumed($unitsConsumed)
    {
        $this->unitsConsumed = $unitsConsumed;

        return $this;
    }

    /**
     * Get unitsConsumed
     *
     * @return integer
     */
    public function getUnitsConsumed()
    {
        return $this->unitsConsumed;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return BillingRegistry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BillingRegistry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set authorization
     *
     * @param Authorization $authorization
     *
     * @return BillingRegistry
     */
    public function setAuthorization(Authorization $authorization = null)
    {
        $this->authorization = $authorization;

        return $this;
    }

    /**
     * Get authorization
     *
     * @return Authorization
     */
    public function getAuthorization()
    {
        return $this->authorization;
    }

    /**
     * Set note
     *
     * @param Note $note
     *
     * @return BillingRegistry
     */
    public function setNote(Note $note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return Note
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set service
     *
     * @param Service $service
     *
     * @return BillingRegistry
     */
    public function setService(Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return BillingRegistry
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set hmo
     *
     * @param Hmo $hmo
     *
     * @return BillingRegistry
     */
    public function setHmo(Hmo $hmo = null)
    {
        $this->hmo = $hmo;

        return $this;
    }

    /**
     * Get hmo
     *
     * @return Hmo
     */
    public function getHmo()
    {
        return $this->hmo;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return BillingRegistry
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return boolean
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @return Patient|null
     */
    public function getPatient(){
        if(!is_null($this->getNote()))
            return $this->getNote()->getPatient();
        if(!is_null($this->getService()))
            return $this->getService()->getPatient();
        return null;
    }

    public function getElement(){
        if(!is_null($this->getNote()))
            return $this->getNote();
        if(!is_null($this->getService()))
            return $this->getService();
        return null;
    }

    /**
     * Add payment
     *
     * @param ExplanationPayment $payment
     *
     * @return BillingRegistry
     */
    public function addPayment(ExplanationPayment $payment)
    {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param ExplanationPayment $payment
     */
    public function removePayment(ExplanationPayment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @return string
     */
    public function __toString(){
        return (string) sprintf('Record #%d, Patient #%s, Billed Amount: %s, %s',
            $this->getId(),
            $this->getPatient()->getCaseNumber(),
            '$' . number_format($this->getAmount(), 2, '.', ','),
            $this->getHmo()
        );
    }

    /**
     * @return float
     */
    public function getTotalPayment(){
        $total = 0.0;
        /** @var ExplanationPayment $payment */
        foreach ($this->getPayments() as $payment){
            $total += $payment->getPaymentAmount();
            $total -= $payment->getReversalPaymentAmount();
        }
        return $total;
    }
}
