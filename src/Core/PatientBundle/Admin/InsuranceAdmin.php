<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Nomenclator;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\Insurance;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Patient;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Validator\Constraints\NotBlank;

class InsuranceAdmin extends BaseAdmin
{
    /**
     * Return all hmo by type medicare/medicaid.
     * @param string $nom
     * @return mixed
     */
    public function getHmoByType($nom){
        $column = strtolower($nom);
        $sql = sprintf('SELECT hmo FROM PatientBundle:Hmo hmo WHERE hmo.%s = true', $column);
        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(Hmo::class);
        return $em->createQuery($sql);
    }
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*$datagridMapper
            ->add('id', null, array(
                'label' => 'form.insurance.id',
            ))
            ->add('number', null, array(
                'label' => 'form.insurance.number',
            ))
        ;*/
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        /*$listMapper
            ->add('id', null, array(
                'label' => 'general.code',
            ))
            ->add('insuranceTemplate', null, array(
                'label' => 'menu.entity.insurance',
            ))
            ->add('insuranceTemplate.hmo', null, array(
                'label' => 'HMO',
            ))
            ->add('number', null, array(
                'label' => 'form.insurance.number',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;*/
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$formMapper
            ->add('insuranceTemplate', null, array(
                'label' => 'menu.entity.insurance',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('number', null, array(
                'label' => 'general.number',
            ))
        ;*/
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /*$showMapper
            ->add('id', null, array(
                'label' => 'form.insurance.id',
            ))
            ->add('name', null, array(
                'label' => 'form.insurance.name',
            ))
            ->add('number', null, array(
                'label' => 'form.insurance.number',
            ))
        ;*/
    }

    /**
     * @param Insurance $object
     */
    public function postUpdate($object){
//        $doctrine = $this->container()->get('doctrine');
//        $patient = $doctrine->getRepository(Patient::class)->getPatientByInsurance($object);
//
//        $isPrimary = $patient->isPrimaryInsurance($object);
//
//        //Create a Registry of insurance elegibility
//        $ie = new InsuranceEligibility();
//        $ie->setPatient($patient);
//        $ie->setNumber($object->getNumber());
//        $ie->setInsuranceTemplate($object->getInsuranceTemplate());
//        $ie->setPrimary($isPrimary);
//        $ie->setResponsible($this->getRealUser());
//        $this->save($ie);
//
//        //Todo: Update roles notification
//        $url = $this->getConfigurationPool()->getContainer()->get('router')->generate('show_patient', ['id' => $patient->getId()]);
//        $this->container()->get(NotificationManager::class)->createNotification(User::ROLE_SUPER_ADMIN, 'Create new eligibility entry on patient ' . $patient->getFullName(), $url, Notification::SUCCESS);
    }

    /**
     * @param Insurance $object
     * @return string
     */
    public function toString($object)
    {
        if($object instanceof Insurance && $object->getNumber() != null)
            return $object->getNumber();
            //return $object->getNumber() . ' (' . $object->getName() .')';
        return $this->trans('menu.entity.insurance',array(),'BackEndBundle');
    }
}
