<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\ServiceParent;
use Core\PatientBundle\Entity\ServicePdfForm;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

class ServiceParentAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'code',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('code', null, array(
                'label' => 'general.code',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('type', null, array(
                'label' => 'service.type',
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
            ))
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('code', null, array(
                'label' => 'general.code',
            ))
            ->add('presence', null, array(
                'label' => 'Presence Type',
                'template' => '@BackEnd/structures/list_field_nom_many_to_many.html.twig',
            ))
            ->add('modifiers', null, array(
                'label' => 'Modifiers',
            ))
            ->add('timePerUnits', null, array(
                'label' => 'Time per units (min)',
            ))
            ->add('feePerUnits', null, array(
                'label' => 'Fee per units ($)',
            ))
            ->add('maxUnits', null, array(
                'label' => 'Maximum units (per service)',
            ))
            ->add('enable', null, array(
                'label' => 'Enabled',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'Description of Service',
            ))
            ->add('code', null, array(
                'label' => 'Procedure Code',
            ))
            ->add('modifiers', 'sonata_type_model', array(
                'label' => 'Modifiers',
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
                'btn_add' => false,
            ))
            ->add('medicare', null, array(
                'label' => 'Apply for medicare?',
            ))
            ->add('medicaid', null, array(
                'label' => 'Apply for medicaid?',
            ))
            ->add('specialty', 'sonata_type_model', array(
                'label' => 'form.service.parent.specialty',
                'query' => $this->getQueryNomenclators(NomUtil::NOM_INTAKE_TYPE),
                'choice_translation_domain' => 'BackEndBundle',
                'btn_add' => false,
                'constraints' => [
                    new NotBlank(),
                ],
            ))
            ->add('type', 'sonata_type_model', array(
                'label' => 'service.type',
                'query' => $this->getQueryNomenclators(NomUtil::NOM_SERVICE_TYPE),
                'choice_translation_domain' => 'BackEndBundle',
                'btn_add' => false,
                'constraints' => [
                    new NotBlank(),
                ],
                'help' => 'Only intervention service required authorization to proceed.'
            ))
            ->add('group', null, [
                'label' => 'Is Group?'
            ])
            ->add('hasDischarge')
            ->add('enable', null, [
                'label' => 'Enabled'
            ])
            ->add('presence', 'sonata_type_model', array(
                'label' => 'Presence Type',
                'query' => $this->getQueryNomenclators(NomUtil::NOM_SERVICE_PRESENCE_TYPE),
                'choice_translation_domain' => 'BackEndBundle',
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'constraints' => [
                    new NotBlank(),
                    new Count(['min' => 1]),
                ],
            ))
            ->add('forms', 'sonata_type_model', array(
                'label' => 'general.forms',
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ))


            ->add('feePerUnits', null, array(
                'label' => 'Fee per units ($)',
            ))
            ->add('timePerUnits', null, array(
                'label' => 'Time per units (minutes)',
            ))
            ->add('maxUnits', null, array(
                'label' => 'Maximum units (per service)',
            ))
            ->add('alwaysMaxDuration', null, array(
                'label' => 'Always use max duration',
            ))
            ->add('feePerService', null, [
                'label' => 'Use fee per service'
            ])
            ->add('billing', null, array(
                'label' => 'Generate Billing',
            ))


            ->add('amount', null, array(
                'label' => 'Number',
            ))
            ->add('modifierAmount', 'sonata_type_model', array(
                'label' => 'Amount Type',
                'query' => $this->getQueryNomenclators(NomUtil::NOM_MODIFIER_AMOUNT_TYPE),
                'choice_translation_domain' => 'BackEndBundle',
                'btn_add' => false,
                'required' => false,
            ))
            ->add('frequency', 'sonata_type_model', array(
                'label' => 'Frequency',
                'query' => $this->getQueryNomenclators(NomUtil::NOM_FREQUENCY_TYPE),
                'choice_translation_domain' => 'BackEndBundle',
                'btn_add' => false,
                'required' => false,
            ))
        ;

        $this->addFieldsetForm('name', 'init', 'General Options');
        $this->addFieldsetForm('forms', 'end');

        $this->addFieldsetForm('feePerUnits', 'init', 'Advanced Options');
        $this->addFieldsetForm('billing', 'end');

        $this->addFieldsetForm('amount', 'init', 'Service Limitations');
        $this->addFieldsetForm('frequency', 'end');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.serviceparent.id',
            ))
            ->add('name', null, array(
                'label' => 'form.serviceparent.name',
            ))
            ->add('code', null, array(
                'label' => 'form.serviceparent.code',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof ServiceParent && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.service.parent',array(),'BackEndBundle');
    }

    /**
     * @param ServiceParent $obj
     */
    public function prePersist($obj)
    {
        if(!is_null($obj->getMaxUnits()) && !is_null($obj->getTimePerUnits())){
            $obj->setMaxDuration($obj->getMaxUnits() * $obj->getTimePerUnits());
        }
    }

    /**
     * @param ServiceParent $obj
     */
    public function preUpdate($obj)
    {
        $this->prePersist($obj);
    }
}
