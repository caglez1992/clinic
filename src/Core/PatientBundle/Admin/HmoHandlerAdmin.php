<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\HmoHandler;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class HmoHandlerAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*$datagridMapper
            ->add('id', null, array(
                'label' => 'general.code',
            ))
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('hmo', null, array(
                'label' => 'HMO',
            ))
        ;*/
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof HmoHandler && $object->getName() != null)
            return $object->getName();
        return "HMO Handler";
    }
}
