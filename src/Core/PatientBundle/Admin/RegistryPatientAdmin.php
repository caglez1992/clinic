<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\PatientBundle\Entity\RegistryPatient;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class RegistryPatientAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'entryDate',
    );

    /**
     * @return boolean
     */
    public function isCreateRoute()
    {
        return false;
    }
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('entryDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'form.registry.patient.entry.date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('caseManager', null, array(
                'label' => 'form.patient.case.manager',
            ))
            ->add('inactiveCmh', null, array(
                'label' => 'form.registry.patient.inactive.cmhc',
            ))
            ->add('inactiveTcm', null, array(
                'label' => 'form.registry.patient.inactive.tcm',
            ))
            ->add('dischargeCmh', null, array(
                'label' => 'Discharge CMH',
            ))
            ->add('dischargeTcm', null, array(
                'label' => 'Discharge TCM',
            ))
            ->add('dischargeCmhDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'Discharge CMH Date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('dischargeTcmDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'Discharge TCM Date',
                'field_type' => 'sonata_type_date_picker',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('entryDate', null, array(
                'label' => 'form.registry.patient.entry.date',
                'format' => 'M/d/Y'
            ))
            ->add('therapist', null, array(
                'label' => 'form.registry.patient.therapist',
            ))
            ->add('caseManager', null, array(
                'label' => 'form.patient.case.manager',
            ))
            ->add('inactiveCmh', null, array(
                'label' => 'form.registry.patient.inactive.cmhc',
                'editable' => true,
            ))
            ->add('inactiveTcm', null, array(
                'label' => 'form.registry.patient.inactive.tcm',
                'editable' => true,
            ))
            ->add('dischargeCmh', null, array(
                'label' => 'Discharge CMH',
            ))
            ->add('dischargeCmhDate', null, array(
                'label' => 'Discharge CMH Date',
                'format' => 'M/d/Y'
            ))
            ->add('dischargeTcm', null, array(
                'label' => 'Discharge TCM',
            ))
            ->add('dischargeTcmDate', null, array(
                'label' => 'Discharge TCM Date',
                'format' => 'M/d/Y'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    //'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('entryDate', 'sonata_type_date_picker', array(
                'label' => 'form.registry.patient.entry.date',
                'format' => 'MM/dd/yyyy',
            ))
            ->add('therapist', 'sonata_type_model', array(
                'label' => 'form.registry.patient.therapist',
                'btn_add' => false,
                'required' => false,
            ))
            ->add('caseManager', 'sonata_type_model', array(
                'label' => 'form.patient.case.manager',
                'btn_add' => false,
            ))
            ->add('inactiveCMHC', null, array(
                'label' => 'form.registry.patient.inactive.cmhc',
            ))
            ->add('inactiveTCM', null, array(
                'label' => 'form.registry.patient.inactive.tcm',
            ))
            ->add('discharge', null, array(
                'label' => 'form.registry.patient.discharge',
            ))
            ->add('dischargeDate', 'sonata_type_date_picker', array(
                'label' => 'form.registry.patient.discharge.date',
                'format' => 'MM/dd/yyyy',
                'required' => false,
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.registrypatient.id',
            ))
            ->add('entryDate', null, array(
                'label' => 'form.registrypatient.entryDate',
            ))
            ->add('inactiveCMHC', null, array(
                'label' => 'form.registrypatient.inactiveCMHC',
            ))
            ->add('inactiveTCM', null, array(
                'label' => 'form.registrypatient.inactiveTCM',
            ))
            ->add('discharge', null, array(
                'label' => 'form.registrypatient.discharge',
            ))
            ->add('dischargeDate', null, array(
                'label' => 'form.registrypatient.dischargeDate',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.registrypatient.createdAt',
            ))
        ;
    }

    public function toString($object)
    {
       return $this->trans('registry.patient',array(),'BackEndBundle');
    }

    /**
     * @param RegistryPatient $object
     */
    public function postPersist($object)
    {
//        $patient = $object->getPatient();
//        $patient->setCaseManager($object->getCaseManager());
//        $bm = $this->container()->get(BaseManager::class);
//        $bm->save($patient);
//
//        $url = $this->getConfigurationPool()->getAdminByAdminCode('patient.bundle.admin.patient')->generateUrl('list');
//        $this->container()->get(NotificationManager::class)->createNotification($object->getCaseManager(), 'A new patient has been added to you.', $url, 'success', $important = true, $dataMsg = array());
    }

    /**
     * @param RegistryPatient $object
     */
    public function postUpdate($object)
    {
//        if(!$object->getDischarge()){
//            $this->postPersist($object);
//        }
    }

    /**
     * @param RegistryPatient $object
     */
    public function preRemove($object)
    {
//        if(!$object->getDischarge()){
//            $patient = $object->getPatient();
//            $patient->setCaseManager(null);
//            $bm = $this->container()->get(BaseManager::class);
//            $bm->save($patient);
//        }
    }


}
