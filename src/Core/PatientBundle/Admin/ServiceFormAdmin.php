<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ServiceFormAdmin extends BaseAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('show');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'form.serviceform.id',
            ))
            ->add('name', null, array(
                'label' => 'form.serviceform.name',
            ))
            ->add('filename', null, array(
                'label' => 'form.serviceform.filename',
            ))
            ->add('filled', null, array(
                'label' => 'form.serviceform.filled',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.serviceform.createdAt',
            ))
            ->add('updatedAt', null, array(
                'label' => 'form.serviceform.updatedAt',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => '#Record',
            ))
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('service', null, array(
                'label' => 'Service Record',
            ))
            ->add('filled')
            ->add('createdAt', null, array(
                'format' => 'M d, Y H:i'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, array(
                'label' => 'form.serviceform.id',
            ))
            ->add('name', null, array(
                'label' => 'form.serviceform.name',
            ))
            ->add('filename', null, array(
                'label' => 'form.serviceform.filename',
            ))
            ->add('filled', null, array(
                'label' => 'form.serviceform.filled',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.serviceform.createdAt',
            ))
            ->add('updatedAt', null, array(
                'label' => 'form.serviceform.updatedAt',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.serviceform.id',
            ))
            ->add('name', null, array(
                'label' => 'form.serviceform.name',
            ))
            ->add('filename', null, array(
                'label' => 'form.serviceform.filename',
            ))
            ->add('filled', null, array(
                'label' => 'form.serviceform.filled',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.serviceform.createdAt',
            ))
            ->add('updatedAt', null, array(
                'label' => 'form.serviceform.updatedAt',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof ServiceFormAdmin && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.serviceformadmin',array(),'BackEndBundle');
    }
}
