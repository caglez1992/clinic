<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\NotBlank;

class GroupPatientRegistryAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('remove');
        $collection->remove('show');
        $collection->remove('batch');
        $collection->remove('edit');
    }

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'inDate',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('group')
            ->add('inDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('outDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('active')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('group')
            ->add('inDate', null, array(
                'label' => 'In Date',
                'format' => 'M d, Y',
            ))
            ->add('outDate', null, array(
                'label' => 'Out Date',
                'format' => 'M d, Y',
            ))
            ->add('active', null, array(
                'label' => 'Is Active?',
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('group', null, [
                'constraints' => [
                    new NotBlank()
                ],
                'required' => true,
            ])
            ->add('inDate', 'sonata_type_date_picker', array(
                'label' => 'Entry Date',
                'format' => 'MM/dd/yyyy'
            ))
            ->add('active', null, array(
                'label' => 'Is Active?',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.grouppatient.id',
            ))
            ->add('inDate', null, array(
                'label' => 'form.grouppatient.inDate',
            ))
            ->add('outDate', null, array(
                'label' => 'form.grouppatient.outDate',
            ))
            ->add('active', null, array(
                'label' => 'form.grouppatient.active',
            ))
        ;
    }

    /*public function postPersist($object)
    {
        $workers = $object->getGroup()->getTherapists();
        foreach ($workers as $worker){
            if(!$worker->getPatients()->contains($object->getPatient())) {
                $worker->addPatient($object->getPatient());
                $this->container()->get(NotificationManager::class)->createNotification($worker, sprintf('Patient %s has been add to the group %s', $object->getPatient()->getFullName(), $object->getGroup()->getName()), $url = null, Notification::SUCCESS, $important = false);
                $this->save($worker);
            }
        }
    }*/

    public function toString($object)
    {
        return 'Group Registry';
    }
}
