<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Repository\UserRepository;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Repository\PatientRepository;
use Core\WorkerBundle\Entity\Week;
use Core\WorkerBundle\Repository\WeekRepository;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class ServiceAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'scheduleStart',
    );

    protected $createRoute = false;

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);
        $root = $query->getRootAliases()[0];

        if($userReal->isServiceWorker()){
            $query->andWhere("$root.worker = :worker");
            $query->setParameter('worker', $userReal);
        }

        if($userReal->isSupervisor()){
            $query->andWhere("$root.worker IN (:assignedWorkers)");
            $query->setParameter('assignedWorkers', $userReal->getAssignedWorkers());
        }

        if($userReal->isQa()){
            $condition = $userReal->hasRole(User::ROLE_QA_TCM) ? "sc.code = 'T1017'" : "sc.code != 'T1017'";
            $query->join("$root.serviceParent", 'sp')
                ->join("sp.code", 'sc')
                ->andWhere($condition);
        }

        return $query;
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
    }

    /**
     * Configure Batch Actions
     * @param array $actions
     * @return array
     */
    public function configureBatchActions($actions)
    {
        $actions = [];
        $userReal = $this->getRealUser();
        if ($this->hasRoute('edit') && $this->hasAccess('edit') && $userReal->isServiceWorker()) {
            $actions['closeService'] = array(
                'ask_confirmation' => true,
            );

        }
        return $actions;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $userReal = $this->getRealUser();
        $d = $this->container()->get('doctrine');
        
        $primaryOptionsWorker = [
            'label' => 'Worker',
            'show_filter' => !$userReal->isServiceWorker(),
        ];
        $secondaryOptionsWorker = [
            'choices' => $d->getRepository(User::class)->getUsersByRole([User::ROLE_TCM, User::ROLE_CMH, User::ROLE_SPECIALIST])
        ];

        if($userReal->isSupervisor()){
            $secondaryOptionsWorker['choices'] = $userReal->getAssignedWorkers();
        }

        if($userReal->hasRole(User::ROLE_QA_TCM)){
            $secondaryOptionsWorker['choices'] = $d->getRepository(User::class)->getUsersByRole([User::ROLE_TCM]);
        }

        if($userReal->hasRole(User::ROLE_QA_CMH)){
            $secondaryOptionsWorker['choices'] = $d->getRepository(User::class)->getUsersByRole([User::ROLE_CMH, User::ROLE_SPECIALIST]);
        }

        $datagridMapper
            ->add('id', null, array(
                'label' => 'Service Record',
            ))
            ->add('patient', null, [], 'entity', [
                'query_builder' => function(PatientRepository $pr){
                    return $pr->getOpenPatients(true);
                },
            ])
            ->add('week', null, ['show_filter' => true], 'entity', [
                'query_builder' =>function(WeekRepository $wr){
                    return  $wr->getWeeksForSelect(true);
                },
            ])
            ->add('serviceDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'Date of Service',
            ))
            ->add('worker', null, $primaryOptionsWorker, 'entity', $secondaryOptionsWorker)
            ->add('status', null, [], 'entity', [
                //'class' => 'Core\CoreBundle\Entity\NomNomenclator',
                //'property' => 'title',
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' =>function(EntityRepository $er){
                    return  $this->getQueryBuilderNomenclators($er, NomUtil::NOM_STATUS);
                },
            ])
            ->add('revised', null, array(
                'label' => 'Approved',
            ))
            ->add('approved', null, [
                'label' => 'Revised'
            ])
        ;
    }

    public function configureDefaultFilterValues(array &$filterValues)
    {
        $thisWeek = $this->container()->get('doctrine')->getRepository(Week::class)->getWeekByDay(new \DateTime());
        $filterValues['week'] = array(
            //'type'  => ChoiceFilter::TYPE_CONTAINS,
            'value' => $thisWeek->getId(),
        );
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'Service Record',
            ))
            ->add('patient', null, array(
                'template' => '@BackEnd/structures/list_field_patient.html.twig',
            ))
            ->add('serviceParent', null, array(
                'label' => 'Service',
            ))
            /*->add('serviceParent.type', null, array(
                'label' => 'Type',
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
            ))*/
            ->add('scheduleStart', null, array(
                'format' => 'M d, Y h:i a'
            ))
            ->add('scheduleEnd', null, array(
                'format' => 'M d, Y h:i a'
            ))
            ->add('serviceDurationMin', null, array(
                'label' => 'Duration (min)',
            ))
            ->add('unitsUsed', null, array(
                'label' => 'Units',
            ))
            ->add('worker', null, array(
                'label' => 'Worker',
            ))
            ->add('forms', null, array(
                'template' => '@BackEnd/structures/list_field_associate_forms.html.twig',
            ))
            ->add('status', null, array(
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
            ))
            ->add('revised', null, array(
                'label' => 'Approved',
            ))
            ->add('approved', null, [
                'label' => 'Revised'
            ])
            ->add('billingRetroActive', null, [
                'label' => 'Retroactive'
            ])
        ;

        /*if ($this->container()->get('security.authorization_checker')->isGranted(User::ROLE_BILLING)) {
            $listMapper
                ->add('billed', null, array(
                    'label' => 'Billed',
                ))
                ->add('payed', null, array(
                    'label' => 'Payed',
                ))
            ;
        }*/

        $listMapper->add('_action', null, array(
            'actions' => array(
                'show' => array('label' => false),
                'edit_service' => array(
                    'template' => '@BackEnd/btn_actions/service/edit_service.html.twig',
                ),
                'close_service' => array(
                    'template' => '@BackEnd/btn_actions/service/close_service.html.twig',
                ),
                'review_service' => array(
                    'template' => '@BackEnd/btn_actions/service/review_service.html.twig',
                ),
                'approve_service' => array(
                    'template' => '@BackEnd/btn_actions/service/approve_service.html.twig',
                ),
                'my_delete' => array(
                    'template' => '@BackEnd/btn_actions/service/btn_delete.html.twig',
                ),
                'service_annexes' => array(
                    'template' => '@BackEnd/btn_actions/service/service_annexes.html.twig',
                ),
            )
        ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('scheduleStart', 'sonata_type_date_picker', array(
                'label' => 'Start Date',
                'format' => 'MM/dd/yyyy',
            ))
            ->add('scheduleEnd', 'sonata_type_date_picker', array(
                'label' => 'End Date',
                'format' => 'MM/dd/yyyy',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General', ['class' => 'col-xs-6'])
                ->add('id', null, array(
                    'label' => 'Service Record',
                ))
                ->add('serviceParent', null, [
                    'label' => 'Service Name'
                ])
                ->add('fullServiceCode', null, [
                    'label' => 'Full Code'
                ])
                ->add('patient')
                ->add('worker', null, [
                    'label' => 'Worker'
                ])
                ->add('settingCode')
                ->add('status', null, [
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig'
                ])
                ->add('sessionType', null, [
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig'
                ])
                ->add('presence', null, [
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig'
                ])
                ->add('hmo')
                ->add('diagnosisTemplate', null , [
                    'label' => 'Diagnosis'
                ])

            ->end()

            ->with('More Info', ['class' => 'col-xs-6'])
                ->add('scheduleStart', null, array(
                    'format' => 'M d, Y, h:i a',
                ))
                ->add('scheduleEnd', null, array(
                    'format' => 'M d, Y, h:i a',
                ))
                ->add('createdAt', null, array(
                    'format' => 'M d, Y, h:i a',
                ))
                ->add('week')
                ->add('serviceDurationMin', null, array(
                    'label' => 'Duration (min)',
                ))
                ->add('unitsUsed', null, array(
                    'label' => 'Units',
                ))
                ->add('revised')
                ->add('approved')
                ->add('approvedBy')
                ->add('billed')
                ->add('payed')
            ->end()
        ;
    }

    public function toString($object)
    {
        return (string) sprintf('Service Record #%s', $object->getId());
    }
}
