<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Form\Type\MyFileType;
use Core\CoreBundle\Manager\NotificationManager;
use Core\PatientBundle\Entity\Group;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Patient;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Util\NomUtil;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\Count;

class PatientAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);
        $root = $query->getRootAliases()[0];

        if ('list' === $context && isset($parameters['_sort_by'])) {
            $parameters = $this->getFilterParameters();
            if ('authorizations' === $parameters['_sort_by']) {
                $query
                    ->orderBy("$root.amountAuth", $parameters['_sort_order'])
                ;
                return $query;
            }
        }

        if($userReal->isServiceWorker()){
            $query->join("$root.workers", 'worker');
            $query->andWhere("worker.id = :userRealId");
            $query->setParameter('userRealId', $userReal->getId());
        }

        if($userReal->isSupervisor()){
            $query->join("$root.workers", 'worker');
            $query->andWhere("worker IN (:assignedWorkers)");
            $query->setParameter('assignedWorkers', $userReal->getAssignedWorkers());
        }

        return $query;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('caseNumber', null, array(
                'label' => 'form.patient.case.number',
            ))
            ->add('name', null, array(
                'label' => 'form.patient.name',
            ))
            ->add('lastname', null, array(
                'label' => 'general.lastname',
            ))
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'form.patient.intake.date',
            ))
            ->add('age', null, array(
                'label' => 'form.patient.age',
            ))
            ->add('homePhone', null, array(
                'label' => 'form.patient.home.phone',
            ))
            ->add('cellPhone', null, array(
                'label' => 'form.patient.cellphone',
            ))            
            /*->add('countryBirth', null, array(
                'label' => 'form.patient.country.birth',
            ))*/
            ->add('group', CallbackFilter::class, array(
                'callback' => array($this, 'filterGroup'),
                'label' => 'Group',
                'field_type' => 'entity',
                'class' => Group::class,
            ), null, ['class' => Group::class])

            ->add('patientStatus', null, [], 'entity', [
                'label' => 'Status',
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' =>function(EntityRepository $er){
                    return  $this->getQueryBuilderNomenclators($er, NomUtil::NOM_STATUS);
                },
            ])

            ->add('activeAuthorization', CallbackFilter::class, array(
                'callback' => array($this, 'filterActiveAuthorization'),
                'label' => 'Active Authorization',
                'field_type' => BooleanType::class,
            ), null, [] )
        ;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $alias
     * @param string $field
     * @param array $value
     * @return bool|void
     */
    public function filterGroup($queryBuilder, $alias, $field, $value)
    {
        $group = $value['value'];
        if (!$group)
            return;

        $queryBuilder->join("$alias.groupPatientRecords", 'gp')
            ->andWhere('gp.group = :group')
            ->andWhere('gp.active = true')
            ->setParameter('group', $group);
        
        return true;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $alias
     * @param string $field
     * @param array $value
     * @return bool|void
     */
    public function filterActiveAuthorization($queryBuilder, $alias, $field, $value)
    {
        $active = $value['value'];
        if (!$active)
            return;

        $active = $active == '1';
        if($active) {
            $queryBuilder
                ->join("$alias.authorizations", 'auth')
                ->andWhere('auth.active = true');
        } else {
            $d = $this->container()->get('doctrine');
            $patientNoAuth = $d->getRepository(Patient::class)->getPatientWithNoActiveAuthorizations();
            $queryBuilder
                ->andWhere("$alias IN (:patients)")
                ->setParameter('patients', $patientNoAuth);
        }

        return true;
    }

    public function configureBatchActions($actions)
    {
        return [];
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper   
            ->add('caseNumber', null, array(
                'label' => 'form.patient.case.number',
            ))
            ->add('fullname', null, array(
                'label' => 'Full Name',
                'template' => '@BackEnd/structures/list_patient_name.html.twig',
            ))
            ->add('medicare.hmo', null, array(
                'label' => 'Medicare',
                'template' => '@BackEnd/structures/list_insurance_patient.html.twig',
            ))
            ->add('medicaid.hmo', null, array(
                'label' => 'Medicaid',
                'template' => '@BackEnd/structures/list_insurance_patient.html.twig',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.patient.intake.date',
                'format' => 'M d, Y'
            ))          
            ->add('intakeTypes', null, array(
                'label' => 'form.patient.intake.type',
                'template' => '@BackEnd/structures/list_field_nom_many_to_many.html.twig',
            ))   
            ->add('patientStatus', null, array(
                'label' => 'form.patient.status',
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
                'sortable' => true,
                'sort_field_mapping' => ['fieldName' => 'id'],
                'sort_parent_association_mappings' => [],
            ))
            ->add('authorizations', null, array(
                'label' => 'Active Authorizations',
                'template' => '@BackEnd/structures/list_field_authorizations.html.twig',
                'sortable' => true,
                'sort_field_mapping' => ['fieldName' => 'id'],
                'sort_parent_association_mappings' => [],
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show_patient' => ['template' => '@BackEnd/btn_actions/patient/show_patient.html.twig'],
                    //'tasks' => ['template' => '@BackEnd/btn_actions/patient/tasks.html.twig'],
                    //'registry' => ['template' => '@BackEnd/btn_actions/patient/registry.html.twig'],
                    //'eligibility' => ['template' => '@BackEnd/btn_actions/patient/eligibility.html.twig'],
                    'diagnosis' => ['template' => '@BackEnd/btn_actions/patient/diagnosis.html.twig'],
                    'authorization' => ['template' => '@BackEnd/btn_actions/patient/authorization.html.twig'],
                    'group_registry' => ['template' => '@BackEnd/btn_actions/patient/group_registry.html.twig'],
                    'annexes' => ['template' => '@BackEnd/btn_actions/patient/patient_annexes.html.twig'],
                    'edit' => ['label' => false],
                    'delete' => ['label' => false],
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $optionsMedicare = [
            'label' => 'Medicare',
            //'btn_delete' => false,
            'btn_list' => false,
            'btn_edit' => false,
            'required' => false,
        ];

        $optionsMedicaid = [
            'label' => 'Medicaid',
            //'btn_delete' => false,
            'btn_list' => false,
            'btn_edit' => false,
            'required' => false,
        ];

        $formMapper
            ->with('block.intake.patient', ['class'=>'col-md-6', 'icon'=>'fa fa-user', 'subtitle'=>'block.intake.patient.subtitle'])
                ->add('intakeTypes', 'sonata_type_model', array(
                    'label' => 'form.patient.intake.type',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_INTAKE_TYPE),
                    'btn_add' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'required' => true,
                    'choice_translation_domain'=>'BackEndBundle',
                    'constraints' => [
                        new Count(['min' => 1])
                    ],
                ))
                /*->add('patientStatus', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.status',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_PATIENT_STATUS_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                ))*/
                ->add('name', null, array(
                    'label' => 'form.patient.name',
                ))
                ->add('lastname', null, array(
                    'label' => 'general.lastname',
                ))
                ->add('dob', 'sonata_type_date_picker', array(
                    'label' => 'form.patient.dob',
                    'format' => 'MM/dd/yyyy',
                ))
                ->add('gender', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.gender',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_SEX_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('socialSecurity', null, array(
                    'label' => 'Social Security',
                    'attr' => ['class' => 'mask-social-security'],
                ))
                /*->add('caseNumber', null, array(
                    'label' => 'form.patient.case.number',
                    'required' => false,
                ))*/
                ->add('address', null, array(
                    'label' => 'form.patient.address',
                ))
                ->add('city', null, array(
                    'label' => 'City',
                ))
                ->add('state', 'sonata_type_model', array(
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_STATE_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('zipCode', null, array(
                    'label' => 'Zip Code',
                ))
                ->add('homePhone', null, array(
                    'label' => 'form.patient.home.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('cellPhone', null, array(
                    'label' => 'form.patient.cellphone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('otherPhone', null, array(
                    'label' => 'form.patient.otherPhone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('email', null, array(
                    'label' => 'form.patient.email',
                    'required' => false,
                ))

                /*->add('age', null, array(
                    'label' => 'form.patient.age',
                    'required' => false,
                    'attr' => ['disabled' => 'disabled'],
                ))*/
                ->add('ssn', null, array(
                    'label' => 'form.patient.ssn',
                    'attr' => ['placeholder' => '999-99-9999']
                ))
                ->add('race', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.race',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_RACE_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('ethnicity', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.ethnicity',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_ETHNICITY_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('maritalStatus', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.marital.status',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_MARITAL_STATUS_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('educationalLevel', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.educational.level',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_EDUCATIONAL_LEVEL_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('religion', null, array(
                    'label' => 'form.patient.religion',
                    'required' => false,
                ))
                ->add('primaryLanguage', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.primary.language',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_LANGUAGE_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('otherLanguage', null, array(
                    'label' => 'form.patient.otherLanguage',
                    'required' => false,
                ))
                ->add('read', null, array(
                    'label' => 'form.patient.read',
                    'required' => false,
                ))
                ->add('speak', null, array(
                    'label' => 'form.patient.speak',
                    'required' => false,
                ))
                ->add('understand', null, array(
                    'label' => 'form.patient.understand',
                    'required' => false,
                ))
                ->add('employmentStatus', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.employment.status',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_EMPLOYMENT_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('residentialStatus', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.residential.status',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_RESIDENTIAL_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('familyIncome', null, array(
                    'label' => 'form.patient.family.income',
                ))
                ->add('incomeSource', null, array(
                    'label' => 'form.patient.income.source',
                    'required' => false,
                ))
                ->add('guardianName', null, array(
                    'label' => 'form.patient.guardian.name',
                    'required' => false,
                ))
                ->add('guardianPhone', null, array(
                    'label' => 'form.patient.guardian.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('guardianAddress', null, array(
                    'label' => 'form.patient.guardian.address',
                    'required' => false,
                ))
                ->add('guardianCity', null, array(
                    'label' => 'form.patient.guardian.city',
                    'required' => false,
                ))
                ->add('referredBy', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.referred.by',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_REFERRED_TYPE),
                    'btn_add' => false,
                    'required' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                ))
                ->add('referredByName', null, array(
                    'label' => 'form.patient.referred.by.name',
                    'required' => false,
                ))
                ->add('referredByPhone', null, array(
                    'label' => 'general.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('referredByAddress', null, array(
                    'label' => 'general.address',
                    'required' => false,
                ))
                ->add('referredByCity', null, array(
                    'label' => 'form.patient.guardian.city',
                    'required' => false,
                ))
                ->add('referredByTitle', null, array(
                    'label' => 'form.patient.referred.by.title',
                    'required' => false,
                ))
                ->add('agency', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.agency',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_AGENCY_TYPE),
                    'btn_add' => false,
                    'required' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                ))
                ->add('referredByOther', null, array(
                    'label' => 'form.patient.referred.by.other',
                    'required' => false,
                ))
                ->add('selfReferralWalkIn', null, array(
                    'label' => 'form.patient.selfreferral.walk.in',
                    'required' => false,
                ))
                ->add('selfReferralPhoneCall', null, array(
                    'label' => 'form.patient.selfreferral.phone.call',
                    'required' => false,
                ))
                ->add('mentalHealthProv', null, array(
                    'label' => 'form.patient.mental.heatlh.prov',
                    'required' => false,
                ))
                ->add('mhSpecification', null, array(
                    'label' => 'form.patient.mh.specification',
                    'required' => false,
                ))
                ->add('eligibilityCriteria', null, array(
                    'label' => 'form.patient.eligibility.criteria',
                    'required' => false,
                ))
                ->add('referralReasons', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.referral.reasons',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_REFERRAL_REASONS_TYPE),
                    'btn_add' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'required' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                ))
            ->end()


            ->with('block.patient.intake.continue', array('class' => 'col-md-6'))

                ->add('medicare', 'sonata_type_model_list', $optionsMedicare)

                ->add('medicaid', 'sonata_type_model_list', $optionsMedicaid)

                ->add('school', null, array(
                    'label' => 'form.patient.school',
                    'required' => false,
                ))
                ->add('grade', null, array(
                    'label' => 'form.patient.grade',
                    'required' => false,
                ))
                ->add('schoolProgram', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.school.program',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_SCHOOL_PROGRAM_TYPE),
                    'btn_add' => false,
                    'required' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                ))
                ->add('teacherName', null, array(
                    'label' => 'form.patient.teacher.name',
                    'required' => false,
                ))
                ->add('teacherPhone', null, array(
                    'label' => 'form.patient.teacher.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('countryBirth', null, array(
                    'placeholder' => 'placeholder.select',
                    'label' => 'form.patient.country.birth',
                ))
                ->add('yearUSA', null, array(
                    'label' => 'form.patient.year.usa',
                ))
                ->add('status', 'sonata_type_model', array(
                    'class' => 'Core\CoreBundle\Entity\Nomenclator',
                    'label' => 'form.patient.status',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_RESIDENTIAL_STATUS_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('statusExplination', null, array(
                    'label' => 'form.patient.status.explination',
                    'required' => false,
                ))
                ->add('primaryContactName', null, array(
                    'label' => 'form.patient.primary.contact.name',
                ))
                ->add('primaryContactPhone', null, array(
                    'label' => 'form.patient.primary.contact.phone',
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('primaryContactType', 'sonata_type_model', array(
                    'label' => 'form.patient.primary.contact.relationship',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_PARENT_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('secondaryContactName', null, array(
                    'label' => 'form.patient.secondary.contact.name',
                    'required' => false,
                ))
                ->add('secondaryContactPhone', null, array(
                    'label' => 'form.patient.secondary.contact.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('secondaryContactType', 'sonata_type_model', array(
                    'label' => 'form.patient.secondary.contact.relationship',
                    'placeholder' => 'placeholder.select',
                    'query' => $this->getQueryNomenclators(NomUtil::NOM_PARENT_TYPE),
                    'btn_add' => false,
                    'choice_translation_domain'=>'BackEndBundle',
                    'required' => false,
                ))
                ->add('contactPermit', null, array(
                    'label' => 'form.patient.contact.permit',
                ))
                ->add('psychiatristName', null, array(
                    'label' => 'form.patient.psychiatrist.name',
                    'required' => false,
                ))
                ->add('psychiatristPhone', null, array(
                    'label' => 'form.patient.psychiatrist.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('psychiatristAddress', null, array(
                    'label' => 'form.patient.psychiatrist.address',
                    'required' => false,
                ))
                ->add('psychiatristCity', null, array(
                    'label' => 'form.patient.psychiatrist.city',
                    'required' => false,
                ))
                ->add('pcpName', null, array(
                    'label' => 'form.patient.pcp.name',
                    'required' => false,
                ))
                ->add('pcpPhone', null, array(
                    'label' => 'form.patient.pcp.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('pcpAddress', null, array(
                    'label' => 'form.patient.pcp.address',
                    'required' => false,
                ))
                ->add('pcpCity', null, array(
                    'label' => 'form.patient.pcp.city',
                    'required' => false,
                ))
                ->add('otherProviderName', null, array(
                    'label' => 'form.patient.other.provider.name',
                    'required' => false,
                ))
                ->add('otherProviderPhone', null, array(
                    'label' => 'form.patient.other.provider.phone',
                    'required' => false,
                    'attr' => ['class' => 'mask-phone'],
                ))
                ->add('otherProviderAddress', null, array(
                    'label' => 'form.patient.other.provider.address',
                    'required' => false,
                ))
                ->add('otherProviderCity', null, array(
                    'label' => 'form.patient.other.provider.city',
                    'required' => false,
                ))
                ->add('spetialAccommodation', null, array(
                    'label' => 'form.patient.spetial.accommodation',
                    'required' => false,
                ))
                ->add('accommodationSpecification', null, array(
                    'label' => 'form.patient.accommodationSpecification',
                    'required' => false,
                ))
                /*->add('caseManager', 'sonata_type_model', array(
                    'placeholder' => 'placeholder.select',
                    'label' => 'form.patient.case.manager',
                ))*/
                ->add('caseManagerCredentials', null, array(
                    'label' => 'form.patient.case.manager.credentials',
                    'required' => false,
                ))
                ->add('caseManagerNotes', null, array(
                    'label' => 'form.patient.case.manager.notes',
                    'required' => false,
                ))
                ->add('mediaFile', MyFileType::class, array(
                    'label' => 'block.user.avatar',
                    'required' => false,
                ))
            ->end();

        //$this->addFieldsetForm('intakeDate', 'init', 'fieldset.intake.data');
        //$this->addFieldsetForm('patientStatus', 'end');

        $this->addFieldsetForm('name', 'init', 'fieldset.client.information');
        $this->addFieldsetForm('guardianCity', 'end');

        $this->addFieldsetForm('referredBy', 'init', 'fieldset.referral.information');
        $this->addFieldsetForm('referralReasons', 'end');

        $this->addFieldsetForm('school', 'init', 'fieldset.school.information');
        $this->addFieldsetForm('teacherPhone', 'end');

        $this->addFieldsetForm('countryBirth', 'init', 'fieldset.imigration.status');
        $this->addFieldsetForm('statusExplination', 'end');

        $this->addFieldsetForm('primaryContactName', 'init', 'fieldset.emergency.information');
        $this->addFieldsetForm('contactPermit', 'end');

        $this->addFieldsetForm('psychiatristName', 'init', 'fieldset.providers.information');
        $this->addFieldsetForm('accommodationSpecification', 'end');
        
        $this->addFieldsetForm('medicare', 'init', 'fieldset.insurance.information');
        $this->addFieldsetForm('medicaid', 'end');
        
        $this->addFieldsetForm('caseManagerCredentials', 'init', 'fieldset.case.manager.information');
        $this->addFieldsetForm('caseManagerNotes', 'end');

        $this->addFieldsetForm('mediaFile', 'init', 'Picture');
        //$this->addFieldsetForm('mediaFile', 'end');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if($this->request->isXmlHttpRequest()){
            $showMapper
                ->add('caseManager', null, array(
                    'label' => 'Actual Case Manager',
                ))
                ->add('age', null, array(
                    'label' => 'form.patient.age',
                ))
                ->add('gender', null, array(
                    'label' => 'form.patient.gender',
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig',
                ))
                ->add('homePhone', null, array(
                    'label' => 'form.patient.home.phone',
                ))
                ->add('cellPhone', null, array(
                    'label' => 'form.patient.cellphone',
                ))
                ->add('primaryContactName', null, array(
                    'label' => 'form.patient.primary.contact.name',
                ))
                ->add('countryBirth', null, array(
                    'label' => 'form.patient.country.birth',
                    'template' => '@BackEnd/structures/show_field_country.html.twig',
                ))
                ->add('status', null, array(
                    'label' => 'form.patient.residential.status',
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig',
                ))
                ->add('referralReasons', null, array(
                    'label' => 'form.patient.referral.reasons',
                    'template' => '@BackEnd/structures/show_field_nom_many_to_many.html.twig',

                ))
            ;
            return;
        }
       
    }

    /**
     * @param Patient $object
     */
    public function postPersist($object)
    {
        $this->generateInsuranceEligibility($object);
        
        $url = $this->getConfigurationPool()->getContainer()->get('router')->generate('show_patient', ['id' => $object->getId()]);
        $this->container()->get(NotificationManager::class)->createNotification([User::ROLE_SUPER_ADMIN, User::ROLE_OPERATION_MANAGER], 'notification.add.new.patient', $url, Notification::SUCCESS);
    }

    /**
     * @param Patient $object
     */
    public function postUpdate($object)
    {
        $this->generateInsuranceEligibility($object);
        
        //Update patient if open update intake status
        $openRegister = $object->getOpenRegistry();
        if(!is_null($openRegister)){
            if($object->hasTcm())
                $openRegister->setOpenByTcm(true);

            if($object->hasCmh())
                $openRegister->setOpenByCmh(true);
            $this->save($openRegister);
        }
    }

    /**
     * @param Patient $object
     */
    public function generateInsuranceEligibility($object){
        $doctrine = $this->container()->get('doctrine');
        $save = false;

        //Create news entry's of insurance eligibility
        if(!is_null($object->getMedicare())){
            $ieMedicareLast = $doctrine->getRepository(InsuranceEligibility::class)->getLastByPatient($object, NomUtil::MEDICARE);
            if(is_null($ieMedicareLast) || !$ieMedicareLast->compareWithInsurance($object->getMedicare())) {
                $ie = new InsuranceEligibility();
                $ie->setPatient($object);
                $ie->setResponsible($this->getRealUser());
                $ie->setNumber($object->getMedicare()->getNumber());
                $ie->setHmo($object->getMedicare()->getHmo());
                $ie->setType(NomUtil::MEDICARE);
                $this->save($ie);
                $save = true;
            }
        }

        if(!is_null($object->getMedicaid())){
            $ieMedicaidLast = $doctrine->getRepository(InsuranceEligibility::class)->getLastByPatient($object, NomUtil::MEDICAID);

            if(is_null($ieMedicaidLast) || !$ieMedicaidLast->compareWithInsurance($object->getMedicaid())) {
                $ie = new InsuranceEligibility();
                $ie->setPatient($object);
                $ie->setResponsible($this->getRealUser());
                $ie->setNumber($object->getMedicaid()->getNumber());
                $ie->setHmo($object->getMedicaid()->getHmo());
                $ie->setType(NomUtil::MEDICAID);
                $this->save($ie);
                $save = true;
            }
        }

        if($save) {
            //Todo: Update roles notification
            $url = $this->getConfigurationPool()->getContainer()->get('router')->generate('show_patient', ['id' => $object->getId(), 'tab' => 'eligibilityPatient']);
            $this->container()->get(NotificationManager::class)->createNotification(User::ROLE_SUPER_ADMIN, 'Create new eligibility entry on patient ' . $object->getFullName(), $url, Notification::SUCCESS);
        }
    }

    public function toString($object)
    {
        if($object instanceof Patient && $object->getFullName() != null)
            return $object->getFullName();
        return $this->trans('menu.entity.patient',array(),'BackEndBundle');
    }
}
