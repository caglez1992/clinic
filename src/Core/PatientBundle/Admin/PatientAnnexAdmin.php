<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Form\Type\MyFileDragDropType;
use Core\PatientBundle\Entity\PatientAnnex;
use Core\PatientBundle\Entity\PdfForm;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PatientAnnexAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('settingCode', null, array(
                'format' => 'Setting Code'
            ))
            ->add('formDate', null, array(
                'format' => 'M d, Y'
            ))
            ->add('createdBy', null, array(
            ))
            ->add('createdAt', null, array(
                'label' => 'general.created.at',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'download' => array(
                        'template' => '@BackEnd/btn_actions/service_pdf_form/download_pdf.html.twig'
                    ),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('formDate', 'sonata_type_date_picker', array(
                'label' => 'Form Date',
                'format' => 'MM/dd/YYYY',
            ))
            ->add('settingCode', null, array(
                'label' => 'Setting Code',
            ))
            ->add('formFile', MyFileDragDropType::class, array(
                'label' => 'general.pdf.file',
            ))
        ;
    }

    public function prePersist($object)
    {
        $object->setCreatedBy($this->getRealUser());
    }

    public function toString($object)
    {
        if($object instanceof PatientAnnex && $object->getName() != null)
            return $object->getName();
        return 'Annex';
    }
}
