<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Repository\UserRepository;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\RegistryPatient;
use Core\PatientBundle\Entity\Task;
use Core\PatientBundle\Repository\PatientRepository;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\NotBlank;

class TaskAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    /**
     * @return boolean
     */
    public function isCreateRoute()
    {
        return true;
    }

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);
        $root = $query->getRootAliases()[0];

        if($userReal->isServiceWorker()){
            $query->andWhere("$root.worker = :userReal");
            $query->setParameter('userReal', $userReal);
        }

        if($userReal->isSupervisor()){
            $query->andWhere("$root.worker IN (:assignedWorkers)");
            $query->setParameter('assignedWorkers', $userReal->getAssignedWorkers());
        }

        return $query;
    }
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, [
                'label' => 'Task Code',
            ])
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'form.registry.patient.entry.date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('closeAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'Close Date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('patient', null, [], 'entity', [
                'query_builder' => function(PatientRepository $pr){
                    return $pr->getOpenPatients(true);
                },
            ])
            ->add('serviceParent', null, [
                'label' => 'Service'
            ])
            ->add('status', null, [], 'entity', [
                //'class' => 'Core\CoreBundle\Entity\NomNomenclator',
                //'property' => 'title',
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' =>function(EntityRepository $er){
                    return  $this->getQueryBuilderNomenclators($er, NomUtil::NOM_STATUS);
                },
            ])
        ;

        if(!$this->getRealUser()->isServiceWorker()){
            $datagridMapper->add('worker', null, ['label' => 'Assigned To'], 'entity', [
                'query_builder' => function(UserRepository $repo){
                    return $repo->getWorkersWithTask(true);
                },
            ]);
        }
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, [
                'label' => 'Code'
            ])
            ->add('createdAt', null, array(
                'label' => 'general.created.at',
                'format' => 'M d, Y',
            ))
            ->add('closeAt', null, array(
                'label' => 'Closed At',
                'format' => 'M d, Y',
            ))
            ->add('createdBy', null, array(
                'label' => 'Created By',
                'template' => '@BackEnd/structures/list_field_many_to_one.html.twig',
            ))
            ->add('worker', null, array(
                'label' => 'To Worker',
                'template' => '@BackEnd/structures/list_field_many_to_one.html.twig',
            ))
            ->add('patient', null, array(
                'label' => 'Patient',
                'template' => '@BackEnd/structures/list_field_patient.html.twig'
            ))
            /*->add('action', null, array(
                'label' => 'Task',
                'template' => '@BackEnd/structures/list_field_nom.html.twig'
            ))*/
            ->add('serviceParent', null, array(
                'label' => 'Service',
                'template' => '@BackEnd/structures/list_field_many_to_one.html.twig',
            ))
            ->add('status', null, array(
                'label' => 'Status',
                'template' => '@BackEnd/structures/list_field_nom.html.twig'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'closeTask' => ['template' => '@BackEnd/btn_actions/patient/close_task.html.twig'],
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('patient', 'sonata_type_model', array(
                'label' => 'Patient',
                'btn_add' => false,
                'required' => true,
                'property' => 'fullToString',
                'query' => $this->getPatientsActive(),
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('worker', 'sonata_type_model', array(
                'label' => 'Employer',
                'btn_add' => false,
                'required' => true,
                'placeholder' => 'Select a worker',
                'query' => $this->getQueryUserByRoles([User::ROLE_SPECIALIST, User::ROLE_TCM, User::ROLE_CMH]),
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('serviceParent', 'sonata_type_model', array(
                'label' => 'Service',
                'btn_add' => false,
                'placeholder' => 'Select a service',
                'help' => '<i class="fa fa-warning"></i> <strong class="color-yellow">Group Service like H2017 or H2019/HQ will not show here</strong>',
                'constraints' => [
                    new NotBlank(),
                ]
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General', ['class' => 'col-xs-6'])
                ->add('serviceParent', null, array(
                    'label' => 'Service',
                    //'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig'
                ))
                ->add('createdBy', null, array(
                    'label' => 'Created By',
                ))
                ->add('worker', null, array(
                    'label' => 'To Worker',
                ))
                ->add('patient', null, array(
                    'label' => 'Patient',
                    'template' => '@BackEnd/structures/show_field_patient.html.twig'
                ))
            ->end()

            ->with('Status', ['class' => 'col-xs-6'])
                ->add('id', null, array(
                    'label' => 'Task Code',
                ))
                ->add('status', null, array(
                    'label' => 'Status',
                    'template' => '@BackEnd/structures/show_field_nom_many_to_one.html.twig'
                ))
                ->add('createdAt', null, array(
                    'label' => 'general.created.at',
                    'format' => 'M d, Y - h:i a',
                ))
                ->add('closeAt', null, array(
                    'label' => 'Close At',
                    'format' => 'M d, Y - h:i a',
                ))
            ->end()
        ;



    }

    /**
     * @param Task $object
     * @return string
     */
    public function toString($object)
    {
        if(!is_null($object)){
            return sprintf('Task #%s', $object->getId());
        }
        return 'Task';
    }

    /**
     * @param Task $object
     */
    public function prePersist($object)
    {
        $object->setCreatedBy($this->getRealUser());
    }

    /**
     * @param Task $object
     */
    public function postPersist($object)
    {
        $worker = $object->getWorker();
        $worker->addPatient($object->getPatient());
        $this->save($worker);

        $url = $this->container()->get('sonata.admin.pool')->getAdminByAdminCode('patient.bundle.admin.task')->generateUrl('list');
        $this->container()->get(NotificationManager::class)->createNotification($object->getWorker(), sprintf('The task with code %s has been assigned to you.', $object->getId()), $url, 'success', true);
    }

    /**
     * @param Task $object
     */
    public function postUpdate($object)
    {
        $this->postPersist($object);
    }
}
