<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Insurance;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Core\PatientBundle\Entity\Medicare;
use Core\PatientBundle\Entity\Patient;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Validator\Constraints\NotBlank;

class MedicareAdmin extends InsuranceAdmin
{

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Add new insurance')
                ->add('hmo', 'sonata_type_model', array(
                    'label' => 'HMO',
                    'query' => $this->getHmoByType(NomUtil::MEDICARE),
                    'btn_add' => false,
                    'required' => true,
                    'choice_translation_domain'=>'BackEndBundle',
                ))
                ->add('number', null, array(
                    'label' => 'general.number',
                ))
                ->add('alternateId', null, array(
                    'label' => 'Alternate ID',
                    'required' => false,
                ))
            ->end()
        ;
    }

    /**
     * @param Medicare $object
     * @return string
     */
    public function toString($object)
    {
        if($object instanceof Medicare && $object->getNumber() != null)
            return $object->getNumber() . ' [' . $object->getHmo()->__toString() . ']';
        return $this->trans('menu.entity.insurance',array(),'BackEndBundle');
    }
}
