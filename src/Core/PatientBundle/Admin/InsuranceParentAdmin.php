<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class InsuranceParentAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'form.insuranceparent.id',
            ))
            ->add('name', null, array(
                'label' => 'form.insuranceparent.name',
            ))
            ->add('contract', null, array(
                'label' => 'form.insuranceparent.contract',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'form.insuranceparent.id',
            ))
            ->add('name', null, array(
                'label' => 'form.insuranceparent.name',
            ))
            ->add('contract', null, array(
                'label' => 'form.insuranceparent.contract',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, array(
                'label' => 'form.insuranceparent.id',
            ))
            ->add('name', null, array(
                'label' => 'form.insuranceparent.name',
            ))
            ->add('contract', null, array(
                'label' => 'form.insuranceparent.contract',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.insuranceparent.id',
            ))
            ->add('name', null, array(
                'label' => 'form.insuranceparent.name',
            ))
            ->add('contract', null, array(
                'label' => 'form.insuranceparent.contract',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof InsuranceParentAdmin && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.insuranceparentadmin',array(),'BackEndBundle');
    }
}
