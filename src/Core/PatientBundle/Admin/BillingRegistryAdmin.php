<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\PatientBundle\Entity\BillingRegistry;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class BillingRegistryAdmin extends BaseAdmin
{
    protected $createRoute = false;

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'Record',
            ))
            ->add('hmo')
            ->add('unitsConsumed', null, array(
                'label' => 'Units',
            ))
            ->add('amount', null, array(
                'label' => 'Amount ($)',
            ))
            ->add('authorization')
            ->add('note')
            ->add('service')
            ->add('createdBy')
            ->add('payed')
            ->add('createdAt', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array(
                'label' => 'general.id',
            ))
            ->add('patient', null, array(
                
            ))
            ->add('hmo', null, array(
                'label' => 'HMO',
            ))
            ->add('unitsConsumed', null, array(
                'label' => 'Units',
            ))
            ->add('amount', null, array(
                'label' => 'Amount ($)',
            ))
            ->add('authorization')
            ->add('note')
            ->add('service')
            ->add('createdBy')
            ->add('createdAt', null, [
                'format' => 'M d, Y'
            ])
            ->add('payed')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    //'edit' => array(),
                    //'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$formMapper
            ->add('id', null, array(
                'label' => 'form.billingregistry.id',
            ))
            ->add('unitsConsumed', null, array(
                'label' => 'form.billingregistry.unitsConsumed',
            ))
            ->add('amount', null, array(
                'label' => 'form.billingregistry.amount',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.billingregistry.createdAt',
            ))
        ;*/
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'Record',
            ))
            ->add('hmo')
            ->add('unitsConsumed', null, array(
                'label' => 'Units',
            ))
            ->add('amount', null, array(
                'label' => 'Amount ($)',
            ))
            ->add('authorization')
            ->add('note')
            ->add('service')
            ->add('createdBy')
            ->add('payed')
            ->add('createdAt')
        ;
    }

    public function toString($object)
    {
        if(!is_null($object) && $object instanceof BillingRegistry)
            return sprintf('Billing Registry #%s', $object->getId());
        return 'Billing Registry';
    }
}
