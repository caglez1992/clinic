<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Form\Type\MyFileDragDropType;
use Core\PatientBundle\Entity\PdfForm;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PdfFormAdmin extends BaseAdmin
{

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('formatSize', null, array(
                'label' => 'size',
            ))
            ->add('createdAt', null, array(
                'label' => 'general.created.at',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'download' => array(
                        'template' => '@BackEnd/btn_actions/service_pdf_form/download_pdf.html.twig'
                    ),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('formFile', MyFileDragDropType::class, array(
                'label' => 'general.pdf.file',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof PdfForm && $object->getName() != null)
            return $object->getName();
        return $this->trans('menu.entity.pdf.form',array(),'BackEndBundle');
    }
}
