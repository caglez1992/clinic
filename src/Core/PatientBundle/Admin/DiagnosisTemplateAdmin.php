<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\PatientBundle\Entity\DiagnosisTemplate;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DiagnosisTemplateAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('primaryCode', null, array(
                'label' => 'Primary Code',
            ))
            ->add('secondaryCode', null, array(
                'label' => 'Secondary Code',
            ))
            ->add('description', null, array(
                'label' => 'general.description',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('primaryCode', null, array(
                'label' => 'Primary Code',
            ))
            ->add('secondaryCode', null, array(
                'label' => 'Secondary Code',
            ))
            ->add('description', null, array(
                'label' => 'general.description',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('primaryCode', null, array(
                'label' => 'Primary Code',
            ))
            ->add('secondaryCode', null, array(
                'label' => 'Secondary Code',
            ))
            ->add('description', null, array(
                'label' => 'general.description',
            ))
        ;
    }


    public function toString($object)
    {
        if($object instanceof DiagnosisTemplate && $object->__toString() != null)
            return $object->__toString();
        return $this->trans('menu.entity.diagnosis.template',array(),'BackEndBundle');
    }
}
