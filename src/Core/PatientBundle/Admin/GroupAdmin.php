<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Repository\UserRepository;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Group;
use Core\PatientBundle\Entity\GroupPatientRegistry;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\ServiceParent;
use Core\PatientBundle\Repository\ServiceParentRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

class GroupAdmin extends BaseAdmin
{

    public function createQuery($context = 'list')
    {
        /** @var User $userReal */
        $userReal = $this->getRealUser();
        $query = parent::createQuery($context);

        if($userReal->hasRole(User::ROLE_CMH)){
            $root = $query->getRootAliases()[0];
            $query->join("$root.therapists", 't');
            $query->andWhere("t.id = :userRealId");
            $query->setParameter('userRealId', $userReal->getId());
        }
        return $query;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'Group Code',
            ))
            ->add('name', null, array(
                'label' => 'Group Name',
            ))
            ->add('serviceParent', null, [], 'entity', [
                'label' => 'Service',
                'query_builder' =>function(ServiceParentRepository $repo){
                    return  $repo->getServicesGroup(true);
                },
            ])
            ->add('therapists', null, [], 'entity', [
                'label' => 'Therapist',
                'query_builder' =>function(UserRepository $repo){
                    return  $repo->getUsersByRole(User::ROLE_CMH, true);
                },
            ])
            ->add('status', null, [], 'entity', [
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' =>function(EntityRepository $er){
                    return  $this->getQueryBuilderNomenclators($er, NomUtil::NOM_STATUS);
                },
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            /*->add('id', null, array(
                'label' => 'Group Code',
            ))*/
            ->add('name', null, array(
                'label' => 'Group Name',
            ))
            ->add('serviceParent', null, array(
                'label' => 'Service',
            ))
            ->add('therapists', null, array(
                'label' => 'Therapist(s)',
                'template' => '@BackEnd/structures/list_field_ul_many_to_many.html.twig',
            ))
            ->add('status', null, array(
                'label' => 'Status',
                'template' => '@BackEnd/structures/list_field_nom.html.twig'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'members' => array(
                        'template' => '@BackEnd/btn_actions/group/members.html.twig'
                    ),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'Group Name',
            ))
            ->add('serviceParent', 'sonata_type_model', array(
                'label' => 'Service that is a group therapy',
                'query' => $this->getGroupServices(),
                'btn_add' => false,
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('therapists', 'sonata_type_model', array(
                'label' => 'Therapist(s)',
                'query' => $this->getQueryUserByRoles([User::ROLE_CMH]),
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'constraints' => [
                    new Count(['min' => 1]),
                ]
            ))
            ->add('patients', 'sonata_type_model', array(
                'label' => 'Members',
                'query' => $this->getPatientsActive(),
                'property' => 'fullToString',
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ))
            ->add('status', 'sonata_type_model', array(
                'query' => $this->getQueryNomenclators(NomUtil::NOM_STATUS),
                'btn_add' => false,
                'choice_translation_domain'=>'BackEndBundle',
            ))
        ;
    }

    /**
     * @param Group $object
     */
    public function postPersist($object)
    {
        $d = $this->container()->get('doctrine');
        $registryOfGroup = $d->getRepository(GroupPatientRegistry::class)->findBy([
            'group' => $object,
        ]);

        //Check patients that leave group
        /** @var GroupPatientRegistry $registry */
        foreach ($registryOfGroup as $registry){
            if($registry->getActive() && !$object->getPatients()->contains($registry->getPatient())){
                $registry->setActive(false);
                $registry->setOutDate(new DateTime());
                $this->save($registry);
            }
        }

        //Check new patients
        /** @var Patient $patient */
        foreach ($object->getPatients() as $patient){
            $registryOfPatient = $d->getRepository(GroupPatientRegistry::class)->findBy([
                'group' => $object,
                'patient' => $patient,
            ], ['inDate' => 'DESC']);

            $lastRegistry = count($registryOfPatient) == 0 ? null : $registryOfPatient[0];
            if(is_null($lastRegistry) || !$lastRegistry->getActive()){
                $r = new GroupPatientRegistry();
                $r->setPatient($patient);
                $r->setGroup($object);
                $r->setInDate(new DateTime());
                $r->setActive(true);
                $this->save($r);

                //Add this patient that enter new to the group to the therapists
                $therapists = $object->getTherapists();
                foreach ($therapists as $worker){
                    if(!$worker->getPatients()->contains($patient)) {
                        $worker->addPatient($patient);
                        $this->save($worker);
                    }
                    $this->container()->get(NotificationManager::class)->createNotification($worker, sprintf('Patient %s has been add to the group %s', $patient->getFullName(), $object->__toString()), $url = null, Notification::SUCCESS, $important = false);
                }
            }
        }
    }

    /**
     * @param Group $object
     */
    public function postUpdate($object)
    {
        $this->postPersist($object);
    }


    /**
     * @return \Doctrine\ORM\Query
     */
    public function getGroupServices(){
        $sql = "SELECT sp FROM PatientBundle:ServiceParent sp WHERE sp.group = true";

        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(ServiceParent::class);
        return $em->createQuery($sql);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.group.id',
            ))
            ->add('name', null, array(
                'label' => 'form.group.name',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof Group && $object->getName() != null)
            return $object->getName();
        return 'Group';
    }
}
