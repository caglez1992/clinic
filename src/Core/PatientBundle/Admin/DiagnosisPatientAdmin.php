<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Repository\UserRepository;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\DiagnosisPatient;
use Core\PatientBundle\Entity\ServiceForm;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\NotBlank;

class DiagnosisPatientAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('diagnosisDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'form.diagnosis.patient.diagnosis.date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('diagnosisTemplate', null, array(
                'label' => 'diagnosis.patient',
            ))
            ->add('ip', null, array(
                'label' => 'BRIEF',
            ))
            ->add('bio', null, array(
                'label' => 'BIO',
            ))
            ->add('pcp', null, array(
                'label' => 'PSY EVAL',
            ))
            ->add('createdBy', null, ['label' => 'Created By'], 'entity', [
                'query_builder' => function(UserRepository $repo){
                    return $repo->getWorkersWithDiagnosis(true);
                },
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('diagnosisDate', null, array(
                'label' => 'form.diagnosis.patient.diagnosis.date',
                'format' => 'M d, Y',
            ))
            ->add('diagnosisTemplate', null, array(
                'label' => 'diagnosis.patient',
            ))
            ->add('ip', null, array(
                'label' => 'BRIEF',
                'editable' => true,
            ))
            ->add('bio', null, array(
                'label' => 'BIO',
                'editable' => true,
            ))
            ->add('pcp', null, array(
                'label' => 'PSY EVAL',
                'editable' => true,
            ))
            ->add('createdBy', null, array(
                'label' => 'Created By',
            ))
            ->add('createdAt', null, array(
                'label' => 'general.created.at',
            ))
            ->add('forms', null, array(
                'label' => 'Evaluation Forms',
                'template' => '@BackEnd/structures/list_field_diagnosis_form.html.twig'
            ))
            ->add('currentDiagnosis', null, array(
                'label' => 'Current Diagnosis',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'set_current' => ['template' => '@BackEnd/btn_actions/diagnosis_patient/set_current.html.twig']
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('diagnosisDate', 'sonata_type_date_picker', array(
                'label' => 'form.diagnosis.patient.diagnosis.date',
                'format' => 'MM/dd/yyyy',
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('diagnosisTemplate', null, array(
                'label' => 'diagnosis.patient',
                'constraints' => [
                    new NotBlank(),
                ]
            ))
            ->add('ip', null, array(
                'label' => 'BRIEF',
            ))
            ->add('bio', null, array(
                'label' => 'BIO',
            ))
            ->add('pcp', null, array(
                'label' => 'PSY EVAL',
            ))
            ->add('forms', 'sonata_type_model', array(
                'label' => 'Evaluation Forms',
                'query' => $this->getQueryForm(),
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ))
        ;
    }

    public function getQueryForm(){
        $sql = "SELECT sf FROM PatientBundle:ServiceForm sf JOIN sf.service s JOIN s.serviceParent sp ";
        if(!is_null($this->getParent())){
            $sql .= "WHERE sf.filled = true AND s.patient = :patient AND sp.type = :serviceType AND s.worker = :worker";
        }

        /** @var EntityManagerInterface $em */
        $em = $this->getModelManager()->getEntityManager(ServiceForm::class);
        return $em->createQuery($sql)
            ->setParameter('patient', $this->getParent()->getSubject())
            ->setParameter('serviceType', NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_EVALUATION))
            ->setParameter('worker', $this->getRealUser());

    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.diagnosispatient.id',
            ))
            ->add('diagnosisDate', null, array(
                'label' => 'form.diagnosispatient.diagnosisDate',
            ))
            ->add('diagnosis', null, array(
                'label' => 'form.diagnosispatient.diagnosis',
            ))
            ->add('ip', null, array(
                'label' => 'form.diagnosispatient.ip',
            ))
            ->add('bio', null, array(
                'label' => 'form.diagnosispatient.bio',
            ))
            ->add('pcp', null, array(
                'label' => 'form.diagnosispatient.pcp',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.diagnosispatient.createdAt',
            ))
        ;
    }

    /**
     * @param DiagnosisPatient $object
     */
    public function prePersist($object)
    {
        $object->setCreatedBy($this->getRealUser());
    }

    /**
     * @param DiagnosisPatient $object
     */
    public function postPersist($object)
    {
        $msg = sprintf('New diagnosis created by %s on patient %s', $this->getRealUser(), $object->getPatient()->getFullName());
        $url = $this->container()->get('router')->generate('show_patient', ['id' => $object->getPatient()->getId(), 'tab' => 'diagnosis']);
        $this->container()->get(NotificationManager::class)->createNotification([User::ROLE_OPERATION_MANAGER], $msg, $url, Notification::SUCCESS, $important = true, $dataMsg = array(), $pusher = true);
    }

    public function toString($object)
    {
        if($object instanceof DiagnosisPatient && $object->getDiagnosisTemplate() != null)
            return $object->getDiagnosisTemplate()->__toString();
        return $this->trans('diagnosis.patient',array(),'BackEndBundle');
    }
}
