<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Hmo;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class HmoAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*$datagridMapper
            ->add('id', null, array(
                'label' => 'general.code',
            ))
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('hmo', null, array(
                'label' => 'HMO',
            ))
        ;*/
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('medicare', null, array(
                'label' => 'Medicare',
            ))
            ->add('medicaid', null, array(
                'label' => 'Medicaid',
            ))
            ->add('handler', null, array(
                'label' => 'Handler',
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'general.name',
            ))
            ->add('medicare', null, array(
                'label' => 'Medicare',
            ))
            ->add('medicaid', null, array(
                'label' => 'Medicaid',
            ))
            ->add('handler', 'sonata_type_model', array(
                'label' => 'general.insurance.handler',
                'required' => false,
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof Hmo && $object->getName() != null)
            return $object->getName();
        return $this->trans('HMO',array(),'BackEndBundle');
    }
}
