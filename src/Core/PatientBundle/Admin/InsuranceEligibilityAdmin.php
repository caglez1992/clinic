<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\PatientBundle\Entity\InsuranceEligibility;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;

class InsuranceEligibilityAdmin extends BaseAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'runDate',
    );

    protected $parentAssociationMapping = 'patient';

    protected $createRoute = false;
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('runDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'label' => 'form.insurance.eligibility.run.date',
                'field_type' => 'sonata_type_date_picker',
            ))
            ->add('insuranceTemplate', null, array(
                'label' => 'general.insurance',
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('number', null, array(
                'label' => 'Insurance Number',
            ))
            ->add('insuranceTemplate', null, array(
                'label' => 'general.insurance',
            ))
            ->add('insuranceTemplate.type', null, array(
                'label' => 'Type',
                'template' => '@BackEnd/structures/list_field_nom.html.twig',
            ))
            ->add('primary', null, array(
                'label' => 'Is primary',
            ))
            ->add('runDate', null, array(
                'label' => 'form.insurance.eligibility.run.date',
                'format' => 'M/d/Y'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    //'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('insuranceTemplate', 'sonata_type_model', array(
                'label' => 'general.insurance',
            ))
            ->add('runDate', 'sonata_type_date_picker', array(
                'label' => 'form.insurance.eligibility.run.date',
                'format' => 'MM/dd/yyyy',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.insuranceeligibility.id',
            ))
            ->add('runDate', null, array(
                'label' => 'form.insuranceeligibility.runDate',
            ))
            ->add('createdAt', null, array(
                'label' => 'form.insuranceeligibility.createdAt',
            ))
        ;
    }

    public function toString($object)
    {
        if($object instanceof InsuranceEligibility && $object->getInsuranceTemplate()->getName() != null)
            return $object->getInsuranceTemplate()->getName();
        return $this->trans('menu.entity.insurance.eligibility',array(),'BackEndBundle');
    }
}
