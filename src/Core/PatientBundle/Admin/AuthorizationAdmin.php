<?php

namespace Core\PatientBundle\Admin;

use Core\CoreBundle\Admin\BaseAdmin;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Form\Type\MyFileType;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\Hmo;
use Core\PatientBundle\Entity\ServiceCode;
use Core\PatientBundle\Manager\PatientManager;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Validator\Constraints\NotBlank;

class AuthorizationAdmin extends BaseAdmin
{
    protected $parentAssociationMapping = 'patient';

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'beginDate',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array(
                'label' => 'Unique Code',
            ))
            ->add('patient', null, array(
                'label' => 'Patient',
            ))
            ->add('patientCode', CallbackFilter::class, array(
                'callback' => array($this, 'filterPatientCode'),
                'label' => 'Patient Code',
            ))
            ->add('hmo', null, array(
                'label' => 'HMO',
            ))
            ->add('number', null, array(
                'label' => 'Authorization Number',
            ))
            ->add('beginDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'Begin Date',
            ))
            ->add('endDate', CallbackFilter::class, array(
                'callback' => array($this, 'filterDate'),
                'field_type' => 'sonata_type_date_picker',
                'label' => 'End Date',
            ))
            ->add('unitsApproved', null, [])
            ->add('unitsConsumed', null, [])
            ->add('RU', CallbackFilter::class, array(
                'callback' => array($this, 'filterRemainedUnits'),
                'label' => 'Remained Units (RU)',
            ))
            ->add('serviceCodes', null, [])
        ;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $alias
     * @param string $field
     * @param array $value
     * @return bool|void
     */
    public function filterPatientCode($queryBuilder, $alias, $field, $value)
    {
        $value = $value['value'];
        if (!$value)
            return;

        $queryBuilder->join("$alias.patient", 'p')
            ->andWhere("p.caseNumber = :caseNumber")
            ->setParameter('caseNumber', $value);
        return true;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $alias
     * @param string $field
     * @param array $value
     * @return bool|void
     */
    public function filterRemainedUnits($queryBuilder, $alias, $field, $value)
    {
        $value = $value['value'];
        if ($value === '')
            return;

        $queryBuilder
            ->andWhere("$alias.unitsApproved - $alias.unitsConsumed = :ru")
            ->setParameter('ru', $value);
        return true;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $userReal = $this->getRealUser();

        $listMapper->add('patient.caseNumber', null, [
            'label' => 'Case Number'
        ]);
        if(is_null($this->getParent())){
            $listMapper->add('patient', null, [
                'associated_property' => 'fullname',
                'label' => 'Patient',
                'template' => '@BackEnd/structures/list_field_patient.html.twig'
            ]);
        }
        $listMapper
            ->add('hmo', null, array(
                'label' => 'Insurance',
                'template' => '@BackEnd/structures/list_insurance_patient.html.twig',
            ))
            ->add('number', null, array(
                'label' => 'Authorization Number',
            ))
            ->add('beginDate', null, array(
                'label' => 'Start Date',
                'format' => 'M d, Y'
            ))
            ->add('endDate', null, array(
                'label' => 'End Date',
                'format' => 'M d, Y'
            ))
            ->add('unitsApproved', null, array(
                'label' => 'Approved Units',
            ))
            ->add('unitsConsumed', null, array(
                'label' => 'Consumed Units',
            ))
            ->add('remainedUnits', null, array(
                'label' => 'RU',
            ))
            ->add('serviceCodes', null, array(
                'label' => 'Approved Services',
                'template' => '@BackEnd/structures/list_field_ul_many_to_many.html.twig',
            ))
            ->add('active');

            if(!$userReal->hasRole(User::ROLE_BILLING)) {
                $listMapper->add('_action', null, array(
                    'actions' => array(
                        //'show' => array(),
                        'show_billing_consume' => ['template' => '@BackEnd/btn_actions/authorization/show_billing_consume.html.twig'],
                        'edit' => array(),
                        'delete' => array(),
                    )
                ));
            }
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if(is_null($this->getParent())){
            $formMapper->add('patient', 'sonata_type_model', [
                'label' => 'Patient',
                'required' => true,
                'constraints' => [new NotBlank()],
                'query' => $this->getPatientsActive(),
                'btn_add' => false,
            ]);
        }
        $formMapper
            ->add('hmo', 'sonata_type_model', array(
                'label' => 'Insurance',
                'required' => true,
                'constraints' => [new NotBlank()],
                'btn_add' => false,
                'query' => $this->queryHmo(),
            ))
            ->add('number', null, array(
                'label' => 'Authorization Number',
            ))
            ->add('beginDate', 'sonata_type_date_picker', array(
                'label' => 'Start Date',
                'format' => 'MM/dd/yyyy',
            ))
            ->add('endDate', 'sonata_type_date_picker', array(
                'label' => 'End Date',
                'format' => 'MM/dd/yyyy',
            ))
            ->add('unitsApproved', null, array(
                'label' => 'Approved Units',
            ))
            ->add('serviceCodes', 'sonata_type_model', array(
                'label' => 'Approved Services',
                'placeholder' => 'placeholder.select',
                'btn_add' => false,
                'multiple' => true,
                'by_reference' => false,
                'query' => $this->queryServiceCodeIntervention(),
            ))
            ->add('observations', null, array(
                'label' => 'Observations',
            ))
            ->add('mediaFile', MyFileType::class, array(
                'label' => 'Document Attachment',
                'required' => false,
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array(
                'label' => 'form.authorization.id',
            ))
            ->add('units', null, array(
                'label' => 'form.authorization.units',
            ))
            ->add('beginDate', null, array(
                'label' => 'form.authorization.beginDate',
            ))
            ->add('expiredDate', null, array(
                'label' => 'form.authorization.expiredDate',
            ))
        ;
    }

    /**
     * @return mixed
     */
    public function queryHmo(){
        $em = $this->getModelManager()->getEntityManager(Hmo::class);
        $query = $em->createQuery('SELECT hmo FROM PatientBundle:Hmo hmo WHERE hmo.free = false');
        return $query;
    }

    /**
     * @return mixed
     */
    public function queryServiceCodeIntervention(){
        $nomServiceIntervention = NomUtil::getNomenclatorsByName(NomUtil::NOM_SERVICE_TYPE_INTERVENTION);
        $sql = "SELECT sc FROM PatientBundle:ServiceCode sc JOIN sc.services sp WHERE sp.type = :nom_intervention";

        $em = $this->getModelManager()->getEntityManager(ServiceCode::class);
        return $em->createQuery($sql)
            ->setParameter('nom_intervention', $nomServiceIntervention);
    }

    /**
     * @param Authorization $object
     */
    public function postPersist($object)
    {
        $this->container()->get(PatientManager::class)->updateAuthorizationStatus($object->getPatient());
        $patient = $object->getPatient();
        $patient->updateAmountAuth();
        $this->save($patient);
    }

    /**
     * @param Authorization $object
     */
    public function postUpdate($object)
    {
        $this->postPersist($object);
    }

    /**
     * @param Authorization $object
     */
    public function postRemove($object)
    {
        $patient = $object->getPatient();
        $patient->updateAmountAuth();
        $this->save($patient);
    }

    public function toString($object)
    {
        return $this->trans('menu.entity.authorization',array(),'BackEndBundle');
    }
}
