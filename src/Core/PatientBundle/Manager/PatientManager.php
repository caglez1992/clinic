<?php
namespace Core\PatientBundle\Manager;


use Core\CoreBundle\Manager\BaseManager;
use Core\PatientBundle\Entity\Authorization;
use Core\PatientBundle\Entity\GroupPatientRegistry;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\Task;
use DateTime;

class PatientManager extends BaseManager
{
    /**
     * @param Patient $patient
     * @return array
     */
    public function getRegistryOfPatient($patient){
        $d = $this->container->get('doctrine');
        $patientTasks = $d->getRepository(Task::class)->getTaskOfPatient($patient);
        $groupRegistry = $d->getRepository(GroupPatientRegistry::class)->findBy(['patient' => $patient]);
        $allRegistry = array_merge($patientTasks, $groupRegistry);
        //Order by date bubble algorithm;
        for($i = 0; $i < count($allRegistry); $i++) {
            for ($j = $i; $j < count($allRegistry); $j++) {
                $dateI = $allRegistry[$i] instanceof Task ? $allRegistry[$i]->getCreatedAt() : $allRegistry[$i]->getInDate();
                $dateJ = $allRegistry[$j] instanceof Task ? $allRegistry[$j]->getCreatedAt() : $allRegistry[$j]->getInDate();
                if ($dateI < $dateJ){
                    $tmp = $allRegistry[$i];
                    $allRegistry[$i] = $allRegistry[$j];
                    $allRegistry[$j] = $tmp;
                }
            }
        }
        return $allRegistry;
    }

    /**
     * @param Patient $patient
     * @return void
     */
    public function updateAuthorizationStatus($patient){
        $authorizations = $patient->getAuthorizations();
        $now = new DateTime();
        /** @var Authorization $auth */
        foreach ($authorizations as $auth){
            //Check Dates
            if($auth->getBeginDate()->getTimestamp() <= $now->getTimestamp() && $now->getTimestamp() <= $auth->getEndDate()->getTimestamp() ){
                $auth->setActive(true);
            } else {
                $auth->setActive(false);
                $this->save($auth);
                continue;
            }

            //Check Insurance Patient
            $authHmo = $auth->getHmo()->getId();
            $medicarePatient = $auth->getPatient()->getMedicare();
            $medicaidPatient = $auth->getPatient()->getMedicaid();

            if( (!is_null($medicarePatient) && $authHmo == $medicarePatient->getHmo()->getId()) || (!is_null($medicaidPatient) && $authHmo == $medicaidPatient->getHmo()->getId()) ){
                $auth->setActive(true);
            } else {
                $auth->setActive(false);
                $this->save($auth);
                continue;
            }

            //Check amount units
            if($auth->getRemainedUnits() > 0){
                $auth->setActive(true);
            } else {
                $auth->setActive(false);
                $this->save($auth);
                continue;
            }

            $this->save($auth);
        }
    }
}
