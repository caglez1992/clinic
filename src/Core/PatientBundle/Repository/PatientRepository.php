<?php

namespace Core\PatientBundle\Repository;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\Group;
use Core\PatientBundle\Entity\Patient;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * PatientRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PatientRepository extends EntityRepository
{
    /**
     * @param boolean $onlyQb
     * @return \Core\PatientBundle\Entity\Patient[]
     */
    public function getOpenPatients($onlyQb = false){
        return $this->getPatientsByStatus(NomUtil::NOM_STATUS_OPEN, $onlyQb);
    }

    /**
     * @return Patient[]
     */
    public function getClosePatients(){
        return $this->getPatientsByStatus(NomUtil::NOM_STATUS_CLOSE);
    }

    /**
     * @return Patient[]
     */
    public function getPendingPatients(){
        return $this->getPatientsByStatus(NomUtil::NOM_STATUS_PENDING);
    }

    /**
     * @return \Core\PatientBundle\Entity\Patient[]
     */
    public function getPendingForTasks(){
        $nomOpen = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN);

         $sql = "SELECT DISTINCT p.*, COUNT(t.id) as cant_task FROM patient p 
                LEFT JOIN task t ON p.id = t.patient_id
                GROUP BY p.id
                HAVING cant_task = 0 AND p.patient_status_id = ? AND p.open_date != null";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(Patient::class, 'p');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $nomOpen->getId());

        return $query->getResult();
    }

    /**
     * @param string $nom
     * @param bool $onlyQb
     * @return \Core\PatientBundle\Entity\Patient[]
     */
    public function getPatientsByStatus($nom, $onlyQb = false){
        $status = NomUtil::getNomenclatorsByName($nom);
        $queryBuilder = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.patientStatus = :status')
            ->setParameter('status', $status);

        if($onlyQb)
            return $queryBuilder;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param Group $group
     * @return Patient[]
     */
    public function getPatientByGroup($group){
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->join('p.groups', 'gp')
            ->where('gp.group = :group')
            ->setParameter('group', $group);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param integer $weekId
     * @param integer $workerId
     * @param null|string $speciality
     * @return \Core\PatientBundle\Entity\Patient[]
     */
    public function getPatientForBilling($weekId, $workerId, $speciality = null){
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->join('p.services', 's')
            ->join('s.week', 'w')
            ->where('w.id = :weekId')
            ->andWhere('s.revised = true')
            ->andWhere('s.approved = true')
            ->setParameter('weekId', $weekId);

        if(!is_null($speciality)){
            $qb->join('s.serviceParent', 'sp')
                ->join('sp.specialty', 'speciality')
                ->andWhere('speciality.name = :nom')
                ->setParameter('nom', $speciality);
        }

        if($workerId != -1){
            $qb->join('s.worker', 'worker')
                ->andWhere('worker.id = :workerId')
                ->setParameter('workerId', $workerId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $worker
     * @return Patient[]
     */
    public function getPatientHistory($worker){
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->distinct(true)
            ->join('p.services', 's')
            ->where('s.worker = :worker')
            ->setParameter('worker', $worker);
        return $qb->getQuery()->getResult();
    }

    /**
     * @return \Core\PatientBundle\Entity\Patient[]
     */
    public function getPatientWithNoActiveAuthorizations(){

        $sql = "SELECT DISTINCT p.*, COUNT(auth.id) as cant_auth FROM patient p 
                LEFT JOIN authorization auth ON p.id = auth.patient_id
                GROUP BY p.id
                HAVING cant_auth = 0";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(Patient::class, 'p');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        //$query->setParameter(1, $nomOpen->getId());

        return $query->getResult();
    }
    
    public function tempQuery(){
        $nom = NomUtil::getNomenclatorsByName(NomUtil::NOM_STATE_TYPE_FL);
        $qb = $this->createQueryBuilder('p')
            ->update()
            ->set('p.state', $nom->getId());
        
        $qb->getQuery()->execute();
    }
}
