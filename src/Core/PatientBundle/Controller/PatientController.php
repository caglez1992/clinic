<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 6/8/17
 * Time: 3:32 AM
 */

namespace Core\PatientBundle\Controller;

use Core\CoreBundle\Entity\Notification;
use Core\CoreBundle\Entity\Setting;
use Core\CoreBundle\Entity\User;
use Core\CoreBundle\Manager\BaseManager;
use Core\CoreBundle\Manager\NotificationManager;
use Core\CoreBundle\Manager\UserManager;
use Core\CoreBundle\Util\NomUtil;
use Core\CoreBundle\Util\StaticUtil;
use Core\PatientBundle\Entity\DiagnosisPatient;
use Core\PatientBundle\Entity\GroupPatientRegistry;
use Core\PatientBundle\Entity\Patient;
use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\RegistryPatient;
use Core\PatientBundle\Entity\Service;
use Core\PatientBundle\Entity\ServiceForm;
use Core\PatientBundle\Entity\ServiceParent;
use Core\PatientBundle\Entity\Task;
use Core\PatientBundle\Form\ServiceType;
use Core\PatientBundle\Manager\PatientManager;
use Core\WorkerBundle\Entity\ServiceAcl;
use Core\WorkerBundle\Entity\SettingCode;
use Core\WorkerBundle\Entity\Week;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PatientController extends Controller
{
    /**
     * @Route("/show/{id}", name="show_patient", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function showPatientAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);

        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();

        if($userReal->isServiceWorker() && !$userReal->getPatients()->contains($patient)){
            throw $this->createAccessDeniedException('Access Denied: This patient is not assigned to you.');
        }

        return $this->render('@BackEnd/custom/patient/show/show_patient.html.twig', array(
            'patient' => $patient,
        ));
    }

    /**
     * @Route("/ajax/show/tabs/patient/{id}", name="ajax_show_tabs_patient", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function ajaxShowTabsPatientAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);
        return $this->render('@BackEnd/custom/patient/show/ajax_tabs_patients.twig', array(
            'patient' => $patient,
            'serviceForms' => $d->getRepository(ServiceForm::class)->getServiceFormsByPatient($patient),
            'serviceAdmin' => $this->get('sonata.admin.pool')->getAdminByClass(Service::class),
        ));
    }

    /**
     * @Route("/ajax/show/info/patient/{id}", name="ajax_show_info_patient", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function ajaxShowInfoPatientAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);

        $allRegistry = $this->get(PatientManager::class)->getRegistryOfPatient($patient);

        $lastGroup = null;
        $lastTcm = null;
        $lastCmh = null;
        $lastSpecialist = null;
        foreach ($allRegistry as $registry){
            if(is_null($lastGroup) && $registry instanceof GroupPatientRegistry){
                $lastGroup = $registry->getGroup();
            }

            if(is_null($lastTcm) && $registry instanceof Task && $registry->getServiceParent()->getCode()->getCode() == 'T1017'){
                $lastTcm = $registry->getWorker();
            }

            if(is_null($lastCmh) && $registry instanceof Task && $registry->getServiceParent()->getCode()->getCode() != 'T1017' && $registry->getWorker()->hasRole(User::ROLE_CMH)){
                $lastCmh = $registry->getWorker();
            }

            if(is_null($lastSpecialist) && $registry instanceof Task && $registry->getServiceParent()->getCode()->getCode() != 'T1017' && $registry->getWorker()->hasRole(User::ROLE_SPECIALIST)){
                $lastSpecialist = $registry->getWorker();
            }
        }
        
        return $this->render('@BackEnd/custom/patient/show/ajax_info.html.twig', array(
            'patient' => $patient,
            'lastGroup' => $lastGroup,
            'lastTcm' => $lastTcm,
            'lastCmh' => $lastCmh,
            'lastSpecialist' => $lastSpecialist,
        ));  
    }

    /**
     * @Route("/ajax/show/other/info/patient/{id}", name="ajax_show_other_info_patient", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function ajaxShowOtherInfoPatientAction($id)
    {
        $d = $this->getDoctrine();
        $patient = $d->getRepository(Patient::class)->find($id);

        return $this->render('@BackEnd/custom/patient/show/ajax_other_info.html.twig', array(
            'patient' => $patient,
        ));
    }

    /**
     * @Route("/get/services/{type}/{idPatient}", name="get_services", options={"expose"=true})
     * @param string $type
     * @param integer $idPatient
     * @return Response
     */
    public function getServicesAction($type, $idPatient)
    {
        $d = $this->get('doctrine');
        $patient = $d->getRepository(Patient::class)->find($idPatient);
        /** @var User $worker */
        $worker = $this->get(UserManager::class)->getRealUser();
        $servicesParent = $d->getRepository(ServiceParent::class)->getServicesByType($type);

        $rol = $worker->getRoles()[0];
        $serviceAcl = $d->getRepository(ServiceAcl::class)->findOneBy(['role' => $rol]);

        $services = [];
        /** @var ServiceParent $service */
        foreach ($servicesParent as $service){
            if(!is_null($serviceAcl) && $serviceAcl->getServices()->contains($service)) {
                $valid = $service->isValid($patient, $worker);
                $row = [
                    'service' => $service,
                    'valid' => $valid['valid'],
                    'tip' => $valid['tip'],
                ];
                $services[] = $row;
            }
        }

        return $this->render('@BackEnd/custom/patient/service/ajax_list_services.html.twig', [
            'services' => $services,
            'patient' => $patient,
        ]);
    }
    
    /**
     * @Route("/ajax/show/service/{idService}", name="ajax_show_service_patient", options={"expose"=true})
     * @param integer $idService
     * @return Response
     */
    public function ajaxShowServicePatientAction($idService)
    {
        $doctrine = $this->getDoctrine();
        $service = $doctrine->getRepository(Service::class)->find($idService);

        return $this->render('@BackEnd/custom/patient/service/ajax_show_service.html.twig', array(
            'service' => $service,
        ));
    }

    /**
     * @Route("/{idPatient}/add/service/parent/{idServiceParent}", name="add_service_patient", options={"expose"=true})
     * @param integer $idPatient
     * @param integer $idServiceParent
     * @return Response
     */
    public function addServicePatientAction($idPatient, $idServiceParent)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $d = $this->getDoctrine();
        /** @var User $userReal */
        $userReal = $this->get(UserManager::class)->getRealUser();

        $editMode = $request->query->has('idService');

        $patient = $d->getRepository(Patient::class)->find($idPatient);
        $serviceParent = $d->getRepository(ServiceParent::class)->find($idServiceParent);

        //Check that is a valid service and role ACL.
        $isValid = $serviceParent->isValid($patient, $userReal);
        $rol = $userReal->getRoles()[0];
        $serviceAcl = $d->getRepository(ServiceAcl::class)->getByRolAndService($rol, $serviceParent);
        $urlShowPatient = $this->generateUrl('show_patient', ['id' => $patient->getId()]);
        if($isValid['valid'] == 'invalid' || is_null($serviceAcl)){
            $this->addFlash(Notification::ERROR, 'You can not access to this service.');
            return $this->redirect($urlShowPatient);
        }

        //Check billing 0
        $billingZero = $isValid['valid'] == 'warning' && $isValid['type'] == 'auth';

        $service = new Service();
        $service->setServiceParent($serviceParent);
        $service->setWorker($userReal);
        $service->setPatient($patient);
        $service->setBillingZero($billingZero);
        $service->setSettingCode($d->getRepository(SettingCode::class)->findOneBy(['code' => 53]));

        if($serviceParent->getPresence()->count() == 1){
            $service->setPresence($serviceParent->getPresence()->first());
        }

        if($editMode){
            $service = $d->getRepository(Service::class)->find($request->query->get('idService'));
            if($service->getWorker() != $userReal || $service->getRevised()){
                $this->addFlash(Notification::ERROR, 'You can not access to this service.');
                return $this->redirect($urlShowPatient);
            }
        }

        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $serviceCode = $serviceParent->getCode()->getCode();
            if($serviceParent->hasPreDuration()){
                if($serviceCode == 'H2017'){
                    if($service->getSessionType()->getName() == NomUtil::NOM_SERVICE_SESSION_TYPE_AM)
                        $service->getScheduleStart()->modify('+9 hour');
                    else
                        $service->getScheduleStart()->modify('+13 hour');
                }
                $ss = $service->getScheduleStart();
                $se = new DateTime();
                $duration = $serviceParent->getMaxDuration() * 60;
                $se->setTimestamp($ss->getTimestamp() + $duration);
                $service->setScheduleEnd($se);
            }

            //Check limitations of service
            if($serviceParent->existLimitations() && !$editMode){
                $trans = $this->get('translator');
                $serviceHistory = $d->getRepository(Service::class)->getServiceHistory($serviceParent, $patient, $service->getScheduleStart());

                $modifierAmount = $serviceParent->getModifierAmount()->getName() == NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT ? '' : $trans->trans($serviceParent->getModifierAmount()->getName(), [], 'BackEndBundle');
                $frequency = $trans->trans($serviceParent->getFrequency()->getName(), [], 'BackEndBundle');
                $errorMsg = sprintf('Can not create service on patient %s, the limit of %s has been reached.',
                    $patient->getFullName(),
                    $serviceParent->getAmount() . " $modifierAmount $frequency"
                );

                if($serviceParent->getModifierAmount()->getName() == NomUtil::NOM_MODIFIER_AMOUNT_TYPE_AMOUNT && count($serviceHistory) >= $serviceParent->getAmount()){
                    $this->addFlash(Notification::ERROR, $errorMsg);
                    return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
                        'patient' => $patient,
                        'serviceParent' => $serviceParent,
                        'form' => $form->createView(),
                    ));
                }

                if($serviceParent->getModifierAmount()->getName() == NomUtil::NOM_MODIFIER_AMOUNT_TYPE_HOURS) {
                    $minutesLimit = $serviceParent->getAmount() * 60;
                    $minutes = 0;
                    foreach ($serviceHistory as $s){
                        $minutes += $s->getServiceDurationMin();
                    }
                    $minutes += $service->getServiceDurationMin();
                    if($minutes >= $minutesLimit) {
                        $this->addFlash(Notification::ERROR, $errorMsg);
                        return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
                            'patient' => $patient,
                            'serviceParent' => $serviceParent,
                            'form' => $form->createView(),
                        ));
                    }
                }

                if($serviceParent->getModifierAmount()->getName() == NomUtil::NOM_MODIFIER_AMOUNT_TYPE_UNITS) {
                    $unitsLimit = $serviceParent->getAmount();
                    $units = 0;
                    foreach ($serviceHistory as $s){
                        $units += $s->getUnitsUsed();
                    }
                    $units += $service->getUnitsUsed();
                    if($units >= $unitsLimit) {
                        $this->addFlash(Notification::ERROR, $errorMsg);
                        return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
                            'patient' => $patient,
                            'serviceParent' => $serviceParent,
                            'form' => $form->createView(),
                        ));
                    }
                }
            }
            
            //Check the services schedule in a patients
            $servicesPatient = $d->getRepository(Service::class)->getByPatient($patient);

            foreach ($servicesPatient as $oldService){
                if($oldService->getId() == $service->getId() && $editMode){
                    continue;
                }

                $check = StaticUtil::checkDateRanges($service, $oldService);
                if(!$check && $oldService->getPresence()->getId() == $service->getPresence()->getId()){
                    $this->addFlash(Notification::ERROR, sprintf('Can not create service on patient %s. %s have scheduled the service %s with this patient at [%s - %s].',
                        $patient->getFullName(),
                        $oldService->getWorker()->getFullName(),
                        $oldService->getServiceParent()->getName(),
                        $oldService->getScheduleStart()->format('M d, Y, h:i a'),
                        $oldService->getScheduleEnd()->format('M d, Y, h:i a')
                    ));
                    /*return $this->redirect($this->generateUrl('add_service_patient', [
                        'idPatient' => $patient->getId(),
                        'idServiceParent' => $serviceParent->getId()
                    ]));*/
                    return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
                        'patient' => $patient,
                        'serviceParent' => $serviceParent,
                        'form' => $form->createView(),
                    ));
                }
            }

            //Check the services schedule in a doctor
            $servicesDoctor = $d->getRepository(Service::class)->getByDoctor($service->getWorker());

            foreach ($servicesDoctor as $oldService){
                //Dont check if service is a group
                if($service->getCode() == $oldService->getCode() && $service->getCode() == 'H2017'){
                    continue;
                }
                
                if($oldService->getId() == $service->getId() && $editMode){
                    continue;
                }

                $check = StaticUtil::checkDateRanges($service, $oldService);
                if(!$check){
                    $this->addFlash(Notification::ERROR, sprintf('Can not create service, you have another service (%s) scheduled to [%s - %s] with the patient %s.',
                        $oldService->getServiceParent()->getName(),
                        $oldService->getScheduleStart()->format('M d, Y, h:i a'),
                        $oldService->getScheduleEnd()->format('M d, Y, h:i a'),
                        $oldService->getPatient()->getFullName()
                    ));
                    return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
                        'patient' => $patient,
                        'serviceParent' => $serviceParent,
                        'form' => $form->createView(),
                    ));
                }
            }

            //Add Week to service
            $day = $service->isBillingRetroActive() ? new DateTime() : $service->getScheduleStart();
            $week = $d->getRepository(Week::class)->getWeekByDay($day);

            if($editMode && $service->getWeek() != $week){
                $this->addFlash(Notification::ERROR, 'You can not modify Schedule Start beyond the current week of this service. You must create another one.');
                return $this->redirect($urlShowPatient);
            }

            $service->setWeek($week);
            
            $this->get(BaseManager::class)->save($service);
            $this->addFlash(Notification::SUCCESS, 'flash.success.new.service');
            
            $url = $this->generateUrl('show_patient', ['id' => $service->getPatient()->getId()]);
            $url .= '?idService=' . $service->getId();
            $roles = [User::ROLE_OPERATION_MANAGER];
            $this->get(NotificationManager::class)->createNotification($roles, 'notification.add.new.service', $url, Notification::SUCCESS, $important = true, ['%patient%' => $patient->getName()]);

            //Generate the unfilled forms of this service
            if(!$editMode) {
                $originForms = $service->getPdfForms();
                if (!is_null($originForms)) {
                    /** @var PdfForm $originForm */
                    foreach ($originForms as $originForm) {
                        $serviceForm = new ServiceForm();
                        $serviceForm->setName($originForm->getName());
                        $serviceForm->setFilename($originForm->getFilename());
                        $serviceForm->setService($service);
                        $this->get(BaseManager::class)->save($serviceForm);
                        $this->get('pdf.util')->copyOriginForm($originForm, $serviceForm, $service);
                    }
                }
            }

            if($editMode){
                $serviceAdmin = $this->get('sonata.admin.pool')->getAdminByAdminCode('patient.bundle.admin.service');
                $url = $serviceAdmin->generateUrl('list') . sprintf('?filter[week][value]=%s&filter[worker][value]=%s', $service->getWeek()->getId(), $service->getWorker()->getId());
                return $this->redirect($url);
            }

            return $this->redirect($this->generateUrl('show_patient', ['id'=>$patient->getId()]));
        }

        return $this->render('@BackEnd/custom/patient/service/add_service.html.twig', array(
            'patient' => $patient,
            'serviceParent' => $serviceParent,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admit/{id}", name="admit_patient", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function admitPatientAction($id)
    {
        $d = $this->get('doctrine');
        $bm = $this->get(BaseManager::class);
        $patient = $d->getRepository(Patient::class)->find($id);
        $userReal = $this->get(UserManager::class)->getRealUser();

        $config = $d->getRepository(Setting::class)->find(1);

        $patient->setPatientStatus(NomUtil::getNomenclatorsByName(NomUtil::NOM_STATUS_OPEN));
        $patient->setCaseNumber($config->getActualCaseNumber());
        $patient->setOpenDate(new DateTime());
        $bm->save($patient);

        $config->updateCaseNumber();
        $bm->save($config);

        $registryPatient = new RegistryPatient();
        $registryPatient->setEntryDate(new DateTime());
        $registryPatient->setPatient($patient);
        $registryPatient->setResponsible($userReal);
        //$registryPatient->setAction(NomUtil::getNomenclatorsByName(NomUtil::NOM_ACTION_REGISTRY_PATIENT_TYPE_ADMITTED));

        if($patient->hasTcm())
            $registryPatient->setOpenByTcm(true);

        if($patient->hasCmh())
            $registryPatient->setOpenByCmh(true);

        $bm->save($registryPatient);

        $this->addFlash('success', sprintf('The Patient %s has been admitted correctly.', $patient->getName()));

        $url = $this->get('router')->generate('show_my_profile') . '?tab=pendingAssignation';
        $this->get(NotificationManager::class)->createNotification([User::ROLE_OPERATION_MANAGER, User::ROLE_SUPER_ADMIN], sprintf('Patient %s have been admitted.', $patient->getFullName()), $url, Notification::SUCCESS, true);

        return $this->redirect($this->generateUrl('show_patient', ['id'=>$patient->getId()]));
    }

    /**
     * @Route("ajax/show/totals/{weekId}", name="ajax_show_totals_by_week", options={"expose"=true})
     * @param integer $weekId
     * @return Response
     */
    public function ajaxShowTotalByWeekAction($weekId)
    {
        $d = $this->getDoctrine();
        $week = $d->getRepository(Week::class)->find($weekId);
        
        $totalUnits = 0;
        $totalDuration = 0;
        
        $services = $d->getRepository(Service::class)->findBy(['week' => $week]);

        foreach ($services as $service){
            $totalUnits += $service->getUnitsUsed();
            $totalDuration += $service->getServiceDurationMin();
        }

        return $this->render('@BackEnd/service_admin/ajax_show_totals.html.twig', [
            'totalUnits' => $totalUnits,
            'totalDuration' => $totalDuration,
        ]);
    }

    /**
     * @Route("/set/current/diagnosis/{id}", name="set_current_diagnosis", options={"expose"=true})
     * @param integer $id
     * @return Response
     */
    public function setCurrentDiagnosisAction($id)
    {
        $d = $this->getDoctrine();
        $bm = $this->get(BaseManager::class);
        $diagnosis = $d->getRepository(DiagnosisPatient::class)->find($id);
        $patient = $diagnosis->getPatient();

        /** @var DiagnosisPatient $diagnosis */
        foreach($patient->getDiagnosisEntrys() as $diag){
            $diag->setCurrentDiagnosis(false);
            $bm->save($diag);
        }

        $diagnosis->setCurrentDiagnosis(true);
        $bm->save($diagnosis);

        $this->addFlash(Notification::SUCCESS, 'Set current diagnosis succefully');

        $patientAdmin = $this->get('sonata.admin.pool')->getAdminByClass(Patient::class);
        return $this->redirect($patientAdmin->generateObjectUrl('patient.bundle.admin.diagnosis.patient.list', $patient));
    }
}