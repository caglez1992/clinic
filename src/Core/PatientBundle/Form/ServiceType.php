<?php

namespace Core\PatientBundle\Form;

use Core\CoreBundle\Form\Type\MyDatePickerType;
use Core\CoreBundle\Form\Type\MyDateTimePickerType;
use Core\CoreBundle\Util\NomUtil;
use Core\PatientBundle\Entity\PdfForm;
use Core\PatientBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Service $service */
        $service = $builder->getData();
        $serviceCode = $service->getServiceParent()->getCode()->getCode();

        if($serviceCode == 'H2017'){
            $builder->add('scheduleStart', MyDatePickerType::class, [
                'constraints' => [new NotBlank()],
                'required' => true,
                'attr' => ['readonly' => 'readonly'],
                'dp_min_date' => null,
            ]);

            $builder->add('sessionType', null, [
                'constraints' => [new NotBlank()],
                'required' => true,
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' => function(EntityRepository $er){
                    return  $er->createQueryBuilder('n')
                        ->where('n.type = :nom_type')
                        ->setParameter('nom_type', NomUtil::NOM_SERVICE_SESSION_TYPE);
                },
            ]);
        } else {
            $builder->add('scheduleStart', MyDateTimePickerType::class, [
                'constraints' => [new NotBlank()],
                'required' => true,
                'attr' => ['readonly' => 'readonly'],
                'dp_min_date' => null,
            ]);
        }

        if(!$service->getServiceParent()->hasPreDuration()) {
            $builder->add('scheduleEnd', MyDateTimePickerType::class, [
                'constraints' => [new NotBlank()],
                'required' => true,
                'attr' => ['readonly' => 'readonly'],
                'dp_min_date' => null,
            ]);
        }

        $builder->add('settingCode', null, [
            'constraints' => [new NotBlank()],
            'required' => true,
        ]);

        if($service->getServiceParent()->getPresence()->count() > 1) {
            $builder->add('presence', null, [
                'constraints' => [new NotBlank()],
                'required' => true,
                'choice_translation_domain' => 'BackEndBundle',
                'query_builder' => function(EntityRepository $er){
                    return  $er->createQueryBuilder('n')
                        ->where('n.type = :nom_type')
                        ->setParameter('nom_type', NomUtil::NOM_SERVICE_PRESENCE_TYPE);
                },
            ]);
        }

        if($service->getServiceParent()->getHasDischarge()) {
            $builder->add('discharge');
        }

        $builder->add('billingRetroActive', null, [
            'label' => 'Billing Retroactive'
        ]);

        /*if($service->getServiceParent()->getForms()->count() > 0) {
            $builder->add('pdfForms', EntityType::class, [
                'class' => PdfForm::class,
                'label' => 'Add forms to service',
                'multiple' => true,
                'required' => false,
                'choice_translation_domain' => 'BackEndBundle',
                'choices' => $service->getServiceParent()->getForms(),
            ]);
        }*/

    }

    public function getBlockPrefix()
    {
        return 'service_type';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Service::class,
        ));
    }
}